<?php
$file = fopen("config_datos_generales.txt", "r");
//Output a line of the file until the end is reached

$nombret=fgets($file);
$nombre=fgets($file);
$direccion=fgets($file);
$nif=fgets($file);
$telefono=fgets($file);

fclose($file);
$hora= date ("h:i");
$fecha= date ("j-n-Y");

?>
@if(isset($imprimir))
    {{$imprimir->impresora}}#nada#{{$imprimir->id}}#
@endif
<div id="cont"  style=" font-size: 10px;  ">
    <div style="text-align: center"> <b>{{$nombret}}</b></div>
    <div align="center">{{$nombre}}<br>
    {{$direccion}}<br>
    TEL: {{$telefono}} NIF: {{$nif}}<br> <br>
    @if(isset($orden->idFactura)) N&uacute;m. Factura: {{$orden->idFactura}} @else Orden de la @endif Mesa:@if($orden->mesa==null){{$orden->N}} Para llevar @else{{$orden->mesa}}@endif<br>
    Cliente: @if($orden->cliente=='00000000A') Tiket Fiscal @else{{$orden->getcliente->nombreCompleto}}@endif<br>
    Mesero: {{$orden->getmesero->name}}<br>
    @if(isset($orden->getcajero)) Cajero: {{$orden->getcajero->name}} <br>@endif
    Fecha: {{$orden->created_at}}</div>  
    <hr>
    <table  style="width: 100%;font-size: 10px; ">
        <tr> 
            <th style="text-align:left;">
                Producto
            </th>
            <th style="text-align:center;">Precio U.</th>
            <th style="text-align:right;">Total</th>      
        </tr>
        @if(isset($orden->detalles))
        @foreach ($orden->detalles as $detalle)
        <tr>                
            <th style="text-align:left;">
                {{substr($detalle->getproducto->nombre,0,9)}}<br> {{substr($detalle->getpresentacion->presentacion,0,9)}}
            </th>
            <th style="text-align:center;">{{$detalle->cantidad}}x{{ number_format($detalle->precio, 2, ',', '.')}}&euro;</th>
            <th style="text-align:right;">{{number_format(($detalle->precio*$detalle->cantidad), 2, ',', '.') }}&euro;</th> 
            @foreach ($detalle->modificadores as $modificador)
                @if($modificador->pivot->total !=0)
                    <div style="text-align:left;margin-top:-5px">
                        {{$modificador->pivot->cantidadDetalles." Item: Ext.".substr($modificador->nombre,0,4)." ".$modificador->pivot->cantidadModificador." x "}}
                        {{ number_format($modificador->pivot->precioExtra, 2, ',', '.')}}&euro; 
                        <span style="position: absolute;top:auto;right: 0px;">{{number_format($modificador->pivot->total, 2, ',', '.')}}&euro;</span>
                    </div>
                @endif
            @endforeach
        </tr>
        @endforeach
        @else
            @foreach  ($detalles as $detalle) 
             <tr>                
                <th style="text-align:left;">
                    {{substr($detalle->getproducto->nombre,0,9)}}<br> {{$detalle->getpresentacion->presentacion}}
                </th>
                <th style="text-align:center;">{{$detalle->cantidad}}x{{ number_format($detalle->precio, 2, ',', '.')}}&euro;</th>
                <th style="text-align:right;">{{number_format(($detalle->precio*$detalle->cantidad), 2, ',', '.') }}&euro;</th> 
                    @if($detalle->subconjuntos!=false)
                        @foreach ($detalle->subconjuntos as $subconjunto) 
                                 @foreach ($subconjunto->modificadores as $modificador)
                                    @if($modificador->precio!=0)
                                     <div style="text-align:left;margin-top:-5px">
                                        {{$subconjunto->cantidad}} Item: Ext. {{substr($modificador->nombre,0,4)}} {{$modificador->canT}} x {{ number_format($modificador->precio, 2, ',', '.')}}&euro;
                                         <span style="position: absolute;top:auto;right: 0px;">{{number_format((($modificador->precio*$modificador->canT)*$subconjunto->cantidad), 2, ',', '.')}}&euro;</span>
                                     </div> 
                                    @endif
                                 @endforeach 
                        @endforeach
                    @endif
            @endforeach
        @endif
    </table><br>
    <hr>
  <div> 
     <div>Subtotal: <b style="position: absolute;top:auto;right: 0px">{{number_format($orden->subTotal, 2, ',', '.')}} &euro;</b></div>
     <div>Impuesto: <b style="position: absolute;top:auto;right: 0px">{{number_format($orden->impuesto, 2, ',', '.')}} &euro;</b></div>
     @if(isset($orden->descuento))
     <div>Descuentos: <b style="position: absolute;top:auto;right: 0px">{{number_format($orden->descuento, 2, ',', '.')}} &euro;</b></div>
     @endif
     <div>Servicio: <b style="position: absolute;top:auto;right: 0px">{{number_format($orden->propina, 2, ',', '.') }} &euro;</b></div>
     <div style="font-size: 12px">Total: <b style="position: absolute;top:auto;right: 0px">{{number_format($orden->total, 2, ',', '.') }} &euro;</b></div>
     @if(isset($orden->pagos))
     <div style="text-align: center">Pagos realizados:</div>
    @foreach ($orden->pagos as $pago)
        <div>{{$pago->getModoPago->tipoPago}}: 
            <b style="position: absolute;top:auto;right: 0px">{{number_format($pago->monto,2,',','.')}} &euro;</b>
        </div>
     @endforeach
      <div>Cambio: <b style="position: absolute;top:auto;right: 0px">@if($orden->cambio==null) 0.00 &euro; @else {{number_format($orden->cambio, 2, ',', '.') }} &euro;@endif</b></div>
     @endif
      <p align="center">!!Gracias por preferirnos!!</p>
  </div>
 

</div>

