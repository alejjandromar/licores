@extends('master')

@section('title', 'Mesas')
<html ng-app="mySuperApp">
    @section('head_css')
    @parent
    <!--csss especificos de esta pagina-->
    <style type="text/css"> 
        .footerM{display: block!important;}
    </style> 
    @stop
    @section('head_scripts')
    @parent
    <!--script especificos de esta pagina-->
    @stop
    @section('contenido')
    @parent
    <div class="contenidoM">
  
		<div class="card" style="height:100%; margin: 0px"> 
			<div class="item item-text-wrap">
				<ion-scroll zooming="true" id="scroll" direction="xy" style=" background-color:#F0F3F8;" >    
					<center>
						<div id="visor">
							<br>
							@foreach ($mesa as $mesas)                                               
							<div on-hold="mostrarMenu($event,{{$mesas->idMesa}})" id="draggable3{{$mesas->idMesa}}" class="draggable"> 
								<a href="#" style="margin: 11px;  left: -12px; top:-3px; text-shadow: 3px 3px 4px black" id ="<?php echo $mesas->idMesa; ?>" class=" button button-clear button-balanced icon ion-android-time" >
									<b style="position: absolute; top: -20px; left: -15px; color:white;text-shadow: 2px 2px 8px black" class="icon ion-chatbox"></b>
								</a> 
								<a style="font-size:10px;  position: absolute; top: -10px; left: -4px;">
									<b>{{$mesas->idMesa}}</b></a></div>                    
							@endforeach
						</div>
						
					</center>
				</ion-scroll>
			</div>             
		</div>
    </div>
@stop
@section('footer') 
@parent
<ion-scroll direction="x" style=' color: white; width: 100%;'>
    @if(count($area)!=0) 
    <div class="row" id='areas' >
        <button class="button button-block button-assertive BtmMesa" onclick="cargarAlas(-1)">Todas</button>
        @foreach ($area as $areas) 
        <button class="button button-block button-assertive BtmMesa" onclick="cargarAlas({{$areas-> idArea}})">{{$areas->nombre}}
        </button>
        @endforeach
    </div> 
    @endif     
</ion-scroll> 

@stop
@section('scripts')
@parent
<script type="text/javascript">

                var o = {{$mesas->idMesa}};
                var x = {{$mesas->idMesa}};
                var unir = false;
                var mover = false;
                var div = false;
                var url = 'mesa/coor_get';
                $.ajax({
                url: url,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        async: false,
                        type: "GET",
                        success: function (data) {

                        var sPositions = "" || "{}",
                                positions = JSON.parse(sPositions);
                                for (i = 0; i <= o; i++) {

                        $("#draggable3" + i).draggable({
                        containment: "#visor",
                                cancel: "#draggable3" + i,
                                scroll: false,
                                stop: function (event, ui)
                                {
                                positions[this.id] = ui.position
                                        //save_coor(JSON.stringify(positions));
                                }

                        });
                        }

                        $.each(positions, function (id, pos) {
                        $("#" + id).css(pos)
                        })
                        },
                        error: function () {
                        alert("error");
                        }
                })

    </script>
<script >
            var mesa;
            angular.module('mySuperApp', ['ionic'])
            .controller('controlador', function($ionicPopover, $scope, $ionicLoading, $ionicPopup, $ionicActionSheet, $timeout, $ionicSideMenuDelegate, $compile, $interval, $http) {
            $scope.unir = false;
                    $scope.toggleLeftSideMenu = function() {
                    $ionicSideMenuDelegate.toggleLeft();
                    };
                    $scope.data = {comentario:''};
                    $scope.destino = "";
                    //cierra el popover de las opciones de un detalle
                    $scope.closepopover = function() {
                    $scope.popover.hide();
                    $scope.popover = null;
                    }
            $scope.mostrarMenu = function($event, mesa) {
            var opciones = $("#" + mesa).attr('opciones');
                    if (opciones == '1'){
            $scope.mesa = mesa;
                    $scope.showConfirm(mesa, $event)
            }
            }
            //crea y abre el popover de las opciones de una mesa
            $scope.showConfirm = function(idMesa, $event) {
            mesa = idMesa;
                    $scope.mesa = idMesa;
                    $ionicPopover.fromTemplateUrl('opciones.html', {
                    scope: $scope,
                            animation: 'slide-in-up'
                    }).then(function (popover) {
                    $scope.popover = popover;
                    $scope.popover.show($event);
            });
            }

            $scope.UnirMesas = function(v) {
                    mover = false;
                    div = false
                    unir = true;
                    $scope.unir = unir;
                    $scope.msg_mesas="Unir Mesas"
                    cargarEstado();
            }

            $scope.cancelar = function() {
                    mover = false;
                    unir = false;
                    div = false
                    $scope.unir = unir;
                    $scope.msg_mesas=""
                    cargarEstado();
            }

            $scope.MoverMesa = function(v) {
                    unir = false;
                    div = false
                    mover = true;
                    $scope.unir = mover;
                    $scope.msg_mesas="Mover Mesas"
                    cargarEstado();
            }

            $scope.DividirMesa = function(v) {
                    unir = false;
                    div = true
                    mover = true;
                    $scope.unir = mover;
                    $scope.msg_mesas="Dividir Mesas"
                    cargarEstado();
            }


            angular.element(document).ready(function() {
            cargarEstado();
            setInterval('cargarEstado()', 15000);
            });
                    //funcio que agrega un comentario a la orden
                    $scope.openPopupC = function(X) {
                    $scope.destino = X
                            var popup = $ionicPopup.show({
                            templateUrl: 'comentario.html',
                                    scope: $scope,
                                    });
                            $scope.popupComentario = popup;
                    }

            //funcion que cierra el popup para agregar un comentario
            $scope.closepopupComentario = function() {
                    $scope.popupComentario.close();
                    $scope.popupComentario = null;
            }
            $scope.CancelarOrden = function() {
            $http({
            url:'cancelarOrden',
                    method: 'GET',
                    params:{
                    mesa:$scope.mesa,
                            comentario:$scope.data.comentario
                    },
                    headers: {
                    "Content-Type": "application/json"
                    }

            }).success(function(response){
            $scope.closepopupComentario();
                    if (!response.error){
                        console.log($scope.popover);
                        $ionicLoading.show({
                        template: response.mensaje,
                                duration:3000
                        });
            }
            }).error(function(error){
            $scope.closepopupComentario();
            });
            }
            });
            //Cambia el estado cada 2 segundos dependiendo de la varible de cache
                    function cargarEstado(){
                    console.log('cargo el estado');
                            var url = 'mesas/all';
                            $.ajax({
                            url: url,
                                    dataType: "JSON",
                                    type: "get",
                                    success: function (data) {


                                    var x = 1;
                                            for (var i = 0; i < data["mesa"].length; i++, x++) {
                                             data["mesa"][i].link = unir && data["mesa"][i].opciones && data["mesa"][i].link != "#"?"#U": unir?"#EU":data["mesa"][i].link;
                                            data["mesa"][i].link = mover && !data["mesa"][i].opciones && data["mesa"][i].link != "#"?"#U": mover?"#EU":data["mesa"][i].link;
                                            $("#" + data["mesa"][i].idMesa).attr("href", data["mesa"][i].link); //Cambiar direccion
                                            $("#" + data["mesa"][i].idMesa).attr('class', data["mesa"][i].icono);
                                            if (data["mesa"][i].link == "#no_caja"){
                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "return alert('Debe seleccionar una caja')");
                                    }





                                    if (data["mesa"][i].link == "#"){
                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "return alert('Mesa bloqueada por otro cajero/mesero')");
                                    } else if (data["mesa"][i].link == "#nc"){
                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "return alert('Esa mesa no ha sido envia a ser pagada')");
                                    } else if (data["mesa"][i].link == "#EU"){
                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "return alert('Debe Seleccionar una mesa que"+(mover || div?" no ":" ya ")+" haya ordenado')");
                                    } else
                                            if (data["mesa"][i].link == "#U"){

                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "unir_mesa(" + data["mesa"][i].idMesa + ")");
                                    } else{

                                    $("#" + data["mesa"][i].idMesa).attr('onclick', "");
                                    }

                                    if (data["mesa"][i].opciones == true){
                                    $("#" + data["mesa"][i].idMesa).attr('opciones', "1");
                                    } else{
                                    $("#" + data["mesa"][i].idMesa).attr('opciones', "0");
                                    }
                                    }


                                    },
                                    error: function () {
                                    }
                            });
                    }



            function cargarAlas(id){
            var url = 'mesas/ala';
                    var data = {id:id};
                    $("#visor div").hide();
                         @foreach ($mesa as $mesas)                                               
                                $("#draggable3{{$mesas->idMesa}}").css("top",0);
                                $("#draggable3{{$mesas->idMesa}}").css("left",0);
                         @endforeach
                        if (id!=-1){
                            $.ajax({
                            url: url,
                                    dataType: "JSON",
                                    data:data,
                                    type: "get",
                                    success: function (data) {
                                    for (var i = 0; i < data["area"].length; i++) {
                                        $('#draggable3' + data["area"][i].idMesa).show();
                                    }

                                    $("#area div").attr('class', 'button button-block button-outline button-assertive');
                                            get_coor();
                                            $("#btn" + id).attr('class', 'button button-block button-full button-assertive');
                                    },
                                    error: function () {
                                    alert('Ocurrio un error al realizar la busqueda de las mesas');
                                    }
                            });
                        }else{
                             @foreach ($mesa as $mesas)                                               
                                $('#draggable3{{$mesas->idMesa}}').show();
                            @endforeach
                        }
            }


            function get_coor(){
            cargarEstado();
                    
                    var url = 'mesa/coor_get';
                    $.ajax({
                    url: url,
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            async: false,
                            type: "GET",
                            success: function (data) {
                                    var sPositions = data.linea || "{}",
                                    positions = JSON.parse(sPositions);
                                    for (i = 0; i <= o; i++) {
                                       $("#draggable3" + i).draggable({
                                          containment: "#visor",
                                          cancel: "#draggable3" + i,
                                          scroll: false,
                                          stop: function (event, ui)
                                          {
                                        positions[this.id] = ui.position

                                    }

                            });
                            }
                            $.each(positions, function (id, pos) {
                                $("#" + id).css(pos)
                            })
                            },
                            error: function () {
                            alert("error");
                            }
                    })
                    }

            function  unir_mesa(id){
            if (mover){
            if (div){
            location.href = "dividir_m/" + mesa + "/" + id;
            } else if (confirm("Desea  mover esta mesa?")){
            $.ajax({
            url: "mover_mesa/" + mesa + "/" + id,
                    dataType: "JSON",
                    type: "get",
                    success: function (data) {
                    $("#btn_clr_u").click();
                            cargarEstado();
                    },
                    error: function () {
                    $("#btn_clr_u").click();
                            alert('Ocurrio un error al unir las mesas');
                            cargarEstado();
                    }
            });
            }
            } else
                    if (confirm("Desea unir estas dos mesas?")){

            $.ajax({
            url: "unir_mesa/" + mesa + "/" + id,
                    dataType: "JSON",
                    type: "get",
                    success: function (data) {
                    $("#btn_clr_u").click();
                            cargarEstado();
                    },
                    error: function () {
                    $("#btn_clr_u").click();
                            alert('Ocurrio un error al unir las mesas');
                            cargarEstado();
                    }
            });
            }
            }





</script>

<script id="opciones.html" type="text/ng-template">
    <ion-popover-view id="popover" style="height:70px">
    <ion-content style="margin:-1px;border-radius:5px;">
    <div class="row header-pizza"><div class="col-90"><strong>Mesa @{{mesa}} &rarr; Opciones</strong></div>
    <button class="col-10 button-clear ion-close-circled" ng-click="closepopover()" style="font-size: larger; color:#FFE6DE;"></button></div>
    <div class="button-bar button-balanced">
    <a class="button button-clear ion-shuffle" title="Unir Mesas" ng-click="UnirMesas();closepopover()"  style="color:  #FF8E0A;"></a>
    <a class="button  button-clear" title="Dividir Mesa"ng-click="DividirMesa();closepopover()" style="color: #FF8E0A;font-weight: bold;">%</a>
    <a class="button  button-clear ion-arrow-move" title="Mover Mesa" ng-click="MoverMesa(); closepopover()" style="color:  #FF8E0A;"></a>
    <!--  <a class="button  button-clear ion-locked" title="Bloquear Mesa" ng-click="BloquearMesa(); closepopover()" style="color:  #FF8E0A;"></a>
    <a class="button  button-clear ion-person-add" title="Factura Personalizada" ng-click="AgregarCliente(); closepopover()" style="color:  #FF8E0A;"></a> -->
    <a class="button  button-clear" title="Cancelar Orden" ng-click="openPopupC('Orden');closepopover()" style="color:  #FF8E0A;">
    <i class="ion-document-text" style="font-size: 14px;position: relative;top: -4px;left: 5px;"></i>
    <i class="ion-ios-circle-outline" style="font-size: 23px;position: relative;left: -13px;top: 0px;"></i>
    <span class="" style="font-size: 20px;position: relative;left: -31px;top: -2px;">/</span>
    </a>
    </div>
    </ion-content>
    <ion-popover-view>
</script>
<script id="comentario.html" type="text/ng-template">
    <div id="comentario" class="co">
    <ion-popup-view>
    <ion-header-bar class="header-pizza">
    <div class="title title-left T" style="font-size:15px;color: white;">@{{destino}}&rarr; Agregar comentario</div>
    <div class="buttons">
    <button class="button-clear ion-close-circled" ng-click="closepopupComentario()" style="color:#FFE6DE;font-size: 21px;"></button>
    </div>
    </ion-header-bar>
    <ion-content class="has-header" style="background-color: white;padding: 10px;">
    <textarea name="comentario" rows="4" autofocus="true" ng-model="data.comentario" placeholder="Escriba un comentario u observaci&oacute;n" resize="false" style="height: 100%;" required>
    </textarea>
    <span ng-if="data.comentario==''||!data.comentario" style="position: relative;bottom: 14px; color:red">Por favor introduzca un comentario</span>
    </ion-content>
    <ion-footer-bar class="header-pizza">
    <button  class="button-clear BtnM"  style="font-size:15px;width: 50%;"  ng-click="CancelarOrden()" ng-disabled="data.comentario==''||!data.comentario">Guardar</button>
    <button  class="button-clear BtnM " style="font-size:15px;border-left: solid white 1px;width: 50%;" ng-click="closepopupComentario()">Cancelar</button>
    </ion-footer-bar>
    </ion-popup-view>
    </div>
</script>
<script id="page2.html" type="text/ng-template">
    <!-- The title of the ion-view will be shown on the navbar -->
    <ion-view title="Page 2">
    <ion-nav-buttons side="left">
    <button class="button">
    LeftButton!
    </button>
    </ion-nav-buttons>        
    <ion-nav-buttons side="right">
    <button class="button">
    RightButton!
    </button>
    </ion-nav-buttons>        

    <ion-content class="padding">
    <!-- The content of the page -->
    <a class="button" ui-sref="page1">Go To Page 1</a>
    <a class="button" ui-sref="page3">Go To Page 3</a>
    </ion-content>
    </ion-view>
</script> 
@stop