
<?php
if (!isset($datos)){
	$file = fopen("config_datos_generales.txt", "r");
	//Output a line of the file until the end is reached

$nombret = strtoupper(fgets($file));
$nombre = strtoupper(fgets($file));
$direccion = strtoupper(fgets($file));
$nif = strtoupper(fgets($file));
$telefono = fgets($file);
fclose($file);
}else{	
//Output a line of the file until the end is reached
$nombret = $datos[0];
$nombre = $datos[1];
$direccion = $datos[2];
$nif = $datos[3];
$telefono = $datos[4];
}
	




?>
@if(isset($imprimir))
    {{$imprimir->impresora}}#nada#{{$imprimir->id}}#
@endif
<center>


    <div id="cont" style=" font-size: 10px;" >
        <center>
            <span id="nombretM"  align="center"><b style="font-size:12px">{{$nombret}}</b></span><br></b>
            <span id="nombreM" align="center">{{$nombre}}</span><br>
            <div style="width: 210px;"> {{$direccion}}<span  id="direccionM" align="center"></span></div>
            NIF:<span id="nifM" align="center">{{$nif}}</span> 
            TEL:<span id="telefonoM" align="center">{{$telefono}}</span> <br></center>
        <div>{{$titulo}} <b>{{$n_caja}}</b></div>
        Turno de: {{$inicio}} - {{$final}}<br>
         <br><hr style="    margin: 0px;">
        <a align="center" style="padding: 10px">PAGOS</a>
        <hr style="    margin: 0px;"><br>
         @if(count($Pagos) > 0)
            @foreach ($Pagos as $pago)
              <div style="text-align: left;">{{$pago->nombre}} x : {{$pago->n_facturas}}:
                  <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($pago->totales,2,',','.')}} &euro;</b>
              </div>
           @endforeach
         @else
             <div style="text-align: center">
                NO SE REALIZARON PAGOS
            </div>
          @endif
        <br>
         <hr style="    margin: 0px;">
                        <a align="center">Descuentos</a> 
        <hr style="    margin: 0px;"><br>
        <div align="left">
            @if(count($Descuentos) > 0)
             @foreach ($Descuentos as $descuento)
                <div style="text-align: left;">{{$descuento->nombre}} x : {{$descuento->n_facturas}}:
                    <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($descuento->totales,2,',','.')}} &euro;</b>
                </div>
             @endforeach
            @else
                 <div style="text-align: center">
                    NO SE REALIZARON DESCUENTOS
                </div>
            @endif
        </div> 
        <br><hr style="    margin: 0px;">
                        <a align="center">Categorias</a> 
        <hr style="    margin: 0px;"><br>
        <div align="left">
             @if(count($Categoria) > 0)
             @foreach ($Categoria as $categoria)
                <div style="text-align: left;">{{$categoria->nombre}} x : {{$categoria->cantidad_total}}:
                    <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($categoria->totales,2,',','.')}} &euro;</b>
                </div>
             @endforeach
             @else
                <div style="text-align: center">
                   NO SE HAN REALIZADO VENTAS
               </div>
             @endif
        </div><br>
         <hr style="    margin: 0px;">
        <diff>
        <a align="center">Productos</a> 
        <hr style="    margin: 0px;">
        @if(count($Productos) >0)
           <table  style="width: 100%; font-size: 10px;">
                <tr>
                    <th style=" width: 33%;text-align:left;">Producto </td>
                    <th style=" width: 33%;text-align: center;">Cant</td>
                    <th style=" width: 33%;text-align: right;">Total</td>  
                </tr>
                    @foreach ($Productos as $Producto)
                    <tr>
                        <th style="text-align:left;">
                            {{substr($Producto->nombre,0,9)}}<br> {{$Producto->presentacion}}
                        </th>
                        <th style="text-align:center;"> x {{$Producto->cantidad_total}}</th>
                        <th style="text-align:right;">{{number_format($Producto->totales, 2, ',', '.').' &euro;'}}</th> 
                    </tr>
                    @endforeach
            </table>
        @else   
            <br>
             <div style="text-align: center">
                NO SE HAN REALIZADO VENTAS
            </div>
            <br>
        @endif
        <hr style="    margin: 0px;">
                        <a align="center">Ventas por Empleados (Meseros)</a> 
        <hr style="    margin: 0px;"><br>
        <div align="left">
            @if(count($meseros) >0)
                @foreach ($meseros as $mesero)
                   <div style="text-align: left;">{{$mesero->nombre}} x : {{$mesero->n_facturas}}:
                       <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($mesero->totales,2,',','.')}} &euro;</b>
                   </div>
                @endforeach
             @else
             <div style="text-align: center">
                NO SE HAN REALIZADO VENTAS
                <br>
            </div>
          @endif
        </div><br>
          <hr style="    margin: 0px;">
                        <a align="center">Ventas por Empleados (Cajero)</a> 
          <hr style="    margin: 0px;"><br>              
        <div align="left">
              @if(count($cajeros) >0)
                    @foreach ($cajeros as $cajero)
                        <div style="text-align: left;">{{$cajero->nombre}} x : {{$cajero->n_facturas}}
                            <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($cajero->totales,2,',','.')}} &euro;</b>
                        </div>
                        <div style="text-align: left;">Tikects Cancelados x : {{$cajero->C}}
                            <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($cajero->TC,2,',','.')}} &euro;</b>
                        </div>
                        <div style="text-align: left;">Tikects Procesados x : {{$cajero->C}}
                            <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($cajero->TF,2,',','.')}} &euro;</b>
                        </div>
                     @endforeach
              @else
                    <div style="text-align: center;">
                        NO SE HAN REALIZADO VENTAS
                    </div>
              @endif
        </div><br>
       <hr style="    margin: 0px;">
                        <a align="center">Tramos</a> 
       <hr style="    margin: 0px;"><br>
        <div align="left">
            @if(count($cierres)>0)
                @foreach ($cierres as $cierre)
                <div style="text-align: left;">De {{substr($cierre->inicio,-8)}} A {{substr($cierre->final,-8)}}:
                        <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($cierre->ventas,2,',','.')}} &euro;</b>
                </div>
                @endforeach
             @else
                 <div style="text-align: center">
                    NO SE HAN REALIZADO VENTAS
                </div>
             @endif
        </div><br>
        <hr style="    margin: 0px;">
        <span align="center"><b>Totales:</b></span><br>
        <div align="left">
            @if(count($Productos)>0)
                @foreach ($General as $General)
                    <div>Ventas Realizadas:: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->n_facturas, 2, ',', '.')}} &euro;</b></div>
                    <div>Total Monto en productos: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->subtotales, 2, ',', '.')}} &euro;</b></div>
                    <div>Total Impuestos: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->impuestos, 2, ',', '.')}} &euro;</b></div>
                    <div>Total propina: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->propinas, 2, ',', '.') }} &euro;</b></div>
                    <div>Total Descuentos: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->decuentos, 2, ',', '.')}} &euro;</b></div>
                    <div>Total Extras: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->extras, 2, ',', '.') }} &euro;</b></div>
                    <div>Total general: <b style="position: absolute;top:auto;right: 0px;padding-right: 10px;">{{number_format($General->totales, 2, ',', '.') }} &euro;</b></div>
                 @endforeach
            @else
            <div style="text-align: center">
                NO SE HAN REALIZADO VENTAS
            </div>
            @endif
             </div>
        
        
        
    </div>

</center>
