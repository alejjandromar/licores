{{$imprimir->impresora}}#nada#{{$imprimir->id}}#
<?php
$file = fopen("config_datos_generales.txt", "r");
//Output a line of the file until the end is reached

$nombret = strtoupper(fgets($file));
$nombre = strtoupper(fgets($file));
$direccion = strtoupper(fgets($file));
$nif = strtoupper(fgets($file));
$telefono = fgets($file);

fclose($file);
?>
<cb>
    {{$nombret}}<line><line>
    {{$nombre}}<line>
    {{$direccion}}<line>
    TEL: {{$telefono}} NIF: {{$nif}}<line>
    CIERRE    DE    CAJA <b>{{$n_caja}}<line>
        {{$inicio}} - {{$final}}<line>
        </cb>

        <l>    __________________________________________   </l><line>
        <cb>PAGOS</cb> 
        <l>    __________________________________________  </l><line>
        <lista>
            <?php
            foreach ($Pagos as $pago) {?>
                <l> {{$pago->nombre . ' x ' . $pago->n_facturas . ":"}} </l>
                <ri>{{$pago->totales}} </ri>;<line>
           <?php }
            ?>
        </lista> 
        <l>      ____________________________     </l>
        <cb>DESCUENTOS</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($Descuentos as $descuento) {?>
                <l> {{$descuento->nombre . ' x ' . $descuento->n_facturas . ":"}} </l>
                <ri>{{$descuento->totales}} </ri><line>
           <?php }
            ?>
        </lista> 
        <l>      ____________________________     </l><line>
        <cb>CATEGORIAS</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($Categoria as $categoria) {?>
                <l> {{$categoria->nombre . ' x ' . $categoria->n_facturas . ":"}} </l>
                <ri>{{$categoria->totales}} </ri><line>
           <?php }
            ?>
        </lista> 
        <l>      ____________________________     </l><line>
        <cb>PRODUCTOS</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($Productos as $Producto) {?>
                <l> {{$Producto->nombre . '  ' . $Producto->presentacion. ":"}} </l> <line>
                <l>{{$Producto->cantidad_total}} </l> <ri>{{$Producto->totales}}</ri><line>
           <?php }
            ?>
        </lista> 
          <l>      ____________________________     </l><line>
        <cb>VENTAS POR MESERO</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($meseros as $mesero) {?>
                <l> {{$mesero->nombre . ' x ' . $mesero->n_facturas . ":"}} </l>
                <ri>{{$mesero->totales}} </ri><line>
           <?php }
            ?>
        </lista> 
         <l>      ____________________________     </l><line>
        <cb>VENTAS POR CAJERO</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($cajeros as $cajero) {?>
                <l> {{$cajero->nombre . ' x ' . $cajero->n_facturas . ":"}} </l>
                <ri>{{$cajero->totales}} </ri><line>
                <l> {{"Tikects Cancelados". ' x ' .  $cajero->C . ":"}} </l>
                <ri>{{ $cajero->TC}} </ri><line>
                 <l> {{"Tikects Procesados". ' x ' .  $cajero->F . ":"}} </l>
                <ri>{{ $cajero->TF}} </ri><line>
           <?php }
            ?>
        </lista> 
         <l>      ____________________________     </l><line>
        <cb>VENTAS POR TRAMO</cb> 
        <l>      ____________________________     </l><line>
        <lista>
            <?php
            foreach ($cierres as $cierre) {?>
                <l> {{$cierre->inicio . ' - ' . $cierre->final . " x " . $cierre->ventas. ":"}} </l>
                <ri>{{ $cierre->total}} </ri><line>
               
           <?php }
            ?>
        </lista> 
         <l>      ____________________________     </l><line>
        <cb>TOTALES:</cb> 
<?php if ($General != null) {
    foreach ($General as $General) {
        ?>
                   <l> Ventas Realizadas: </l><ri>{{ $General->n_facturas}} </ri><line>
                   <l>  Total Monto en productos: </l><ri>{{ $General->subtotales}}&euro; </ri><line>
                   <l>  Total Impuestos: </l><ri>{{ $General->impuestos}}&euro; </ri><line>
                   <l>  Total propina: </l><ri>{{$General->propinas}}&euro; </ri><line>
                   <l>  Total Descuentos: </l><ri>{{$General->decuentos}}&euro; </ri><line>
                   <l>  Total Extras: </l><ri>{{$General->extras}}&euro; </ri><line><line>


                   <l> Total general:</l><ri>{{$General->totales}}&euro;</ri><line><line>
                </p>
    <?php }
} else
    echo "NO SE REALIZARON VENTAS";
?>



