<?php  

$file = fopen("config_datos_generales.txt", "r");
//Output a line of the file until the end is reached

$nombret=fgets($file);
$nombre=fgets($file);
$direccion=fgets($file);
$nif=fgets($file);
$telefono=fgets($file);

fclose($file);

?>
@extends('master')
@section('title', 'Pagar Orden')
<html ng-app="ionicApp">
@section('head_css')
@parent
     <!--csss especificos de esta pagina-->
@stop
@section('head_scripts')
@parent
        <script src="http://mrrio.github.io/jsPDF/dist/jspdf.debug.js"></script>
        <script>
        //funcion del media queri
         function cambioMediaQuery(mql){
            console.log(mql);
           if(mql.matches){
               if(mql.media=="screen and (min-width: 0px) and (orientation: portrait)"){
                   ResponsiVerticalCelularTableta();//celulares y tabletas en vertical
               }else{
                if(mql.media=="screen and (max-height: 400px) and (min-width: 200px) and (orientation: landscape)"){
                    //celulares en horizontal.
                     responsiCelularHorizontal();
                }else{
                    if(mql.media=="screen and (min-width: 600px) and (orientation: portrait)"){
                        //tabletas en vertical(cambiando algunas cosas)
                        responsiTabletaVertical();
                     }else{
                        if(mql.media=="screen and (min-width: 1024px) and (orientation: landscape)"){
                            //tablets en horizontal y pcs.
                            responsiTabletaHorizontal();
                        }
                     }
                }
                
               }
            }
         }
       /* function cambioMediaQuery(mql){
            console.log(mql);
            if(mql.matches){
                ResponsiVerticalCelularTableta()
                 $('#pagar').show();
                 $('#factura').hide();
                 $('#metodos').hide();
                 $('#contenido').addClass("has-footer");  
                 $('#footer').show();
            }else{
                 $('#pagar').show();
                  $('#teclado').show();
                   $('#metodos').show();
                 $('#factura').show();
                  $('#footer').hide(); 
                  $('#contenido').removeClass("has-footer");     
            }
        }*/
        
        function maximizar(e){
            if(e.requestFullScreen){
                e.requestFullScreen
           }else if(e.webkitRequestFullScreen){
                e.webkitRequestFullScreen();
           }else if(e.mozRequestFullScreen){
                e.mozRequestFullScreen();
           
           }
        }
        function maxi(){
             maximizar(document.documentElement);
        }
         var pagos = [];
         var ang   = angular.module('ionicApp', ['ionic'])
.controller('controlador',function($scope, $compile,$ionicLoading,$ionicPopup,$http) {
    $scope.total= {{$total}};           //Total a paga por la persona
    $scope.pagado= 0.0;
    $scope.bandera= false;
    $scope.subtotal= {{$subtotal}};                    //Monto que ya ha sido cancelado
    $scope.falta= {{$total}};           //Monto resta o que se le debe al cliente
    $scope.descuento1=0.0;
    $scope.impuesto={{$impuesto}};
    $scope.propina={{$propina}};
    $scope.agregados={{$impuesto}}+{{$propina}}; 
    $scope.StylePago={'color':'red'};   //Estilo que indica si el cliente ha pagado
    $scope.dsbbtnpagar=true;            //Desabilita el boton de pago
    $scope.data={comentario:''};
    
    //Cambia el monto de un pago en especifico
    $scope.changepaid= function(idMetodoPago){
        pago=teclado(); //Tomado del Teclado
        var monto=$("#pago"+idMetodoPago+" #monto");
        var topaid=pago - parseInt(monto.text()) 
        if (pago > 0){
            monto.text(pago);
        }else{
            var monto=$("#pago"+idMetodoPago);
            monto.remove();
        }
         $scope.act_pago(topaid,"act");
    };
    
    //Remueve un metodo de pago
    $scope.rmpago= function(idMetodoPago){
        var monto=$("#pago"+idMetodoPago+" #monto");        
        var topaid=0 - parseInt(monto.text()) 
        var monto=$("#pago"+idMetodoPago).parent(".row").parent(".row");
            
            monto.remove();
            $scope.act_pago(topaid,"act");
    }
    
    //Remueve un descuento
    $scope.rmdes= function(id){
        var monto=$("#descuento"+id+" .row #monto").attr("monto1");       
        var topaid= parseFloat(monto)*-1 ;
        var monto=$("#descuento"+id);
        monto.remove();
        $("#BtnDescuento"+id).prop('disabled',false);
        $scope.descuento1+=  topaid;
        if(!$scope.bandera){
            $scope.act_pago(topaid,"%");
        }else{
            $scope.act_pago((topaid*-1),"%");
        } 
    }
    
    //Agrega o Aumenta un metodo de pago
    $scope.colocar_pago= function(idMetodoPago, imagen, Nombre) {
        pago=teclado(); //Tomado del teclado 
        if (pago > 0){
        if(!$("#pago"+idMetodoPago).length){
                var nuevo_pago= "<ion-item id='pago"+idMetodoPago+"' style=' padding: 0;width: 100%; height: 10%;min-width: 290px; min-height: 44px; background-color: #F5F3F3; padding-left: 5px;padding-top: 1px;padding-bottom: 2px; padding-right: 10px;'> "+
                                 '<a class="row" href="#"  ng-click="changepaid('+idMetodoPago+')" style=" width: 100%; color: grey;  font-weight: bold; text-decoration: none; ">'+  
                                 '<div class="col-10" style=" height: 100%;">'+
                                    ' <img src="'+imagen+'" class="imagenP" alt="Image not found">'+
                                                        '</div>'+
                                 '<div class="col col-center itemP">'+
                                             Nombre+
                                                        '</div>'+
                                 '<div class="col-33 col-center" id="detalle_tol" style="text-align: right; font-size:18px;">'+
                                 '<span id="monto" met_pay="'+idMetodoPago+'">'+pago+'</span> &euro;'
                                 '</div></a></ion-item>';

                              var nuevo_pagof= '<span>'+Nombre+': '+pago+' </span> &euro;<br>';
                                

            $('#paymet').append (nuevo_pago);
                   $('#fac').append (nuevo_pagof);
                $compile($('#pago'+idMetodoPago) )($scope); 
        }else{
            var monto=$("#pago"+idMetodoPago+" #monto");
            var intmonto= parseFloat(monto.text());
            intmonto += pago;
            monto.text(intmonto);
        }
        $scope.act_pago(pago,"act");
            }
    };
    
    //Agrega o Aumenta un descuento
    $scope.colocar_descuento= function(idDescuento,valor,tipo) {
        $scope.dsbbtnpagar=true;
            if(tipo=="porcentaje"){
                var descuento ="0."+valor;
                var valorD=$scope.total*descuento;
            }else{
                var valorD=valor;
            }
            
        if(!$("#descuento"+idDescuento).length){
            var nuevo_des= "<div class='' id='descuento"+idDescuento+"' style='min-height: 36px; padding: 0;width: 100%; height: 10%; margin-top:2px;' >"+ 
							"<a id='descuento"+idDescuento+"'  class='item'  style=' background-color: #F5F3F3; color: grey;  font-weight: bold; text-decoration: none;'>"+
                                               '<div class="row" style="">'+
                                               '<div class=" imagenDes">'+
                                                           '<img src="imagenes/descuentos/images.png" style="height: 100%; width: 100%;" alt="Image not found">'+
                                               '<div class="spam1" met_pay="'+tipo+'" class="col-center">';
                                                          var a= '<strong>-'+valor;
                                                           if(tipo == "porcentaje"){
                                                            a+='%';
                                                            }else{
                                                                a+='&euro;';
                                                            }
                                                           a+='</strong>'+
                                                        '</div>'+
                                                        '</div>' +
                                               '<div id="" class="col-center columna3">'+valor;
                                                 if(tipo == "porcentaje"){
                                                            a+=' % ';
                                                            }else{
                                                                a+=' &euro; ';
                                                            }
                                                a+= 'De Descuento'+
                                               '</div>'+
                                               '<div class="col-center columna4" id="valor">-'+
                                               "<samp id='monto' met_pay='"+idDescuento+"' monto1='"+valorD+"' tipo='"+tipo+"' valor_des='"+valor+"'>"+
                                              valorD.toFixed(2)+
                                               '</samp>&euro;</div>'+
                                               '<div class="col columna5">'+
                                                    '<button class="ion-close-circled" ng-click="rmdes('+idDescuento+')"></button>'+
                                               '</div>'+
                                               '</div>'+
                                                    '</a>'+
                                                '</div>';
                                                nuevo_des+=a;
            $('#Des').append (nuevo_des); 
            $compile($('#descuento'+idDescuento))($scope);
        }
        $scope.descuento1+=valorD;
        $scope.act_pago(valorD,'%');
         
         var n = document.getElementById("BtnDescuento"+idDescuento);
         n.disabled=true;
    }
    
    $scope.bloquearDesbloquear= function(contenedor,accion) {
        var bloquear=false;
        if(accion=='bloquear'){bloquear=true;}else{bloquear=false;}
        $("#"+contenedor+" button").each(function (index) 
             { 
            $(this).prop('disabled',bloquear);
             }) ;
    }
    
    
    //Actualiza un metodo de pago
    $scope.act_pago= function(pago,tipo) {
        if(tipo!="%"){
            $scope.pagado += pago;
            $scope.falta =  $scope.total-$scope.pagado;
            $scope.falta-=$scope.descuento1;
        }else{
            $scope.falta-=pago;
            if($scope.bandera){ $scope.falta*=-1;}
        }
        if ($scope.falta>0){
            $scope.StylePago={'color':'red'};
             $scope.dsbbtnpagar=true;
             $scope.bandera=false;
             $("#txtfalta").html("Falta: ");
             $scope.bloquearDesbloquear('formas_pago','desbloquear');
             $scope.bloquearDesbloquear('Contenedor_descuentos','desbloquear');
        }else{
            $scope.falta*=(-1)
             $scope.StylePago={'color':'green'};
            $("#txtfalta").html("Vuelto: ");
            $scope.dsbbtnpagar=false;
            $scope.bandera=true;
             $scope.bloquearDesbloquear('formas_pago','bloquear');
             $scope.bloquearDesbloquear('Contenedor_descuentos','bloquear');
            
        }
    };
    
     //funcio que agrega un comentario a la orden
   $scope.openPopupC= function(X) {
        $scope.destino=X
        var popup = $ionicPopup.show({
            templateUrl: 'comentario.html',
            scope: $scope,
});
        $scope.popupComentario=popup;
   }
   
   //funcion que cierra el popup para agregar un comentario
   $scope.closepopupComentario= function() {
       $scope.popupComentario.close();
       $scope.popupComentario=null;
   }
    $scope.CancelarOrden = function() {
        $http({
            url:'cancelarOrden',
            method: 'GET',
            params:{
              mesa:{{$num_mesa}},
              comentario:$scope.data.comentario
            },

            headers: {
                "Content-Type": "application/json"
            }

        }).success(function(response){
            $scope.closepopupComentario();
            if(!response.error){
                $ionicLoading.show({
                    template: response.mensaje,
                    duration:3000
});
                 window.location="mesas";
            }
        }).error(function(error){
            $scope.closepopupComentario();
        });
        
   }
    
   
});
function teclado(){
    var display=$("#display").val();
     pantalla="";
    document.getElementById("display").value="";
    if(display != "" && display != "DISPLAY"){
        return parseFloat(display);
    }else{
        return 0;
    }
}
	function pagar(){
        var pagos = [];
        var descuentos = [];
        $("#paymet .row #monto").each(function(){
            var pago={
                tipoPago: $(this).attr("met_pay"),
                monto: $(this).text()
            };
            pagos.push(pago);
        });
        $("#Des .row #monto").each(function(){
            var Descu={
                idDescuento: $(this).attr("met_pay"),
                valor: $(this).text(),
                valor_des: $(this).attr("valor_des"),
                tipo: $(this).attr("tipo")
            };
            descuentos.push(Descu);
        });
        toSend={
            pagos:pagos,
            descuentos:descuentos
        }
         $.ajax({
             url:"orden_pagar/{{$num_mesa}}",
             data: toSend,
             type: "get",
              success: function (data){
                if(data.error==false){
                  if (data.imprimio == true){
                      alert(data.mensaje);
                       window.location="mesas";
                  }else{
                       alert(data.mensaje);
                        window.open('pdf'+data.numero+'/false/-1','_blank');
                        window.location="mesas";
                  }
                   
                }else{
                     alert(data.mensaje);
                }
              },
              error: function (){
                  alert("Ha ocurrido un error al tratar de procesar el pago");
              }
         })
    }
         </script>
@stop

@section('contenido')
@parent 
        <div  id="contenedor" class="row" style="height: 100%; width: 100%; margin: 0px;">   
           <div id="pagar" class="col pagar" style="width: 70%; height: 100%; padding: 2px;"> 
                 <div id="cabecera" class="row" style="width: 100%; padding: 0px;"> 
                 <div class="row" style="height: 100%; width: 100%;padding: 0px;">
                        <div class="col" style="padding: 0px;">
                            <div class="row filap">
                                <div class="columna1 col-center">
                                <div class="row"  style="padding: 0px; margin: 0px;">
                                    <div class="ancho col-center">Sub-Total:&nbsp;</div>
                                    <div class="col-center">@{{subtotal.toFixed(2)}} &euro;</div>
                                </div>
                                </div>
                                <div class="columna2 col-center">
                                    <div class="row"  style="padding: 0px; margin: 0px;">
                                        <div class="ancho col-center">Agregados:&nbsp;</div>
                                        <div class=" col-center"> @{{agregados.toFixed(2)}} &euro;</div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="row filap" style="margin-top: 1px;">
                                <div class="columna1 col-center">
                                <div class="row"  style="padding: 0px; margin: 0px;">
                                    <div class="ancho col-center">Total:&nbsp; </div>
                                    <div id="txttotal" class=" col-center"> @{{total.toFixed(2)}} &euro;</div>
                                </div>
                                </div>
                                <div id="mntdesc" class="columna2 col-center">
                                    <div class="row"  style="padding: 0px; margin: 0px;">
                                        <div class="ancho col-center">Descuento: &nbsp;</div>
                                        <div class=" col-center"> @{{descuento1.toFixed(2)}} &euro;</div>
                                    </div>
                                </div>
                        
                            </div>
                            <div class="row filap" style="margin-top: 1px;"id="pagado">
                                <div class="columna1 col-center">
                                    <div class="row"  style="padding: 0px; margin: 0px;">
                                        <div class="ancho col-center">Padago: &nbsp;</div>
                                        <div class=" col-center"> @{{pagado.toFixed(2)}} &euro;</div>
                                    </div>
                                </div>
                                <div class="columna2 col-center"  ng-style="StylePago">
                                    <div class="row"  style="padding: 0px; margin: 0px;">
                                        <div id="txtfalta" class="ancho col-center">Falta: &nbsp;</div>
                                        <div class=" col-center"> @{{falta.toFixed(2)}} &euro;</div>
                                    </div>
                                </div>
                                    
                                
                                    
                                
                            </div>
                        </div>
                        </div>
                    </div>
                 <!--Tabs de descuento y pago!-->
              <div  id="TF">
               <div  id="tag">
                    <div class="tabs tabs-icon-only"style="position: relative;height: 100%; width: 30%; border: none">
                        <a class="tab-item" id="tabBtnPago" style="background-color: #D6D4D4; ">
                             <i class=" icon fa fa-university" style="line-height: 37px; font-size: 25px;" ></i>
                         </a>
                         <a class="tab-item" id="tabBtnDescuento" style="background-color: #F5F3F3; ">
                           <img src="imagenes/descuentos/descuento2.png" class="imgDP" id="imgD" alt="Descuento">
                         </a>
                     </div>
                </div>
                <div id="Contenedor_descuentos" class="row contenedor" style="margin-top: 0px;display: none;     background-color: rgb(224, 223, 223); "> 
                    
                    <ion-scroll zooming="true" direction="x" id="scroll">
                               <div id="">
                                    <div id="">
                                    <div class="row" id="fila">
                                     @if(count($descuentos) > 0)
                                        @foreach ($descuentos as $descuento)
                                                    <div class="row" id="contenedorPro" style="margin-top: 4px;"> 
                                                        <button id="BtnDescuento{{$descuento->idDescuento}}" class="producto" ng-disabled="btn{{$descuento->idDescuento}}" ng-click='colocar_descuento({{$descuento->idDescuento}},{{$descuento->valor}},"{{$descuento->tipo}}")' >
                                                            <div class="" style="height: 94%; width: 94%; margin-left: 3%; position: relative;">
                                                                <img src="imagenes/images.png" id="scroll">
                                                                <!--div que contiene el nombre-->
                                                            <div class="col-center spam">
                                                               <strong>-{{$descuento->valor}} @if($descuento->tipo == "porcentaje") % @else &euro; @endif</strong>
                                                            </div>
                                                            </div>
                                                        </button>
                                                    </div>
                                        @endforeach
                                     @else
                                     no hay descuentos en el sistema
                                     @endif
                                     </div>
                                </div>
                                </div>
                    </ion-scroll>
                </div>
                 <div id="formas_pago" class="row contenedor" style="margin-top: 0px;     background-color: rgb(224, 223, 223); "> 
                                 <ion-scroll zooming="true" direction="xy" id="scroll">
                                    <div id="productoss">
                                    <?php $cont=0 ; $conta=0;?>
                                     @if(count($metodos_pago) > 0)
                                    @foreach ($metodos_pago as $metodo_pago)
                                        <?php $cont++ ;?>
                                        <!--cada una de las filas de los métodos de pago-->
                                        @if($cont==1)
                                            <?php $conta+=8;?>
                                            <div class="row " id="fila">
                                        @endif
                                         <!--cada una de las columnas de los productos-->
                                            @if($cont==1) 
                                                <div class="row" id="contenedorPro" style="margin-top: 4px;"> 
                                            @else          
                                                <div class="row" id="contenedorPro" style=" margin-top: 6px;"> 
                                            @endif 
                                                    <button id="producto{{$metodo_pago->idPago}}" class="producto"  ng-click='colocar_pago({{$metodo_pago->idPago}},"{{$metodo_pago->imagen}}","{{$metodo_pago->tipoPago}}")'>
                                                        <div class="imgproducto">
                                                            <img src="{{$metodo_pago->imagen}}" id="scroll">
                                                        </div>
                                                        <!--div que contiene el nombre-->
                                                        <div style="height: 20px;">
                                                            <center>{{ $metodo_pago->tipoPago }}</center>
                                                        </div>
                                                    </button>
                                                </div>     
                                        @if($cont==8)
                                            </div>
                                        <?php $cont=0 ; $pri=false;?>
                                        @endif
                                    @endforeach
                                    @if(count($metodos_pago)<$conta)
                                      </div><!-- ifn contenedo-->    
                                        <?php $conta=0 ;?>
                                     @endif
                                     @else
                                     no hay metodos de pago
                                     @endif
                                </div>
                                 </ion-scroll>
                </div>
                </div>
                 <div id="TM" class=""> 
                    <div  id="teclado" class="" style="margin-top: 0px;  background-color: rgb(224, 223, 223);">
                            <div >
                             <div class="pantalla">
                                <input maxlength="5" readonly="true" class="row pantallaI" type="text" id="display" style="background-color: white;border-top: 1px solid;border-top-color: black">
                            </div>
                            <div  class="filaTec" >
                                <button class="button boton button-tecla button-tecla-responsive boton" id="1" onclick="pres(this.id)">1</button>
                                <button class="button boton button-tecla button-tecla-responsive boton" id="2"  onclick="pres(this.id)" >2</button>
                                <button class="button boton button-tecla button-tecla-responsive boton" id="3" onclick="pres(this.id)" >3</button>
                                <button class="button boton button-teclaEsp button-tecla-responsive icon ion-backspace-outline boton" id="back"  onclick="borraruno()" ></button>
                            </div>
                            <div  class="filaTec" >
                                <button class="button button-tecla button-tecla-responsive boton" id="4" onclick="pres(this.id)" >4</button>
                                <button class="button button-tecla button-tecla-responsive boton" id="5" onclick="pres(this.id)" >5</button>
                                <button class="button button-tecla button-tecla-responsive boton" id="6" onclick="pres(this.id)">6</button>
                                <button id="btnpagar"  ng-disabled="dsbbtnpagar" onclick="pagar()" class="button button-teclaEsp button-tecla-responsive boton icon ion-cash" ></button>
                            </div>
                             <div  class="filaTec">
                                <button id="7" onclick="pres(this.id)" class="button boton button-tecla button-tecla-responsive" >7</button>
                                <button id="8" onclick="pres(this.id)" class="button boton button-tecla button-tecla-responsive" >8</button>
                                <button id="9" onclick="pres(this.id)" class="button boton button-tecla button-tecla-responsive" >9</button>
                                 <a href="mesas"><button id="volver" title="Volver a Mesas"  class="button boton button-teclaEsp button-tecla-responsive icon "><img src="img/table.gif" width="25" height="25"></button></a>
                            </div>
                            <div class="filaTec">
                                <button id="." onclick="pres(this.id)" class="button boton button-teclaEsp button-tecla-responsive" >.</button>
                                <button id="0" onclick="pres(this.id)" class="button boton button-tecla button-tecla-responsive " >0</button>
                                <button id="." onclick="pres(this.id)" class="button boton button-teclaEsp button-tecla-responsive" >.</button>
                                <a class="button button-teclaEsp button-tecla-responsive boton" title="Cancelar Orden" ng-click="openPopupC('Orden')">
                                    <i class="ion-document-text" style="font-size: 22px;position: relative;top: 6px;left: 20px;"></i>
                                    <i class="ion-ios-circle-outline" style="font-size: 32px;position: relative;left: -4px;top: 10px;"></i>
                                    <span class="" style="font-size: 28px;position: relative;left: -27px;top: 7px;">/</span>
                               </a>
                            </div>
                         </div >   
                    </div>
                     <div id="metodos" class="item" style="background-color: #F5F3F3; border: none; border-left: solid 1px #ededed">
                        <ion-scroll zooming="true" direction="xy" id="scroll">
                        <div id="paymet">
                            <div class="item item-divider" style="margin: 0px;background-color: #DC4E1E;color: white;padding-left: 5px;">
                                Pagos
                        </div>
                        </div>
                        <div id="Des">
                            <div class="item item-divider" style="margin: 0px;background-color: #DC4E1E;color: white;padding-left: 5px;">
                                Descuentos
                        </div>
                        </div>
                        </ion-scroll>
                    </div>
                    
                </div>
           </div>
           <div id="factura" class="factura">
               <ion-scroll zooming="true" direction="xy" id="scroll">
                 <?php $hora= date ("h:i");
                    $fecha= date ("j-n-Y"); 
                    //Variables de la factura.
                    $mesa=$num_mesa;
                    $totalFatura=$total;
                    $cant=3;
                    $productos="";
                    $precioUni="15";
                    $totalxProducto=0;
                    $empleado=$vendedor;

                    //-----------------------------
                    ?>
<div id="cont"  style="width:100%; font-family:monospace;">
<center>
<span id="nombretM"  align="center">{{$nombret}}</span><br></b>
<span id="nombreM" align="center">{{$nombre}}</span><br>
<div style="width: 250px;"> {{$direccion}}<span  id="direccionM" align="center"></span></div>
 NIF:<span id="nifM" align="center">{{$nif}}</span> 
 TEL:<span id="telefonoM" align="center">{{$telefono}}</span> <br>
 
    Mesa: <?php echo $mesa?> <br>
   <?php echo $fecha." ".$hora ?> </p> 
   </center>    
   <p style="text-align: center;"><b>O R D E N / P E D I D O</b></p> 
   <hr>
   </div>
    <table  style="width: 100%;  text-align: center; ">
          <?php foreach ($detalles as $detalles) { ?>
        <tr>                
            <td style="width: 60%;"><?php echo $detalles->getproducto->nombre." ".$detalles->presentacion." x ".$detalles->cantidad;?></td>
            <td style="width: 20%;"><?php echo $detalles->precio;?></td>
             <?php if ($detalles->cantidad > 1){?>

                <th style=""><?php echo $detalles->precio*$detalles->cantidad; ?>&euro;</th>   
             <?php } else{?>   
            <td style="width: 20%;"><?php echo $detalles->precio; ?>&euro;</td>     
        </tr>
        <?php }} ?>
    </table>
    <hr>
  <p style="text-align: center;"> 
      <span >Subtotal: <?php echo $subtotal." &euro;"; ?></span><br>
     <span>Impuesto: <?php echo $impuesto." &euro;"; ?></span><br>
    <span>Descuentos: 0 &euro;</span><br>
    <span>Servicios: 0 &euro;</span><br>


<h3 align="center">Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo round($totalFatura,2);?>&euro;</h3> 
 <p align="center">!!Gracias por preferirnos!!</p>
          </ion-scroll>
           </div>
        </div>
@stop
@section('footer')          
   <div id="footer" class="bar bar-footer bar-calm" style="background-color: #DC4E1E;">
        <button id="BtnT" class="FBoton button-clear BtnM"><spam class="fa fa-keyboard-o"></spam></button>
        <button id="BtnFor" class="FBoton button-clear BtnM"><spam class="fa fa-eye"></spam></button>
        <button id="BtnPagos" class="FBoton button-clear BtnM"><spam class="icon ion-android-apps"></spam></button>
        <button id="BtnPreviu" class="FBoton button-clear BtnM"><spam class="fa fa-file-text-o"></spam></button>
    </div>  
@stop

@section('scripts')
@parent
        
    
    <script>
    var actual="formasPago";
    $( document ).ready(function() {
        $('#BtnPreviu').click(
          function (e) {factura
                 $('#pagar').hide();
                $('#factura2').show();
        });
        $('#volver2').click(
          function (e) {factura
                 $('#factura2').hide();
                $('#pagar').show();
        });
        });

var pantalla="";
    
  
    
     function pres(id)
        {
           pantalla=pantalla+id;
            document.getElementById("display").value=pantalla;
        
        }
        function borrartodo()
        {
        pantalla="";
        document.getElementById("display").value="";
        
        }
        
        
        
        
        function borraruno()
        {
         var contenido = document.getElementById("display").value;
         var tamano = contenido.length;
         var contenidos="";
         
         for (var i = 0, max = tamano-1; i < max; i++) {
            contenidos=contenidos+contenido[i];
        }  
        
        document.getElementById("display").value=contenidos;
        pantalla=contenidos;
        }
        function ResponsiVerticalCelularTableta(){
            $('#footer').show();
            $('#display').css('height','100%');
            $('#teclado').css('background-color',"white");
            $('#teclado').show();
            $('#pagar').show();
            $('#factura').hide();
            $('#metodos').hide();
        }
        function  responsiCelularHorizontal(){
            $('#pagar').hide();
            $('#factura').hide();
            $('#footer').hide();
            alert("Por favor coloque el dispositivo en posicion vertical");
        }
        function responsiTabletaVertical(){
            $('#contenido').addClass("has-footer");  
            $('#footer').show();
            $('#TF').css('height',"150px");
            $('#tag').css('height',"35px");
            var ocupado=$('#cabecera').height()+$('#TF').height();
            var alto=$('#pagar').height();
            var porCont=((ocupado*100)/alto);
            var disponible=100-porCont;
            $('#metodos').show();
            $('#TM').css('height',disponible+"%");
        }
        function responsiTabletaHorizontal(){
            $('#tag').css('height',"18%");
            $('#pagar').show();
            $('#factura').show();
            $('#contenido').removeClass("has-footer");  
            $('#footer').hide();
            $('#TF').css('height',"29%");
            $('#TM').css('height',"50%");
        }
    $( document ).ready(function() {
        var mql=window.matchMedia("screen and (min-width: 0px) and (orientation: portrait)");
        var mql1=window.matchMedia("screen and (min-width: 200px) and (max-height: 400px) and (orientation: landscape)");
        var mql2=window.matchMedia("screen and (min-width: 600px) and (orientation: portrait)");
        var mql3=window.matchMedia("screen and (min-width: 1024px) and (orientation: landscape)");
        cambioMediaQuery(mql);
        cambioMediaQuery(mql1);
        cambioMediaQuery(mql2);
        cambioMediaQuery(mql3);
        mql.addListener(cambioMediaQuery);
        mql1.addListener(cambioMediaQuery);
        mql2.addListener(cambioMediaQuery);
        mql3.addListener(cambioMediaQuery);        
    $('#tabBtnPago').click(
          function (e) {
                $('#Contenedor_descuentos').hide();
                $('#formas_pago').show();
                $('#tabBtnPago').attr("style","background-color: #D6D4D4;");
                $('#tabBtnDescuento').attr("style","background-color: #F5F3F3;");
                
        });
        
        $('#tabBtnDescuento').click(
          function (e) {
                $('#formas_pago').hide();
                $('#Contenedor_descuentos').css("display","block");
                $('#tabBtnDescuento').attr("style","background-color: #D6D4D4;");
                $('#tabBtnPago').attr("style","background-color: #F5F3F3;");
                
                
        });
        $('#BtnPreviu').click(
          function (e) {factura
                $('#pagar').hide();
                $('#factura').show();
                pantalla="factura";
        });
        $('#BtnFor').click(
          function (e) {factura
                 $('#factura').hide();
                 $('#teclado').hide();
                  $('#pagar').show();
                $('#metodos').show();
        });
        $('#BtnT').click(
          function (e) {factura
                 $('#factura').hide();
                 $('#metodos').hide();
                 $('#teclado').show();
                $('#pagar').show();
        });
        $('#BtnPagos').click(
          function (e) {factura
                 $('#factura').hide();
                $('#pagar').show();
        });
        });
        /*
        function cambioMediaQuery(mql){
            if(mql.matches){
                var alto=$("#pagar").height();
                var p=alto/100;
                var a=(p*21)-82;
                var falta =a/p;
                $('#pagar').show();
                 $('#factura').hide();
                 var dif=parseInt(64+falta);
                 var alto=dif.toString()+"%";
                 $('#TM').css("height",alto); 
                 $('#metodos').hide();
                 $('#contenido').addClass("has-footer");  
                 $('#footer').show();
            }else{
                 $('#pagar').show();
                  $('#teclado').show();
                   $('#metodos').show();
                 $('#factura').show();
                  $('#footer').hide(); 
                  $('#contenido').removeClass("has-footer"); 
                   $('#TM').css("height","50%");     
            }
        }
        */
    </script>
<script id="comentario.html" type="text/ng-template">
    <div id="comentario" class="co">
        <ion-popup-view>
            <ion-header-bar class="header-pizza">
            <div class="title title-left T" style="font-size:15px;color: white;">@{{destino}}&rarr; Agregar comentario</div>
                <div class="buttons">
                    <button class="button-clear ion-close-circled" ng-click="closepopupComentario()" style="color:#FFE6DE;font-size: 21px;"></button>
                </div>
            </ion-header-bar>
            <ion-content class="has-header" style="background-color: white;padding: 10px;">
                    <textarea name="comentario" rows="4" autofocus="true" ng-model="data.comentario" placeholder="Escriba un comentario u observaci&oacute;n" resize="false" style="height: 100%;" required>
                    </textarea>
                    <span ng-if="data.comentario==''||!data.comentario" style="position: relative;bottom: 14px; color:red">Por favor introduzca un comentario</span>
            </ion-content>
            <ion-footer-bar class="header-pizza">
                    <button  class="button-clear BtnM"  style="font-size:15px;width: 50%;"  ng-click="CancelarOrden()" ng-disabled="data.comentario==''||!data.comentario">Guardar</button>
                    <button  class="button-clear BtnM " style="font-size:15px;border-left: solid white 1px;width: 50%;" ng-click="closepopupComentario()">Cancelar</button>
            </ion-footer-bar>
        </ion-popup-view>
    </div>
</script>
@stop     





