@extends('layouts.master')

@section('title', 'Gestión de usuarios')
@section('bread', 'Configuracion / Gestión de usuarios')

@section('head_css')
@parent

{!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
{!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
{!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
<!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent



<div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">
    {!!Form::open(['route'=>'metodos_pago_add', 'method'=>'POST', 'id' => 'registrationForm'])!!}      

    <!--Panel para agregar el nuevo metodo de Pago-->
    <div class="panel widget light-widget panel-bd-top " id="PDatosDocumentos" style="display:none">
        <div class="panel-body">
            <div class="alert alert-danger vd_hidden">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Todos!</strong> los campos son requeridos.
            </div>
            <div class="alert alert-success vd_hidden">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Bien Hecho!</strong>.
            </div>
            <div class="panel-heading vd_bg-default">
                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-hospital-o"></i> </span>Datos del Empleado</h3>
                <div class="vd_panel-menu">
                    <span class="vd_red">*</span> <span class="vd_red">Indica Campos Requeridos</span>
                </div>
            </div>
            <div class="panel-body" style="border: 1px solid #ddd">
                <div class="row">
                    <div class="col-lg-3 form-group">
                        <div class="form-label ">DNI <span class="vd_red">*</span></div>
                        <input type="text" id="dni" class="mgbt-xs-20 mgbt-sm-0 form-control" name="dni" >
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Nombre <span class="vd_red">*</span></div>
                        <input type="text" id="name" class="mgbt-xs-20 mgbt-sm-0 form-control" name="name">
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Email <span class="vd_red">*</span></div>
                        <input type="email" id="email" class="mgbt-xs-20 mgbt-sm-0 form-control"  name="email" >
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Telefono <span class="vd_red">*</span></div>
                        <input type="tel" id="telefono" class="mgbt-xs-20 mgbt-sm-0 form-control" name="telefono">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Contraseña <span class="vd_red">*</span></div>
                        <input type="password" id="password" class="mgbt-xs-20 mgbt-sm-0 form-control" name="password">
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Confirme  Contraseña: <span class="vd_red">*</span></div>
                        <input type="password" id="password2" class="mgbt-xs-20 mgbt-sm-0 form-control" name="password2">
                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Tipo <span class="vd_red">*</span></div>
                        <select id="tipo" name="tipo" class="form-control">
                            <option value="Admin">Admin</option>
                            <option value="Supervisor">Supervisor</option>
                            <option value="Mesero">Mesero</option>
                            <option value="Cajero">Cajero</option>
                        </select>

                    </div>
                    <div class="col-lg-3 form-group">
                        <div class="form-label">Cargo <span class="vd_red">*</span></div>
                        <input type="text" id="cargo" class="mgbt-xs-20 mgbt-sm-0 form-control" name="cargo" >
                    </div>
                </div>
                <div class="mgtp-20">
                <div class="row mgbt-xs-0">
                    <div class="col-xs-6"></div>
                    <div class="col-xs-6 text-right">
                        <button id="BtnCClinicas"  type="button" class="btn vd_btn btn-default" style="color: #0E2145!important">
                            Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                        </button>

                        <button id="BtnGDocumento" type="submit" value="Enviar" class="btn vd_btn vd_bg-green">
                            <!--top-right-success-->
                            Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                        </button>

                    </div>
                </div>
            </div>
                </div>
        </div>
    </div>
</form>
<!--panel de resultados-->
<div class="row" id="Ptabla">
    <div class="col-md-12">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Empleados</h3>
                <div class="vd_panel-menu">
                    <button id="BtnEmpleado" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                </div>
            </div>
            <div class="panel-body table-responsive" style="padding: 15px;">
                <table class="table table-striped" id="data-tables">
                    <thead>
                        <tr>
                            <th>DNI</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                            <th>Cargo</th>
                            <th>Email</th>
                            <th>Telefono</th>
                            <th>Estado</th>
                            <th style="display:none;"></th>  
                            <th style="display:none;"></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
        <!-- Panel Widget -->
    </div>
    <!-- col-md-12 -->
</div>

<!-- contenido -->



</div>


</div>

</div>

</div>
<div id="error"></div>




@endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
<!--script especificos de esta pagina-->
<script type="text/javascript">


    function save_method(id, formData) {
        var url = 'empleado/add';
        var titulo='registrado';

        if (id != 0) {
            url = 'empleado/update';
            titulo='modificado';
        }

        $.ajax({
            url: url,
            dataType: 'json',
            processData: false,
            contentType: false,
            data: formData,
            type: "POST",
            success: function (data) {
                if (data['error'] == false) {

                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: 'El usuario ha sido '+titulo+' exitosamente',
                        type: BootstrapDialog.TYPE_SUCCESS,
                    });
                    load_table();
                } else {
                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: 'Error: ' + data['mensaje'],
                        type: BootstrapDialog.TYPE_DANGER,
                    });
                }

            },
            error: function (data) {
                BootstrapDialog.danger('Ocurrio un error al agregar el usuario, ' + data);
            }
        }
        );
    }
    var num = 0;
    function load_table() {
        var url = 'empleado/all';

        $.ajax({
            url: url,
            dataType: "JSON",
            type: "get",
            success: function (data) {

                if (num == 2) {
                    $("#data-tables").DataTable().destroy();
                } else {
                    num = 2;
                }
                $("#data-tables tbody").empty();
                var row = "";
                for (var i = 0; i < data["empleado"].length; i++) {
                    row += '<tr class="odd gradeX" id="flotante1' + data["empleado"][i].dni + '" value="' + data["empleado"][i].dni + '">';
                    row += '  <td class="celda center">' + data["empleado"][i].dni + '</td>';
                    row += '  <td class="celda center">' + data["empleado"][i].name + '</td>';
                    row += '  <td class="celda center">' + data["empleado"][i].tipo + '</td>';
                    row += '  <td class="celda center">' + data["empleado"][i].cargo + '</td>';
                    row += '  <td class="celda center">' + data["empleado"][i].email + '</td>';
                    row += '  <td class="celda center">' + data["empleado"][i].telefono + '</td>';
                    if (data["empleado"][i].estado == '1') {
                        row += '  <td class="celda center"><span class="label vd_bg-green">Activa</span></td>';
                    } else {
                        row += '  <td class="celda center"><span class="label vd_bg-red">Inactiva</span></td>';
                    }
                    row += '</td>';
                    row += '  <td class="menu-action celda center" style="width: 141px;">';
                    row += '  <a data-original-title="Editar" onclick="mostrardiv(' + "'" + data["empleado"][i].dni + "'" + ',' + "'" + data["empleado"][i].name + "'" + ',' + "'" + data["empleado"][i].email + "',"+ "'" + data["empleado"][i].telefono + "',"+ "'" + data["empleado"][i].tipo + "',"+ "'" + data["empleado"][i].cargo + "'" + ')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';
                    if (data["empleado"][i].estado == '1') {
                        row += '<span id="status"> <a data-original-title="Desactivar" onclick="estado(this,0,\'' + data["empleado"][i].dni + '\')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                    } else {
                        row += '<span id="status"> <a data-original-title="Activar"  onclick="estado(this,1,\'' + data["empleado"][i].dni + '\')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                    }
                    row += '      <a data-original-title="Eliminar" id="btnEliminar"    onclick="eliminar(this, \'' + data["empleado"][i].dni + '\')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                    row += '  </span></td>';
                    row += '  <td colspan="12" id="flotante2' + data["empleado"][i].dni + '" style="display:none;">';
                    row += '   {!!Form::open(['route'=>'empleado_updt', 'method'=>'POST','id' => 'registrationForm'])!!}' +
                            '                                              <div class="panel-body" style="border: 1px solid #ddd">         <div class="row">' +
                            '                                                <div class="row">'+
                            '                                                     <div class="col-lg-3">' +
                            '                                                        <div class="form-label">DNI <span class="vd_red">*</span></div>' +
                            '                                                        <input type="text" id="dni'+data["empleado"][i].dni+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="dni" value= "' + data["empleado"][i].dni + '"required>' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Nombre <span class="vd_red">*</span></div>' +
                            '                                                        <input type="text" id="name'+data["empleado"][i].dni+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="name" value="' + data["empleado"][i].name + '" required>' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Email <span class="vd_red">*</span></div>' +
                            '                                                        <input type="email" id="email'+data["empleado"][i].dni+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="email" value="' + data["empleado"][i].email + '" required>' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Telefono <span class="vd_red">*</span></div>' +
                            '                                                        <input type="tel" id="telefono'+data["empleado"][i].dni+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="telefono" value="' + data["empleado"][i].telefono + '" required>' +
                            '                                                    </div></div>' +
                            '                                                <div class="row">'+
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Contraseña <span class="vd_red">*</span></div>' +
                            '                                                        <input type="password" id="password" class="mgbt-xs-20 mgbt-sm-0" title="" name="password">' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Confirme  Contraseña: <span class="vd_red">*</span></div>' +
                            '                                                        <input type="password" id="password2" class="mgbt-xs-20 mgbt-sm-0" title="" name="password2">' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Tipo <span class="vd_red">*</span></div>' +
                            '                                                        <select id="tipo'+data["empleado"][i].dni+'" name="tipo" value="' + data["empleado"][i].tipo + '">' +
                            '                                                            <option '+(data["empleado"][i].tipo=='Admin'?'Selected':'')+'>Admin</option>' +
                            '                                                            <option '+(data["empleado"][i].tipo=='Supervisor'?'Selected':'')+'>Supervisor</option>' +
                            '                                                            <option '+(data["empleado"][i].tipo=='Mesero'?'Selected':'')+'>Mesero</option>' +
                            '                                                            <option '+(data["empleado"][i].tipo=='Cajero'?'Selected':'')+'>Cajero</option>' +
                            '                                                        </select>' +
                            '                                                        ' +
                            '                                                    </div>' +
                            '                                                    <div class="col-lg-3">' +
                            '                                                        <div class="form-label">Cargo <span class="vd_red">*</span></div>' +
                            '                                                        <input type="text" id="cargo'+data["empleado"][i].dni+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="cargo" value="' + data["empleado"][i].cargo + '" required>' +
                            '                                                    </div>' +
                            '                                                </div></div>';
                    row += '              <div class="mgtp-20">';
                    row += '                  <div class="row mgbt-xs-0">';
                    row += '                      <div class="col-xs-6"></div>';
                    row += '                      <div class="col-xs-6 text-right">';
                    row += '                          <button id="BtnCClínicas" type="button" value=" ocultar" onclick="ocultardiv(\'' + data["empleado"][i].dni + '\')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                    row += '                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                    row += '                          </button>';
                    row += '                          <button id="BtnGClínicas" type="submit" class="btn vd_btn vd_bg-green">';
                    row += '                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                    row += '                          </button>';
                    row += '                       </div>';
                    row += '               </div>';
                    row += '          </div>';
                    row += '       </form>';
                    row += '    </td>';
                    row += '   </tr>';
                    $("#data-tables tbody").append(row);
                    row = "";

                }

                validate();

                $('#data-tables').dataTable({
                    language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }

                    }

                }

                );


            },
            error: function () {
                BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los impuestos');
            }
        }
        );
    }

    function estado(btn, val, id) {
        var url = 'empleado/update_stus';
        $.ajax({
            url: url,
            data: {id: id, estado: val},
            dataType: "JSON",
            type: "get",
            success: function (data) {
                if (data.error == false) {
                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: '' + data.mensaje,
                        type: BootstrapDialog.TYPE_SUCCESS,
                    });
                    if (val == 1) {
                        $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, \'' + id + '\')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                        $('#estadoD' + id).html('<span class="label vd_bg-green">Activo</span>');
                        $('[data-toggle="tooltip"]').tooltip();
                    } else {
                        $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, \'' + id + '\')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                        $('#estadoD' + id).html('<span class="label vd_bg-red">Inactivo</span>');
                        $('[data-toggle="tooltip"]').tooltip();
                    }
                } else {
                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: 'Error: ' + data.mensaje,
                        type: BootstrapDialog.TYPE_DANGER,
                    });
                }
            }
        }

        );
    }
    function eliminar(btn, id) {
        var url = 'empleado/delete';
        $.ajax({
            url: url,
            data: {id: id, estado: 0},
            dataType: "JSON",
            type: "get",
            success: function (data) {
                if (data.error == false) {
                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: '' + data.mensaje,
                        type: BootstrapDialog.TYPE_SUCCESS,
                    });
                    load_table();

                } else {
                    BootstrapDialog.alert({
                        title: 'Notificación',
                        message: 'No se pudo eliminar el usuario, error: ' + data.mensaje,
                        type: BootstrapDialog.TYPE_DANGER,
                    });
                }
            }
        }

        );
    }



    function validate() {
        $('tbody form').bootstrapValidator({
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un nombre'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]([a-zA-Z]| )*$/,
                            message: 'solo se permiten caracteres alfanumericos'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un email'
                        },
                    }
                },
                password: {
                    validators: {
                        regexp: {
                            regexp: /^[0-9][0-9][0-9][0-9]$/,
                            message: 'Debe ser 4 digitos exactamente'
                        }
                    }
                },
                password2: {
                    validators: {
                        identical: {
                            field: 'password',
                            message: 'Las contraseñas no coinciden'
                        }
                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un dni'
                        },
                        regexp: {
                            regexp: /^[0-9]{8}[A-Z]$/,
                            message: 'Debe ser 8 digitos y una letra exactamente'
                        }
                    }
                },
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un telefono'
                        }
                    }
                },
                cargo: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un cargo'
                        }
                    }
                }


            }
        });

        //Upadate function
        $('tbody form').submit(function (e) {
            if (e.isDefaultPrevented()) {
                //wrong
            } else {
                var id = $(this).parents("tr").attr("value");
                var formData = new FormData(this);
                save_method(id, formData);
                e.preventDefault();
            }
        });
    }
    //editar
    function mostrardiv(dni, nombre,correo,telefono,tipo,cargo) {
        var nombreDiv = 'flotante2' + dni.toString();
        var nombrediv1 = '#flotante1' + dni.toString();
        $(nombrediv1).children('.celda').addClass('noprint');
        div = document.getElementById(nombreDiv);

        div.style.display = "";
        $('#flotante2'+dni+' #registrationForm').bootstrapValidator('resetForm',true);
        $('#dni'+dni).val(dni);
        $('#name'+dni).val(nombre);
        $('#email'+dni).val(correo);
        $('#telefono'+dni).val(telefono);
        $('#tipo'+dni).val(tipo);
        $('#cargo'+dni).val(cargo);
    }

    function ocultardiv(codigo) {
        var pnombre = 'flotante2' + codigo.toString();
        var nombrediv1 = '#flotante1' + codigo.toString();
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    }
    $(document).ready(function () {
        $("#BtnEmpleado").on("click", function () {
            $('#PDatosDocumentos').show("slow");
            $('#registrationForm').bootstrapValidator('resetForm',true);
           $('#tipo').val('Admin');
        });

        load_table();

        $("#BtnCClinicas").on("click", function () {
            $('#PDatosDocumentos').hide("slow");


        });
        //para buscar las ciudades de ese pais al cargar la pagina
        $('#registrationForm').bootstrapValidator({
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'El nombre es necesario'
                        },
                        regexp: {
                            regexp: /^[a-zA-Z]([a-zA-Z]| )*$/,
                            message: 'Utilice solo letras y espacios en el nombre'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'El email es necesario'
                        },
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria'
                        },
                        regexp: {
                            regexp: /^[0-9][0-9][0-9][0-9]$/,
                            message: 'Debe ser 4 digitos exactamente'
                        },
                    }
                },
                password2: {
                    validators: {
                        notEmpty: {
                            message: 'La contraseña es necesaria'
                        },
                        regexp: {
                            regexp: /^[0-9][0-9][0-9][0-9]$/,
                            message: 'Debe ser 4 digitos exactamente'
                        }

                    }
                },
                dni: {
                    validators: {
                        notEmpty: {
                            message: 'El dni es necesaria'
                        },
                        regexp: {
                            regexp: /^[0-9]{8}[A-Z]$/,
                            message: 'Debe ser 8 digitos y una letra exactamente'
                        }
                    }
                },
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un telefono'
                        }
                    }
                },
                cargo: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un cargo'
                        }
                    }
                }

            }});

        $('#registrationForm').submit(function (e) {

            if (e.isDefaultPrevented()) {
                //wrong
            } else {
                var formData = new FormData(document.getElementById("registrationForm"));
                save_method(0, formData);
                document.getElementById('registrationForm').reset();
                $('#PDatosDocumentos').hide("slow");
                e.preventDefault();
            }
        });

    });


</script>
@stop
