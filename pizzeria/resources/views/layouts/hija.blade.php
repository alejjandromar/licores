@extends('layouts.master')

@section('title', 'Page Title')

@section('head_css')
@parent
     <!--csss especificos de esta pagina-->
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent
<div class="panel widget Margin-min">
                                    <div class="panel-heading vd_bg-grey">
                                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-user"></i> </span> Cliente </h3>
                                    </div>
                                   <form id="registrationForm" action="#">
                                       <div class="panel-body " style="padding-bottom: 5px;">
                                            <div class="row Margin-not">
                                                
                                                <div class="col-lg-7 col-md-7 col-sm-7">
                                                    <div  style=" width:100%">
                                                        <div class="form-label">Client Numbert
                                                        <input class="" type="text" >
                                                        </div>
                                                     </div>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1">
                                                    <br>
                                                    <button id="BtnBuscar" class="btn vd_bg-green vd_white btn-block" type="button"><i class="fa fa-search fa-fw"></i></button>
                                                </div>
                                            </div>
                                    </div>
                                <div class="" id="PDatosCliente" style="display:none">
                                        <div class="panel-body">
                                            <div id="1" class="alert alert-danger vd_hidden">
                                                <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong>Todos!</strong> los campos son requeridos.
                                            </div>
                                                 <div id="" class="alert alert-success vd_hidden">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
                                                <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Bien hecho!</strong>.
                                            </div>
                                            <div class="col-lg-12">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-hospital-o"></i> </span>Datos del nuevo cliente</h3>
                                                <div class="vd_panel-menu">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica Campos Requeridos</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                <div class="row">
                                                     <div class="col-lg-3 form-group">
                                                        <div class="form-label">Client Numbert<span class="vd_red">*</span></div>
                                                        <input type="text" id="ClietNumber" class="mgbt-xs-20 mgbt-sm-0 form-control" name="ClietNumber" maxlength="15">
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Name<span class="vd_red">*</span></div>
                                                        <input type="text" id="name" class="mgbt-xs-20 mgbt-sm-0 form-control" name="name"maxlength="50">
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Last Name <span class="vd_red">*</span></div>
                                                        <input type="text" id="lastName" class="mgbt-xs-20 mgbt-sm-0 form-control" name="lastName" maxlength="50">
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Adress <span class="vd_red">*</span></div>
                                                        <input type="text"  id="adress" class="mgbt-xs-20 mgbt-sm-0 form-control" name="adress"maxlength="200">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">E-mail <span class="vd_red">*</span></div>
                                                        <input type="text" id="email" class="mgbt-xs-20 mgbt-sm-0 form-control" name="email">
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Phone<span class="vd_red">*</span></div>
                                                        <input type="text" id="phone" class="mgbt-xs-20 mgbt-sm-0 form-control" name="phone" maxlength="16">
                                                    </div>
                                                    
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Country<span class="vd_red">*</span></div>
                                                        <select id="country" class="mgbt-xs-20 mgbt-sm-0" onchange="buscarEstados()">
                                                            <option value="VEN">Venezuela</option>
                                                            <option value="MEX">Mexico</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 form-group">
                                                        <div class="form-label">Zip Code<span class="vd_red">*</span></div>
                                                        <input type="text" id="ZipCode" class="mgbt-xs-20 mgbt-sm-0 form-control" name="ZipCode" maxlength="50">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col-lg-3 form-group">
                                                        <div class="form-label">State / District<span class="vd_red">*</span></div>
                                                        <select id="State" class="mgbt-xs-20 mgbt-sm-0" onchange="verificarD()">
                                                            <option value="Amazonas">Amazonas</option>
                                                            <option value="Nueva Esparta">Nueva Esparta</option>
                                                            <option value="otro">Otro</option>
                                                        </select>
                                                    </div>
                                                    <div id="divS" class="col-lg-3 form-group" style="display:none">
                                                        <div class="form-label">New State/District<span class="vd_red">*</span></div>
                                                        <input type="text" id="txtState" class="mgbt-xs-20 mgbt-sm-0 form-control" name="txtState"maxlength="60">
                                                    </div>
                                                    <div class="col-lg-3 form-group" id="Ciudad1">
                                                        <div class="form-label">City<span class="vd_red">*</span></div>
                                                        <select id="Ciudad" class="mgbt-xs-20 mgbt-sm-0" onchange="verificarC()">
                                                            <option value="Porlamar">Porlamar</option>
                                                            <option value="Pampatar">Pampatar</option>
                                                            <option value="otro">Otra</option>
                                                        </select>
                                                    </div>
                                                       <div id="divC" class="col-lg-3 form-group" style="display:none">
                                                        <div class="form-label">New City<span class="vd_red">*</span></div>
                                                        <input type="text" id="txtCyti" class="mgbt-xs-20 mgbt-sm-0 form-control" name="txtCyti"maxlength="60">
                                                    </div> 
                                                     <div class="col-lg-3 form-group" style="left:0px">
                                                            <div class="form-label">Time Zone<span class="vd_red">*</span></div>
                                                            <select id="timeZone" class="mgbt-xs-20 mgbt-sm-0">
                                                                <option>(UTC -4:30)Caracas</option>
                                                                <option>(UTC -5:00)Bogota</option>
                                                            </select>
                                                </div> 
                                                </div>
                                            </div>
                                            <div class="mgtp-20">
                                                <div class="row mgbt-xs-0">
                                                    <div class="col-xs-6"></div>
                                                    <div class="col-xs-6 text-right">
                                                        <button id="BtnCCliente" type="" value="Enviar" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                            Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                        </button>

                                                        <button id="BtnGCliente" type="" value="Enviar" class="btn vd_btn vd_bg-green">
                                                            <!--top-right-success-->
                                                            Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                       </form>
                                   <div class="" id="MDatosCliente">
                                        <div class="panel-body" style="border-bottom: 1px solid #ddd;padding-top: 0px">
                                                <div id="PMostrarC">
                                                        <h4 class="mgbt-xs-15 mgtp-10 font-semibold">Datos de Contacto</h4>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-4 control-label">Full Name:</label>
                                                                    <div class="col-xs-8 controls">yoscar Hernandez</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-4 control-label">Phone:</label>
                                                                    <div class="col-xs-8 controls">+5802952742478</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-3 control-label">Email:</label>
                                                                    <div class="col-xs-9 controls">yoscarHernandez@gmail.com</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-4 control-label">City:</label>
                                                                    <div class="col-xs-8 controls">(UTC-4:30)Porlamar</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-4 control-label">State:</label>
                                                                    <div class="col-xs-8 controls">Nueva Esparta</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="row mgbt-xs-0">
                                                                    <label class="col-xs-3 control-label">Country:</label>
                                                                    <div class="col-xs-9 controls">Venezuela</div>
                                                                    <!-- col-sm-10 -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
    
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        $(document).ready(function () {
            //para buscar las ciudades de ese pais al cargar la pagina
            if (!$('#country').val() == '') {
               console.log("peticion ajaz para encontrar las ciudades de ese pais");
            }
             if (!$('#State').val() == '') {
               console.log("peticion ajaz para encontrar las ciudades del pais que sale por defecto");
            }
             $('#registrationForm').bootstrapValidator({
          
	 fields: {
		 name: {
			 validators: {
				 notEmpty: {
					 message: 'a name is needed.'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]+$/,
					 message: 'the name may only contain letters'
				 }
			 }
		 },
		 lastName: {
			 validators: {
				 notEmpty: {
					 message: 'a surname is needed'
				 },
                                 regexp: {
					 regexp: /^[a-zA-Z]+$/,
					 message: 'the last name may only contain letters'
				 }
			 }
		 },
		 email: {
			 validators: {
				 notEmpty: {
					 message: 'an email is needed'
				 },
				 emailAddress: {
					 message: 'mail address is not valid'
				 }
			 }
		 },
		 adress: {
			 validators: {
				 notEmpty: {
					 message: 'you need a home address'
				 }
			 }
		 },
		 phone: {
			 message: 'the phone is not valid',
			 validators: {
				 notEmpty: {
					 message: 'you need a phone number'
				 },
				 regexp: {
					 regexp: /^[0-9]+$/,
					 message: 'The phone can only contain numbers'
				 },
                                 stringLength: {
					 min: 8,
                                         max: 16,
					 message: 'the phone must be between 8 and 16 digits'
				 }
			 }
		 },
		 ZipCode: {
			 validators: {
                                        notEmpty: {
					 message: 'a postal code is required'
                                        }
			 }
		 },
		 ClietNumber: {
			 validators: {
				 notEmpty: {
					 message: 'you need a ClietNumber'
				 },
				 regexp: {
					 regexp: /^[0-9]+$/,
					 message: 'The ClietNumber can only contain numbers'
				 }
			 }
		 },
                 txtCyti: {
			 validators: {
				 notEmpty: {
					 message: 'a Cyti is needed'
				 },
                                 regexp: {
					 regexp: /^[a-zA-Z]+$/,
					 message: 'the City may only contain letters'
				 }
			 }
		 },
                 txtState: {
			 validators: {
				 notEmpty: {
					 message: 'a State is needed'
				 },
                                 regexp: {
					 regexp: /^[a-zA-Z]+$/,
					 message: 'the State may only contain letters'
				 }
			 }
		 }
 
	 }
        });
            //muestra el formulario de registrar cliente.
            $("#BtnBuscar").on("click", function () {
                $('#PDatosCliente').show("slow");
           });
           $("#BtnCCliente").on("click", function () {
               $('#PDatosCliente').hide("slow");
           });
        });
        function buscarEstados() {
            if (!$('#country').val() == '') {
               console.log("peticion ajaz para encontrar las ciudades de ese pais");
            }
        }
        function verificarD() {
            if ($('#State').val() == 'otro') {
                $('#Ciudad1').hide();
                $('#divS').show();
                $('#divC').show();
            }else{
                console.log("peticion ajaz para encontrar las ciudades de ese estado");
                $('#divS').hide();
                $('#divC').hide();
                 $('#Ciudad1').show();
            }
        }
        function verificarC() {
            if ($('#Ciudad').val() == 'otro') {
                $('#divC').show();
            }else{
                 console.log("peticion ajaz para encontrar la zona horaria de esa ciudad");
                $('#divC').hide();
            }
        }
    </script>
@stop
