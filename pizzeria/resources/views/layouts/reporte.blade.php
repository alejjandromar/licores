@extends('layouts.master')

@section('title', $titulo)
@section('bread', 'Reportes / '.$titulo)

@section('head_css')
@parent
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
     {!! HTML::style('Recursos/css/bootstrap-datepicker.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent

                                    <!--panel de resultados-->
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>{{$titulo}}</h3>
                                                    <div class="vd_panel-menu">
                                                        <button id="BtnEmpleado" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                                                    </div>
                                                </div>
                                                <div class="container">                           </div>
                                                <div class="panel-body table-responsive">
                <div class="row">
                    <div class="col-lg-2" style="width:auto;">
                        <button id="filtro" class="btn vd_btn vd_bg-red btn-xs" onclick="cambiarFiltro()" type="button">A</button>
                         <spam id="titulo">Todas las fechas</spam>
                    </div>
                    <div id="Dmes" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        <select id="mes" class="form-control input-sm" onchange="filtrarPorMesAno()">
                            <option value="01">Enero</option><option value="02">Febrero</option>
                            <option value="03">Marzo</option><option value="04">Abril</option>
                            <option value="05">Mayo</option><option value="06">Junio</option>
                            <option value="07">Julio</option><option value="08">Agosto</option>
                            <option value="09">Septiembre</option><option value="10">Octubre</option>
                            <option value="11">Noviembre</option><option value="12">Diciembre</option>
                        </select>
                    </div>
                    <div id="Daño" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        <select id="año" class="form-control input-sm" onchange="filtrar()">
                               @for ($i = 2015; $i <2031; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                        </select>
                    </div>
                    <div id="DCalendario1" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                            <div class="">
                                <div class='input-group date' id='calendario1'>
                                    <input type='text' class="form-control" readonly="" id="fecha1" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                    <div id="DH" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        Hasta:
                    </div>
                    <div id="DCalendario2" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                        <div class="">
                                <div class='input-group date' id='calendario2'>
                                    <input type='text' class="form-control" readonly="" id="fecha2" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
                <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                @forelse ($columnas as $columna) 
                                                                    <th class="celda center"> {{$columna}}</th>                    
                                                                 @empty
                    
                                                                 @endforelse
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Panel Widget -->
                                        </div>
                                        <!-- col-md-12 -->
                                       
                                    </div>

                                    <!-- contenido -->



</div>


                        </div>

                    </div>

                </div>
                



            
    
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
{!! HTML::script('Recursos/js/bootstrap-datepicker.js') !!} 
{!! HTML::script('Recursos/locales/bootstrap-datepicker.es.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        var num =0;
         var filtro =1;
        var rango =false;
        var meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        function formatear(tabla,cadena){
             $('#'+tabla).DataTable( {
                        language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": cadena,
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

               });
        }
        function filtrarPorAno(){
            var url = '{{$ruta}}/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:"year",
                          vista: "mes",
                          year:$("#año").val()},
                    type: "get",
                    success: function (data) {
                        $('#data-tables').DataTable().destroy();
                        $("#data-tables tbody").empty();
                        actuallizarTabla(data);
                        formatear('data-tables','No se encontraron resultados para la busqueda');
                    },error: function () {
                        BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
                    }
                 });
        }
        function filtrarPorMesAno(){
            var url = '{{$ruta}}/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{
                        tipo:"mes",
                          vista: "dia",
                        year:$("#año").val(),
                        mes:$("#mes").val()},
                    type: "get",
            success: function (data) {
                $('#data-tables').DataTable().destroy();
                $("#data-tables tbody").empty();
                actuallizarTabla(data);
                formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorDiaMesAno(){
            var url = '{{$ruta}}/all'; 
            var F=$("#fecha1").val();
            var F1=F.substring(6, 10)+"-"+F.substring(3, 5)+"-"+F.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:"fecha",
                          vista: "",
                         fechai:F1},
                    type: "get",
            success: function (data) {
                $('#data-tables').DataTable().destroy();
                $("#data-tables tbody").empty();
                actuallizarTabla(data);
                formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorRango(){
            var url = '{{$ruta}}/all';  
            var F=$("#fecha1").val();
            var F1=F.substring(6, 10)+"-"+F.substring(3, 5)+"-"+F.substring(0, 2);
            var F2=$("#fecha2").val();
            var F3=F2.substring(6, 10)+"-"+F2.substring(3, 5)+"-"+F2.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:"intervalo",
                          vista:"dia",
                          fechai:F1,
                          fechaf:F3},
                    type: "get",
            success: function (data) {
              $('#data-tables').DataTable().destroy();
                $("#data-tables tbody").empty();
                actuallizarTabla(data);
                formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrar(){
            if(filtro==2){
                filtrarPorAno();
            }
            if(filtro==3){
                filtrarPorMesAno();
            }
            if(filtro==4){
                filtrarPorDiaMesAno();
            }
            if(rango){
                filtrarPorRango();
            }
        }
        function actuallizarTabla(data){
           for (var i = 0; i < data.datos.length; i++) {
                var row='<tr class="odd gradeX" id="flotante1">';
                var Rdata = data.datos[i];
                for (var j = 0; j < data.columnas.length; j++) {
                    var col = data.columnas[j]
                    row+='  <td class="celda center">'+Rdata[col]+'</td>';
                }
                row+='   </tr>';
                $("#data-tables tbody").append(row);
                row="";
            }
        }
        function load_table(){
            var url = '{{$ruta}}/all';    
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:1},
                    type: "get",
            success: function (data) {
               $('#data-tables').show();
               if(data["impuestos"]!=null && data["base"]!=0){
                    $('#data-tables').DataTable().destroy();
                    $("#data-tables tbody").empty();
                    $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                //formateo la tabla
                formatear('data-tables','Ningún dato disponible en esta tabla');
          },error: function () {
                 BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de las ventas');
           }
           });
        }
        
        function load_table(){
           
            var url = '{{$ruta}}/all';    
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{
                        mes:11,
                        year:2015,
                        mes2:1,
                        year2:2016,
                    },
                    type: "get",
                    success: function (data) {
                        console.log(data);
                             $("#data-tables").DataTable().destroy();
                             $("#data-tables tbody").empty();
                       var row="";
                        for (var i = 0; i < data.datos.length; i++) {
                            row+='<tr class="odd gradeX" id="flotante1">';
                            var Rdata = data.datos[i];
                            for (var j = 0; j < data.columnas.length; j++) {
                                var col = data.columnas[j]
                                row+='  <td class="celda center">'+Rdata[col]+'</td>';
                            }
                            console.log(row);
                            row+='   </tr>';
                             $("#data-tables tbody").append(row);
                             row="";
                            
                         }
                            
                        $('#data-tables').DataTable( {
                                 
                   language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "Ningún dato disponible en esta tabla",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

               }

               );
                     
                    
                    },

                    error: function () {
                         BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los impuestos');
                    }
                }
            );
        }
//funcion que cambia los filtros de fechas de la consulta.
        function cambiarFiltro(){
            filtro++;
            if(filtro==2){
                $("#titulo").html('Año:');
                $("#filtro").html('365');
                $(".col-lg-1").hide();
                $("#Daño").show();
                var fecha = new Date();
                $('#año').val(fecha.getFullYear());
                //Se llama a la funcion ajaz que actualiza las ventas por el año.
                filtrarPorAno();
                rango=false;
            }else{
                if(filtro==3){
                    var fecha = new Date();
                    $('#año').val(fecha.getFullYear());
                    $("#titulo").html('Mes:');
                    $("#filtro").html('31');
                    $(".col-lg-1").hide();
                    $("#Dmes").show();
                    $("#Daño").show();
                    filtrarPorMesAno();
                    rango=false;
                }else{
                    var fecha = new Date();
                    var F="";
                    if(fecha.getDate()<10){
                        F+="0";
                    }
                    F+=fecha.getDate();
                    if((fecha.getMonth()+1)<10){
                        F+="-0";
                    }
                    F+=(fecha.getMonth()+1)+ "-" + fecha.getFullYear();
                    $("#fecha1").val(F);
                    if(filtro==4){
                        $("#titulo").html('Dia:');
                        $("#filtro").html('1');
                        $(".col-lg-1").hide();
                        $("#DCalendario1").show();
                        filtrarPorDiaMesAno();
                        rango=false;
                    }else{
                        if(filtro==5){
                            var fecha = new Date();
                            var F="";
                            if(fecha.getDate()<10){
                                F+="0";
                            }
                            F+=fecha.getDate();
                            if((fecha.getMonth()+1)<10){
                                F+="-0";
                            }
                            F+=(fecha.getMonth()+1)+ "-" + fecha.getFullYear();
                            $("#fecha1").val(F);
                            $("#fecha2").val(F);
                            $("#titulo").html('Desde:');
                            $("#filtro").html('< >');
                            $(".col-lg-1").hide();
                            $("#DCalendario1").show();
                            $("#DCalendario2").show();
                            $("#DH").show();
                            filtrarPorRango();
                            filtro=0;
                            rango=true;
                        }else{
                            if(filtro==1){
                                $("#titulo").html('Todas las fechas.');
                                $("#filtro").html('A');
                                $(".col-lg-1").hide();
                                load_table();
                                rango=false;
                            }
                        }
                    }
                }
            }
        }
        $(document).ready(function () {
            $('#calendario1').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
            $('#calendario2').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
            load_table();
        });
        
        
       
    </script>
@stop
