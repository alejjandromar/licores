@extends('layouts.master')
@section('title', 'Modificadores y Extras')
@section('bread','Cargos/ Modificadores y extras')
@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('css/jquery.dataTables.min.css') !!}
     {!! HTML::style('css/dataTables.bootstrap.css') !!}
      {!! HTML::style('bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent
   <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">  
    <!-- contenedor para registrar propinas-->
    <div id="PAgregarModificador">
        <!-- contenedor para registrar una nueva propina-->
        <div class="panel widget light-widget panel-bd-top " id="PRegistrarModificador" style="display: none">
                <div class="panel-body">
                    <div class="col-lg-12">
                    <div class="panel-heading vd_bg-default">
                        <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos del nuevo modificador o extra.</h3>
                        <div class="vd_panel-menu" style="    padding-right: 13px">
                            <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                        </div>
                    </div>
                    <div class="panel-body" style="border: 1px solid #ddd">
                        {!!Form::open(['method'=>'POST','files'=>true, 'iden'=>'-1', 'id' => 'registrarP'])!!}  
                            <div class="row" style="margin-bottom: 0px">
                                 <div class="col-lg-4 form-group">
                                    <div class="form-label">Nombre<span class="vd_red">*</span></div>
                                    <input type="text" id="NombreR" class="mgbt-xs-20 mgbt-sm-0 form-control encontrar" name="NombreR" maxlength="100">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <div class="form-label">Tipo<span class="vd_red">*</span></div>
                                    <select id="TipoR" name="TipoR" class="mgbt-xs-20 mgbt-sm-0" onchange="cambiarPrecio('TipoR','PrecioR',null)">
                                        <option value="modificador">modificador</option>
                                        <option value="extra">extra</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <div  class="vd_checkbox checkbox-danger form-group" style="margin: 0px;padding-left: 20px;">
                                       <div class="form-label" style="padding-bottom: 8px;">Compuesto<span class="vd_red">*</span></div>
                                       <input type="checkbox" onclick="auxiliar(false,1,'checkbox1','checkbox2','errorCk','agregarOpc','registrarP','opciones')" class="form-control" value="0" id="checkbox1" name="checkbox1">
                                        <label for="checkbox1" class="control-label">Indica que no posee opciones</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row" style="margin-bottom: 0px">
                                <div class="col-lg-4 form-group">
                                   <div class="form-label">Precio<span class="vd_red">*</span></div>
                                   <input type="text" id="PrecioR" class="mgbt-xs-20 mgbt-sm-0 form-control" name="PrecioR" maxlength="4">
                               </div>
                               <div class="col-lg-4 form-group">
                                   <div class="form-label">Imagen</div>
                                    {!! Form::file('file', ['id' =>'imagenR','name'=>'imagenR', 'class' => 'mgbt-xs-20 mgbt-sm-0', 'data-show-upload'=>'false', 'data-initial-caption'=>"", 'data-overwrite-initial'=>"false"])!!}                          
                               </div>
                                <div class="col-lg-4 form-group">
                                    <div  class="vd_checkbox checkbox-danger form-group" style="margin: 0px; padding-top: 30px;padding-left: 20px;">
                                       <input type="checkbox"  class="form-control" onclick="auxiliar(false,2,'checkbox1','checkbox2','errorCk','agregarOpc','registrarP','opciones')" value="1" id="checkbox2" name="checkbox2">
                                        <label for="checkbox2" class="control-label">Indica que si posee opciones</label>
                                        <button id="agregarOpc" class="btn vd_btn vd_bg-red btn-xs" style="margin-left: 5px;display:none" onclick="AgregarOpc('checkbox1','checkbox2','agregarOpc','opciones','registrarP','errorCk')" type="button" data-original-title="Agregar Opcion" data-placement="top" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                                     </div>
                                    <small class="help-block"  id="errorCk" style="display: none;color: #a94442; padding-left: 20px;">Indique si es compuesto o no.</small>
                                </div>
                            </div>
                             <div id="opciones" class="row" style="margin-bottom: 0px" P="0" hijos="0"></div>
                            <div class="mgtp-20">
                                 <div class="row mgbt-xs-0">
                                     <div class="col-xs-6"></div>
                                     <div class="col-xs-6 text-right">
                                         <button  type="reset" onclick="cancelarReg('registrarP','opciones','errorCk','checkbox1','checkbox2','agregarOpc')" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                             Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                         </button>

                                         <button  type="button" class="btn vd_btn vd_bg-green" onclick="validarFormR('checkbox1','checkbox2','errorCk','agregarOpc','registrarP','opciones')">
                                             <!--top-right-success-->
                                             Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                         </button>

                                     </div>
                                 </div>
                             </div>
                       {!!Form::close()!!}
                    </div>
                    </div>
                </div>
        </div> <!-- contenedor para registrar una propina-->
    </div> 
    <!-- contenedor que muestra la tabla con todos las propinas.-->
    <div>
         <div class="row" id="Ptabla">
            <div class="col-md-12">
                 <div class="panel widget">
                     <div class="panel-heading vd_bg-grey">
                            <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Modificadores y extras.</h3>
                    <div class="vd_panel-menu">
                        <button id="BtnAPropina" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar Modificador o extra" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                       </div>
                     </div>
                    <div class="panel-body table-responsive" style="padding: 15px;">
                        <table class="table table-striped" id="data-tables" style="">
                            <thead>
                                <tr role="row">
                                    <th class="celda center">Nombre</th>
                                    <th class="celda center">Tipo</th>
                                    <th class="celda center">Precio</th>
                                    <th class="celda center">Compuesto</th>
                                    <th class="celda center">Estado</th>
                                    <th class=""></th>
                                    <th style="display:none;"></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Panel Widget -->
            </div>
            <!-- col-md-12 -->
        </div>
        
    </div>
     <!-- contenedor que muestra la tabla con todos las propinas.-->
</div>
 <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body" style="padding-bottom: 0px;">
                           <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                           <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Nombre:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="nombreMod"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Tipo:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="tipoMod"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Imagen:</label> 
                                   </div>
                                   <div class="col-xs-6" style="    padding-bottom: 10px;padding-left: 30px;"> 
                                       <img  WIDTH=50 HEIGHT=50 id='img' > 
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Fecha de Creaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaC"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">&Uacute;ltima modificaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaM"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Opciones del <span id="tipoOpc"></span></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <table class="table table-striped table-hover" id="tablaOpciones" style="display:none">
                                           <thead>
                                            <tr role="row">
                                                <th class="" style="text-align: left;padding: 0px;">Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody style="max-height: 140px;overflow: auto">
                                        </tbody>
                                       </table>
                                       <label id="mensajeOpc" class="col-xs-12 control-label"></label>
                                   </div>
                               </div>
                           </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
@endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('js/jquery.dataTables.min.js') !!}
{!! HTML::script('js/dataTables.bootstrap.js') !!}
{!! HTML::script('js/bootstrapValidator.min.js') !!} 
{!! HTML::script('bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script>
        var p=0;
        var hijos=0;
        var validado=false;
        var Opciones=[];
        var datos;
        //para formatear las tablas
        function formato(id) {
                $(id).dataTable(
                   { 
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "No hay modificadores registradas en el sistema",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }
                   }
               }

                   );
            }
        
        //funcion para agregar una opcion a un modificador
        function AgregarOpc(idCk1,idCk2,idBoton,idDiv,idForm,idError) {
            var hijos=parseInt($('#'+idDiv).attr('hijos'));
            var p=parseInt($('#'+idDiv).attr('P'));
            p++;
            hijos++;
            var fila;
            var ancho=$($('#'+idForm).children('div').children('div').children('input.encontrar')).width()+12;
             fila = '<div  class="col-lg-4" id="fila'+p+'">';
             fila+=' <div class="form-group" >';   //
             fila+='<div class="form-label"><span class="inventada" id="nombreOpc-'+p+'">Opcion '+p+'</span><span class="vd_red">*</span>';
             fila+='<button class="btn vd_btn vd_bg-red btn-xs" onclick="EliminarNA('+p+','+"'"+idCk1+"'"+','+"'"+idCk2+"'"+','+"'"+idBoton+"'"+','+"'"+idDiv+"'"+','+"'"+idForm+"'"+','+"'"+idError+"'"+')"  type="button" data-original-title="Eliminar Opcion"';
             fila+='data-placement="top" data-toggle="tooltip" style="position: relative;top: 31px;float: right; margin-right: 5px;"><i class="glyphicon glyphicon-remove"></i> </button>';
             fila+=' </div>';
             fila+=' <input type="text" id="OpcionNueva'+p+'" class="mgbt-xs-20 mgbt-sm-0 form-control" name="Opcion" maxlength="100" style="width:'+ancho+'px;padding-right: 40px;">';
             fila+='</div>';
             fila+=' </div>';
             $('#'+idDiv).append(fila);
             fila = '';
             $('[data-toggle="tooltip"]').tooltip();
             $('#'+idDiv).attr('hijos',hijos); 
             $('#'+idDiv).attr('P',p); 
        }
        
        //elimina una opcion a un modificador de la interfax de usuario.
        function EliminarNA(id,idCk1,idCk2,idBoton,idDiv,idForm,idError) {
         var hijos=parseInt($('#'+idDiv).attr('hijos'));
         var p=parseInt($('#'+idDiv).attr('P'));
          if(hijos<=2){
              BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Un modificador compuesto al menos debe tener dos opciones',
                               type: BootstrapDialog.TYPE_SUCCESS,
                                
                           });
                           return;
          }
          if($('#'+idForm).data('bootstrapValidator')!=null){
              $('#'+idForm).data('bootstrapValidator').destroy();
          }
          $('#fila'+id).remove();
          p--;
          hijos--;
          cambiarNumero(idCk1,idCk2,idBoton,idDiv,idForm,idError);
          if(hijos==0){
                $("#"+idCk2).prop('checked',false);
                $("#"+idCk1).prop('checked',true);
                auxiliar2(idCk1,idCk2,idBoton,idDiv,idForm,idError);
                p=0;
            }
             $('#'+idDiv).attr('hijos',hijos); 
             $('#'+idDiv).attr('P',p); 
            if(validado){
                validarFormR(idCk1,idCk2,idError,idBoton,idForm,idDiv);
            }
         }
        
        //funcoon que valida los checkBox de las opciones
         function auxiliar(X,Y,idCk1,idCk2,idError,idBoton,idForm,idDiv){
             if(Y!=null){
                 if(Y==1){
                     $("#"+idCk1).prop('checked',true);
                     $("#"+idCk2).prop('checked',false);
                 }else{
                     $("#"+idCk2).prop('checked',true);
                     $("#"+idCk1).prop('checked',false);
                 }
             }
             auxiliar2(idCk1,idCk2,idBoton,idDiv,idForm,idError);
             //verifico si alguno de los chebox es valido para quitar los mensajes de error.
             if($("#"+idCk1).prop('checked')||$("#"+idCk2).prop('checked')){
                 $("#"+idError).hide();
                 $("#"+idCk1).parent().removeClass('has-error');
                 $("#"+idCk2).parent().removeClass('has-error');
                 if(X){
                     var i=0; var opciones=[];var Ids=[];
                     
                     var formData = new FormData(document.getElementById(idForm));
                     var X=$("#"+idForm).attr('iden');
                      $("#"+idDiv+" input").each(function(){
                          i++;
                          var nuevo=this.id.indexOf("OpcionNueva");
                          opciones.push(this.value);
                          if(X!=-1){
                              if(nuevo==-1){
                                  Ids.push(this.id.substring((this.id.length-1), (this.id.length)));
                              }else{
                                  Ids.push(-1);
                              }
                        }
                      });
                      formData.append('Ids',Ids);
                      formData.append('Opciones',opciones);
                      formData.append('NumeroOpciones',i);
                      if(X!=-1){
                            formData.append('id',X);
                            save_method(X,formData);
                      }else{
                            save_method(0,formData);
                      }
                 }
             }else{
                 $("#"+idCk1).parent().addClass('has-error');
                 $("#"+idCk2).parent().addClass('has-error');
                  $("#"+idError).show();
                   $("#"+idBoton).hide();
             }
         }
         
         //funcion que borra las opciones del modificador
         function limpiaOpciones(idForm,idDiv){
            var hijos=parseInt($('#'+idDiv).attr('hijos'));
            var p=parseInt($('#'+idDiv).attr('P'));
             if($('#'+idForm).data('bootstrapValidator')!=null){
                $('#'+idForm).data('bootstrapValidator').destroy();
            }
             $("#"+idDiv).empty();
             hijos=0;
             p=0;
             $('#'+idDiv).attr('hijos',hijos); 
             $('#'+idDiv).attr('P',p); 
         }
         
         //funcion para saber que checkbox esta chequeado y asi habilitar las opciones pertinentes
         function auxiliar2(idCk1,idCk2,idBoton,idDiv,idForm,idError){
            var hijos=parseInt($('#'+idDiv).attr('hijos'));
            var p=parseInt($('#'+idDiv).attr('P'));
             if($("#"+idCk1).prop('checked')){
                 $("#"+idBoton).hide();
                 limpiaOpciones(idForm,idDiv);
             }
             if($("#"+idCk2).prop('checked')){
                $("#"+idBoton).show();
                if(hijos==0){
                    AgregarOpc(idCk1,idCk2,idBoton,idDiv,idForm,idError);
                    AgregarOpc(idCk1,idCk2,idBoton,idDiv,idForm,idError);
                }
             }
         }
         
         //funcion para actualizar los ids de las variables
         function cambiarNumero(idCk1,idCk2,idBoton,idDiv,idForm,idError){
             var i=0;
             $("#"+idDiv+" div.col-lg-4").each(function(){
                
                 i++;
                 this.id='fila'+i;
                 $("#"+this.id+"  div.form-label span.inventada").attr("id","nombreOpc-"+i);
                 $("#"+this.id+"  div.form-label span.inventada").html("Opcion "+i);
                 $("#"+this.id+" div.form-group input").attr("id",'Opcion'+i);
                 $("#"+this.id+" div.form-group button").attr("onclick",'EliminarNA('+i+','+"'"+idCk1+"'"+','+"'"+idCk2+"'"+','+"'"+idBoton+"'"+','+"'"+idDiv+"'"+','+"'"+idForm+"'"+','+"'"+idError+"'"+')');
             });
         }
         
         //funcion que cambia el valor del precio dependiendo de si es extra o modificador
         function cambiarPrecio(idSelec,IdPr,precioViejo){
             if($('#'+idSelec).val()=='modificador'){
                  $('#'+IdPr).val(0.00);
                  $('#'+IdPr).attr("readonly",true);
              }else{
                  if(precioViejo!=null){
                        $('#'+IdPr).val(precioViejo);
                  }else{
                        $('#'+IdPr).val('');
                  }
                  
                  $('#'+IdPr).attr("readonly",false);
              }
         }
        
        //para validar los datos del formulario de registro.
        function validarFormR(idCk1,idCk2,idError,idBoton,idForm,idDiv){
            validado=true;
            auxiliar(false,null,idCk1,idCk2,idError,idBoton,idForm,idDiv);
            if($('#'+idForm).data('bootstrapValidator')!=null){
                $('#'+idForm).data('bootstrapValidator').destroy();
            }
             $('#'+idForm).bootstrapValidator({
                fields: {
                        NombreR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un nombre'
                                         },
                                         regexp: {
                                                 regexp: /^[a-zA-Z0-9áéíóú�?É�?ÓÚñÑ\s]+$/,
                                                 message: 'Solo se permiten caracteres alfanumericos.'
                                         }
                                 }
                         },
                         TipoR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un tipo.'
                                         }
                                         
                                 }
                         },
                         imagenR: {
                                 validators: {
                                         file: {
                                                extension: 'jpeg,jpg,png',
                                                type: 'image/jpeg,image/png',
                                                message: 'Por favor seleccione una imagen valida'
                                         }
                                 }
                         },
                         PrecioR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un valor.'
                                         },
                                         regexp: {
                                                 regexp: /^[0-9.]+$/,
                                                 message: 'Solo se permiten caracteres numericos y el punto.'
                                         }
                                 }
                         },
                         Opcion: {
                                 validators: {
                                        notEmpty: {
                                                 message: 'Se necesita una opcion.'
                                         },
                                         regexp: {
                                                 regexp: /^[a-zA-Z0-9áéíóú�?É�?ÓÚñÑ\s]+$/,
                                                 message: 'Solo se permiten caracteres alfanumericos.'
                                         }
                                 }
                         },
                     }
            });
             if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
                 //si el formulario esta bien validado tenbgo queverificar los checkbox.
                 auxiliar(true,null,idCk1,idCk2,idError,idBoton,idForm,idDiv);
             }else{
                 //no es valido el forlario
             }
        }
        
        //para cancelar el registro de un descuento
         function cancelarReg(idFom,idDiv,idError,idCk1,idCk2,idBoton){
              validado=false;
              document.getElementById(idFom).reset();
              limpiaOpciones(idFom,idDiv);
              $("#"+idError).hide();
             $("#"+idCk1).parent().removeClass('has-error');
             $("#"+idCk2).parent().removeClass('has-error');
             $("#"+idBoton).hide();
              $('#PRegistrarModificador').toggle("slow");
              $('#BtnAPropina').toggle();
         }
         
        //para mostrar el formulario de registro de un modificador
         $('#BtnAPropina').click(function () {
             $("#checkbox1").prop('checked',true);
             $('#PRegistrarModificador').toggle("slow");
             $('#BtnAPropina').toggle("slow");
              if($('#TipoR').val()=='modificador'){
                  $('#PrecioR').val(0.00);
                  $('#PrecioR').attr("readonly",true);
              }
         });
         
         //funcion para mostrar el formulario de edicion de un modificador
        function mostrardiv(pCodigo,i,ck1,ck2,error,boton,form,div) {
             actualizarCampos(i,ck1,ck2,error,boton,form,div);
            var nombreDiv = 'flotante2' + pCodigo.toString();
            var nombrediv1 = '#flotante1' + pCodigo.toString();

           
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }
        
        //para ocultar los formularios de edicion
        function ocultardiv(codigo,i,a,b,c,d,idForm,f) {
            var pnombre = 'flotante2' + codigo.toString();
            var nombrediv1 = '#flotante1' + codigo.toString();
            div = document.getElementById(pnombre);
            div.style.display = "none";
            $(nombrediv1).children('.celda').removeClass('noprint');
            if($('#'+idForm).data('bootstrapValidator')!=null){
                $('#'+idForm).data('bootstrapValidator').destroy();
            }
        }
        
        //muestra los detalles de un modificador en un modal
        function mostrar(imagen,indice,tipo,nombre,fechaC,fechaM) {
            
            $('#myModal').modal('show'); 
            document.getElementById("Mtitulo").innerHTML='Detalles del modificador:';
            document.getElementById("nombreMod").innerHTML=nombre;
            document.getElementById("tipoMod").innerHTML=tipo;
            document.getElementById("tipoOpc").innerHTML=tipo+':';
            document.getElementById("fechaC").innerHTML=fechaC;
            document.getElementById("fechaM").innerHTML=fechaM;
            document.getElementById('img').src = 'imagenes/modificadores/'+imagen;
            if(Opciones[indice].length>0){
                var row;
                $("#tablaOpciones tbody").empty();
                for (var i = 0; i < Opciones[indice].length; i++) {
                     row+='<tr role="row" class="odd gradeX">';
                     row+='  <td class="celda center">'+Opciones[indice][i].opcion+'</td>';
                     row+='   </tr>';
                    $("#tablaOpciones tbody ").append(row);
                     row="";
                }
                
                $('#tablaOpciones').show();
                  $('#mensajeOpc').hide();
            }else{
                $('#tablaOpciones').hide();
                 document.getElementById("mensajeOpc").innerHTML='No posee opciones disponibles';
                $('#mensajeOpc').show();
            }
        }
         
         //funcion para inicializar el imput de tipo file
         function inicializarImagen(input){
            $("#"+input).fileinput({
                 showUpload:false, 
                 showPreview:false, 
                previewFileType:false,
                previewClass: "bg-warning"
            });  
         }
         
        $(document).ready(function () {
            //se inicializa el plugin de imput de imagen
             inicializarImagen("imagenR");
             cargarDatosTabla();
             formato('#data-tables');
         });
         
        
        //para cargar todas las propinas
        function cargarDatosTabla(){
            var url = 'modificador/all';
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    async: true,
                    type: "get",
                    
                    success: function (data) {
                       var row="";
                       if(data["modificadores"]!=null){
                       $('#data-tables').DataTable().destroy();
                       $("#data-tables tbody").empty();
                        for (var i = 0; i < data["modificadores"].length; i++) {
                            row+='<tr role="row" class="odd gradeX" id="flotante1'+data["modificadores"][i].id+'">';
                            row+='  <td class="celda center">'+data["modificadores"][i].nombre+'</td>';
                            row+='  <td class="celda center">'+data["modificadores"][i].tipo+'</td>';
                            var precio=0.0;if(data["modificadores"][i].precio!=null){precio=data["modificadores"][i].precio;}
                            var compuesto='Si';if(data["modificadores"][i].compuesto==0){compuesto='No';}
                             var Id1='checkbox1'+data["modificadores"][i].id;var Id2='checkbox2'+data["modificadores"][i].id; var error="errorCk"+data["modificadores"][i].id;
                            var boton='agregarOpc'+data["modificadores"][i].id; var form='registrarP'+data["modificadores"][i].id; var idDiv='opciones'+data["modificadores"][i].id;
                            row+='  <td class="celda center">'+precio.toFixed(2)+' {{$restaurante->simbolo}}</td>';
                             row+='  <td class="celda center">'+compuesto+'</td>';
                             row+=' <td id="estadoD'+data["modificadores"][i].id+'" class="celda center">';
                             if(data["modificadores"][i].estado==1){
                                 row+='<span class="label vd_bg-green">Activo</span>';
                             }else{
                                  row+='<span class="label vd_bg-red">Inactivo</span>';
                             }
                             row+='</td>';
                            row+='  <td class="menu-action celda center" style="width: 141px;">';
                            Opciones.push(data["modificadores"][i].opciones);
                            row+='<a id="btnver" onclick="mostrar('+"'"+data["modificadores"][i].imagen.replace("imagenes/modificadores/", "")+"'"+','+i+','+"'"+data["modificadores"][i].tipo+"'"+','+"'"+data["modificadores"][i].nombre+"'"+','+"'"+data["modificadores"][i].created_at+"'"+','+"'"+data["modificadores"][i].updated_at+"'"+')" class="btn menu-icon vd_bd-green vd_green"><i data-original-title="Ver Detalles" data-toggle="tooltip" data-placement="top" class="fa fa-eye"></i></a>';
                            row+='      <a data-original-title="Editar" onclick="mostrardiv('+data["modificadores"][i].id+','+i+",'"+Id1+"'"+",'"+Id2+"'"+",'"+error+"'"+",'"+boton+"'"+",'"+form+"'"+",'"+idDiv+"'"+')" id="btnEditar'+data["modificadores"][i].id+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';
                            
                            if(data["modificadores"][i].estado==1){
                            row+='<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+data["modificadores"][i].id+')"  id="btnDesact'+data["modificadores"][i].id+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                            }else{
                            row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 1, '+data["modificadores"][i].id+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                            }
                            row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion(this, '+data["modificadores"][i].id+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                            row+='  </td>';
                             row+='  <td colspan="12" id="flotante2'+data["modificadores"][i].id+'" style="display:none; padding:0;">';
                            row+='<div class="panel-body" style="border: 1px solid #ddd">\n\
                            <form class="" method="post" role="form" iden="'+data["modificadores"][i].id+'" id="registrarP'+data["modificadores"][i].id+'">'+  
'                            <div class="row">'+
'                               <input type="hidden" name="_token" value="{{ csrf_token() }}">'+
 '                                <div class="col-lg-4 form-group">'+
  '                                  <div class="form-label">Nombre<span class="vd_red">*</span></div>'+
   '                                 <input type="text" id="NombreR'+data["modificadores"][i].id+'" value="' +data["modificadores"][i].nombre+ '" class="mgbt-xs-20 mgbt-sm-0 form-control encontrar" name="NombreR" maxlength="100" style="width: 100%;">'+
    '                            </div>'+
     '                           <div class="col-lg-4 form-group">'+
      '                              <div class="form-label">Tipo<span class="vd_red">*</span></div>'+
       '                             <select id="TipoR'+data["modificadores"][i].id+'" name="TipoR" class="mgbt-xs-20 mgbt-sm-0" onchange="cambiarPrecio('+"'TipoR"+data["modificadores"][i].id+"'"+",'PrecioR"+data["modificadores"][i].id+"',"+precio+')">'+
        '                                <option value="modificador"';if(data["modificadores"][i].tipo=='modificador'){row+='selected';}
                                            row+='>Modificador</option>'+
                                            '<option value="extra"';if(data["modificadores"][i].tipo=='extra'){row+='selected';}
                                            row+='>Extra</option>'+
                                        '</select>'+
           '                     </div>'+
            '                    <div class="col-lg-4">';
    row+=         '                       <div  class="vd_checkbox checkbox-danger form-group" style="margin: 0px;padding-left: 20px;">'+
              '                         <div class="form-label" style="padding-bottom: 8px;">Compuesto<span class="vd_red">*</span></div>'+
               '                        <input type="checkbox" onclick="auxiliar(false,1,'+"'"+Id1+"'"+",'"+Id2+"'"+",'"+error+"'"+",'"+boton+"'"+",'"+form+"'"+",'"+idDiv+"'"+')" class="form-control" value="0" id="checkbox1'+data["modificadores"][i].id+'" name="checkbox1">'+
                '                        <label for="checkbox1'+data["modificadores"][i].id+'" class="control-label">Indica que no posee opciones</label>'+
                 '                   </div>'+
                  '              </div> '+
                   '         </div>'+
                    '        <div class="row" >'+
                     '           <div class="col-lg-4 form-group">'+
                      '             <div class="form-label">Precio<span class="vd_red">*</span></div>';
                                     var id="imagenR"+data["modificadores"][i].id; var lectura='';if(data["modificadores"][i].tipo=="modificador"){lectura='readonly';}                                   
  row+='                                  <input type="text" id="PrecioR'+data["modificadores"][i].id+'" value="' +precio+'"  class="mgbt-xs-20 mgbt-sm-0 form-control" name="PrecioR" '+lectura+' maxlength="4" style="width: 100%;">'+
  '                             </div>'+
   '                            <div class="col-lg-4 form-group">'+
    '                               <div class="form-label">Imagen</div>'+
     '                             <input id="'+id+'" name="imagenR" type="file"class="mgbt-xs-20 mgbt-sm-0" style="width:100%">     '+
       '                        </div>'+
        '                       <div class="col-lg-4 form-group">';
          row+='                          <div  class="vd_checkbox checkbox-danger form-group" style="margin: 0px; padding-top: 30px;padding-left: 20px;">'+
'                                       <input type="checkbox"  class="form-control" onclick="auxiliar(false,2,'+"'"+Id1+"'"+",'"+Id2+"'"+",'"+error+"'"+",'"+boton+"'"+",'"+form+"'"+",'"+idDiv+"'"+')" value="1" id="checkbox2'+data["modificadores"][i].id+'" name="checkbox2">'+
 '                                       <label for="checkbox2'+data["modificadores"][i].id+'" class="control-label">Indica que si posee opciones</label>'+
  '                                      <button id="agregarOpc'+data["modificadores"][i].id+'" class="btn vd_btn vd_bg-red btn-xs" style="margin-left: 5px;display:none" onclick="AgregarOpc('+"'"+Id1+"'"+",'"+Id2+"'"+",'"+boton+"'"+",'"+idDiv+"'"+",'"+form+"'"+",'"+error+"'"+')" type="button" data-original-title="Agregar Opcion" data-placement="top" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>'+
   '                                  </div>'+
    '                                <small class="help-block"  id="errorCk'+data["modificadores"][i].id+'" style="display: none;color: #a94442; padding-left: 20px;">Indique si es compuesto o no.</small>'+
     '                           </div>'+
      '                      </div>'+
       '                      <div id="opciones'+data["modificadores"][i].id+'" P="0" hijos="'+data["modificadores"][i].opciones.length+'" class="row" style="margin-bottom: 0px">';
                              
                                row+='</div>'+
        '                    <div class="mgtp-20">'+
         '                        <div class="row mgbt-xs-0">'+
          '                           <div class="col-xs-6"></div>'+
           '                          <div class="col-xs-6 text-right">'+
            '                             <button  type="reset" onclick="ocultardiv('+data["modificadores"][i].id+','+i+",'"+Id1+"'"+",'"+Id2+"'"+",'"+error+"'"+",'"+boton+"'"+",'"+form+"'"+",'"+idDiv+"'"+')" class="btn vd_btn btn-default" style="color: #0E2145!important">'+
             '                                Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>'+
              '                           </button>'+
 '                                        <button  type="button" class="btn vd_btn vd_bg-green" onclick="validarFormR('+"'"+Id1+"'"+",'"+Id2+"'"+",'"+error+"'"+",'"+boton+"'"+",'"+form+"'"+",'"+idDiv+"'"+')">'+
 '                                            <!--top-right-success-->'+
  '                                           Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>'+
   '                                      </button>'+
 '                                    </div>'+
                                ' </div>';
                            row+='       </form>';
                            row+='    </td>';
                            row+='   </tr>';
                             $("#data-tables tbody").append(row);
                             row="";
                             $('[data-toggle="tooltip"]').tooltip(); 
                            
                            }
                             formato('#data-tables');
                            datos=data["modificadores"];
                            for (var i = 0; i < data["modificadores"].length; i++) {
                                        inicializarImagen("imagenR"+datos[i].id);
                                }    
                              }
                    },

                    error: function (data) {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda. '+data);
                    }
                }
            );
        }
        
        function actualizarCampos(i,ck1,ck2,error,boton,form,div){
             var idDiv='opciones'+datos[i].id; $('#'+idDiv).empty();
            $('#'+idDiv).attr('hijos',datos[i].opciones.length); 
            $('#'+idDiv).attr('P',datos[i].opciones.length); 
                                if(datos[i].compuesto!=0){
                                     $('#checkbox2'+datos[i].id).prop('checked',true);
                                     $('#agregarOpc'+datos[i].id).show();
                                         for (var j = 0; j < datos[i].opciones.length; j++) {
                                                var ancho=$('#PrecioR'+datos[i].id).width()+68;
                                                 fila = '<div  class="col-lg-4" id="fila'+j+'">';
                                                 fila+=' <div class="form-group" style="width:100%" >';
                                                 fila+='<div class="form-label"><span class="inventada" id="nombreOpc-'+datos[i].id+datos[i].opciones[j].id+'">Opcion '+(j+1)+'</span><span class="vd_red">*</span>';
                                                 fila+='<button class="btn vd_btn vd_bg-red btn-xs" onclick="EliminarNA('+j+','+"'"+ck1+"'"+','+"'"+ck2+"'"+','+"'"+boton+"'"+','+"'"+div+"'"+','+"'"+form+"'"+','+"'"+error+"'"+')"  type="button" data-original-title="Eliminar Opcion"';
                                                 fila+='data-placement="top" data-toggle="tooltip" style="position: relative;top: 31px;margin-right: 5px;float: right;"><i class="glyphicon glyphicon-remove"></i> </button>';
                                                 fila+=' </div>';
                                                 fila+=' <input type="text" id="Opcion'+datos[i].id+datos[i].opciones[j].id+'" value="' +datos[i].opciones[j].opcion+ '" style="width:100%" class="mgbt-xs-20 mgbt-sm-0 form-control" name="Opcion" maxlength="100" style="padding-right: 40px;">';
                                                 fila+='</div>';
                                                 fila+=' </div>';
                                                  $('#'+idDiv).append(fila);
                                                    fila = '';
                                                    $('[data-toggle="tooltip"]').tooltip(); 
                                         }
                                    }else{
                                        $('#checkbox1'+datos[i].id).prop('checked',true);
                                        $('#agregarOpc'+datos[i].id).hide();
                                    }
        }
        
        //para activar o desactivar un descuento(1 para activar, 0 parea desactivar).
        function estado(btn, val, id){
            var url = 'modificador/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           if(val==1){
                               $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+id+')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-green">Activo</span>');
                                $('[data-toggle="tooltip"]').tooltip();
                            }else{
                               $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, '+id+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-red">Inactivo</span>');
                               $('[data-toggle="tooltip"]').tooltip(); 
                           }
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
                        
            );
        }
        
        //para eliminar logicamente un registro
         function eliminar(btn,id){
            var url = 'modificador/delete';
            $.ajax({
                    url: url,
                    data:{id:id,estado:0},
                    dataType: "JSON",
                    type: "get",    
                    success: function (data) {
                         if(data.error==false){
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                               cargarDatosTabla();
                               formato('#data-tables');
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
                        
            );
        }
        
        //funcion para registrar o actualizar un descuento dependiendo del valor del id (0=registrar)
        function save_method(id,formData){
            var url = 'modificador/add';//ruta para agregar
            var data= formData;
             //si el id es diferente de 0 voy a actualizar
            if (id !=0){
                url = 'modificador/update'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {
                         if(data.error==false){
                            //si estoy editando oculto el panel de edicion. 
                            if(id!=0){
                             ocultardiv(id);
                             }else{
                                 //estoy insertando
                                 cancelarReg('registrarP','opciones','errorCk','checkbox1','checkbox2','agregarOpc');
                             }
                            cargarDatosTabla();
                             BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                    error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
            );
        } 
        
        //funcion que confirma si van a eliminar un desccuento
        function confirmacion(btn,pCodigo) {
            BootstrapDialog.confirm({
                title: 'Confirmacion',
                type: BootstrapDialog.TYPE_DANGER,
                message:  '¿Esta seguro que desea eliminar este modificador?',
                callback: function (result) {
                if (result) {
                    eliminar(btn,pCodigo);
                }
            }});
        }
    </script>
@stop