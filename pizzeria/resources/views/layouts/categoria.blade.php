@extends('layouts.master')

@section('title', 'Categoria')

@section('bread','Men&uacute;  /  Categoria')


@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
    
                                    <!--panel de Agregar-->
          
      <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">                                     
                                     
      
                                    <!--panel de para agregar los datos de la nueva categorias-->
                             {!!Form::open(['route' => 'categoria_add', 'method'=>'POST','files'=>true, 'id' => 'registrationForm'])!!}        
                                <div class="panel widget light-widget panel-bd-top" id="PDatosCategoria" style="display: none">
                                        <div class="panel-body">
                                            <div class="col-lg-12">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos de nueva Categoria.</h3>
                                                <div class="vd_panel-menu" style="    padding-right: 13px">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                <form id="registrarC">
                                                    <div class="row">
                                                         <div class="col-lg-3 form-group">
                                                            <div class="form-label">Nombre<span class="vd_red">*</span></div>
                                                            <input type="text" id="nombre" class="mgbt-xs-20 mgbt-sm-0 form-control" maxlength="255" title="Se requiere un Nombre" name="nombre" required>
                                                        </div>
                                                        <div class="col-lg-3 form-group">
                                                            <div class="form-label">Descripcion<span class="vd_red">*</span></div>
                                                            <input type="text" id="descripcion" class=" form-control" name="descripcion" maxlength="500" title="Se requiere una Descripcion" required>
                                                        </div> 
                                                        <div class="col-lg-3 form-group" id="select_parent">
                                                            <div class="form-label">Pertenece a:<span class="vd_red">*</span></div>
                                                            <select id="select_categoriaP" name="parent_category" class="form-control">
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-3 form-group">
                                                         <div class="form-label">Imagen de la Categoria<span class="vd_red">*</span></div>
                                                       {!! Form::file('file', ['id' =>'imagenC', 'class' => 'mgbt-xs-20 mgbt-sm-0', 'data-show-upload'=>'false', 'data-preview-file-type'=>'any', 'data-initial-caption'=>"", 'data-overwrite-initial'=>"false"])!!}                          
                                                        </div>
                                                     </div>
                                                    
                                                    <div class="mgtp-20">
                                                         <div class="row mgbt-xs-0">
                                                             <div class="col-xs-6"></div>
                                                             <div class="col-xs-6 text-right">
                                                                 <button  type="reset" id="BtnCCategoria" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                                     Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                                 </button>

                                                                 <button id="BtnGC" type="button" onclick="validarFormR('registrationForm',0)" value="Enviar" class="btn vd_btn vd_bg-green">
                                                                     <!--top-right-success-->
                                                                     Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                                 </button>

                                                             </div>
                                                         </div>
                                                     </div>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
            </div> {!!Form::close()!!}
                     
                                <!--panel de tabla-->           
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Categorias.</h3>
                                                     <div class="vd_panel-menu">
                                                    <button id="BtnCategoria" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar Categoria" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                                                   </div>
                                                </div>
                                                <div class="panel-body table-responsive" style="padding: 15px;">
                                                    <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre</th>
                                                                <th>Descripcion</th>
                                                                <th>Estado</th>
                                                                <th></th>
                                                                <th style="display:none;"></th>  

                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                    
                                                          
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    
   
                                
                                
                                
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body">
                           <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                            <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Nombre:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="nombreCat"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Descripcion:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="descripcionCat"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Imagen:</label> 
                                   </div>
                                   <div class="col-xs-6" style="    padding-bottom: 10px;padding-left: 30px;"> 
                                       <img  WIDTH=50 HEIGHT=50 id='img' > 
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Fecha de Creaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaC"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">&Uacute;ltima modificaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaM"></label>
                                   </div>
                                  
                               </div>
                           </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
 
        
        <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body">
                           <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                           <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="form-img text-center mgbt-xs-15"> <img  WIDTH=200 HEIGHT=160 id='img' > </div>
                                   <br>
                                   <div>
                                       <table class="table table-striped table-hover">
                                           <tbody>
                                               <tr>
                                                   <td style="width:60%;">Nombre</td>
                                                   <td><span id="Mnombre"></span></td>
                                               </tr>
                                               <tr>
                                                   <td style="width:60%;">Descripcion</td>
                                                   <td><span id="Mdescripcion"></span></td>
                                               </tr>
                                               
                                               <tr>
                                                   <td style="width:60%;">Status</td>
                                                   <td><span class="label label-success">Active</span></td>
                                               </tr>
                                              
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
      </div>   
            
        
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
 
             

<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>


    <script type="text/javascript">
          
            $("#btnEliminar").on("click", function () {

                BootstrapDialog.confirm({
                    title: 'Confirmación',
                    message: 'Esta seguro que desea eliminar la clínica?',
                    type: BootstrapDialog.TYPE_PRIMARY, // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Si', // <-- Default value is 'OK',
                    btnOKClass: 'btn-primary' // <-- colocar el color del boton del tipo del mensaje,
                });

            });

            function validarFormR(idForm,X){
                if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
                    //si el formulario esta bien validado mando a guardar
                    var formData = new FormData(document.getElementById(idForm));
                    if(X!=0){
                        save_method(X,formData);
                    }else{
                        console.log('regustrar');
                        save_method(0,formData);
                    }
                }else{
                    //no es valido el forlario
                }
            } 
            $("#BtnCategoria").on("click", function () {

                $('#PDatosCategoria').show("slow");
                $('#BtnCategoria').hide("slow");
          
            });
                  
            $("#BtnCCategoria").on("click", function () {

                $('#PDatosCategoria').hide("slow");
                $('#BtnCategoria').show("slow");

            });
            
           

            $("#imagenC").fileinput({
               showUpload:false, 
                showPreview:false, 
                previewFileType:false
            });
            $("#fileEdit").fileinput({
                previewFileType: ["image", "text"],
                allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
                previewClass: "bg-warning"
            });           
            $('#data-tables').dataTable(
               {

                   language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "Ningún dato disponible en esta tabla",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

               }

             );
       
        
   
   function save_method(id,formData){
       
            var url = 'categoria/add';
            
             if (id !=0){
                url = 'categoria/update'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {                      
                          if(data.error==false){
                                    BootstrapDialog.alert({
                                      title: 'Notificacion',
                                      message: ''+data.mensaje,
                                      type: BootstrapDialog.TYPE_SUCCESS,

                                  });
                                  cargarDatosTabla();
                                 $('#PDatosCategoria').hide();
                                 $('#BtnCategoria').show("slow");
                                 $('#registrarC').trigger("reset"); 
                                   document.getElementById('registrationForm').reset();
                          }else{
                                  BootstrapDialog.alert({
                                    title: 'Notificación',
                                    message:''+data.mensaje,
                                    type: BootstrapDialog.TYPE_DANGER,}); // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY

                          }
                       
                    },

                    error: function () {
                                               
                    }
                })
            
        }  
   function cerrar(){
    $('#editar').modal('hide');
   }
                
   function cargarDatosTabla(){
            var url = 'categoria/all';
            
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    
                    success: function (data) {                   
                       var row="";
                       $("#data-tables").DataTable().destroy();
                       $("#data-tables tbody").empty();
                       var select="<option value='-1'></option>";
                        for (var i = 0; i < data["categoria"].length; i++) {
                             select+="<option value='"+data["categoria"][i].idCategoria+"'>"+data["categoria"][i].nombre+"</option>";
                            row+='<tr role="row" class="odd gradeX" id="flotante1'+data["categoria"][i].idCategoria+'" value="'+data["categoria"][i].idCategoria+'">';
                            row+='  <td class="celda center">'+data["categoria"][i].nombre+'</td>';
                            row+='  <td class="celda center">'+data["categoria"][i].descripcion+'</td>';                                                   
                           
                            if (data["categoria"][i].estado =='1'){
                                  row+='  <td class="celda center"><span class="label vd_bg-green">Activa</span></td>'; 
                            }else{
                                 row+='  <td class="celda center"><span class="label vd_bg-red">Inactiva</span></td>'; 
                            } 
                            row+='  <td class="menu-action celda center" style="width: 141px;">';
                            row+='<a id="btnver" onclick="mostrar('+"'"+data["categoria"][i].imagen+"'"+','+"'"+data["categoria"][i].nombre+"'"+','+"'"+data["categoria"][i].descripcion+"'"+','+"'"+data["categoria"][i].created_at+"'"+','+"'"+data["categoria"][i].updated_at+"'"+')"  class="btn menu-icon vd_bd-green vd_green">';
                            
                            row+='<i data-original-title="Detalles"  data-toggle="tooltip" data-placement="top" class="fa fa-eye"></i>';
                            row+='</a>';
                            row+='  <a data-original-title="Editar" onclick="mostrardiv('+data["categoria"][i].idCategoria+','+"'"+data["categoria"][i].nombre+"'"+','+"'"+data["categoria"][i].descripcion+"'"+')" id="btnEditar'+data["categoria"][i].idCategoria+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';        
                            if(data["categoria"][i].estado=='1'){
                                row+='<span id="status"> <a data-original-title="Desactivar" onclick="estado(0,'+data["categoria"][i].idCategoria+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                            }else{
                                row+='<span id="status"> <a data-original-title="Activar"  onclick="estado(1,'+data["categoria"][i].idCategoria+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                            }
                            row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion(this, '+data["categoria"][i].idCategoria+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                            row+='  </td>';
                            row+='  <td colspan="12" id="flotante2'+data["categoria"][i].idCategoria+'" style="display:none; padding:0;">';
                            row+='  {!!Form::open(['route'=>'categoria_udt', 'method'=>'POST','accept-charse'=>'UTF-8', 'novalidate'=>'novalidate','id' => 'registrationForm'])!!}'+
 '          <input name="id" type="hidden" value="'+data["categoria"][i].idCategoria+'">';
                            row+='   <div class="panel-body" style="border: 1px solid #ddd">   <div class="row"> '+
 '                                                    <div class="col-lg-3  form-group">'+
 '                                                        <div class="form-label">Nombre<span class="vd_red">*</span></div>'+
 '                                                        <input type="text" id="nombre"  class="mgbt-xs-20 mgbt-sm-0" name="nombre" value="'+data["categoria"][i].nombre+'" maxlength="255" required>'+
 '                                                    </div>'+
 '                                                    <div class="col-lg-3  form-group">'+
 '                                                        <div class="form-label">Descripcion<span class="vd_red">*</span></div>'+
 '                                                        <input type="text" class="mgbt-xs-20 mgbt-sm-0"  id="descripcion" name="descripcion" value="'+data["categoria"][i].descripcion+'" maxlength="500" required>'+
 '                                                    </div>'+
 '                                                     <div class="col-lg-3 form-group" id="select_parent">'+
'                                                            <div class="form-label">Pertenece a:<span class="vd_red">*</span></div>'+
'                                                            <select id="select_categoria" value="'+data["categoria"][i].primaria+'" name="parent_category" class="form-control">'+
'                                                                <option></option>';
                                                                      for (var j = 0; j < data["categoria"].length; j++) {
                                                                          if (data["categoria"][j].idCategoria != data["categoria"][i].idCategoria)
                                                                             row+="<option value='"+data["categoria"][j].idCategoria+"' "+(data["categoria"][j].idCategoria==data["categoria"][i].primaria?" selected ":"")+">"+data["categoria"][j].nombre+"</option>";
                                                                     }
row+='                                                            </select>'+
'                                                        </div>'+
 '                                                     <div class="col-lg-3" id="file_input_edit">'+
 '                                                        <div class="form-label">Imagen<span class="vd_red">*</span></div>'+
 '                                      {!! Form::file('file', ['id' =>'imagenC', 'class' => 'a mgbt-xs-20 mgbt-sm-0', 'data-show-upload'=>'false', 'data-preview-file-type'=>'any', 'data-initial-caption'=>'','data-overwrite-initial'=>'false'])!!}'+                                                   
 '                                                    </div>'+
 '                                                </div>';
                            row+='              <div class="mgtp-20">';
                            row+='                  <div class="row mgbt-xs-0">';
                            row+='                      <div class="col-xs-6"></div>';
                            row+='                      <div class="col-xs-6 text-right">';
                            row+='                          <button type="reset" onclick="ocultardiv('+data["categoria"][i].idCategoria+')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                            row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                            row+='                          </button>';
                            row+='                          <button  class="btn vd_btn vd_bg-green">';
                            row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                            row+='                          </button>';
                            row+='                       </div>';
                            row+='               </div>';
                            row+='          </div>';
                            row+='       ';
                            row+='    </td>';
                            row+='   </tr>';
                            $("#data-tables tbody").append(row);
                            $('[data-toggle="tooltip"]').tooltip(); 
                            row="";
                          
                            }
                             $("#select_categoriaP").html(select);
                              $("#data-tables").DataTable(
                   {
                   
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",                  
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

                   }); 
                      validate();       
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda');
                    }
                }
            );
        }
            
   function validate(){
         
      
            
            
            
            $('tbody form').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                     var id = $(this).parents("tr").attr("value");
                    var formData = new FormData(this);
                  save_method(id,formData);     
                   cargarDatosTabla();                   
                    e.preventDefault();
                }
            });
        }         
            
   function mostrardiv(pCodigo) {
         $("tbody form #imagenC").fileinput({
                previewFileType: ["image", "text"],
                allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
                previewClass: "bg-warning"
            });

        var nombreDiv = 'flotante2' + pCodigo;
        var nombrediv1 = '#flotante1' + pCodigo;


        $(nombrediv1).children('.celda').addClass('noprint');
        div = document.getElementById(nombreDiv);

        div.style.display = "";
    }
    
    
        function mostrar(imagen,nombre,descripcion,created,update) {
            
            $('#myModal').modal('show'); 
           
            document.getElementById("Mtitulo").innerHTML='Detalles de la Categoria:';
            document.getElementById("nombreCat").innerHTML=nombre;
            document.getElementById("descripcionCat").innerHTML=descripcion;
            document.getElementById('img').src = imagen;
            document.getElementById("fechaC").innerHTML=created;
            document.getElementById("fechaM").innerHTML=update;
        }

   function ocultardiv(codigo) {
        //$('#formulario'+codigo).bootstrapValidator('resetForm',true);
        var pnombre = 'flotante2' + codigo;
        var nombrediv1 = '#flotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    }  

   function eliminar(btn,id){
        var url = 'categoria/delete';
        $.ajax({
                url: url,
                data:{id:id},
                dataType: "JSON",
                type: "get",    
                success: function (data) {
                     if(data.error==false){
                       BootstrapDialog.alert({
                           title: 'Notificación',
                           message: ''+data.Message,
                           type: BootstrapDialog.TYPE_SUCCESS,
                           });
                           cargarDatosTabla();
                    }else{
                        BootstrapDialog.alert({
                           title: 'Notificación',
                           message: 'No se pudo eliminar el descuento, error: '+data.mensaje,
                           type: BootstrapDialog.TYPE_DANGER,

                       });
                    }
                },
                    error: function (data) {
                    BootstrapDialog.danger('Ocurrio un error al eliminar la categoria, '+data);
                }
            }

        );
    }
    
    function estado(val,id){
            var url = 'categoria/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           cargarDatosTabla();
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al actualizar el Area, '+data);
                    }
                }
                        
            );
        }
        

   function confirmacion(btn,pCodigo) {
        BootstrapDialog.confirm({
            title: 'Confirmacion',
            type: BootstrapDialog.TYPE_DANGER,
            message:  '¿Seguro desea Eliminar esta categoria?',
            callback: function (result) {
            if (result) {
                eliminar(btn,pCodigo);
            }
        }});
    }
    
 $(document).ready(function () {
      cargarDatosTabla();
      });
          $('#registrationForm').bootstrapValidator({
                fields: {
                    nombre: {
			 validators: {
				 notEmpty: {
					 message: 'Debe colocar un nombre'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'El nombre comienza con letras y no contiene caractes especiales'
				 }
			 }
                    },
                    descripcion: {
			 validators: {
				 notEmpty: {
					 message: 'Debe colocar una descripcion'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'La descripcion comienza con letras y no contiene caractes especiales'
				 }
			 }
                    },
                    file:{
                        validators: {
				 notEmpty: {
					 message: 'Debe colocar una imagen'
				 },
				 file: {
                                        extension: 'jpeg,jpg,png',
                                        type: 'image/jpeg,image/png',
                                        message: 'Por favor seleccione una imagen valida'
                                 }
			 }
                    }
                }
            });
      
      
      
 </script>
     @stop