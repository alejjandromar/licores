@extends('layouts.master')

@section('title', 'Cajas')
@section('bread','Restaurante  / Cajas')

@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
    
                                    <!--panel de Agregar-->
              
             <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">                           
                                    
                                    <!--panel de para agregar los datos de la nueva mesa-->
                                    <form id="registrationForm" method="post">       
                                <div class="panel widget light-widget panel-bd-top " id="PDatosCategoria" style="display: none">
                                        <div class="panel-body">
                                            <div class="col-lg-12">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos de la nueva caja.</h3>
                                                <div class="vd_panel-menu" style="    padding-right: 13px">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                    <div class="row">
                                                         <div class="col-lg-6 form-group">
                                                            <div class="form-label">Numero<span class="vd_red">*</span></div>
                                                            <input type="number" id="numero" min="0" class="mgbt-xs-20 mgbt-sm-0 form-control" maxlength="30" name="numero">
                                                        </div>
                                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="col-lg-6 form-group">
                                                            <div class="form-label">Impresora<span class="vd_red">*</span></div>
                                                            <select  name="impresora" id="impresora">
                                                             @foreach ($impresoras as $impresora)
                                                                <option value="{{ $impresora->id }}">{{ $impresora->nombre }}</option>
                                                             @endforeach
                                                        </select>
                                                        </div> 
                                                     </div>                                                   
                                                    <div class="mgtp-20">
                                                         <div class="row mgbt-xs-0">
                                                             <div class="col-xs-6"></div>
                                                             <div class="col-xs-6 text-right">
                                                                 <button  type="reset" id="BtnCCategoria" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                                     Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                                 </button>

                                                                 <button id="BtnGC" type="button" onclick="verificarForm('registrationForm',0)" value="Enviar" class="btn vd_btn vd_bg-green">
                                                                     <!--top-right-success-->
                                                                     Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                                 </button>

                                                             </div>
                                                         </div>
                                                     </div>
                                            </div>
                                            </div>
                                        </div>
                                </div></form> 
                    
                                <!--panel de tabla-->           
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Cajas</h3>
                                                    <div class="vd_panel-menu">
                                                        <button id="BtnCategoria" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                                                    </div>
                                                </div>
                                                <div class="panel-body table-responsive" style="padding: 15px;">
                                                    <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th class="celda center">N&uacute;mero</th>
                                                                <th class="celda center">Impresora</th>
                                                                <th class="celda center">Estado</th>                                               
                                                                <th></th>
                                                                <th style="display:none;"></th>  

                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                    
                                                          
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>             
   </div> 
        
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
 
             

<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>


    <script type="text/javascript">
          
            $("#btnEliminar").on("click", function () {

                BootstrapDialog.confirm({
                    title: 'Confirmación',
                    message: 'Esta seguro que desea eliminar la clínica?',
                    type: BootstrapDialog.TYPE_PRIMARY, // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY
                    closable: true, // <-- Default value is false
                    draggable: true, // <-- Default value is false
                    btnCancelLabel: 'No', // <-- Default value is 'Cancel',
                    btnOKLabel: 'Si', // <-- Default value is 'OK',
                    btnOKClass: 'btn-primary' // <-- colocar el color del boton del tipo del mensaje,
                });

            });

            function validarReg(idForm){
                $('#'+idForm).bootstrapValidator({
                    fields: {
                            numero: {
                                     validators: {
                                             notEmpty: {
                                                     message: 'Se necesita un numero'
                                             },
                                             regexp: {
                                                     regexp: /^[0-9]+$/,
                                                     message: 'Solo se permiten caracteres numericos.'
                                             }
                                     }
                             },
                             impresora: {
                                     validators: {
                                             notEmpty: {
                                                     message: 'Se necesita una impresora.'
                                             }
                                     }
                             }
                         }
                });
            }
            //para verificar si un formulario esta correcto
            function verificarForm(idForm,X){
                console.log(idForm);
                if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
                     //si el formulario esta bien validado mando a guardar
                     var formData = new FormData(document.getElementById(idForm));
                     if(X!=0){
                        var formData = new FormData(document.getElementById(idForm));
                        save_method(X,formData);      
                     }else{
                         save_method(0,formData);
                     }
                 }else{
                     //no es valido el forlario
                 }
            }
            
                  
            $("#BtnCategoria").on("click", function () {

                $('#PDatosCategoria').show("slow");
                $('#BtnCategoria').hide("slow");
                $('#registrationForm').bootstrapValidator('resetForm',true);
                $('#impresora').val(1);
                $('#numero').val(1);
          
            });
                  
            $("#BtnCCategoria").on("click", function () {

                $('#PDatosCategoria').hide("slow");
                $('#BtnCategoria').show("slow");

            });          
            $('#data-tables').dataTable(
               {

                   language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "Ningún dato disponible en esta tabla",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

               }

             );
       
        
   
   function save_method(id,formData){
       
            var url = 'caja/add';
            
             if (id !=0){
                url = 'caja/update'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {  
                          if(data.error==false){
                                    BootstrapDialog.alert({
                                      title: 'Notificacion',
                                      message: ''+data.mensaje,
                                      type: BootstrapDialog.TYPE_SUCCESS,

                                  });
                                  cargarDatosTabla();
                                 $('#PDatosCategoria').hide();
                                 $('#BtnCategoria').show("slow");
                                 $('#registrarMesas').trigger("reset"); 
                                   document.getElementById('registrationForm').reset();
                          }else{
                                  BootstrapDialog.alert({
                                    title: 'Notificación',
                                    message:''+data.mensaje,
                                    type: BootstrapDialog.TYPE_DANGER,}); // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY

                          }
                       
                    },

                    error: function () {
                                               
                    }
                })
            
        } 
   function mostrar(imagen,nombre,descripcion) {
            
            $('#myModal').modal('show'); 
            document.getElementById("Mnombre").innerHTML=nombre;
            document.getElementById("Mtitulo").innerHTML=nombre;
            document.getElementById("Mdescripcion").innerHTML=descripcion;
           document.getElementById('img').src = imagen;
        }
       
   function cerrar(){
    $('#editar').modal('hide');
   }
                
   function cargarDatosTabla(){
            var url = 'caja/all';
            
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    async: false,
                    type: "get",
                    
                    success: function (data) {  
                       
                       var row="";
                       $("#data-tables").DataTable().destroy();
                       $("#data-tables tbody").empty();
                       console.log(data);
                        for (var i = 0; i < data["caja"].length; i++) {
                            row+='<tr role="row" class="odd gradeX" id="flotante1'+data["caja"][i].numero+'" value="'+data["caja"][i].numero+'">';                    
                            row+='  <td class="celda center">'+data["caja"][i].numero+'</td>';   
                            row+='  <td class="celda center">'+data["caja"][i].impresora+'</td>';
                            if (data["caja"][i].estado =='1'){
                                  row+='  <td class="celda center"><span class="label vd_bg-green">Activa</span></td>'; 
                            }else{
                                 row+='  <td class="celda center"><span class="label vd_bg-red">Inactiva</span></td>'; 
                            }                             
                            row+='</td>';
                            row+='  <td class="menu-action celda center" style="width: 141px;">';
                            row+='  <a data-original-title="Editar" onclick="mostrardiv('+data["caja"][i].numero+','+data["caja"][i].get_impresora.id+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';        
                             if(data["caja"][i].estado=='1'){
                            row+='<span id="status"> <a data-original-title="Desactivar" onclick="estado(0,'+data["caja"][i].numero+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                            }else{
                            row+='<span id="status"> <a data-original-title="Activar"  onclick="estado(1,'+data["caja"][i].numero+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                            }
                            row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion(this, '+data["caja"][i].numero+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';                 
                            row+='  </td>';
                            row+='  <td colspan="12" id="flotante2'+data["caja"][i].numero+'" style="display:none; padding:0;">';
                             row+='   <form class="form-horizontal" method="post" role="form" iden="'+data["caja"][i].numero+'" id="formulario'+data["caja"][i].numero+'">';
                            row+='   <div class="panel-body" style="border: 1px solid #ddd">     <div class="row"> '+
 '                                                        <input name="id"  type="hidden" value="'+data["caja"][i].numero+'">'+                                                                                                       
 '                                                    <div class="col-lg-6"><input type="hidden" name="_token" value="{{ csrf_token() }}">'+
 '                                                        <div class="form-label">N&uacute;mero<span class="vd_red">*</span></div>'+
 '                                                        <input type="number" id="numero'+data["caja"][i].numero+'" min="0" class="mgbt-xs-20 mgbt-sm-0" name="numero"  maxlength="30" value="'+data["caja"][i].numero+'" required>'+
 '                                                    </div>'+ 
 '                                                    <div class="col-lg-6 form-group">'+
 '                                                           <div class="form-label">Impresora<span class="vd_red">*</span></div>'+
 '                                                           <select id="impresora'+data["caja"][i].numero+'" name="impresora">'+
                                                             @foreach ($impresoras as $impresora)
'                                                                <option value="{{ $impresora->id }}" '+('{{ $impresora->nombre }}'== data["caja"][i].impresora?"selected":"")+'>{{ $impresora->nombre }}</option>'+
                                                             @endforeach
'                                                        </select>'+
'                                                        </div> '+
 '                                                </div>';
                            row+='              <div class="mgtp-20">';
                            row+='                  <div class="row mgbt-xs-0">';
                            row+='                      <div class="col-xs-6"></div>';
                            row+='                      <div class="col-xs-6 text-right">';
                            row+='                          <button type="reset" onclick="ocultardiv('+data["caja"][i].numero+')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                            row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                            row+='                          </button>';
                            row+='                          <button type="button" onclick="verificarForm('+"'"+'formulario'+data["caja"][i].numero+"',"+data["caja"][i].numero+')"  class="btn vd_btn vd_bg-green">';
                            row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                            row+='                          </button>';
                            row+='                       </div>';
                            row+='               </div>';
                            row+='          </div>';
                            row+='       ';
                            row+=' </form></td>';
                            row+='   </tr>';
                            $("#data-tables tbody").append(row);
                             validarReg('formulario'+data["caja"][i].numero); 
                            row="";
                          
                            }
                             
                              $("#data-tables").DataTable(
                   {
                   
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",                  
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

                   });       
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda las cajas');
                    }
                }
            );
        }    
            
   function mostrardiv(pCodigo,nombre) {
        var idForm='formulario'+pCodigo;
        var nombreDiv = 'flotante2' + pCodigo;
        var nombrediv1 = '#flotante1' + pCodigo;
        $(nombrediv1).children('.celda').addClass('noprint');
        div = document.getElementById(nombreDiv);
        div.style.display = "";
        $('#'+idForm).bootstrapValidator('resetForm',true);
        $('tbody #numero'+pCodigo).val(nombre);
        $('tbody #impresora'+pCodigo).val(nombre);
    }
    
    
    

    function estado(val,id){
            var url = 'caja/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           cargarDatosTabla();
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al tratar de actualizar la caja');
                    }
                }
                        
            );
        }
        
        
        
        
        
        
        
        
   function ocultardiv(codigo) {
        //$('#formulario'+codigo).bootstrapValidator('resetForm',true);
        var pnombre = 'flotante2' + codigo;
        var nombrediv1 = '#flotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    }  

   function eliminar(btn,id){
        var url = 'caja/delete';
        $.ajax({
                url: url,
                data:{id:id},
                dataType: "JSON",
                type: "get",    
                success: function (data) {
                     if(data.error==false){
                       BootstrapDialog.alert({
                           title: 'Notificación',
                           message: ''+data.mensaje,
                           type: BootstrapDialog.TYPE_SUCCESS,
                           });
                           cargarDatosTabla();
                    }else{
                        BootstrapDialog.alert({
                           title: 'Notificación',
                           message: data.mensaje,
                           type: BootstrapDialog.TYPE_DANGER,

                       });
                    }
                },
                    error: function (data) {
                    BootstrapDialog.danger('Ocurrio un error al eliminar la caja, '+data);
                }
            }

        );
    }

   function confirmacion(btn,pCodigo) {
        BootstrapDialog.confirm({
            title: 'Confirmacion',
            type: BootstrapDialog.TYPE_DANGER,
            message:  '¿Seguro desea Eliminar esta caja?',
            callback: function (result) {
            if (result) {
                eliminar(btn,pCodigo);
            }
        }});
    }
    
 $(document).ready(function () {
     validarReg('registrationForm');
      cargarDatosTabla();
      });
    
      
      
      
 </script>
     @stop