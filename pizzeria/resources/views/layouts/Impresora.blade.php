@extends('layouts.master')

@section('title', 'Impresoras')
@section('bread', 'Configuracion / Impresoras')

@section('head_css')
@parent
    
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent

                       

                            <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">
                                  <!--panel de resultados-->
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Impresora</h3>
                                                    
                                                </div>
                                                <div class="panel-body table-responsive" style="padding: 15px;">
                                                    <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th class="celda center">Nombre</th>
                                                                <th class="celda center">Modelo</th>  
                                                                 <th class="celda center">Estado</th>  
                                                                <th   class="celda center"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Panel Widget -->
                                        </div>
                                        <!-- col-md-12 -->
                                    </div>
                            </div>


            
    
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        
        
     function formato(id) {
            $('#'+id).dataTable(
               { 
                 language: {
                   "sProcessing": "Procesando...",
                   "sLengthMenu": "Mostrar _MENU_ registros",
                   "sZeroRecords": "No se encontraron resultados",
                   "sEmptyTable": "No hay Impuestos registradas en el sistema",
                   "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                   "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                   "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                   "sInfoPostFix": "",
                   "sSearch": "Buscar:",
                   "sUrl": "",
                   "sInfoThousands": ",",
                   "sLoadingRecords": "Cargando...",
                   "oPaginate": {
                       "sFirst": "Primero",
                       "sLast": "Último",
                       "sNext": "Siguiente",
                       "sPrevious": "Anterior"
                   },
                   "oAria": {
                       "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                       "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                   }
               }
           }

               );
        }
        function save_method(id, formData){
            var url = 'impresora/add';
            
            if (id !=0){
                url = 'impresora/update'; 
            }
            $.ajax({
                     url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {
                    if(data['error']==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'La impresora ha sido agregada exitosamente',
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           load_table();
                    }else{
                        console.log(data);
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data['mensaje'],
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    
                    },

                    error: function (data) {
                         BootstrapDialog.danger('Ocurrio un error, '+data);
                    }
                }
            );
        } 
        var num =0;
        function load_table(){
            var url = 'impresora/all';
           
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    
                    success: function (data) {
                      console.log(data);
                       if (num==2){
                             $("#data-tables").DataTable().destroy();
                       }else{
                            num=2;
                       }
                       $("#data-tables tbody").empty();
                       var row="";
                        for (var i = 0; i < data["impresoras"].length; i++) {
                            row+='<tr class="odd gradeX" id="flotante1'+data["impresoras"][i].id+'" value="'+data["impresoras"][i].id+'">';
                            row+='  <td class="celda center" style="text-align: center;">'+data["impresoras"][i].nombre+'</td>';

                            row+='  <td class="celda center" style="text-align: center;" style="text-align: center;">'+data["impresoras"][i].real_name+'</td>';
                            row+=' <td id="estadoD'+data["impresoras"][i].id+'" class="celda center" style="text-align: center;">';
                             if(data["impresoras"][i].estado==1){
                                 row+='<span class="label vd_bg-green">Activo</span>';
                             }else{
                                  row+='<span class="label vd_bg-red">Inactivo</span>';
                             }
                             row+='</td>';
                            row+='  <td class="menu-action celda center" style="text-align: center;">';        
                            if(data["impresoras"][i].estado==1){
                                row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 0, \''+data["impresoras"][i].id+'\')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                            }else{
                                row+='<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 1, \''+data["impresoras"][i].id+'\')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                            }
                            row+='      <a data-original-title="Eliminar" id="btnEliminar"    onclick="eliminar(this, \''+data["impresoras"][i].id+'\')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red">  <i class="fa fa-trash"></i> </a>';
                            row+='  </span></td>';
                            row+='   </tr>';
                             $("#data-tables tbody").append(row);
                             row="";
                            
                            }
                            
                             validate(); 
                       $("form #name").empty();
                     
                       
                    },

                    error: function () {
                         BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de las impresoras');
                    }
                }
            );
        }
        
        function estado(btn, val, id){
            var url = 'impresora/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",    
                    success: function (data) {
                       if(val==1){
                               $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+id+')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-green">Activo</span>');
                                $('[data-toggle="tooltip"]').tooltip();
                            }else{
                               $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, '+id+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-red">Inactivo</span>');
                               $('[data-toggle="tooltip"]').tooltip(); 
                           }
                    }
                }
                        
            );
        }
        function eliminar(btn,id){
            var url = 'impresora/delete';
            $.ajax({
                    url: url,
                    data:{id:id,estado:0},
                    dataType: "JSON",
                    type: "get",    
                    success: function (data) {
                        if(data.error==false){
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                               load_table();
                               
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    }
                }
                        
            );
        }
        
        
        
        function validate(){
            $('tbody form').bootstrapValidator({
                fields: {
                    
                     alto:{
			 validators: {
                                 notEmpty: {
					 message: 'La altura es necesaria'
				 },
				 regexp: {
					 regexp: /^[0-9]+(.[0-9]+)?$/,
					 message: 'Debe ser xx.xx'
				 }
			 }
                    },
                    ancho:{
			 validators: {
                                 notEmpty: {
					 message: 'La anchura es necesaria'
				 },
				 regexp: {
					 regexp: /^[0-9]+(.[0-9]+)?$/,
					 message: 'Debe ser xx.xx'
				 }
			 }
                    }
                   
                    
            }});
            
            //Upadate function
            $('tbody form').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                      var id = $(this).parents("tr").attr("value");
                    var formData = new FormData(this);
                    save_method(id, formData);
                    e.preventDefault();
                }
            });
        }
        //editar
        function mostrardiv(pCodigo, pDescripcion, pCodEstatus) {

            $(nombretxt).val(pDescripcion);
            var nombreDiv = 'flotante2' + pCodigo.toString();
            var nombretxt = '#txtNuevaDescripcion' + pCodigo.toString();
            var nombrediv1 = '#flotante1' + pCodigo.toString();
            var nombreCMB = '#cmbEstatus' + pCodigo.toString();

            $(nombretxt).val(pDescripcion);
            $(nombreCMB).val(pCodEstatus);
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }

        function ocultardiv(codigo) {

            //var nombreboton = 'btnEditar' + codigo.toString();
            var nombretxt = '#txtNuevaDescripcion' + codigo.toString();
            var pnombre = 'flotante2' + codigo.toString();
            var nombrediv1 = '#flotante1' + codigo.toString();

            $(nombretxt).val('');
            //  $(nombreboton).style.display = "";
            div = document.getElementById(pnombre);
            div.style.display = "none";
            $(nombrediv1).children('.celda').removeClass('noprint');
            //boton = document.getElementById(nombreboton);
            //boton.style.display = "";

        }
        $(document).ready(function () {
            $("#BtnEmpleado").on("click", function () {
                $('#PDatosDocumentos').show("slow");
            });
            
            load_table();
            
            $("#BtnCMetodo").on("click", function () {
                $('#PDatosDocumentos').hide("slow");
            });
            //para buscar las ciudades de ese pais al cargar la pagina
             $('#registrationForm').bootstrapValidator({
                fields: {
                    
                     alto:{
			 validators: {
                                 notEmpty: {
					 message: 'La altura es necesaria'
				 },
				 regexp: {
					 regexp: /^[0-9]+(.[0-9]+)?$/,
					 message: 'Debe ser xx.xx'
				 }
			 }
                    },
                    ancho:{
			 validators: {
                                 notEmpty: {
					 message: 'La anchura es necesaria'
				 },
				 regexp: {
					 regexp: /^[0-9]+(.[0-9]+)?$/,
					 message: 'Debe ser xx.xx'
				 }
			 }
                    }
                   
                    
            }});
            
            $('#registrationForm').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                    var formData = new FormData(document.getElementById("registrationForm"));
                    save_method(0, formData);
                    document.getElementById('registrationForm').reset();
                     $('#PDatosDocumentos').hide("slow");
                    e.preventDefault();
                }
            });
            
        });
        
       
    </script>
@stop
