@extends('layouts.master')

@section('title', 'Productos')
@section('bread','Men&uacute;  /  Comidas y Bebidas')

@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
<script type="text/javascript">
    var modificadoresP=[];
    var categoria1=<?php echo($categoria[0]->idCategoria)?>;
    var area1=<?php echo($area[0]->idArea)?>;
    var modificadores=[];
     @foreach ($productos as $producto)
        var ids=[];
        @foreach ($producto->modificadores as $modificador)
            ids.push(<?php echo($modificador->id)?>);
        @endforeach
        modificadoresP.push(ids);
     @endforeach
      modificadores=<?php echo($modificadores)?>;
</script> 
      <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">  
        <!--panel de para agregar los datos de la nuevo producto-->                      
        <div class="panel widget light-widget panel-bd-top" id="PDatosProducto" style="display: none">
                <div class="panel-body">
                    <div class="col-lg-12">
                    <div class="panel-heading vd_bg-default">
                        <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos del nuevo Producto.</h3>
                        <div class="vd_panel-menu" style="    padding-right: 13px">
                            <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                        </div>
                    </div>
                    <div class="panel-body" style="border: 1px solid #ddd">
                        <form id="registrarP" method="post">
                            <div class="row">
                                  <div class="col-lg-4 form-group">
                                        <div class="form-label">Categoria<span class="vd_red">*</span></div>
                                        <select id="categoria" name="categoria" style=" width:100%">
                                                @foreach ($categoria as $categorias)                                            
                                                <option value="{{$categorias->idCategoria}}">{{$categorias->nombre}}</option>                                                        
                                                @endforeach
                                        </select>
                                 </div>
                                <div class="col-lg-4 form-group">
                                        <div class="form-label">Area<span class="vd_red">*</span></div>
                                        <select id="area" name="area" style=" width:100%"  >
                                                @foreach ($area as $areas)                                                 
                                                <option value="{{$areas->idArea}}">{{$areas->nombre}}</option>                 
                                            @endforeach
                                        </select>
                                 </div>
                                 <div class="col-lg-4 form-group">
                                    <div class="form-label">Nombre<span class="vd_red">*</span></div>
                                    <input type="text" id="nombre" class="mgbt-xs-20 mgbt-sm-0 form-control"  name="nombre">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <div class="form-label">Descripcion<span class="vd_red">*</span></div>
                                    <input type="text" id="descripcion" class=" form-control" name="descripcion">
                                </div> 
                                 <div class="col-lg-4 form-group">
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <div class="mgtp-20">
                                        <div class="mgbt-xs-0">
                                            <div class="col-xs-12"></div>
                                            <div class="col-xs-12 text-right">
                                                <button  type="button" onclick="cancelarReg('registrarP')" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                    Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                </button>

                                                <button  type="button" onclick="validarFormR('registrarP',0)" value="Enviar" class="btn vd_btn vd_bg-green">
                                                    <!--top-right-success-->
                                                    Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                        </form>
                    </div>
                    </div>
                </div>
        </div>  
              
               {!!Form::open(['route' => 'present_add', 'method'=>'POST','files'=>true, 'id' => 'registrarPre'])!!}                 
                <div class="panel widget light-widget panel-bd-top" id="PDatosPresentacion" style="display: none">
                                        <div class="panel-body">
                                            <div class="col-lg-12">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"></span><span id="nombrePAP"></span> &rarr; Agregar  Presentaci&oacute;n</h3>
                                                <div class="vd_panel-menu" style="    padding-right: 13px">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                    <div class="row">
                                                          <div class="col-lg-4 form-group">
                                                                <div class="form-label">Producto</div>
                                                                <input id="producto" class="mgbt-xs-20 mgbt-sm-0 form-control" name="producto" type="text" readonly="">
                                                         </div>
                                                        <div class="col-lg-4 form-group">
                                                            <div class="form-label">Presentacion<span class="vd_red">*</span></div>
                                                            <input type="text" id="presentacion" class="mgbt-xs-20 mgbt-sm-0 form-control"   name="presentacion">
                                                        </div>
                                                         <div class="col-lg-4 form-group">
                                                            <div class="form-label">Costo<span class="vd_red">*</span></div>
                                                            <input onkeypress="return justNumbers(event);" type="text" maxlength="10" id="costo" class="mgbt-xs-20 mgbt-sm-0 form-control"  name="costo">
                                                        </div>
                                                        <div class="col-lg-4 form-group">
                                                         <div class="form-label">Imagen<span class="vd_red">*</span></div>
                                                             {!! Form::file('file', ['id' =>'imagenC', 'class' => 'mgbt-xs-20 mgbt-sm-0', 'data-show-upload'=>'false', 'data-preview-file-type'=>'any', 'name'=>"imagen", 'data-overwrite-initial'=>"false"])!!} 
                                                        </div>
                                                        <div class="col-lg-4 form-group">
                                                            <input id="estado" name="estado" value="1" type="hidden">
                                                       </div>
                                                        <div class="col-lg-4 form-group">
                                                            <div class="mgtp-20">
                                                                <div class=" mgbt-xs-0">
                                                                    <div class="col-xs-12"></div>
                                                                    <div class="col-xs-12 text-right">
                                                                        <button  type="button" id="BtnCPresentacion" onclick="cancelarReg('registrarPre')" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                                            Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                                        </button>

                                                                        <button id="BtnGPre" type="button" onclick="validarFormP('registrarPre',0)" value="Enviar" class="btn vd_btn vd_bg-green">
                                                                            <!--top-right-success-->
                                                                            Guardar <span   class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                                       </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                     </div>
                                            </div>
                                            </div>
                                        </div>
                                </div>                     
                            
               {!!Form::close()!!}       
           


    <!-- tabla de productos--> 
    <div class="col-lg-6" style="padding:0px" id="Tproductos">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Productos</h3>
            <div class="vd_panel-menu">
              <button id="BtnProducto" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar Producto" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
             </div>
            </div>          
            <div class="panel-body table-responsive" style="padding: 15px;" >
                <form class="form-horizontal" action="#" role="form" id="register-form">
                    <table  class="table table-striped" id="data-tablesPro">
                        <thead>
                            <tr>
                                <th class="text-left">Categoria &rarr; Producto</th>
                                <th class="center">Estado</th>
                                <th></th>
                                <th style="display:none;"></th>

                            </tr>
                        </thead>
                        <tbody>


                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

    <!--tabla presentacion-->
    <div class="col-lg-6" style="padding-right:0px">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-th"></i> </span>Presentaciones <span id="discriminante">  </span></h3>
                        <div class="vd_panel-menu">
                            <button id="BtnPresentacion" style="display:none" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar presentaci&oacute;n" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                        </div>
            </div>
            <div class="panel-body table-responsive" style="padding: 15px;" id="Tpresentaciones">
                <table class="table table-striped" id="data-tablesPre">
                        <thead>
                            <tr>
                                <th width=80><b> Presentacion</b></th>
                                <th width=30>Costo</th>
                                <th width=30 class="center">Estado</th>
                                <th class="right" style="width:100px"></th>
                                <th style="display:none;"></th>

                            </tr>
                        </thead>

                    </table>
            </div>
        </div>
    </div>
      </div>  
      <!-- Modal presentacion-->
    <div class="modal fade" id="myModalPre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body">
                           <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                            <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Producto:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="nombrePro"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Presentaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="presentacionPro"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Imagen:</label> 
                                   </div>
                                   <div class="col-xs-6" style="    padding-bottom: 10px;padding-left: 30px;"> 
                                       <img  WIDTH=50 HEIGHT=50 id='img' > 
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Precio:</label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label" id="precioPro"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Fecha de Creaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaC"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">&Uacute;ltima modificaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaM"></label>
                                   </div>
                               </div>
                           </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
 <!-- Modal para agregar modificadores-->
    <div class="modal fade" id="myModalMod" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body" style="padding-bottom: 0px">
                           <h3 class="mgbt-xs-20">Activar modificadores:</h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                           <label class="col-xs-12 control-label" style="padding: 0px">Seleccione los modificadores que desee activar al producto <span id="ModProducto"></span></label>
                           <label class="col-xs-12 control-label" style="padding: 0px"><input type="checkbox" id="ckTodos" onclick="marcarTodos(this.id)"> <span>Marcar Todos</span></label>
                           <div class="list-group" id="listaMod" style="max-height: 200px;overflow: auto;border: 1px solid #ddd;">
                                    
                            </div>
                            <div class="col-lg-12 form-group" style="padding: 0px;">
                                <div class="">
                                    <div class=" mgbt-xs-0">
                                        <div class="col-xs-12 text-right" style="padding: 0px;">
                                            <label class="control-label" style="float: left;padding-right: 40px;">Modificadores activos: <span id="ModAct"></span></label> 
                                            <label class="control-label" style="float: left">Modificadores seleccionados: <span id="ModSel"></span></label> 
                                            <button  type="button" onclick="agregarModificadores()"  class="btn vd_btn vd_bg-blue">
                                                <!--top-right-success-->
                                                Guardar <span   class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
       
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
 
             


    <!--script especificos de esta pagina-->
        

   <script type="text/javascript">
          var chekeados;
          var producto=-1;
          var productoS=-1;
          var nombreProductoS=-1;
          var presentacionS=-1;
        
      function justNumbers(e){
        var keynum = window.event ? window.event.keyCode : e.which;
        if ((keynum == 8) || (keynum == 46))
        return true;

        return /\d/.test(String.fromCharCode(keynum));
        }
        
     //para validar los datos del formulario de registro de un producto
    function validarFormR(idForm,X){
            //no existe el objeto bostrad validators para este formulario por lo tanto lo creo
            $('#'+idForm).bootstrapValidator({
                fields: {
                    categoria: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una categoria'
                                     }
                             }
                     },
                     nombre: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un nombre'
                                     },
                                     regexp: {
                                             regexp: /^[a-zA-Z0-9éíóúÁÉÍÓÚñÑ./-\s]+$/,
                                             message: 'Solo se permiten caracteres alfanumericos.'
                                     }

                             }
                     },
                     area: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un area'
                                     }
                             }
                     },
                     descripcion: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una descripcion.'
                                     },
                                     regexp: {
                                             regexp: /^[a-zA-Z0-9áéíóú�?É�?ÓÚñÑ\s]+$/,
                                             message: 'Solo se permiten caracteres alfanumericos.'
                                     }
                             }
                     }
                 }
            });
         if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
             //si el formulario esta bien validado mando a guardar
             var formData = new FormData(document.getElementById(idForm));
             if(X!=0){
                 console.log('modificadoa');
                 save_pro(X,formData);
             }else{
                 save_pro(0,formData);
             }
         }else{
             //no es valido el forlario
         }
    }
    
    //funcion que valida el formulario de registro de una presentacion
    function validarFormP(idForm,X){
        if(X!=0){
            $('#'+idForm).bootstrapValidator({
                fields: {
                    producto: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un producto'
                                     }
                             }
                     },
                     presentacion: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una presentacion'
                                     }

                             }
                     },
                     costo: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un costo'
                                     },
                                     regexp: {
                                             regexp: /^[0-9]+(.[0-9]+)?$/,
                                             message: 'formato invalido'
                                     }
                             }
                     },
                     imagen: {
                             validators: {
                                     file: {
                                            extension: 'jpeg,jpg,png',
                                            type: 'image/jpeg,image/png',
                                            message: 'Por favor seleccione una imagen valida'
                                     }
                             }
                     }
                 }
            });
        }else{
            $('#'+idForm).bootstrapValidator({
                fields: {
                    producto: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un producto'
                                     }
                             }
                     },
                     presentacion: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una presentacion'
                                     }

                             }
                     },
                     costo: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un costo'
                                     },
                                     regexp: {
                                             regexp: /^[0-9]+(.[0-9]+)?$/,
                                             message: 'formato invalido'
                                     }
                             }
                     },
                     imagen: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una imagen'
                                     },
                                     file: {
                                            extension: 'jpeg,jpg,png',
                                            type: 'image/jpeg,image/png',
                                            message: 'Por favor seleccione una imagen valida'
                                     }
                             }
                     }
                 }
            });
        }
         if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
             //si el formulario esta bien validado mando a guardar
             var formData = new FormData(document.getElementById(idForm));
             if(X!=0){
                 formData.append('id',presentacionS);
                 save_method(X,formData);
             }else{
                 formData.append('id',productoS);
                 save_method(0,formData);
             }
         }else{
             //no es valido el forlario
         }
    }
        
    //para cancelar el registro de un prodcuto
    function cancelarReg(idFom){
         document.getElementById(idFom).reset();
        if($('#'+idFom).data('bootstrapValidator')!=null){
          $('#'+idFom).data('bootstrapValidator').destroy();
        }
          if(idFom=='registrarPre'){
              console.log('presentacion');
              $('#PDatosPresentacion').hide("slow");
              $('#BtnProducto').show("slow");
              $('#BtnPresentacion').show("slow");
          }else{
             $('#categoria').val(categoria1);
             $('#area').val(area1);
             $('#PDatosProducto').hide("slow");
             $('#BtnProducto').show("slow");
             $('#BtnPresentacion').show("slow");
          }
     }
     
    //funcion que inicializa los imput de tipo file
    function iniciarImagen(idFile){
        $("#"+idFile).fileinput({
            previewFileType: ["image", "text"],
            showPreview:false, 
            allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
            previewClass: "bg-warning"
        });
    }
            
        
     function formatear(id,x){
         var cadena;
         if(x==0){
             cadena='No se encontraron resultados';
         }else{
             cadena='Por favor seleccione un producto para mostrar sus presentaciones';
         }
         $("#"+id).DataTable({
             language: {
               "sProcessing": "Procesando...",
               "sLengthMenu": "Mostrar _MENU_ registros",
               "sZeroRecords": ""+cadena,                  
               "sInfo": "Registros del _START_ al _END_ de _TOTAL_ registros",
               "sInfoEmpty": "Mostrando 0 registros",
               "sInfoFiltered": "",
               "sInfoPostFix": "",
               "sSearch": "Buscar:",
               "sUrl": "",
               "sInfoThousands": ",",
               "sLoadingRecords": "Cargando...",
               "oPaginate": {
                   "sFirst": "Primero",
                   "sLast": "Último",
                   "sNext": "",
                   "sPrevious": ""
               },
               "oAria": {
                   "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                   "sSortDescending": ": Activar para ordenar la columna de manera descendente"
               }
           }
       }); 
     }
            
     //agregar y editar presentacion       
     function save_method(id,formData){
       
            var url = 'producto/addpre';
            
             if (id !=0){
                url = 'producto/updatePre'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    async: true,
                    data:formData,
                    type: "POST",
                    success: function (data) {                      
                          if(data.error==false){
                            if(id!=0){
                                ocultardivE(id);
                             }else{
                                 //estoy insertando
                                 cancelarReg('registrarPre');
                             }
                             actualizar2(data,0);
                             BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                          }else{
                                  BootstrapDialog.alert({
                                    title: 'Notificación',
                                    message:''+data.mensaje,
                                    type: BootstrapDialog.TYPE_DANGER,}); // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY

                          }
                       
                    },

                    error: function () {
                                               
                    }
                })
            
        } 
            
      //agregar y editar productos        
     function save_pro(id,formData){
       
            var url = 'producto/add';
            
             if (id !=0){
                url = 'producto/updatepro'; 
            }
            $.ajax({
                url: url,
                dataType: 'json',
                processData: false,
                contentType: false,
                async: true,
                data:formData,
                type: "POST",
                success: function (data) {                      
                  if(data.error==false){
                     if(id!=0){
                        ocultardiv(id);
                     }else{
                         //estoy insertando
                         cancelarReg('registrarP');
                     }
                     actualizar(data,1);
                     BootstrapDialog.alert({
                       title: 'Notificación',
                       message: ''+data.mensaje,
                       type: BootstrapDialog.TYPE_SUCCESS,

                   });
                       
                  }else{
                      BootstrapDialog.alert({
                        title: 'Notificación',
                        message:''+data.mensaje,
                        type: BootstrapDialog.TYPE_DANGER,});

                  }

                },

                error: function () {
                    BootstrapDialog.danger('Ocurrio un error al realizar la operacion');
                }
            })
            
        }         
                
          

    $("#BtnProducto").on("click", function () {
        $('#PDatosProducto').show("slow");
        $('#BtnProducto').hide("slow");
        $('#BtnPresentacion').hide("slow");

    });

    $("#BtnPresentacion").on("click", function () {
        iniciarImagen('imagenC');
        $('#PDatosPresentacion').show("slow");
        $('#BtnProducto').hide("slow");
        $('#BtnPresentacion').hide("slow");
        $('#nombrePAP').html(nombreProductoS);
        $('#producto').val(nombreProductoS);

    });
    //select
    $("#Select1").select2();
    $("#estatus").select2();
    $("#area").select2();
    $("#categoria").select2();
    

       
    //Muestra modal de presentaciones 
    function mostrarPre(nombre,costo,presentacion,imagen,fechaC,fechaM) {
         var X=parseFloat(costo);
         document.getElementById("Mtitulo").innerHTML='Detalles de la presentacion:';
         document.getElementById("nombrePro").innerHTML=nombre;
         document.getElementById("presentacionPro").innerHTML=presentacion;
         document.getElementById("precioPro").innerHTML=X.toFixed(2)+" {{$restaurante->simbolo}}"
         document.getElementById('img').src = imagen;
         document.getElementById("fechaC").innerHTML=fechaC;
         document.getElementById("fechaM").innerHTML=fechaM;
            $('#myModalPre').modal('show'); 
        }
        
     //para activar o desactivar un descuento(1 para activar, 0 parea desactivar).
        function estado(btn, val, id,tabla){
            if(tabla==0){
                 var url = 'producto/update_stus';
            }else{
                 var url = 'producto/update_stusPre';
            }
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           if(val==1){
                               $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+id+','+tabla+')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-green">Activo</span>');
                            }else{
                               $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, '+id+','+tabla+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-red">Inactivo</span>');
                           }
                           $('[data-toggle="tooltip"]').tooltip(); 
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
                        
            );
        }
        
    function actualizar(data,X){
        $('#discriminante').html('');
        $('#BtnPresentacion').hide();
        var row="";
       $("#data-tablesPre").DataTable().destroy();
       $("#data-tablesPro").DataTable().destroy();
       $("#data-tablesPro tbody").empty();
       $("#data-tablesPre tbody").empty();
        for (var i = 0; i < data["producto"].length; i++) {
            row+='<tr role="row" class="odd gradeX" id="flotante1'+data["producto"][i].idProducto+'" value="'+data["producto"][i].idProducto+'">'+                                             
             '@foreach ($categoria as $categorias)';                        
                            if(data["producto"][i].categoria=={{$categorias->idCategoria}}){
                                row+=' <td class="celda"><b>{{$categorias->nombre}}</b> &rarr; '+data["producto"][i].nombre+'</td>';
                            }           
                          row+='@endforeach';  
            if (data["producto"][i].estado==0){
                row+='  <td id="estadoD'+data["producto"][i].idProducto+'" class="celda center"><span class="label vd_bg-red">Inactivo</span></td>';
            }else{
             row+='  <td id="estadoD'+data["producto"][i].idProducto+'" class="celda center"><span class="label vd_bg-green">Activo</span></td>';   
            }                                                  
            row+='</td>';
            row+='  <td class="menu-action celda center" style="width: 183px;">';
            row+='<a id="btnpre'+data["producto"][i].idProducto+'" onclick="cargarDatosTablaPre('+data["producto"][i].idProducto+','+0+')"  class="btn menu-icon vd_bd-green vd_green buscar">';
            row+='<i data-original-title="Ver presentaciones"  data-toggle="tooltip" data-placement="top" class="fa fa-eye"></i>';
            row+='</a>';   
            row+='  <a data-original-title="Activar modificadores" style="padding-right: 1px;padding-left: 5px;" onclick="mostrarModificadores('+i+','+data["producto"][i].idProducto+')" id="btnEditar'+data["producto"][i].idProducto+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-blue vd_blue"> <i class="fa fa-maxcdn"></i><i class="fa fa-plus" style="position: relative;font-size: 8px;left: -2px;top: 1px;"></i> </a>';        
            if(data["producto"][i].estado==1){
            row+='<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+data["producto"][i].idProducto+',0)"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
            }else{
            row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 1, '+data["producto"][i].idProducto+',0)"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
            }
            row+='  <a data-original-title="Editar" onclick="mostrardiv('+data["producto"][i].idProducto+','+"'"+data["producto"][i].nombre+"'"+','+"'"+data["producto"][i].descripcion+"'"+','+"'"+data["producto"][i].estado+"'"+','+"'"+data["producto"][i].area+"'"+')" id="btnEditar'+data["producto"][i].idProducto+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';        
            row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacionPro(this, '+data["producto"][i].idProducto+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
            row+='  </td>';                        
            row+='  <td colspan="12" id="flotante2'+data["producto"][i].idProducto+'" style="display:none; padding:0;">';
             row+='  <form class="" method="post" role="form" iden="'+data["producto"][i].idProducto+'" id="registrarP'+data["producto"][i].idProducto+'">'+  
                    '<input name="id" type="hidden" value="'+data["producto"][i].idProducto+'"><input type="hidden" name="_token" value="{{ csrf_token() }}">';
            row+='<div class="panel-body" style="border: 1px solid #ddd;padding: 10px;">   <div class="row">'+
'                                <div class="col-lg-6 form-group" style="margin:0px;margin-bottom: 20px;">'+
'                                    <div class="form-label">Categoria<span class="vd_red">*</span></div>'+
'                                    <select id="categoria" class="form-control" name="categoria" style=" width:100%"  >'+
'                                          @foreach ($categoria as $categorias)' +                             
'                                              <option value="{{$categorias->idCategoria}}"';
                            if(data["producto"][i].categoria=={{$categorias->idCategoria}}){
                                row+=' selected ';
                            }
row+='                                         >{{$categorias->nombre}}</option>' +             
'                                          @endforeach'+
'                                     </select>'        +                                                                                     
'                                 </div>'+
'                                 <div class="col-lg-6 form-group" style="margin:0px;margin-bottom: 20px;">'+
'                                     <div class="form-label">Area<span class="vd_red">*</span></div>'+
'                                          <select id="area" name="area" class="form-control" style=" width:100%"  >'+
'                                              @foreach ($area as $areas)'                       +                          
'                                                   <option value="{{$areas->idArea}}"';
                            if(data["producto"][i].area=={{$areas->idArea}}){
                                row+=' selected ';
                            }
row+='                                         >{{$areas->nombre}}</option>' +  
'                                               @endforeach'+
'                                           </select>'       +                                                                                      
'                                  </div>'+
'                                  <div class="col-lg-6 form-group" style="margin:0px;">'+
'                                       <div class="form-label">Nombre<span class="vd_red">*</span></div>'+
'                                        <input type="text" id="nombre"  class="mgbt-xs-20 mgbt-sm-0 form-control" style="width:100%" name="nombre">'+
'                                  </div>'+
'                                  <div class="col-lg-6 form-group" style="margin:0px;">'+
'                                       <div class="form-label">Descripcion<span class="vd_red">*</span></div>'+
'                                       <input type="text" class="mgbt-xs-20 mgbt-sm-0 form-control"  id="descripcion" name="descripcion" style="width:100%">'+
'                                  </div>';
            row+='              <div class="col-lg-12" style="padding-top: 17px;">';
            row+='                  <div class="mgbt-xs-0">';
            row+='                      <div class=""></div>';
            row+='                      <div class="text-right">';
            row+='                          <button type="button" onclick="ocultardiv('+data["producto"][i].idProducto+')" class="btn vd_btn btn-default" style="color: #0E2145!important;padding-left: 10px; padding-right: 10px">';
            row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
            row+='                          </button>';
            row+='                          <button type="button" class="btn vd_btn vd_bg-green" onclick="validarFormR('+"'registrarP"+data["producto"][i].idProducto+"',"+data["producto"][i].idProducto+')" style="padding-left: 10px; padding-right: 10px">';
            row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
            row+='                          </button>';
            row+='                       </div>';
            row+='               </div></div>';
            row+='          </div>';
            row+='  </form>  </td>';
            row+='   </tr>';
           $("#data-tablesPro tbody").append(row);
            $('[data-toggle="tooltip"]').tooltip(); 
            row="";
            }   
           formatear('data-tablesPro',X);
            validate();
            $('#Tpresentaciones').css('height',($('#Tproductos').height()-56)+'px');
            formatear('data-tablesPre',X);
            $('#Tpresentaciones').show();
    }
 
    function cargarDatosTablaPro(X){
            var url = 'producto/all';
            $("#data-tablesPro tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    async: true,
                    type: "get",
                    
                    success: function (data) { 
                       $('#BtnPresentacion').hide();
                       actualizar(data,X);
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los descuentos');
                    }
                }
            );
        }
        
     function validate(){    
      $("tbody form #estado").select2();
      $("tbody form #estatus").val(document.getElementById("estado").value);
        }    
     
    var posicion;
    
     function mostrarModificadores(indice,id){
         producto=id;
         posicion=indice;
         var row="";            
         $("#listaMod").empty();
         $("#ModProducto").text('nombreProducto');
         for (var i = 0; i < modificadores.length; i++) {
             row+='<div  class="list-group-item" style="padding: 6px!important;"><div class="row" style="margin: 0px!important;">';
             row+='<div class="col-lg-12" style="padding: 0px!important;">';
             row+='<div class="input-group">';
             row+='<span class="input-group-addon" style="background-color: white;border: none;">';
             row+='<input type="checkbox" id="ck'+modificadores[i].id+'" idm="'+modificadores[i].id+'" onclick="verificarChekeado(this)" value='+modificadores[i].id;
             var existe=false;
             if(modificadoresP[indice].indexOf(modificadores[i].id)>=0){
                 existe=true;
                 row+=' checked';
             }
             row+='></span>';
             var nombre;var valor=0.0;
             if(modificadores[i].tipo=="extra"){ nombre='Ext. de '+modificadores[i].nombre;
             }else{ nombre='Sin '+modificadores[i].nombre;}
             if(modificadores[i].compuesto==1){ nombre=modificadores[i].nombre;}
             if(modificadores[i].tipo=="extra"){valor=modificadores[i].precio;}
             row+='<span class="col-xs-4" style="padding-left: 0px;padding-top: 2px;">'+nombre+'</span>';
             row+='<span class="col-xs-2" style="padding-top: 2px;">'+valor.toFixed(2)+' {{$restaurante->simbolo}}</span>';
             if(existe){
                 row+='<span id="estadoMod'+modificadores[i].id+'" class="col-xs-3 text-center" style="padding-left: 0px;padding-top: 2px;"><span class="label vd_bg-green">Activo</span></span>';
             }else{
                 row+='<span id="estadoMod'+modificadores[i].id+'" class="col-xs-3 text-center" style="padding-left: 0px;padding-top: 2px;"><span class="label vd_bg-red">Inactivo</span></span>';
             }
             row+='<div class="col-xs-2">';
             row+='<img src="'+modificadores[i].imagen+'" style="height:29px">';
             row+='</div>';
             row+='<i id="icono'+modificadores[i].id+'"  class="fa fa-check col-xs-1" style="color: green;float: right;padding: 7px;';
             if(!existe){row+='display:none;'}
             row+='"></i></div></div></div> </div>';
             $("#listaMod").append(row);
             row="";
         }
          chekeados=modificadoresP[indice].length;
          if(chekeados==modificadores.length){
             $("#ckTodos").prop('checked',true);
            }else{
                 $("#ckTodos").prop('checked',false);
            }
         $('#ModAct').html(chekeados);
         $('#ModSel').html(chekeados);
         $('#myModalMod').modal('show');
     }
     
     function verificarChekeado(ck){
        var id=$("#"+ck.id).attr('idm');
        if($("#"+ck.id).prop('checked')){
            chekeados++;
            $("#icono"+id).show();
        }else{
            $("#icono"+id).hide();
            chekeados--;
        }
        if(chekeados==modificadores.length){
             $("#ckTodos").prop('checked',true);
        }else{
             $("#ckTodos").prop('checked',false);
        }
        $('#ModSel').html(chekeados);
     } 
     
     function marcarTodos(id){
         if($("#"+id).prop('checked')){
             chekeados=0;
         }else{
             chekeados=modificadores.length;
         }
        $("#listaMod input").each(function(){
            var idM=$("#"+this.id).attr('idm');
             if($("#"+id).prop('checked')){
                  $("#"+this.id).prop('checked',true);
                   $("#icono"+idM).show();
                  chekeados++;
             }else{
                  $("#"+this.id).prop('checked',false);
                  $("#icono"+idM).hide();
                  chekeados--;
             }
         });
         $('#ModSel').html(chekeados);
     } 
     
     function agregarModificadores(){
             var activar=[];var desactivar=[];
             $("#listaMod input").each(function(){
                 if($("#"+this.id).prop('checked')){
                     activar.push(parseInt(this.value));
                 }else{
                     desactivar.push(parseInt(this.value));
                 }
             });
             activar_modificadores(activar,desactivar);
     }
     
     function activar_modificadores(idsActivar,idsDesactivar){
         var url = 'producto/activarMod';
            $.ajax({
                    url: url,
                    data:{id:producto,ModActivar:idsActivar,ModDescativar:idsDesactivar},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            $('#myModalMod').modal('hide');
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           modificadoresP[posicion]=idsActivar;
                           
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
                        
            );
     }
     
    function actualizar2(data,X){
        nombreProductoS=data["nombre"][0].nombre;
               var row="";
               $("#data-tablesPre").DataTable().destroy();
               $("#data-tablesPre tbody").empty();
               $('#discriminante').html('del producto '+data["nombre"][0].nombre);
               $('#BtnPresentacion').show();
                for (var i = 0; i < data["presentacion"].length; i++) {
                    row+='<tr role="row" class="odd gradeX" id="Pflotante1'+data["presentacion"][i].id+'" value="'+data["presentacion"][i].id+'">';
                    row+='  <td class="celda center">'+data["presentacion"][i].presentacion+'</td>';
                    row+='  <td class="celda center">'+data["presentacion"][i].costo+' {{$restaurante->simbolo}}</td>';                                                   
                     if (data["presentacion"][i].estado==0){
                        row+='  <td id="estadoD'+data["presentacion"][i].id+'" class="celda center"><span class="label vd_bg-red">Inactivo</span></td>';
                    }else{
                     row+='  <td id="estadoD'+data["presentacion"][i].id+'" class="celda center"><span class="label vd_bg-green">Activo</span></td>';   
                    }                                                  
                    row+='</td>';;
                    row+='  <td class="menu-action celda center" style="width: 100px;">';
                    row+='<a id="btnver"  onclick="mostrarPre('+"'"+data["nombre"][0].nombre+"'"+','+"'"+data["presentacion"][i].costo+"'"+','+"'"+data["presentacion"][i].presentacion+"'"+','+"'"+data["presentacion"][i].imagen+"'"+','+"'"+data["presentacion"][i].created_at+"'"+','+"'"+data["presentacion"][i].updated_at+"'"+')" class="btn menu-icon vd_bd-green vd_green">';
                    row+='<i data-original-title="Ver detalles"  data-toggle="tooltip" data-placement="top" class="fa fa-eye"></i>';
                    row+='</a>'; 
                    if(data["presentacion"][i].estado==1){
                    row+='<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+data["presentacion"][i].id+',1)"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                    }else{
                    row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 1, '+data["presentacion"][i].id+',1)"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                    }
                     row+=' <a data-original-title="Editar" onclick="mostrardivE('+data["presentacion"][i].id+','+"'"+data["nombre"][0].nombre+"'"+','+"'"+data["presentacion"][i].presentacion+"',"+data["presentacion"][i].costo+')" id="btnEditar'+data["presentacion"][i].id+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';        
                    row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion('+data["presentacion"][i].producto+', '+data["presentacion"][i].id+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                    row+='  </td>';
                    row+='  <td colspan="12" id="Pflotante2'+data["presentacion"][i].id+'" style="display:none; padding:0;">';
                    row+='  <form class="" method="post" role="form"  id="registrarPre'+data["presentacion"][i].id+'"><input type="hidden" name="_token" value="{{ csrf_token() }}">'; 
                    row+=' <div class="panel-body" style="border: 1px solid #ddd;padding: 10px;">   <div class="row">'+
                                                    '<div class="col-lg-6 form-group" style="margin:0px;margin-bottom: 20px;">'+
'                                                        <div class="form-label">Producto<span class="vd_red">*</span></div>'+
'                                                        <input type="text" id="prodcuto" value="'+data["nombre"][0].nombre+'" class="mgbt-xs-20 mgbt-sm-0 form-control" style="width:100%" name="producto" readonly>'+
'                                                    </div>'+
'                                                    <div class="col-lg-6 form-group" style="margin:0px;margin-bottom: 20px;">'+
'                                                        <div class="form-label">Presentacion<span class="vd_red">*</span></div>'+
'                                                        <input type="text" id="presentacion" value="'+data["presentacion"][i].presentacion+'" class="mgbt-xs-20 mgbt-sm-0 form-control" style="width:100%" name="presentacion">'+
'                                                    </div>'+
'                                                    <div class="col-lg-6 form-group">'+
'                                                        <div class="form-label">Costo<span class="vd_red">*</span></div>'+
'                                                        <input type="text" onkeypress="return justNumbers(event);" class="mgbt-xs-20 mgbt-sm-0 form-control" value="'+data["presentacion"][i].costo+'"  id="costo" maxlength="10" style="width:100%" name="costo">'+
'                                                    </div>'+
'                                                     <div class="col-lg-6 form-group">'+
'                                                        <div class="form-label">Imagen<span class="vd_red">*</span></div>'+
'                                                        <input id="imagen'+data["presentacion"][i].id+'"  name="imagen" type="file"  class="mgbt-xs-20 mgbt-sm-0 form-control" data-show-upload="false" data-preview-file-type="any" data-initial-caption="" style="width:100%" data-overwrite-initial="false"> '+
'                                                    </div>'+
'                                                </div>';
                    row+='              <div class="mgtp-20">';
                    row+='                  <div class="">';
                    row+='                      <div class=""></div>';
                    row+='                      <div class=" text-right">';
                    row+='                          <button type="button" onclick="ocultardivE('+data["presentacion"][i].id+')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                    row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                    row+='                          </button>';
                    row+='                          <button type="button" onclick="validarFormP('+"'"+'registrarPre'+data["presentacion"][i].id+"',"+data["presentacion"][i].id+')" class="btn vd_btn vd_bg-green">';
                    row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                    row+='                          </button>';
                    row+='                       </div>';
                    row+='               </div>';
                    row+='          </div></div>';
                    row+='</form></td>';
                    row+='   </tr>';

                   $("#data-tablesPre tbody").append(row);
                   $('[data-toggle="tooltip"]').tooltip(); 
                   row="";
                    }
                     formatear('data-tablesPre',X);

    }
    
    function cargarDatosTablaPre(id,X){
            productoS=id;
            $('a.buscar').removeClass('vd_bd-red');$('a.buscar').removeClass('vd_red');
            $('a.buscar').addClass('vd_bd-green');$('a.buscar').addClass('vd_green');
            $('#btnpre'+id).removeClass('vd_bd-green');$('#btnpre'+id).addClass('vd_bd-red');
            $('#btnpre'+id).removeClass('vd_green');
            $('#btnpre'+id).addClass('vd_red');
            var url = 'producto/allpre';
                var data= {id:id }; 
            $("#data-tablesPre tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:data,
                    async:true,
                    type: "get",
                    
                    success: function (data) { 
                         actualizar2(data,X);
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los productos');
                    }
                }
            );
        }
        
      
     $(document).ready(function () {
            cargarDatosTablaPro(1);         
         }); 
   
     //editor de productos       
     function mostrardiv(pCodigo,nombre,descripcion,estado,area) {
           $('tbody #nombre').val(nombre);
           $('tbody #descripcion').val(descripcion);
           $('tbody #area').val(area);
           $('tbody #estado').val(estado);
           $('tbody #estatus').val(estado);
            var nombreDiv = 'flotante2' + pCodigo;
            var nombrediv1 = '#flotante1' + pCodigo;

           
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }
        
      //editar presentaciones  
     function mostrardivE(pCodigo,prod,pre,costo) {
            presentacionS=pCodigo;
            $('tbody #producto').val(prod);
           $('tbody #presentacion').val(pre);
           $('tbody #costo').val(costo);
            iniciarImagen('imagen'+pCodigo);
            var nombreDiv = 'Pflotante2' + pCodigo;
            var nombrediv1 = '#Pflotante1' + pCodigo;

           
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }
        
         
     function ocultardiv(codigo) {
        $('#registrarP'+codigo).bootstrapValidator('resetForm',true);
        var pnombre = 'flotante2' + codigo;
        var nombrediv1 = '#flotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    } 
                
              
     function ocultardivE(codigo) {
     if($('#registrarPre'+codigo).data('bootstrapValidator')!=null){
            $('#registrarPre'+codigo).data('bootstrapValidator').destroy();
        }
        var pnombre = 'Pflotante2' + codigo;
        var nombrediv1 = '#Pflotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

        } 
        
      //eliminar productos  
     function eliminarP(id){
            var url = 'producto/delete';
            $.ajax({
                    url: url,
                    data:{id:id},
                    dataType: "JSON",
                    async: true,
                    type: "get",    
                    success: function (data) {
                         if(data.error==false){
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                                actualizar(data,1);
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al eliminar el producto, '+data);
                    }
                }
                        
            );
        }
        
        //eliminar presentacion
     function eliminar(id){
            var url = 'producto/deletePre';
            $.ajax({
                    url: url,
                    data:{id:id},
                    dataType: "JSON",
                    async: true,
                    type: "get",    
                    success: function (data) {
                         if(data.error==false){ 
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                              actualizar2(data,0);
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Error: '+data.mensaje);
                    }
                }
                        
            );
        }
        
        //confirmar eliminar presentacion
     function confirmacion(codp,pCodigo) {
            BootstrapDialog.confirm({
                title: 'Confirmacion',
                type: BootstrapDialog.TYPE_DANGER,
                message:  '¿Seguro desea Eliminar esta presentacion?',
                callback: function (result) {
                if (result) {
                   eliminar(pCodigo);
                }
            }});
        }
          //confirmar eliminar producto
     function confirmacionPro(btn,pCodigo) {
            BootstrapDialog.confirm({
                title: 'Confirmacion',
                type: BootstrapDialog.TYPE_DANGER,
                message:  '¿Seguro desea Eliminar este producto?',
                callback: function (result) {
                if (result) {
                    eliminarP(pCodigo);
                }
            }});
        }
        
 
       
                      
     </script>
     @stop