@extends('layouts.master')

@section('title', 'Areas')
@section('bread','Restaurante  / Areas')

@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
<!--panel de Agregar-->
<div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">
    <!--panel de para agregar los datos de la nueva mesa-->
     {!!Form::open(['route' => 'areas_add', 'method'=>'POST','files'=>true, 'id' => 'registrationForm'])!!}        
        <div class="panel widget light-widget panel-bd-top " id="PDatosCategoria" style="display: none">
                <div class="panel-body">
                    <div class="col-lg-12">
                    <div class="panel-heading vd_bg-default">
                        <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos de nueva Area.</h3>
                        <div class="vd_panel-menu" style="    padding-right: 13px">
                            <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                        </div>
                    </div>
                    <div class="panel-body" style="border: 1px solid #ddd">
                        <form id="registrarMesas">
                            <div class="row">
                                 <div class="col-lg-4 form-group">
                                    <div class="form-label">Nombre<span class="vd_red">*</span></div>
                                    <input type="text" id="nombre" class="mgbt-xs-20 mgbt-sm-0 form-control" maxlength="30" name="nombre">
                                </div>
                                <div class="col-lg-4 form-group">
                                    <div class="form-label">Tipo<span class="vd_red">*</span></div>
                                    <select id="tipo" name="tipo">
                                        <option value="0">Empleados</option>
                                         <option value="1">Clientes</option>
                                </select>
                                </div> 
                                <div class="col-lg-4 form-group">
                                    <div class="form-label">Impresora<span class="vd_red">*</span></div>
                                    <select id="impresora" name="impresora">
                                     @foreach ($impresoras as $impresora)
                                        <option value="{{ $impresora->id }}">{{ $impresora->nombre }}</option>
                                     @endforeach
                                </select>
                                </div> 
                             </div>                                                   
                            <div class="mgtp-20">
                                 <div class="row mgbt-xs-0">
                                     <div class="col-xs-6"></div>
                                     <div class="col-xs-6 text-right">
                                         <button  type="button" id="BtnCCategoria" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                             Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                         </button>

                                         <button id="BtnGC" type="button" onclick="verificarForm('registrationForm',0)" value="Enviar" class="btn vd_btn vd_bg-green">
                                             <!--top-right-success-->
                                             Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                         </button>

                                     </div>
                                 </div>
                             </div>
                        </form>
                    </div>
                    </div>
                </div>
        </div> 
     {!!Form::close()!!}

        <!--panel de tabla-->           
        <div class="row" id="Ptabla">
            <div class="col-md-12">
                <div class="panel widget">
                    <div class="panel-heading vd_bg-grey">
                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Area</h3>
                        <div class="vd_panel-menu">
                            <button id="BtnCategoria" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                        </div>
                    </div>
                    <div class="panel-body table-responsive" style="padding: 15px;">
                        <table class="table table-striped" id="data-tables">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Tipo</th>
                                    <th>Impresora</th>
                                    <th>Estado</th>                                               
                                    <th></th>
                                    <th style="display:none;"></th>  

                                </tr>
                            </thead>
                            <tbody>                                                    


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>             
</div> 
        
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
 
             

<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>


<script type="text/javascript">
          
    $("#btnEliminar").on("click", function () {

        BootstrapDialog.confirm({
            title: 'Confirmación',
            message: 'Esta seguro que desea eliminar la clínica?',
            type: BootstrapDialog.TYPE_PRIMARY, // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'No', // <-- Default value is 'Cancel',
            btnOKLabel: 'Si', // <-- Default value is 'OK',
            btnOKClass: 'btn-primary' // <-- colocar el color del boton del tipo del mensaje,
        });

    });

    $("#BtnCategoria").on("click", function () {
        $('#PDatosCategoria').show("slow");
        $('#BtnCategoria').hide("slow");
        $('#registrationForm').bootstrapValidator('resetForm',true);
        $('#tipo').val(0);
        $('#impresora').val('{{ $impresoras[0]->id }}');

    });

    $("#BtnCCategoria").on("click", function () {
        $('#PDatosCategoria').hide("slow");
        $('#BtnCategoria').show("slow");

    });

    //select
     
    function validarReg(idForm){
        $('#'+idForm).bootstrapValidator({
            fields: {
                    nombre: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un nombre'
                                     },
                                     regexp: {
                                             regexp: /^[a-zA-Z0-9áéíóúÁÉIÓÚñÑ\s]+$/,
                                             message: 'Solo se permiten caracteres alfanumericos.'
                                     }
                             }
                     },
                     tipo: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un tipo.'
                                     }
                             }
                     },
                     impresora: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una impresora.'
                                     }
                             }
                     }
                 }
        });
    }
    
    //para verificar si un formulario esta correcto
    function verificarForm(idForm,X){
        if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
             //si el formulario esta bien validado mando a guardar
             var formData = new FormData(document.getElementById(idForm));
             save_method(0,formData);
         }else{
             //no es valido el forlario
         }
    }
       
   //para formatear las tablas
   function formato(id) {
                $(id).dataTable(
                   { 
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "No hay modificadores registradas en el sistema",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }
                   }
               }

                   );
            }   
   
   function save_method(id,formData){
       
            var url = 'areas/add';
            
             if (id !=0){
                url = 'areas/udt'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {                      
                          if(data.error==false){
                                    BootstrapDialog.alert({
                                      title: 'Notificacion',
                                      message: ''+data.mensaje,
                                      type: BootstrapDialog.TYPE_SUCCESS,

                                  });
                                 $('#PDatosCategoria').hide();
                                 $('#BtnCategoria').show("slow");
                                 $('#registrarMesas').trigger("reset"); 
                                 cargarDatosTabla();
                          }else{
                                  BootstrapDialog.alert({
                                    title: 'Notificación',
                                    message:''+data.mensaje,
                                    type: BootstrapDialog.TYPE_DANGER,}); // <-- colocar el color dependiendo del tipo del mensaje BootstrapDialog.TYPE_PRIMARY

                          }
                       
                    },

                    error: function () {
                                               
                    }
                })
            
        } 
   
   function mostrar(imagen,nombre,descripcion) {
        $('#myModal').modal('show'); 
        document.getElementById("Mnombre").innerHTML=nombre;
        document.getElementById("Mtitulo").innerHTML=nombre;
        document.getElementById("Mdescripcion").innerHTML=descripcion;
       document.getElementById('img').src = imagen;
    }
       
   function cerrar(){
    $('#editar').modal('hide');
   }
                
   function cargarDatosTabla(){
            var url = 'areas/all';
            
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    async: false,
                    type: "get",
                    
                    success: function (data) {                   
                       var row="";
                       $("#data-tables").DataTable().destroy();
                       $("#data-tables tbody").empty();
                       
                        for (var i = 0; i < data["area"].length; i++) {
                            row+='<tr role="row" class="odd gradeX" id="flotante1'+data["area"][i].idArea+'" value="'+data["area"][i].idArea+'">';                    
                            row+='  <td class="celda center">'+data["area"][i].nombre+'</td>'; 
                            var area='Empleados';
                            if(data["area"][i].tipo==1){
                                area='Clientes';
                            }
                            row+='  <td class="celda center">'+area+'</td>';
                             row+='<td class="celda center">';
                             if(data["area"][i].impresora==null){
                                row+='No posee';
                            }else{
                                @foreach ($impresoras as $impresora)
                                row+= ('{{ $impresora->id }}'== data["area"][i].impresora?'{{ $impresora->nombre }}':"");
                               @endforeach
                            }
                             row+='</td>';
                            if (data["area"][i].estado ==1){
                                  row+='  <td class="celda center"><span class="label vd_bg-green">Activa</span></td>'; 
                            }else{
                                 row+='  <td class="celda center"><span class="label vd_bg-red">Inactiva</span></td>'; 
                            }                             
                            row+='</td>';
                            row+='  <td class="menu-action celda center" style="width: 141px;">';
                            row+='  <a data-original-title="Editar" onclick="mostrardiv('+"'"+data["area"][i].idArea+"'"+','+"'"+data["area"][i].nombre+"'"+','+data["area"][i].tipo+','+data["area"][i].impresora+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';        
                             if(data["area"][i].estado=='1'){
                            row+='<span id="status"> <a data-original-title="Desactivar" onclick="estado(0,'+data["area"][i].idArea+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                            }else{
                            row+='<span id="status"> <a data-original-title="Activar"  onclick="estado(1,'+data["area"][i].idArea+')"  data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                            }
                            row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion(this, '+data["area"][i].idArea+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';                 
                            row+='  </td>';
                            row+='  <td colspan="12" id="flotante2'+data["area"][i].idArea+'" style="display:none; padding:0;">';
                            row+='   <form class="form-horizontal" method="post" role="form" iden="'+data["area"][i].idArea+'" id="formulario'+data["area"][i].idArea+'">';
                            
                            row+=' <div class="panel-body" style="border: 1px solid #ddd">   <div class="row">  '+
 '                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> <input name="id"  type="hidden" value="'+data["area"][i].idArea+'">'+                                                                                                       
 '                                                    <div class="col-lg-4">'+
 '                                                        <div class="form-label">Nombre<span class="vd_red">*</span></div>'+
 '                                                        <input type="text" id="nombre'+data["area"][i].idArea+'"  class="mgbt-xs-20 mgbt-sm-0 form-control" name="nombre"  maxlength="30" style="width: 100%;">'+
 '                                                    </div>'+                                                
 '                                                    <div class="col-lg-4 ">'+
 '                                                        <div class="form-label">Tipo<span class="vd_red">*</span></div>'+
 '                                                         <select id="tipo'+data["area"][i].idArea+'" name="tipo" class="form-control" style="width: 100%;">'+
 '                                                               <option value="0">Empleados</option>'+
 '                                                                <option value="1">Clientes</option>'+
 '                                                       </select>'+
 '                                                    </div>'+
 '                                                    <div class="col-lg-4">'+
 '                                                           <div class="form-label">Impresora<span class="vd_red">*</span></div>'+
 '                                                           <select id="impresora'+data["area"][i].idArea+'"  name="impresora" class="form-control" style="width: 100%;">'+
                                                             @foreach ($impresoras as $impresora)
'                                                                <option value="{{ $impresora->id }}" '+({{ $impresora->id }}== data["area"][i].impresora?"selected":"")+'>{{ $impresora->nombre }}'+'</option>'+
                                                             @endforeach
'                                                        </select>'+
'                                                        </div> '+
 '                                                </div>';
                            row+='              <div class="mgtp-20">';
                            row+='                  <div class="row mgbt-xs-0">';
                            row+='                      <div class="col-xs-6"></div>';
                            row+='                      <div class="col-xs-6 text-right">';
                            row+='                          <button type="reset" onclick="ocultardiv('+data["area"][i].idArea+')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                            row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                            row+='                          </button>';
                            row+='                          <button  type="button" onclick="verificarFormM('+data["area"][i].idArea+')" class="btn vd_btn vd_bg-green">';
                            row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                            row+='                          </button>';
                            row+='                       </div>';
                            row+='               </div>';
                            row+='          </div>';
                            row+='       ';
                            row+='</form>    </td>';
                            row+='   </tr>';
                            $("#data-tables tbody").append(row);
                            validate('formulario'+data["area"][i].idArea); 
                            row="";
                          
                            }
                             
                            formato("#data-tables") ;
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda las Areas');
                    }
                }
            );
        }
            
   function validate(id){
    $('#'+id).bootstrapValidator({
            fields: {
                    nombre: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un nombre'
                                     },
                                     regexp: {
                                             regexp: /^[a-zA-Z0-9áéíóúÁÉIÓÚñÑ\s]+$/,
                                             message: 'Solo se permiten caracteres alfanumericos.'
                                     }
                             }
                     },
                     tipo: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un tipo.'
                                     }
                             }
                     },
                     impresora: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita una impresora.'
                                     }
                             }
                     }
                 }
        });
    } 
    
    //para verificar si un formulario esta correcto
    function verificarFormM(id){
        var idForm='formulario'+id;
        if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
             //si el formulario esta bien validado mando a guardar
             var formData = new FormData(document.getElementById(idForm));
             save_method(id,formData);        
         }else{
             //no es valido el forlario
         }
    }
            
   function mostrardiv(pCodigo,nombre,area,impresora) {
        var idForm='formulario'+pCodigo;
        var nombreDiv = 'flotante2' + pCodigo;
        var nombrediv1 = '#flotante1' + pCodigo;
        $(nombrediv1).children('.celda').addClass('noprint');
        div = document.getElementById(nombreDiv);
        div.style.display = "";
        $('#'+idForm).bootstrapValidator('resetForm',true);
        $('tbody #nombre'+pCodigo).val(nombre);
         $('tbody #tipo'+pCodigo).val(area);
         $('tbody #impresora'+pCodigo).val(impresora);
    }
    
   function estado(val,id){
            var url = 'areas/estado';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           cargarDatosTabla();
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al actualizar el Area, '+data);
                    }
                }
                        
            );
        }
        
   function ocultardiv(codigo) {
        //$('#formulario'+codigo).bootstrapValidator('resetForm',true);
        var pnombre = 'flotante2' + codigo;
        var nombrediv1 = '#flotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    }  

   function eliminar(btn,id){
        var url = 'areas/delete';
        $.ajax({
                url: url,
                data:{id:id},
                dataType: "JSON",
                type: "get",    
                success: function (data) {
                     if(data.error==false){
                       BootstrapDialog.alert({
                           title: 'Notificación',
                           message: ''+data.Message,
                           type: BootstrapDialog.TYPE_SUCCESS,
                           });
                           cargarDatosTabla();
                    }else{
                        BootstrapDialog.alert({
                           title: 'Notificación',
                           message:data.mensaje,
                           type: BootstrapDialog.TYPE_DANGER,

                       });
                    }
                },
                    error: function (data) {
                    BootstrapDialog.danger('Ocurrio un error al eliminar el Area, '+data);
                }
            }

        );
    }

   function confirmacion(btn,pCodigo) {
        BootstrapDialog.confirm({
            title: 'Confirmacion',
            type: BootstrapDialog.TYPE_DANGER,
            message:  '¿Seguro desea Eliminar esta Area?',
            callback: function (result) {
            if (result) {
                eliminar(btn,pCodigo);
            }
        }});
    }
    
    $(document).ready(function () {
      cargarDatosTabla();
      validarReg('registrationForm');
    });
    
      
      
      
 </script>
     @stop