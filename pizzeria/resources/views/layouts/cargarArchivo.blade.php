@extends('layouts.master')

@section('title', 'Cargar Archivo')
@section('bread','Restaurante  / Cargar Archivo')

@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
                              <!--panel de Agregar-->
          
             <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">                           
              <h3>  Cargar Archivo de excel <span><i style="color:#15346d;" class="glyphicon glyphicon-open" ></i></span>
                   </h3>  
                <p>Por favor seleccione el archivo que desee subir:</p>
                 <form name="idForm">
                  
                        <div class="col-md-6">
                            <input type="text" ng-model="nombre" style="width: 100%;" />
                        </div>
                        <div class="col-md-6">
                            <input  id="file" type="file" file-model="imagen"  style="width: 140px;">
                        </div>
                  
                </form>   
                <div  style="padding-left:75%;">
               <button class="btn" style="background-color:#15346d; color:white; margin-top: 30px;" onclick="validarFormP(idForm)">Subir Archivo</button>
               </form> 
            </div>                
            </div> 
        
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}  @stop
<script>
  function validarFormP(idForm,X){
        if(X!=0){
            $('#'+idForm).bootstrapValidator({
                fields: {
                  imagen: {
                             validators: {
                                     file: {
                                            extension: 'xls',
                                            type: 'xls',
                                            message: 'Por favor seleccione un archivo con extensión .xls (Excel)'
                                     }
                             }
                     }
                 }
            });
        }else{
            $('#'+idForm).bootstrapValidator({
                fields: {
                     imagen: {
                             validators: {
                                     notEmpty: {
                                             message: 'Se necesita un archivo'
                                     },
                                     file: {
                                            extension: 'xls',
                                            type: 'xls',
                                            message: 'Por favor seleccione un archivo con extensión .xls (Excel)'
                                     }
                             }
                     }
                 }
            });
        }
         if( $('#'+idForm).data('bootstrapValidator').validate().isValid()==true){
             //si el formulario esta bien validado mando a guardar
             var formData = new FormData(document.getElementById(idForm));
             if(X!=0){
                 formData.append('id',presentacionS);
                 save_method(X,formData);
             }else{
                 formData.append('id',productoS);
                 save_method(0,formData);
             }
         }else{
             //no es valido el forlario
         }
    }

    function save_method(id, nombre, tipo, valor){
            //var url = 'cargaArchivo/add';//ruta para agregar
            console.log('Holaaaa');
         /*   $.ajax({
                    url: url,
                    dataType: "json",
                    data:data,
                    type: "get",
                    success: function (data) {
                         if(data.error==false){
                            //si estoy editando oculto el panel de edicion. 
                            if(id!=0){
                             ocultardiv(id);
                             }else{
                                 //estoy insertando
                                 $('#PRegistrarPropina').hide();
                                 $('#BtnAPropina').show();
                             }
                            cargarDatosTabla();
                            formato('#data-tables');
                             BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                    error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Ocurrio un error al tratar de realizar la operacion',
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
            );*/
        } 



</script>
    