@extends('layouts.master')

@section('title', 'Metodos de pago')
@section('bread','Cargos  / Metodos de pagos')
@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
      {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent

                       

                            <div class="vd_content-section clearfix" style="padding-top: 0px;padding-left: 0px">
                                {!!Form::open(['route'=>'metodos_pago_add', 'method'=>'POST','files'=>true, 'id' => 'registrationForm'])!!}
                                       
                                    <!--Panel para agregar el nuevo metodo de Pago-->
                                    <div class="panel widget light-widget panel-bd-top " id="PDatosDocumentos" style="display:none">
                                        <div class="panel-body">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-hospital-o"></i> </span>Datos del Metodo de Pago</h3>
                                                <div class="vd_panel-menu">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica Campos Requeridos</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                <div class="row">
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="col-lg-4 form-group">
                                                        <div class="form-label">Nombre <span class="vd_red">*</span></div>
                                                        <input type="text" id="nombre" class="mgbt-xs-20 mgbt-sm-0 form-control" title="Se necesita un nombre" name="nombre" >
                                                    </div>
                                                    <div class="col-lg-4 form-group">
                                                        <div class="form-label">Detalles <span class="vd_red">*</span></div>
                                                        <input type="text" class="mgbt-xs-20 mgbt-sm-0 form-control" title="Se necesitan detalles" name="detalle" id="detalle">
                                                    </div>
                                                    <div class="col-lg-4 form-group">
                                                        <div class="form-label">Icono<span class="vd_red">*</span></div>
                                                        {!! Form::file('file', ['id' =>'file-1a', 'class' => 'mgbt-xs-20 mgbt-sm-0 form-control', 'data-show-upload'=>'false', 'data-preview-file-type'=>'any', 'data-initial-caption'=>"", 'data-overwrite-initial'=>"false"])!!}
                                                    </div>
                                                </div>
                                            <div class="mgtp-20">
                                                <div class="row mgbt-xs-0">
                                                    <div class="col-xs-6"></div>
                                                    <div class="col-xs-6 text-right">
                                                        <button id="BtnCMetodo" type="button" value="Enviar" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                            Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                        </button>

                                                        <button id="BtnGMetodo" type="submit" value="Enviar" class="btn vd_btn vd_bg-green">
                                                            <!--top-right-success-->
                                                            Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                            {!!Form::close()!!}
                                    <!--panel de resultados-->
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Metodos de Pagos.</h3>
                                                    <div class="vd_panel-menu">
                                                        <button id="BtnMetodo" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Agregar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-plus"></i> </button>
                                                    </div>
                                                   
                                                </div>
                                                <div class="panel-body table-responsive" style="padding: 15px;">
                                                    <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th class="celda center">Nombre</th>
                                                                <th class="celda center">Descripcion</th>
                                                                 <th class="celda center">Estado</th>
                                                                <th></th>
                                                                <th style="display:none;"></th>  

                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Panel Widget -->
                                        </div>
                                        <!-- col-md-12 -->
                                    </div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important; ">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                       <div class="panel-body">
                           <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                            <div class="form-group">
                               <div class="col-xs-12">
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Nombre:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="nombreCat"></label>
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Descripcion:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="descripcionCat"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Imagen:</label> 
                                   </div>
                                   <div class="col-xs-6" style="    padding-bottom: 10px;padding-left: 30px;"> 
                                       <img  WIDTH=50 HEIGHT=50 id='img' > 
                                   </div>
                                   <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">Fecha de Creaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaC"></label>
                                   </div>
                                    <div class="col-xs-6">
                                       <label class="col-xs-12 control-label">&Uacute;ltima modificaci&oacute;n:</label> 
                                   </div>
                                   <div class="col-xs-6"> 
                                       <label class="col-xs-12 control-label" id="fechaM"></label>
                                   </div>
                                  
                               </div>
                           </div>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>
                                    <!-- contenido -->

                                    <div id="error">
                                      </div>

</div>


                      


            
    
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        
        
        function save_method(id, formData){
            var url = 'metodos_pago/add';
           
            if (id !=0){
                url = 'metodos_pago/update'; 
            }
            $.ajax({
                    url: url,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data:formData,
                    type: "POST",
                    success: function (data) {
                    if(data['error']==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'El Metodo de Pago ha sido agregado exitosamente',
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           load_table();
                    }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data['mensaje'],
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                        
                    
                    },

                    error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al actualizar el descuento, '+data);
                    }
                }
            ).done(function(result){
                console.log(result);
            });
            
        } 
        var num=1;
        function load_table(){
            var url = 'metodos_pago/all';
            if (num==2){
                             $("#data-tables").DataTable().destroy();
                       }else{
                            num=2;
                       }
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "GET",
                    
                    success: function (data) {
                       $("#data-tables tbody").empty();
                       var row="";
                        for (var i = 0; i < data["modopago"].length; i++) {
                            row+='<tr class="odd gradeX" id="flotante1'+data["modopago"][i].idPago+'" value="'+data["modopago"][i].idPago+'">';
                             row+='  <td class="celda center">'+data["modopago"][i].tipoPago+'</td>';
                            row+='  <td class="celda center">'+data["modopago"][i].detalles+'</td>';
                             row+='  <td id="estadoD'+data["modopago"][i].idPago+'" class="celda center">';
                                 if(data["modopago"][i].estado==1){
                                     row+='<span class="label vd_bg-green">Activo</span>';
                                 }else{
                                      row+='<span class="label vd_bg-red">Inactivo</span>';
                                 }
                                 row+='</td>';
                            row+='  <td class="menu-action celda center" style="width: 131px;">';
                            row+='<a id="btnver" onclick="mostrar('+"'"+data["modopago"][i].imagen+"'"+','+"'"+data["modopago"][i].tipoPago+"'"+','+"'"+data["modopago"][i].detalles+"'"+','+"'"+data["modopago"][i].created_at+"'"+','+"'"+data["modopago"][i].updated_at+"'"+')"  class="btn menu-icon vd_bd-green vd_green">';
                            row+='<i data-original-title="Detalles"  data-toggle="tooltip" data-placement="top" class="fa fa-eye"></i>';
                            row+='</a>';
                            row+='      <a data-original-title="Editar" onclick="mostrardiv('+data["modopago"][i].idPago+')" id="btnEditar'+data["modopago"][i].idPago+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';
                    if(data["modopago"][i].estado==1){
                            row+='<span id="status">      <a data-original-title="Desactivar"  onclick="estado(this, 0, '+data["modopago"][i].idPago+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                                }else{
                                row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 1, '+data["modopago"][i].idPago+')" valor='+data["modopago"][i].idPago+' id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                                }
                            row+='      <a data-original-title="Eliminar" id="btnEliminar"    onclick="eliminar(this, '+data["modopago"][i].idPago+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                            row+='  </td>';
                            row+='  <td colspan="6" id="flotante2'+data["modopago"][i].idPago+'" style="display:none;">';
                            row+='   {!!Form::open(['route'=>'metodos_pago_updt', 'method'=>'POST','accept-charse'=>'UTF-8', 'novalidate'=>'novalidate','id' => 'registrationForm'])!!}'+
                                    '<input name="id" type="hidden" value="'+data["modopago"][i].idPago+'">'+
 
                                 '<div class="panel-body" style="border: 1px solid #ddd"> <div class="row">  <div class="col-lg-4">'+
                                               '<div class="form-label">Nombre<span class="vd_red">*</span></div>'+
                                             '    <input type="text" value="'+data["modopago"][i].tipoPago+'" id="nombre" class="mgbt-xs-20 mgbt-sm-0" title="Se necesita un nombre" name="nombre" required>'+
 '                                                           <small class="help-block" data-bv-validator="notEmpty" data-bv-for="nombre" data-bv-result="NOT_VALIDATED" style="display: none;">El nombre es requerido.</small></div>'+
 '                                              <div class="col-lg-4"> <div class="form-label">Detalles<span class="vd_red">*</span></div>    <input type="text" value="'+data["modopago"][i].detalles+'" class="mgbt-xs-20 mgbt-sm-0" title="Se necesitan detalles" name="detalle" id="detalle" required>'+
 '                                                                                                      </div>                                                    <div class="col-lg-4"> '+
 '                                               <div class="form-label">Icono<span class="vd_red">*</span></div> {!! Form::file('file', ['id' =>'file-1a', 'class' => 'mgbt-xs-20 mgbt-sm-0', 'data-show-upload'=>'false', 'data-preview-file-type'=>'any', 'data-initial-caption'=>"", 'data-overwrite-initial'=>'false'])!!}                                                   <small class="help-block" data-bv-validator="notEmpty" data-bv-for="valor" data-bv-result="NOT_VALIDATED" style="display: none;">Se necesita un valor.</small><small class="help-block" data-bv-validator="regexp" data-bv-for="valor" data-bv-result="NOT_VALIDATED" style="display: none;">Solo se permiten caracteres numericos.</small><small class="help-block" data-bv-validator="stringLength" data-bv-for="valor" data-bv-result="NOT_VALIDATED" style="display: none;">Please enter a value with valid length</small></div>                                                </div>              <div class="mgtp-20">                  <div class="row mgbt-xs-0">                      <div class="col-xs-6"></div>                      <div class="col-xs-6 text-right">                          <button type="reset" onclick="ocultardiv('+data["modopago"][i].idPago+')" class="btn vd_btn btn-default" style="color: #0E2145!important">                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>                          </button>                          <button type="submit" value="Mostrar" class="btn vd_btn vd_bg-green">                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>                          </button>                       </div>               </div>          </div>           </div>';
                            row+='       </form>';
                            row+='    </td>';
                            row+='   </tr>';
                             $("#data-tables tbody").append(row);
                             $('[data-toggle="tooltip"]').tooltip(); 
                             row="";
                            
                            }
                        $('#data-tables').dataTable( {

                   language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "Ningún dato disponible en esta tabla",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

               }

               );
                     validate();    
                       
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los metodos de pago');

                    }
                }
            );
        }
        
        function estado(btn, val, id){
            var url = 'metodos_pago/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",    
                    success: function (data) {
                        if(val==1){
                               $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+id+')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-green">Activo</span>');
                               $('[data-toggle="tooltip"]').tooltip();
                            }else{
                               $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, '+id+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-red">Inactivo</span>');
                               $('[data-toggle="tooltip"]').tooltip(); 
                           }
                    }
                }
                        
            );
        }
        function eliminar(btn,id){
            var url = 'metodos_pago/delete';
            $.ajax({
                    url: url,
                    data:{id:id,estado:0},
                    dataType: "JSON",
                    type: "get",    
                   success: function (data) {
                         if(data.error==false){
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                               load_table();
                               
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'No se pudo eliminar el descuento, error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al eliminar el descuento, '+data);
                    }
                }
                        
            );
        }
        
        function validate(){
            $('tbody form').bootstrapValidator({
                fields: {
                    nombre: {
			 validators: {
				 notEmpty: {
					 message: 'Debe colocar un nombre'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'El nombre comienza con letras y no contiene caractes especiales'
				 }
			 }
                    }
                }
            });
            
            $("tbody form #file-1a").fileinput({
                previewFileType: ["image", "text"],
                allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
                previewClass: "bg-warning"
            });
            
            $('tbody form').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                     var id = $(this).parents("tr").attr("value");
                    var formData = new FormData(this);
                    save_method(id, formData);
                    e.preventDefault();
                }
            });
        }
        
        //editar
        function mostrardiv(pCodigo, pDescripcion, pCodEstatus) {

            $(nombretxt).val(pDescripcion);
            var nombreDiv = 'flotante2' + pCodigo.toString();
            var nombretxt = '#txtNuevaDescripcion' + pCodigo.toString();
            var nombrediv1 = '#flotante1' + pCodigo.toString();
            var nombreCMB = '#cmbEstatus' + pCodigo.toString();

            $(nombretxt).val(pDescripcion);
            $(nombreCMB).val(pCodEstatus);
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }

        function mostrar(imagen,nombre,descripcion,created,update) {
            
            $('#myModal').modal('show'); 
           
            document.getElementById("Mtitulo").innerHTML='Detalles del metodo de pago:';
            document.getElementById("nombreCat").innerHTML=nombre;
            document.getElementById("descripcionCat").innerHTML=descripcion;
            document.getElementById('img').src = imagen;
            document.getElementById("fechaC").innerHTML=created;
            document.getElementById("fechaM").innerHTML=update;
        }
        function ocultardiv(codigo) {

            //var nombreboton = 'btnEditar' + codigo.toString();
            var nombretxt = '#txtNuevaDescripcion' + codigo.toString();
            var pnombre = 'flotante2' + codigo.toString();
            var nombrediv1 = '#flotante1' + codigo.toString();

            $(nombretxt).val('');
            //  $(nombreboton).style.display = "";
            div = document.getElementById(pnombre);
            div.style.display = "none";
            $(nombrediv1).children('.celda').removeClass('noprint');
            //boton = document.getElementById(nombreboton);
            //boton.style.display = "";

        }
        $(document).ready(function () {
            
             $("#BtnMetodo").on("click", function () {
                $('#PDatosDocumentos').show("slow");
            });
             $("#BtnCMetodo").on("click", function () {
                $('#PDatosDocumentos').hide("slow");
            });
            $("#file-1a").fileinput({
                previewFileType: ["image", "text"],
                allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
                previewClass: "bg-warning"
            });

            load_table();
            
            //para buscar las ciudades de ese pais al cargar la pagina
             $('#registrationForm').bootstrapValidator({
                fields: {
                    nombre: {
			 validators: {
				 notEmpty: {
					 message: 'Se requiere un nombre'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'Solo se permiten caracteres alfanumericos'
				 }
			 }
                    },
                    detalle: {
			 validators: {
				 notEmpty: {
					 message: 'Se requieren los detalles'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'Solo se permiten caracteres alfanumericos'
				 }
			 }
                    },
                    file: {
                         validators: {
                              notEmpty: {
					 message: 'Se requiere un icono'
				 },
                            file: {
                                    extension: 'jpeg,jpg,png',
                                    type: 'image/jpeg,image/png',
                                    message: 'Por favor seleccione una imagen valida'
                             }
                         }
                         }
                }
            });
            
            $('#registrationForm').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                     var formData = new FormData(document.getElementById("registrationForm"));
                    save_method(0, formData);
                    document.getElementById('registrationForm').reset();
                    e.preventDefault();
                }
            });
            
        });
        
       
    </script>
@stop
