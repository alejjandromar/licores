@extends('layouts.master')

@section('title', 'General')
@section('bread','Restaurant  / General')
@section('head_css')
@parent
{!! HTML::style('Recursos/fileinput/fileinput.css') !!}
{!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
{!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
{!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
<!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent



<div class="vd_content-section clearfix"  style="padding-top: 0px;padding-left: 0px">


    <div class="row" id="Ptabla">
        <div class="col-md-12">
            <div class="panel widget">
                <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Datos de la empresa</h3>
                    <div class="vd_panel-menu">  
                    </div>

                </div>
                <div class="panel-body table-responsive">
                    <form id="registrarD">
                        <div class="col-lg-6 form-group">
                            <div class="form-label">Nombre:<span class="vd_red">*</span></div>
                            <input type="text" id="nombre" class=" form-control" name="nombret"  maxlength="100">
                        </div> 
                        <div class="col-lg-6 form-group">
                            <div class="form-label">Rif:<span class="vd_red">*</span></div>
                            <input type="text" id="rif"   class=" form-control" name="rif" maxlength="20">
                        </div> 


                        <div class="col-lg-6 form-group">
                            <div class="form-label">Estado:<span class="vd_red">*</span></div>
                            <input type="text" id="estado"   class=" form-control" name="estado" maxlength="20">
                        </div> 


                        <div class="col-lg-6 form-group">
                            <div class="form-label">Ciudad:<span class="vd_red">*</span></div>
                            <input type="text" id="ciudad" class=" form-control" name="ciudad" maxlength="200">
                        </div> 


                        <div class="col-lg-6 form-group">
                            <div class="form-label">Dirección:<span class="vd_red">*</span></div>
                            <input type="text" id="direccion" class=" form-control" name="direccion" maxlength="200">
                        </div>

                        <div class="col-lg-6 form-group">
                            <div class="form-label">Telefono:<span class="vd_red">*</span></div>
                            <input type="text" id="telefono"  class=" form-control" name="telefono" maxlength="50">
                        </div> 
                        <div class="col-lg-6 form-group">
                            <div class="form-label">Moneda:<span class="vd_red">*</span></div>
                            <input type="text" id="moneda"  class=" form-control" name="moneda" maxlength="50">
                        </div> 
                        <div class="col-lg-6 form-group">
                            <div class="form-label">Simbolo:<span class="vd_red">*</span></div>
                            <input type="text" id="simbolo"  class=" form-control" name="simbolo" maxlength="50">
                        </div> 

                        <div class="col-lg-4 form-group text-right">
                            <br>
                            <button type="button" onclick="guardar()" id="save" value="Enviar" class="btn vd_btn vd_bg-green">
                                <!--top-right-success-->
                                Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                            </button>
                            <button type="button" onclick="mostrar()" title="Previsualizar" id="muestra" value="Enviar"  class="btn vd_btn vd_bg-green">
                                <!--top-right-success-->
                                <span class="menu-icon"><i class="fa fa-fw fa-info"></i></span>
                            </button>
                        </div> 
                    </form>
                </div>

            </div>
            <!-- Panel Widget -->
        </div>

        <!-- col-md-12 -->
    </div>

    <!-- contenido -->

    <div id="error">
    </div>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0px !important; padding-left: 0px !important; padding-right: 0px !important;  ">
                <div class="panel widget panel-bd-top vd_todo-widget light-widget">
                    <div class="panel-body">
                        <h3 class="mgbt-xs-20"> <span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Mtitulo"></span></h3>
                        <div class="vd_panel-menu">
                            <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <div style=" font-family: monospace; border: solid 1px; width: 350px; height: 300px;margin: auto;">
                                    <b> <span id=""  align="center">SENIAT</span><br></b>
                                    <span id="rifM" align="center"></span> <br>

                                    <span id="nombreM" align="center"></span><br>
                                    <div style="width: 270px;  margin: auto;">  <span  id="direccionM" ></span></div>

                                    <span id="ciudadM" align="center"></span> <span id="estadoM" align="center"></span> <br><br>
                                    <div class="col-xs-12 text-justify">
                                        DOCUMENTO POS:0000000 <br>
                                        Cliente: JOSE PEREZ  <br>
                                        Atendido por: JUAN RIVAS <br> 
                                    </div>
                                    <hr>
                                    <hr>
                                    <hr>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
@endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
<!--script especificos de esta pagina-->
<script type="text/javascript">

    function validarFormR() {
        //no existe el objeto bostrad validators para este formulario por lo tanto lo creo
        $('#registrarD').bootstrapValidator({
            fields: {
                nombret: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un nombre'
                        }
                    }
                },
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un nombre para la empresa'
                        }

                    }
                },
                direccion: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita una direccion'
                        }
                    }
                },
                rif: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un rIF.'
                        }
                    }
                },
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Se necesita un telefono.'
                        }
                    }
                }
            }
        });
    }
    validarFormR();
    var url = 'general/all';
    $.ajax({
        url: url,
        dataType: 'json',
        type: "GET",
        success: function (data) {
            $("#nombret").val(data.nombret);
            $("#nombret").css("font-weight", "bolder");
            $("#nombre").val(data.nombre);
            $("#nombre").css("font-weight", "bolder");
            $("#direccion").val(data.direccion);
            $("#direccion").css("font-weight", "bolder");
            $("#rif").val(data.rif);
            $("#rif").css("font-weight", "bolder");
            $("#telefono").val(data.telefono);
            $("#telefono").css("font-weight", "bolder");
            $("#moneda").val(data.moneda);
            $("#moneda").css("font-weight", "bolder");
            $("#simbolo").val(data.simbolo);
            $("#simbolo").css("font-weight", "bolder");

            $("#ciudad").val(data.ciudad);
            $("#ciudad").css("font-weight", "bolder");
            $("#estado").val(data.estado);
            $("#estado").css("font-weight", "bolder");
        },
        error: function () {
        }
    })



    function mostrar() {

        $('#myModal').modal('show');
        $("#nombretM").text($("#nombret").val());
        $("#nombreM").text($("#nombre").val());
        $("#direccionM").text($("#direccion").val());
        $("#rifM").text($("#rif").val());
        $("#telefonoM").text($("#telefono").val());
        $("#ciudadM").text($("#ciudad").val());
        $("#estadoM").text($("#estado").val());


    }


    function guardar() {
        if ($('#registrarD').data('bootstrapValidator').validate().isValid() == true) {
            var nombret, nombre, direccion, rif, telefono, moneda, simbolo, estado, ciudad;
            nombret = $("#nombret").val();
            nombre = $("#nombre").val();
            direccion = $("#direccion").val();
            rif = $("#rif").val();
            telefono = $("#telefono").val();
            moneda = $("#moneda").val();
            simbolo = $("#simbolo").val();
            ciudad = $("#ciudad").val();
            estado = $("#estado").val();
            var url = 'general/save';
            $.ajax({
                url: url,
                data: {
                    nombret: nombret,
                    nombre: nombre,
                    direccion: direccion,
                    rif: rif,
                    telefono: telefono,
                    moneda: moneda,
                    simbolo: simbolo,
                    ciudad: ciudad,
                    estado: estado


                },
                type: "GET",
                success: function (data)
                {
                    if (data.error == false) {
                        BootstrapDialog.alert({
                            title: 'Notificacion',
                            message: '¡Datos guardados con exito!',
                            type: BootstrapDialog.TYPE_SUCCESS});
                    } else {
                        BootstrapDialog.alert({
                            title: 'Notificacion',
                            message: data.mensaje,
                            type: BootstrapDialog.TYPE_SUCCESS});
                    }



                },
                error: function ()
                {
                    //alert("error");                      
                }
            });
        }
    }

</script>




@stop
