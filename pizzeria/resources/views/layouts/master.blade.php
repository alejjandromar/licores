<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
        <!-- Bootstrap & FontAwesome -->
         {!! HTML::style('Recursos/drag/jquery-ui.css') !!} 
        {!! HTML::script('Recursos/drag/jquery-1.10.2.js') !!} 
        {!! HTML::script('Recursos/drag/jquery-ui.js') !!} 
        
        {!! HTML::style('Recursos/bootstrap-3.3.4-dist/css/bootstrap.min.css') !!} 
        {!! HTML::style('Recursos/font-awesome-4.3.0/css/font-awesome.min.css') !!} 
        {!! HTML::style('Recursos/css/jquery-ui.custom.min.css') !!}
        <!--COMPONETE DE NOTIFICACIONES-->
        {!! HTML::style('Recursos/css/jquery.pnotify.css') !!} 
        <!--scroll-->
        {!! HTML::style('Recursos/css/jquery.mCustomScrollbar.css') !!}
         {!! HTML::style('Recursos/css/Nestable.css') !!}
        
        <!-- Tema -->
       {!! HTML::style('Recursos/css/theme.min.css') !!}
       {!! HTML::style('Recursos/css/custom.css') !!}
        <!-- Responsive CSS -->
        {!! HTML::style('Recursos/css/theme-responsive.min.css') !!}
        @yield('head_css')
        {!! HTML::script('Recursos/js/modernizr.js') !!} 
        @yield('head_scripts')
    </head>
    <body id="forms" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed      responsive    clearfix" data-active="forms " data-smooth-scrolling="1">
    <div class="vd_body">
        <!--àrea superior-->
        <header class="header-1" id="header">
            <div class="vd_top-menu-wrapper">
                <div class="container ">
                    <!--opciones responsive del menu-->
                    <div class="vd_top-nav vd_nav-width  ">
                        <div class="vd_panel-header">
                            <!--logo-->
                            <div class="logo">
                                <a href="home_admin">{{$restaurante->nombre}}</a>
                            </div>
                            <!-- logo -->
                            <!--opciones de para ocultar el menu-->
                            <div class="vd_panel-menu  hidden-sm hidden-xs" data-intro="
                                <strong>Minimizar la navegación izquierda</strong><br />Tamaño de navegación Toggle al tamaño medio o pequeño. Puede configurar tanto botón o un botón solamente. Ver opción completa en la documentación" data-step=1>
                                <span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Toggle Nav media" data-action="nav-left-medium">
                                    <i class="fa fa-bars"></i>
                                </span>

                                <span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-original-title="Pequeño Nav Toggle" data-action="nav-left-small">
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>

                            </div>
                            <!--opciones de para ocultar el menu-->
                            <!--opciones del menu movil-->
                            <div class="vd_panel-menu left-pos visible-sm visible-xs">

                                <span class="menu" data-action="toggle-navbar-left">
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>


                            </div>
                            <div class="vd_panel-menu visible-sm visible-xs">
                                <span class="menu visible-xs" data-action="submenu">
                                    <i class="fa fa-bars"></i>
                                </span>
                            </div>
                            <!--opciones del menu movil-->
                          
                        </div>
                   

                    </div>
                    <!--opciones responsive del menu-->
                    <!--Barra superior-->
                   <div class="vd_container">
                        <div class="row">
                         <div class="col-sm-7 col-xs-12 col-lg-offset-5">
                                <div class="vd_mega-menu-wrapper">
                                   <div class="vd_mega-menu pull-right">
                                        <ul class="mega-ul">
<!--                                            áreas de notificacion o recordatorio global-->
<!--                                            <li id="top-menu-3" class="one-icon mega-li">
                                                <a href="index.html" class="mega-link" data-action="click-trigger">
                                                    <span class="mega-icon"><i class="fa fa-globe"></i></span>
                                                    <span class="badge vd_bg-red">51</span>
                                                </a>
                                                <div class="vd_mega-menu-content  width-xs-3 width-sm-4  center-xs-3 left-sm" data-action="click-target">
                                                    <div class="child-menu">
                                                        <div class="title">
                                                            Notificaciones
                                                            <div class="vd_panel-menu">
                                                                <span data-original-title="Notification Setting" data-toggle="tooltip" data-placement="bottom" class="menu">
                                                                    <i class="fa fa-cog"></i>
                                                                </span>
                                                              
                                                            </div>
                                                        </div>
                                                        <div class="content-list">
                                                            <div data-rel="scroll">
                                                                <ul class="list-wrapper pd-lr-10">
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></div>
                                                                            <div class="menu-text">
                                                                                Someone has give you a surprise
                                                                                <div class="menu-info"><span class="menu-date">12 Minutes Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_blue"><i class=" fa fa-user"></i></div>
                                                                            <div class="menu-text">
                                                                                Change your user profile details
                                                                                <div class="menu-info"><span class="menu-date">1 Hour 20 Minutes Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_red"><i class=" fa fa-cogs"></i></div>
                                                                            <div class="menu-text">
                                                                                Your setting is updated
                                                                                <div class="menu-info"><span class="menu-date">12 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_green"><i class=" fa fa-book"></i></div>
                                                                            <div class="menu-text">
                                                                                Added new article
                                                                                <div class="menu-info"><span class="menu-date">19 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_green"><img alt="example image" src="Recursos/img/avatar.jpg"></div>
                                                                            <div class="menu-text">
                                                                                Change Profile Pic
                                                                                <div class="menu-info"><span class="menu-date">20 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_red"><i class=" fa fa-cogs"></i></div>
                                                                            <div class="menu-text">
                                                                                Your setting is updated
                                                                                <div class="menu-info"><span class="menu-date">12 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_green"><i class=" fa fa-book"></i></div>
                                                                            <div class="menu-text">
                                                                                Added new article
                                                                                <div class="menu-info"><span class="menu-date">19 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#">
                                                                            <div class="menu-icon vd_green"><img alt="example image" src="Recursos/img/avatar.jpg"></div>
                                                                            <div class="menu-text">
                                                                                Change Profile Pic
                                                                                <div class="menu-info"><span class="menu-date">20 Days Ago</span></div>
                                                                            </div>
                                                                        </a>
                                                                    </li>

                                                                </ul>
                                                            </div>
                                                            <div class="closing text-center" style="">
                                                                 <a href="#">Ver todas <i class="fa fa-angle-double-right"></i></a>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>   
                                            </li>
                                            áreas de notificacion o recordatorio global
                                            áreas de opciones del usuario, deitr perfil etc-->
                                            <li id="top-menu-profile" class="profile mega-li">
                                                <a href="#" class="mega-link" data-action="click-trigger">
                                                    <span class="mega-image">
                                                        <img src="Recursos/img/avatar.jpg" alt="example image" />
                                                    </span>
                                                    <span class="mega-name">
                                                        {!!  $user->name!!} ({!!  $user->tipo!!}) <i class="fa fa-caret-down fa-fw"></i>
                                                    </span>
                                                </a>
                                                <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
                                                    <div class="child-menu">
                                                        <div class="content-list content-menu">
                                                            <ul class="list-wrapper pd-lr-10">
                                                           
                                                                
                                                                <li> <a href="logout"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>  <div class="menu-text">Salir</div> </a> </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </li>
<!--                                            áreas de opciones del usuario, deitr perfil etc-->
                                            
                                        </ul>
                                  
                                </div>
                            </div>
                            </div>

                        </div>
                    </div>
                    <!--Barra superior-->
                </div>
           
            </div>
       

        </header>
        <!--àrea superior-->
       <!--contenido global-->
        <div class="content">
            <div class="container">
                <!--contenido del menu-->
                <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left"><!--area del menu principal-->
                    <div class="navbar-menu clearfix">

                        <div class="vd_menu">
                           <ul>      
                                <li>
                                    <a href="javascript:void(0);" data-action="click-trigger">
                                        <span class="menu-icon"><i class="fa fa-bell"></i></span>
                                        <span class="menu-text">Menú</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                    </a>
                                    <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                <a href="Categorias">
                                                    <span class="menu-text">Categorias</span>
                                                </a>
                                            </li>
                                           
                                            <li>
                                                <a href="Productos">
                                                    <span class="menu-text">Productos</span>
                                                </a>
                                            </li>

                                            <li>
                                                <a href="Modificadores">
                                                    <span class="menu-text">Modificadores y Extras</span>
                                                </a>
                                            </li>
                                            

                                        </ul>
                                    </div>
                                </li>                                
                                <li>
                                    <a href="javascript:void(0);" data-action="click-trigger">
                                        <span class="menu-icon"><i class="fa fa-credit-card"></i></span>
                                        <span class="menu-text">Cargos</span>
                                        <span class="menu-badge">
                                            <span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span>
                                        </span>
                                    </a>
                                    <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                     <a href="Impuestos">
                                                    <span class="menu-text">Impuestos</span>
                                                </a>
                                            </li>
                                            <li>
                                                     <a href="Propinas">
                                                    <span class="menu-text">Propinas</span><span class="menu-badge"></span>
                                                </a>
                                            </li>
                                            <li>
                                                     <a href="Descuentos">
                                                    <span class="menu-text">Decuentos</span><span class="menu-badge"></span>
                                                </a>
                                            </li>
                                            <li>
                                                     <a href="Metodos-De-Pago">
                                                    <span class="menu-text">Metodos de pago</span><span class="menu-badge"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" data-action="click-trigger">
                                        <span class="menu-icon"><i class="fa fa-plus"></i></span>
                                        <span class="menu-text">Restaurante</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                    </a>
                                    <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                     <a href="datos_general">
                                                    <span class="menu-text">General</span>
                                                </a>
                                            </li>
                                            <li>
                                                     <a href="areas">
                                                    <span class="menu-text">Areas</span>
                                                </a>
                                            </li>
                                            <li>
                                        <a href="javascript:void(0);" data-action="click-trigger">   
                                        <span class="menu-text">Mesas</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>

                                        </a>
                                            <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                <a href="adm_mesas">
                                                <span class="menu-text">Administrar Mesas</span>
                                                </a>
                                                 <a href="crear_plano">
                                                    <span class="menu-text">Crear Plano</span>
                                                </a>
                                            </li>
                                            </ul>
                                            </div>

                                            </li>
                                             <li>
                                                 <a href="caja">
                                                    <span class="menu-text">Caja</span>
                                                    <span class="menu-badge"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" data-action="click-trigger">
                                        <span class="menu-icon"><i class="fa fa-money"> </i></span>
                                        <span class="menu-text">Estadisticas y Reportes</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                    </a>
                                    <div class="child-menu" data-action="click-target">
                                        
                                        <ul class="clearfix">
                                           <!-- <li>
                                                     <a href="#">
                                                    <span class="menu-text">Clientes</span>
                                                </a>
                                            </li> OPCION DE CLIENTE PROXIMANET  YOSCAR -->
                                           <li>
                                            <a href="javascript:void(0);" data-action="click-trigger">   
                                        <span class="menu-text">Ventas</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>

                                        </a>
                                            <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                 <a href="ventas_g">
                                                    <span class="menu-text">Ventas</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="cliente_g">
                                                    <span class="menu-text">Ventas por cliente</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="producto_g">
                                                    <span class="menu-text">Ventas por Productos</span>
                                                 </a>
                                            </li>
                                             <li>
                                                 <a href="prensentacion_g">
                                                    <span class="menu-text">Ventas por Presentacion</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="categoria_g">
                                                    <span class="menu-text">Ventas por Categoria</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="mesero_g">
                                                    <span class="menu-text">Ventas por Cajero</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="mesero_g">
                                                    <span class="menu-text">Ventas por Mesero</span>
                                                 </a>
                                            </li>
                                            
                                            <li>
                                                 <a href="pagos_g">
                                                    <span class="menu-text">Ventas por Pago</span>
                                                 </a>
                                           
                                            </li>
                                             <li>
                                                 <a href="Ventas-Por-Impuestos">
                                                    <span class="menu-text">Ventas por Impuestos</span>
                                                 </a>
                                            </li>
                                            <li>
                                                 <a href="Ventas-Diarias-Por-Impuestos">
                                                    <span class="menu-text">Ventas Diarias por Impuestos</span>
                                                 </a>
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                            <li>
                                                 <a href="Ordenes-canceladas">
                                                    <span class="menu-text">Ordenes Canceladas</span>
                                                 </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <a href="javascript:void(0);" data-action="click-trigger">
                                        <span class="menu-icon"><i class="fa fa-gear"></i></span>
                                        <span class="menu-text">Configuración</span>
                                        <span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
                                    </a>
                                    <div class="child-menu" data-action="click-target">
                                        <ul>
                                            <li>
                                                     <a href="empleado">
                                                    <span class="menu-text">Gestión de Usuarios</span>
                                                </a>
                                            </li>
                                            <li>
                                                     <a href="impresora">
                                                    <span class="menu-text">Impresoras</span>
                                                </a>
                                            </li>
                                            
                                            
                                        </ul>
                                    </div>
                                </li>
                           <!-- <li>
                                <a href="mesas">
                                <span class="menu-icon"><i class="glyphicon glyphicon-cutlery"></i></span>
                                    <span class="menu-text">Ir a la Aplicación</span>
                                </a>

                            </li>
                             <li>
                                <a href="cargarArchivo">
                                <span class="menu-icon"><i class="glyphicon glyphicon-open"></i></span>
                                    <span class="menu-text">Cargar Excel</span>
                                </a>

                            </li> -->
                            
                            </ul>
                     
                        </div>
                    </div>
                    <!--area del menu principal-->

                    <!--seepacion entre div-->
                    <div class="navbar-spacing clearfix">
                    </div>
                    <!--seepacion entre div-->
                    <!--opcion salir del sistema-->
                    <div class="vd_menu vd_navbar-bottom-widget">
                        <ul>
                            
                            <li>
                               <a href="logout">
                                    <span class="menu-icon"><i class="fa fa-sign-out"></i></span>
                                    <span class="menu-text">Cerrar Sesión</span>
                                </a>

                            </li>
                        </ul>
                    </div>
                    <!--opcion salir del sistema-->
                </div> 
                <!--contenido del menu-->
                <!--cuerpo del con tenido interno de la pàgina-->
                <div class="vd_content-wrapper">
                    <div class="vd_container">
                        <div class="vd_content clearfix">
                            <!--opciones internas del contenido de la página-->
                            <div class="vd_head-section clearfix">
                               
                                <div class="vd_panel-header">
                                    <!--área de la miga de pan y titula de la página-->
                                   <ul class="breadcrumb">
                                        <li><a href="#">Inicio</a> </li>
                                        <li class="active">@yield('bread')</li>
                                    </ul>
                                    <!--área de la miga de pan y titula de la página-->
                                    <!--área de las opcionesde expandir el à`rea de trabjo-->
                                    <div class="vd_panel-menu hidden-sm hidden-xs" data-intro=">
                                        <strong>Expandir control</strong><br />Para ampliar la página de contenido horizontal, vertical , o ambos . Si sólo tiene un botón simplemente quitar el otro código del botón." data-step=5  data-position="left">
                                        <div data-action="remove-navbar" data-original-title="Retire la barra de navegación Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-navbar-button menu"> <i class="fa fa-arrows-h"></i> </div>
                                        <div data-action="remove-header" data-original-title="Retire Top Menu Toggle" data-toggle="tooltip" data-placement="bottom" class="remove-header-button menu"> <i class="fa fa-arrows-v"></i> </div>
                                        <div data-action="fullscreen" data-original-title="Retire la barra de navegación y el menú principal Toggle" data-toggle="tooltip" data-placement="bottom" class="fullscreen-button menu"> <i class="glyphicon glyphicon-fullscreen"></i> </div>

                                    </div>
                                    <!--área de las opcionesde expandir el à`rea de trabjo-->

                                </div>
                              
                            </div>
                            <!--opciones internas del contenido de la página-->
                  
                            <!--àrea destinada para colocar el contenido de la pàgina-->
                            <div class="vd_content-section clearfix" style="padding-right: 10px;">
                                @yield('contenido')
                            </div>
                            <!--àrea destinada para colocar el contenido de la pàgina-->

                        </div>

                    </div>

                </div>
                <!--cuerpo del con tenido interno de la pàgina-->

            </div>
            <!-- .container -->
        </div>
        <!--contenido global-->
       

     </div>
       
    <!-- Situado al final del documento para que las páginas se cargan más rápido -->
    {!! HTML::script('Recursos/js/jquery.js') !!} 
    {!! HTML::script('Recursos/js/jquery-1.10.2.min.js') !!} 
    {!! HTML::script('Recursos/bootstrap-3.3.4-dist/js/bootstrap.min.js') !!}
    {!! HTML::script('Recursos/js/jquery-ui.custom.min.js') !!}
    
     <!--js para las notificaciones-->
     {!! HTML::script('Recursos/js/jquery.pnotify.min.js') !!} 
     <!--js para los scroll-->
     {!! HTML::script('Recursos/js/jquery.mCustomScrollbar.concat.min.js') !!} 
     {!! HTML::script('Recursos/js/jquery.tagsinput.min.js') !!} 
     {!! HTML::script('Recursos/js/bootstrap-switch.min.js') !!}
     <!--js para los botones de expandir el menu-->
    {!! HTML::script('Recursos/js/breakpoints.js') !!}
    <!--js del tema-->
    {!! HTML::script('Recursos/js/theme.js') !!}
    
    @yield('scripts')
     </body>
</html>