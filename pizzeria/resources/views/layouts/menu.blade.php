<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" ng-app="myApp" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" ng-app="myApp" class="no-js"> <!--<![endif]-->
    <head>        
        <!-- META SECTION -->
        <title>CHELA PLUS</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="5864logo.ico" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="joli/css/theme-default.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="css/css/ionicons.css"/>
        <link rel="stylesheet" type="text/css" id="theme" href="css/style.css"/>
        <link rel="stylesheet" type="text/css" href="joli/css/personales.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body ng-controller="generarCtrl">
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="#dashboard">CHELA PLUS</a>
                    </li>
                    <li class="xn-profile">
                        <a href="joli/#" class="profile-mini">
                            <img src="imagenes/logo.png" alt="Dolphin"  style="border: none; width: 38px"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image" >
                                <img src="imagenes/logo.png" alt="Dolphin" style="border: none; width: 60%"/>
                            </div>
                           

                            
                            <!--<div class="profile-controls">
                                <a href="joli/pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="joli/pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>-->
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Opciones</li>
                    
                     <li class="xn-openable active">
                        <a href="#"><span class="fa fa-coffee"></span> Ordenes</a>
                        <ul>                                    
                            <li><a href="#/ordenes"><span class="fa fa-circle-o"></span> Ver Ordenes</a></li>
                        </ul>
                    </li>
                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-coffee"></span> Men&uacute;</a>
                        <ul>                                    
                            <li><a href="#/categoria"><span class="fa fa-circle-o"></span> Categoria</a></li>
                            <li><a href="#/producto"><span class="fa fa-circle-o"></span>Productos</a></li>
							 <li><a href="#/cupon"><span class="fa fa-circle-o"></span>Cupones</a></li>
                        </ul>
                    </li>
                    
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-cutlery"></span> Empresa</a>
                        <ul>                                    
                            <li><a href="#/general"><span class="fa fa-circle-o"></span> Datos</a></li>
                        </ul>
                    </li>
                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-cogs"></span> Configuracion</a>
                        <ul>                                    
                            <li><a href="#/empleado"><span class="fa fa-circle-o""></span> Gesti&oacute;n de Usuario</a></li>
                        </ul>
                    </li>
					
					<li class="xn-openable">
                                            <a href="#" ><span class="fa fa-cogs" ></span> Reportes</a>
                        <ul>                                    
                            <li><a href="#"><span class="fa fa-circle-o"></span> Ventas              </a></li>
							<li><a href="#"><span class="fa fa-circle-o"></span> Ventas por clientes</a></li>
							<li><a href="#"><span class="fa fa-circle-o"></span> Ventas por productos</a></li>
							<li><a href="#"><span class="fa fa-circle-o"></span> Ventas por categoria</a></li>
							<li><a href="#"><span class="fa fa-circle-o"></span> Ventas por tipo de pago</a></li>
							<li><a href="#"><span class="fa fa-circle-o"></span> Ventas por canceladas</a></li>
                        </ul>
                    </li>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="joli/#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->

                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="joli/#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                    <!-- MESSAGES -->

                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="joli/#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">


                    <!-- Content Wrapper. Contains page content -->
                    <div class="content-wrapper">

                        <div ng-view></div>
                    </div><!-- /.content-wrapper -->	


                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container" style="width: 50%;left: 25%;">
                <div class="mb-middle" style="width: 100%;left: 0%;">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Cerrar sesi&oacute;n?</div>
                    <div class="mb-content">
                        <p>&iquest;Est&aacute;s seguro que desea salir del sistema?</p>                    
                        <p>Presione no para continuar en el sistema. Precione Si para salir del sistema.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="logout_admin" class="btn btn-success btn-lg">Si</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->
		
		<!-- MESSAGE BOX-->
        <!-- alert -->
        <div ng-controller="generarCtrl" class="message-box @{{error.class}} animated fadeIn" id="alerta">
            <div class="mb-container" style="width: 50%;left: 25%;">
                <div class="mb-middle" style="width: 100%;left: 0%;">
                    <div class="mb-title"><span class="fa @{{error.icono}}"></span> @{{error.titulo}}</div>
                    <div class="mb-content">
                        <p>@{{error.mensaje}}</p>
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default btn-lg pull-right mb-control-close" ng-click="cerraAlert('alerta')">Ok</button>
                    </div>
                </div>
            </div>
        </div>	
		
		<!--  comfir-->
		<div ng-controller="generarCtrl" class="message-box @{{confirm.class}} animated fadeIn" id="confirm">
			<div class="mb-container" style="width: 50%;left: 25%;">
				<div class="mb-middle" style="width: 100%;left: 0%;">
					<div class="mb-title"><span class="fa @{{confirm.icono}}"></span> @{{confirm.titulo}}</div>
					<div class="mb-content">
						<p>@{{confirm.mensaje}}</p>   
					</div>
					<div class="mb-footer">
						<div class="pull-right">
							<a class="btn btn-success btn-lg" ng-click="intermediaria()">Si</a>
							<button class="btn btn-default btn-lg" ng-click="cerraConfirm('confirm');">No</button>
						</div>
					</div>
				</div>
			</div>
		</div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="joli/audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="joli/audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  


        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="joli/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->


        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='joli/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="joli/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

        <script type="text/javascript" src="joli/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="joli/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="joli/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src='joli/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='joli/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src='joli/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
        <script type="text/javascript" src="joli/js/plugins/owl/owl.carousel.min.js"></script>                 

        <script type="text/javascript" src="joli/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/daterangepicker/daterangepicker.js"></script>
		<script type="text/javascript" src="joli/js/plugins/bootstrap/bootstrap-select.js"></script>

        <script type="text/javascript" src="joli/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/dropzone/dropzone.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/fileinput/fileinput.min.js"></script>
        <!-- END THIS PAGE PLUGINS-->        
		<audio id="audio-fail" src="joli/audio/fail.mp3" preload="auto"></audio>

        <!-- START TEMPLATE -->

        <script type="text/javascript" src="joli/js/plugins.js"></script>        
        <script type="text/javascript" src="joli/js/actions.js"></script>

        <!-- END TEMPLATE -->

        <script type="text/javascript" src="joli/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="joli/js/plugins/bootstrap/bootstrap-select.js"></script>		
        <!-- END PAGE PLUGINS -->

        <!-- START TEMPLATE -->

        <!--Angular zone-->
        <script src="joli/bower_components/angular/angular.js"></script>
        <script src="joli/bower_components/angular-route/angular-route.js"></script>
        <!--Angular-->
        <script src="joli/app.js"></script>
		<script src="joli/js/general.js"></script>
        <script src="joli/js/dashboard.js"></script>
		<script src="joli/js/reportes.js"></script>
        <script src="joli/js/ordenes.js"></script>


        <script src="joli/components/version/version.js"></script>
        <script src="joli/components/version/version-directive.js"></script>
        <script src="joli/components/version/interpolate-filter.js"></script>

        <!-- END SCRIPTS -->         
    </body>
</html>






