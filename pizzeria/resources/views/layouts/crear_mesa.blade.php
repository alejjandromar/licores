@extends('layouts.master')

@section('title', 'Crear plano')
@section('bread','Restaurante  / Mesas / Crear plano')

@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina //kkdkdkdkdkds//-->   
@stop
@section('contenido')
    

  <style>
  .draggable {
      width: 60px;
      height: 50px;
    padding: 0.5em;
      float: left;
      margin: 0 1px 1px 0;
  }
  #draggable, #draggable2 {
      margin-bottom:20px;
  }
  #draggable {
      cursor: n-resize;
  }
  #draggable3 {
      cursor: move;
  }
  #containment-wrapper {
       
    
      height: 500px;
      width: 650px;
      border:1px solid #000;
      padding: 5px;

   
  }
   
  
  </style>

 
                                                     
     <div class="col-lg-9">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Ubicar mesas.</h3>
            <div class="vd_panel-menu">
              <button id="Btnpos" onclick="position()" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Restablecer Posición" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-refresh"></i> </button>
             </div>
            </div>          
            <div class="panel-body table-responsive">

                       <div  id="containment-wrapper" >
                            @foreach ($mesa as $mesas)
                            <div id="area{{$mesas->Area}}" class="area_mesa">
                                <div id="draggable3{{$mesas->idMesa}}" onclick="ver({{$mesas->idMesa}},
                                    @foreach ($area as $areas)
                                        @if ($areas->idArea== $mesas->Area) 
                                            '{{$areas->nombre}}'
                                        @endif @endforeach)" class="draggable" >
                                                                            
                                        <span style="position: absolute; top: 15px; left: 12px;" id="numero{{$mesas->idMesa}}" class="label label-primary">{{$mesas->idMesa}}</span></span>
                                        <img src="img/11.gif" width="60" height="40"></img>
                                                                            
                                </div>
                            </div>
                            @endforeach
                        </div>
                                

            </div>
        </div>
    </div>

    <!--tabla presentacion-->
    <div class="col-lg-3">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-th"></i> </span>Información</h3>
                        <div class="vd_panel-menu">

                        </div>
            </div>

            <div class="panel-body table-responsive">
                <select id="area_select" onchange="cambiar_plano()">
                    @foreach ($area as $areas)
                        <option value="{{$areas->idArea}}">{{$areas->nombre}}</option> 
                    @endforeach
                </select>
                <a id="mesa" >Mesa seleccionada:</a>
                <a id="area">Pertenece al area:</a>
                      <center></center>
                      <p align="center" id="msj"></p>
            </div>
        </div>
    </div>

                                         






<script type="text/javascript">


var x={{$mesas->idMesa}};




get_coor();


  function ver(x,y){
var u={{$mesas->idMesa}};
    $("#mesa").text('Mesa seleccionada: '+x); 
    $("#msj").text('¡Escoja la ubición!'); 
    $("#area").text('Pertenece al area: '+y);   
    $("#mesa").css("font-weight","bolder");
    $("#area").css("font-weight","bolder");  
    for (i = 0; i <= u; i++) {
        $("#numero"+i).css("border","");

    }
     $("#numero"+x).css("border","solid 1px Orange");

}

function cambiar_plano(){
    var area= $("#area_select").val();
    $("div .area_mesa").hide();
    $("div #area"+area).show();
    
}
cambiar_plano();
function position(){
var url = 'mesa/delete';  
        $.ajax({
           url: url,               
           type: "GET",
           success: function (data) 
           {                      
             document.location.reload();
           },

           error: function () 
           {
            //alert("error");                      
           }
             })
            
}


 function save_coor(coor){
   var url = 'mesa/coor';  
        $.ajax({
           url: url,               
           data:{coor:coor},
           type: "GET",
           success: function (data) 
           {                      
             // alert(coor);
           },

           error: function () 
           {
            //alert("error");                      
           }
             })
            
 }         


function get_coor(){
  var url = 'mesa/coor_get';
   $.ajax({
        url: url,
        dataType: 'json',
        processData: false,
        contentType: false,
        async: false,
        type: "GET",
       success: function (data) {                    
         var sPositions = data.linea || "{}",
          positions = JSON.parse(sPositions);
         for (i = 0; i <= x; i++) {
             $("#draggable3"+i).draggable({
                containment: "#containment-wrapper",
                scroll: false,
                stop: function (event, ui) 
                {
                 positions[this.id] = ui.position
                 save_coor(JSON.stringify(positions));
                }

             });
                        
         }
       $.each(positions, function (id, pos) {
         $("#" + id).css(pos)
        })
          },
         error: function () {
         alert("error");                      
         }
        })            
} 


  </script>


@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
 
     

     @stop

  