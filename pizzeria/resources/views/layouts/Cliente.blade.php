@extends('layouts.master')

@section('title', 'Gestión de usuarios')
@section('bread', 'Configuracion / Gestión de usuarios')

@section('head_css')
@parent
    
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent

                       

                            <div class="vd_content-section clearfix">
                                 {!!Form::open(['route'=>'impresora_add', 'method'=>'POST', 'id' => 'registrationForm'])!!}          
                               
                                    <!-- contenido -->
                                    <!--panel de filtros-->
<!--                                    <div class="panel widget">
                                        <div class="panel-heading vd_bg-grey">
                                            <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-user"></i> </span> Empleados </h3>
                                        </div>
                                       
                                                
                                        <div class="panel-body Margin-not">
                                            <div class="row">
                                                <div class=" col-lg-9 col-md-9 col-sm-9">
                                                    
                                                </div>
                                                 
                                                <div class=" col-lg-3 col-md-3 col-sm-3">
                                                    <br /> <button id="BtnDocumentos" class="btn vd_btn vd_bg-red btn-block" type="button">Empleado <i class="fa fa-plus fa-fw"></i></button>
                                                    <br />
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>-->

                                    
                                            
                                    <!--panel de resultados-->
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Cliente</h3>
                                                    <div class="vd_panel-menu">
                                                        
                                                    </div>
                                                </div>
                                                <div class="panel-body table-responsive">
                                                    <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th class="celda center">dni</th>
                                                                <th class="celda center">Nombre</th>
                                                                <th class="celda center">Teléfono</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <!-- Panel Widget -->
                                        </div>
                                        <!-- col-md-12 -->
                                    </div>

                                    <!-- contenido -->



</div>


                        </div>

                    </div>

                </div>
                <div id="error"></div>


            
    
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        
       

        function load_table(){
            var url = 'cliente/all';
           
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    
                    success: function (data) {
                      
                       $("#data-tables tbody").empty();
                       var row="";
                       console.log(data["Personas"]);
                        for (var i = 0; i < data["Personas"].length; i++) {
                            row+='<tr class="odd gradeX" id="flotante1'+data["Personas"][i].dni+'" value="'+data["Personas"][i].id+'">';
                            row+='  <td class="celda center">'+data["Personas"][i].dni+'</td>';
                            row+='  <td class="celda center">'+data["Personas"][i].nombreCompleto+'</td>';
                            row+='  <td class="celda center">'+data["Personas"][i].telefono+'</td>';
                            row+='  <td class="menu-action celda center">';
                            row+='   </tr>';
                             $("#data-tables tbody").append(row);
                             row="";
                            
                            }
                            
                            alert("error");
                            
                        $('#data-tables').dataTable( {
                               
                   language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "Ningún dato disponible en esta tabla",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

               }

               );
       alert("error");
                     
                       
                    },

                    error: function () {
                         BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los clientes');
                    }
                }
            );
            
      
        }
         $(document).ready(function () {
            $("#BtnEmpleado").on("click", function () {
                $('#PDatosDocumentos').show("slow");
            });
            
            load_table();
            
            $("#BtnCMetodo").on("click", function () {
                $('#PDatosDocumentos').hide("slow");
            });
           
            
            
        });
        
        
       
    </script>
@stop
