<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
        <!-- Bootstrap & FontAwesome -->
        {!! HTML::style('Recursos/bootstrap-3.3.4-dist/css/bootstrap.min.css') !!} 
        {!! HTML::style('Recursos/font-awesome-4.3.0/css/font-awesome.min.css') !!} 
        {!! HTML::style('Recursos/css/jquery-ui.custom.min.css') !!}
        <!--COMPONETE DE NOTIFICACIONES-->
        {!! HTML::style('Recursos/css/jquery.pnotify.css') !!} 
        <!--scroll-->
        {!! HTML::style('Recursos/css/jquery.mCustomScrollbar.css') !!}
        <!-- Tema -->
        {!! HTML::style('Recursos/css/theme.min.css') !!}
        {!! HTML::style('Recursos/css/custom.css') !!}
        <!-- Responsive CSS -->
        {!! HTML::style('Recursos/css/theme-responsive.min.css') !!}
        {!! HTML::script('Recursos/js/modernizr.js') !!} 
        {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
        
        <link rel="stylesheet" type="text/css" id="theme" href="css/style.css"/>
		 <link rel="stylesheet" type="text/css" id="theme" href="css/styles.css"/>
        <style>
            .bootstrap-dialog .bootstrap-dialog-message {
                font-size: 14px;
                color: black;
}
        </style> 
    </head>
    <body class="vd_navbar">
        <div  style="height: 100%"><!--area del menu principal-->
            <div class=" clearfix">
                
                <div class="">
                    {!! Form::open(array('route' => 'auth', 'class' => 'col-lg-offset-4 col-lg-4 col-md-offset-4 col-md-6 col-sm-offset-4 col-sm-4', 'style' => 'padding-top:  10%')) !!}
                   
                        <h1 class="text-center text-uppercase"> <img src="img/Chela+-(Horizontal-Blanco-Azul-Amarillo).png"></h1>
                       </br>
                          <p> <input type="text" class="form-control input-lg" placeholder="Usuarioo" name="email"></p><br>
                        <p> <input type="password" class="form-control input-lg" placeholder="Contraseña" name="pass"></p><br>
                        <div class=" col-sm-8 col-xs-offset-3 col-sm-offset-3 col-md-offset-4">
                            <input type="submit" class="btn vd_btn vd_bg-red input-lg" value="Iniciar Sesión"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
    {!! HTML::script('Recursos/js/jquery.js') !!} 
    {!! HTML::script('Recursos/js/jquery-1.10.2.min.js') !!} 
    {!! HTML::script('Recursos/bootstrap-3.3.4-dist/js/bootstrap.min.js') !!}
    {!! HTML::script('Recursos/js/jquery-ui.custom.min.js') !!}
 !}
    {!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
                <script>
                    $(document).ready(function () {
                        @if (session('error'))
                             BootstrapDialog.alert({
                               title: 'Error',
                               message: '{{ session('error') }}',
                               type: BootstrapDialog.TYPE_ERROR,

                           });
                        @endif
                    });
                   
                    
                    
                </script>
                