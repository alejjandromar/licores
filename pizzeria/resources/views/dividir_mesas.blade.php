@extends('master')

@section('title', 'Dividir Mesas')
<html ng-app="mySuperApp">
    @section('head_css')
     @parent
    <!--csss especificos de esta pagina-->
    <style type="text/css"> 
        .footerM{display: block!important;}
    </style> 
    @stop
    @section('head_scripts')
    @parent
    <!--script especificos de esta pagina-->
    @stop
    @section('contenido')
    @parent
    
    <div class="row">
        <div class="col  col-33 col-offset-10 ">
            <div class="item " id="">
                <ion-scroll class="" direction="y" style="height: 100%">
                    <ion-list id="" >
                        <div ng-repeat="detalle in detalles">
                            <ion-item class=" item  item-thumbnail-left"   id="@{{detalle.producto}}_@{{detalle.idPrecio}}" ng-model="html" ng-if="detalle.cantidad > 0" >
                                <img ng-src="@{{'http://localhost/Restaurant/public/'+detalle.imagen}}"  alt="Image not found">
                                <h2>@{{detalle.cantidad }} Ud X @{{detalle.precio.toFixed(2)}} &euro;@{{detalle.nombre}} @{{detalle.presentacion}} </h2>
                                <button ng-if="detalle.cantsub < detalle.cantidad" class="button button-clear button-positive icon-right ion-chevron-right button-dark" ng-click="mover(detalle, 1)" style="float: right;font-size: 18px;padding-right: 30px">
                                  <i class="icon ion-chevron-right"></i>
                                </button>
                                <br>
                            </ion-item>
                            <div  ng-if="(detalle.cantsub > detalle.cantsub_mov)" id="modificadoresDetalle1" class="item " style="padding: 10px; padding-top: 15px; pafont-size: 10px; min-height: 50px; margin-top: -5px; background-color: white">
                                <a class="row"  ng-repeat="subconjunto in detalle.subconjuntos" ng-if="subconjunto.cantidad> 0" id="@{{$detalle.producto}}_@{{$detalle.idPrecio}}_@{{$index}}" style="margin-top: 3px;padding:0px">   
                                    <div class="" style="font-size:10px; padding-left: 0px;width: 10%;line-height: 1.2;">@{{subconjunto.cantidad}}&nbsp;&nbsp; X &nbsp;&nbsp;</div> 
                                        <div class="col" style="font-size:10px; padding:0;height:auto; line-height:1.2;">
                                            <div ng-repeat="modificador in subconjunto.modificadores">

                                                   @{{modificador.precio==0?modificador.compuesto==1? modificador.nombre+": "+ modificador.nombre: " Sin " + modificador.nombre: modificador.canT + " Ext. de " +modificador.nombre + " X " + modificador.precio +" &euro;"}} 
                                                  

                                             </div>
                                        </div>
                                    <div class="col-33" id="detalle_tolE" style="text-align: right;">
                                        <button  class="button button-clear button-positive icon icon-right ion-chevron-right button-dark" ng-click="mover(detalle, 1,subconjunto)" style="float: right;font-size: 10px;padding-right: 30px">
                                            <i class="icon ion-chevron-right"></i>
                                        </button>
                                    </div>
                                </a> 
                            </div>
                                
                        </div>
                    </ion-list>
                </ion-scroll>

            </div>
        </div>
        <div class="col col-33 col-offset-10" >
            <div class="item " id="">
                <ion-scroll class="" direction="y" style="height: 100%">
                    <ion-list id="" >
                        <div ng-repeat="detalle in detalles">
                            <ion-item class=" item  item-thumbnail-left"   id="@{{detalle.producto}}_@{{detalle.idPrecio}}" ng-model="html" ng-if="detalle.cantidad_mov > 0" >
                                <img ng-src="@{{'http://localhost/Restaurant/public/'+detalle.imagen}}"  alt="Image not found">
                                <h2>@{{detalle.cantidad_mov}} Ud X @{{detalle.precio.toFixed(2)}} &euro;@{{detalle.nombre}} @{{detalle.presentacion}} </h2>
                                 <button ng-if="detalle.cantsub_mov < detalle.cantidad_mov" class="button button-clear button-positive icon-right ion-chevron-left button-dark" ng-click="mover(detalle, 0)" style="float: right;font-size: 18px;padding-right: 30px">
                                      <i class="icon ion-chevron-left"></i>
                                 </button>
                                <br>
                            </ion-item>
                             <div  ng-if="detalle.cantsub_mov > 0 " id="modificadoresDetalle1" class="item  item-thumbnail-left" style="padding: 10px; padding-top: 15px; font-size: 10px; min-height: 50px; margin-top: -5px; background-color: white">
                                <a class="row"  ng-repeat="subconjunto in detalle.subconjuntos" ng-if="subconjunto.cantidad_mov > 0" id="@{{$detalle.producto}}_@{{$detalle.idPrecio}}_@{{$index}}" style="margin-top: 3px;padding:0px">   
                                    <div class="" style="padding-left: 8px;width: 10%;line-height: 1.2;">@{{subconjunto.cantidad_mov}}&nbsp;&nbsp; X &nbsp;&nbsp;</div> 
                                        <div class="col" style="font-size:10px; padding:0;height:auto; line-height:1.2;">
                                            <div ng-repeat="modificador in subconjunto.modificadores">

                                                   @{{modificador.precio==0?modificador.compuesto==1? modificador.nombre+": "+ modificador.nombreM: " Sin " + modificador.nombre: modificador.canT + " Ext. de " +modificador.nombre + " X " + modificador.precio +" &euro"}} 

                                             </div>
                                        </div>
                                     <div class="col-33" id="detalle_tolE" style="text-align: right;">
                                        <button  class="button button-clear button-positive icon icon-right ion-chevron-left button-dark" ng-click="mover(detalle, 0,subconjunto)" style="float: right;font-size: 10px;padding-right: 30px">
                                            <i class="icon ion-chevron-left"></i>
                                        </button>
                                    </div>
                                   
                                </a> 
                            </div>
                        </div>
                    
                    </ion-list>
                </ion-scroll>
            </div>
        </div>
    </div>



    @stop
    @section('footer')
    <div class="bar bar-footer" style='background-color: #DC4E1E; color: white;'>   
        <button id="volver" href="../mesas" class="button button-assertive BtmMesa icon-left ion-chevron-left" >Volver
        </button>
        <button class="button  button-assertive BtmMesa icon-right ion-chevron-right"   ng-click="dividir()">Dividir
        </button>                    
    </div>

    @stop
    @section('scripts')
    @parent
    <script >
        angular.module('mySuperApp', ['ionic'])
                .controller('controlador', function ($scope, $ionicActionSheet, $timeout, $ionicSideMenuDelegate, $http) {
                    $scope.mov_detalles = [];
                    $http({
                        url: '../../get_orden/{{$mesa}}',
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {

                        $scope.detalles = response.detalles;
                        console.log($scope.detalles, $scope.detalles.length);
                        var log;
                        angular.forEach($scope.detalles, function (detalle, key) {
                             detalle.cantsub=0;
                               detalle.cantsub_mov=0;
                               if (detalle.subconjuntos != false){
                                    for(j=0;j < detalle.subconjuntos.length; j++){
                                      detalle.cantsub+= parseInt(detalle.subconjuntos[j].cantidad);
                                    }
                                }
                        },log);
                            console.log( $scope.detalles);
                        $scope.orden = response.orden;
                    }).error(function (error) {

                        $scope.error = error;
                    });

                    $scope.dividir = function () {
                        $http({
                            url: '../../dividir_mesa/{{$mesa}}/{{$mesa2}}',
                            method: 'GET',
                            params: {detalles: JSON.stringify($scope.detalles)},
                            headers: {
                                "Content-Type": "application/json"
                            }

                        }).success(function (response) {

                            $scope.detalles = response.detalles;
                            
                            $scope.orden = response.orden;
                            location.href = "../../mesas";
                        }).error(function (error) {

                            $scope.error = error;
                        });
                    }

                    $scope.mover = function (detalle, tipo, sub) {
                        if (tipo == 1 && detalle.cantidad > 0) {
                            detalle.cantidad--;
                            if (detalle.cantidad_mov == undefined) {
                                detalle.cantidad_mov = 1;
                            } else {
                                detalle.cantidad_mov++;
                            }
                            if (sub != null){
                                sub.cantidad--;
                                if (sub.cantidad_mov == undefined) {
                                    sub.cantidad_mov = 1;
                                } else {
                                    sub.cantidad_mov++;
                                }
                                if (detalle.cantsub_mov == undefined) {
                                    detalle.cantsub_mov = 1;
                                } else {
                                    detalle.cantsub_mov++;
                                }
                            }
                        } else if (detalle.cantidad_mov > 0) {
                            detalle.cantidad++;
                            detalle.cantidad_mov--;
                            if (sub != null){
                                sub.cantidad++;
                                sub.cantidad_mov--;
                                detalle.cantsub_mov--;
                            }
                        }

                    }
                });










    </script>
    @stop