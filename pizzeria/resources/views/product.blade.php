                                <?php $cont=0; ?>
                                 @if(count($datos['productos']) > 0)
                                 @foreach ($datos['productos'] as $producto)
                                 <?php $cont++; ?>
                                        <!--cada una de las columnas de los productos-->
                                              <div class="row" id="contenedorPro" @if($cont==1) style=" margin-top: 8px;" @else style=" margin-top: 10px;" @endif accesskey="">
                                             <!--cada una de los cuadros de los productos (boton)-->
                                               <button id="producto{{$producto['idProducto']}}" class="producto"  ng-click="@if(count($producto['precios'])==1) AddorRemDetalle({{$producto['idProducto']}},{{$producto['precios'][0]['id']}},1,'+') @else armarpopup({{json_encode($producto['precios'])}},'{{$producto['nombre']}}', {{$producto['idProducto']}})@endif ">
                                                   <!--div que contiene la imagen y la presentacion-->
                                                  <?php $j=0 ;?>
                                                   @foreach ($producto['precios'] as $precio)
                                                   <?php $j++ ;?>
                                                   @if($j==1)
                                                   <div class="imgproducto">
                                                        <!--div que contiene la imagen-->
                                                            <img src="{{$precio['imagen']}}"  class="img-P">
                                                            <!--div que contiene la presentacion o un boton de mas presentaciones si tienen mas de una-->
                                                            <div id="spam">
                                                            @if(count($producto['precios'])==1)
                                                                <strong><font color="black">{{$precio['presentacion']}} <br>{{$precio['costo']}} &euro;</font></strong>
                                                             @else
                                                                <img src="imagenes/1.png" style="width:13px; height:13px;">                                                               
                                                             @endif
                                                            </div>
                                                       
                                                   </div>
                                                   <!--div que contiene el nombre-->
                                                   <div style="height: 20px;">
                                                        <center>{{ $producto['nombre'] }}</center>
                                                   </div>
                                                   @endif
                                                @endforeach
                                                </button>
                                             </div>
                                 @endforeach
                              @else
                                no hay categorias n el sistema
                              @endif