<!DOCTYPE html>
<html ng-app="mySuperApp3">
<?php  

$file = fopen("config_datos_generales.txt", "r");
//Output a line of the file until the end is reached

$nombret=fgets($file);
$nombre=fgets($file);
$direccion=fgets($file);
$nif=fgets($file);
$telefono=fgets($file);

fclose($file);

?>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
        {!! HTML::style('lib/ionic/css/ionic.css') !!} 
        {!! HTML::style('font-awesome-4.3.0/css/font-awesome.min.css') !!} 
        {!! HTML::style('Recursos/drag/jquery-ui.css') !!} 
        {!! HTML::script('lib/ionic/js/ionic.bundle.js') !!} 
        {!! HTML::script('lib/cordova-js-master/src/cordova.js') !!}
        {!! HTML::style('nuevo.css') !!} 
        @yield('head_css')
        @yield('head_scripts') 
    </head>
    <body animation="slide-left-right-ios7" ng-app="ionicApp" ng-controller="controlador">

    <ion-side-menus enable-menu-with-back-views="true">
        <ion-side-menu-content >
            <ion-header-bar  style='background-color: #DC4E1E;'>
                <button ng-show="!unir" onclick="maxi()" class="button button-icon expandir icon ion-arrow-expand" style="color: white;"></button>
                <button ng-show="unir" id="btn_clr_u" ng-click="cancelar()" class="button button-icon  icon ion-close" style="color: white;"></button>
               
                <h1 class="title"  style="color: white; text-align: center" ng-init='titulo="{{$nombret}}"'>@{{unir?msg_mesas:titulo}}</h1>
                <button title="Opciones" menu-toggle="right" class="button button-icon icon ion-navicon-round"  style="color: white;" ng-click="toggleLeftSideMenu()"></button>

            </ion-header-bar>
            <ion-content id="contenido" class="has-header has-footer">
                @yield('contenido')
            </ion-content>
            <ion-footer-bar id="footer" class="footerM" style='background-color: #DC4E1E;'>
                @yield('footer')
            </ion-footer-bar>
                
        </ion-side-menu-content>


        <!-- Right menu -->
        <ion-side-menu side="right">
            <ion-nav-view> 
                <ion-header-bar style='background-color: #DC4E1E;'>
                    <a class="button button-icon icon ion-gear-a " style="color: white; ">Opciones</a>
                </ion-header-bar>
                <ion-content  class="has-header has-footer"  >
                    <div class="list">
                        <a class="item item-avatar" href="#" style="padding-top: 15px;">
                          <img src="Recursos/img/sin_foto.png">
                          <h2>{!!  $user->name!!}</h2>
                          <p>{!! $user->tipo!!}</p>
                        </a>
                        @if ( $user->tipo != "Mesero") 
                         <a class="item item-avatar" href="#" style="padding-top: 23px;">
                          <img src="Recursos/img/caja.gif">
                          <h2 id="btn_caja">Caja: {!!  $caja== -1? "No asignada": $caja !!}
                              <button style="float: right;margin-right: 20px;font-size: 21px;" title="Actualizar Caja" class="button-clear ion-loop" onclick=" return cambiar_caja();"></button>
                          </h2>
                        </a>
                        @endif
                        <div id="opciones">
                           @if ($user->tipo != "Mesero" &&$user->tipo != "Cajero") 
                                <a class="item icon ion-android-arrow-dropright" style='text-decoration:none;padding: 15px;font-size: 16px;font-weight: normal;'  href="{{route('Control-de-cajas')}}"><span style="padding-left:5px">Control de cajas</span></a>
                                <a class="item icon ion-android-arrow-dropright" style='text-decoration:none;padding: 15px;font-size: 16px;font-weight: normal;'  href="{{route('ruta_mesas')}}"><span style="padding-left:5px">Mesas</span></a>
                           @endif
                        </div>
                       @if ( $user->tipo == "Admin") 
                            <a  class="item icon ion-android-arrow-dropright" id="administracion" style="padding: 15px;text-decoration: none;font-size: 16px;font-weight: normal;" href="{{route('producto')}}"><span style="padding-left:5px">Administraci&oacute;n del sistema</span></a>
                        @endif
                    </div>
                </ion-content>
                <ion-footer-bar style='background-color: #DC4E1E;' >
                     <a class="button button-icon icon ion-android-exit stable" id="salir" href="logoutcaja{{1}}"> Salir</a>
                </ion-footer-bar>
            </ion-nav-view> 

        </ion-side-menu>

    </ion-side-menus>
    {!! HTML::script('js/jquery.js') !!}
   
    <script>
        var caja= {!!$caja!=null?$caja:-1!!};
    angular.module('mySuperApp3', ['ionic'])
    .controller('controlador',function($ionicPopover, $scope,$ionicLoading,$ionicPopup, $ionicActionSheet, $timeout, $ionicSideMenuDelegate,$compile, $interval,$http) {
        $scope.caja=caja;
        
     angular.element(document).ready(function() {
         if (  "{!!  $user->tipo!!}" == "Cajero") {
            if (caja == -1) {
                 alert('no posee una caja aperturada');   
            }else{
            }
        } 
    });
    });
    function maximizar(e){
            if(e.requestFullScreen){
                e.requestFullScreen;
           }else if(e.webkitRequestFullScreen){
                e.webkitRequestFullScreen();
           }else if(e.mozRequestFullScreen){
                e.mozRequestFullScreen();
           
           }
        }
    function maxi(){
     maximizar(document.documentElement);
     }
      
        $(document).ready(function () {
                if (  "{!!  $user->tipo!!}" == "Cajero") {
                    if (caja == -1) {
                         alert('no posee una caja aperturada');   
                    }else{
                    }
                } 
                if(caja==-1){
                     $('#salir').show();
                     $('#administracion').show();
                }else{
                     $('#salir').hide();
                     $('#administracion').hide();
                }
            }
        );

    function cambiar_caja(X){
        $("#btn_caja").empty();
        $("#btn_caja").text('procesando...');
        $.ajax({
            url: './actualizar_caja',
            dataType: "JSON",
            type: "get",  
            success: function (data) {
                $("#btn_caja").empty();
                if(data.error==false){
                    caja=data.caja;
                     if(data.caja!=-1){
                         var row1='Caja: '+caja+'<button style="float: right;margin-right: 20px;font-size: 21px;" title="Actualizar Caja" class="button-clear ion-loop" onclick=" return cambiar_caja();"></button>';
                         $("#btn_caja").append(row1);
                     }else{
                         $('#salir').show();
                         $('#administracion').show();
                         var row1='Caja: No asignada<button style="float: right;margin-right: 20px;font-size: 21px;" title="Actualizar Caja" class="button-clear ion-loop" onclick=" return cambiar_caja();"></button>';
                         $("#btn_caja").append(row1);
                     }
                }else{ 
                    alert(data.mensaje);
        }
    },
        error: function (data) {
            alert('Ocurrio un error al seleccionar la caja');
    }
});

    }
    </script>
    {!! HTML::script('Recursos/drag/jquery-1.10.2.js') !!} 
    {!! HTML::script('Recursos/drag/jquery-ui.js') !!} 
    @yield('scripts')
</body>
</html>