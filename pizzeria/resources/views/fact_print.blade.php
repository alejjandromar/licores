<?php
$file = fopen("config_datos_generales.txt", "r");
//Output a line of the file until the end is reached

$nombret=fgets($file);
$nombre=fgets($file);
$direccion=fgets($file);
$nif=fgets($file);
$telefono=fgets($file);
fclose($file);
?>
<c>
    {{$nombret}}<br><br>
</c>
<c>
    {{$nombre}}<br>
    $direccion<br>
    TEL: {{$telefono}} NIF: {{$nif}}<br>
    Núm. Proforma: 131<br>
    Cliente: <?php ?> <br>
    Atendido por: <?php ?><br>
    <?php echo date("j-n-Y") . " " . date("h:i") ?>  <br>
    </c>
    <l>
        ____________________________________________________<br>
    </l>
    <lista>
        <?php foreach ($detalles as $detalle) {
        ?>
        <l>
            {{ $detalle->getproducto->nombre}}
        </l>
        <c>
            {{$detalle->precio}}€
        </c>
        <ri>
            {{$detalle->precio*$detalle->cantidad}}€
        </ri>
   
        <l>
            {{ " x ".$detalle->cantidad}}
        </l>
        <br>
       
        <br>

        @foreach ($detalle->modificadores as $modificador)
        @if($modificador->pivot->total > 0)
        <l>
            {{ $modificador->nombre." x ".$detalles->cantidad}}
        </l>
        <ri>
            {{$modificador->pivot->total}}
        </ri>
        <br>
        @endif
        @endforeach
        @if($detalle->subconjuntos)
            @foreach ($detalle->subconjuntos as $sub)
                @if($sub->extras > 0)
                @foreach ($sub->modificadores as $mod)
                    @if($mod->precio > 0)
                       
                         <l>
                            {{$mod->canT}} X {{$mod->nombre}} 
                        </l>
                        
                        <ri>
                            {{$mod->precio}}
                        </ri>
                        <br>
                    @endif
                @endforeach
                @endif
            @endforeach
            @endif
        <?php } ?>
    </lista>

    <l>
        ____________________________________________________<br>
    </l>
    <c>
        Subtotal: <?php  echo $orden->subTotal; ?>€;<br>
        Impuesto: <?php  echo $orden->impuesto; ?>€;<br>
        Descuentos:<?php echo $orden->descuento; ?>€;<br>
        Servicios:<?php  echo $orden->propina; ?>€;<br>
    </c>
    
    <lista>
        <l>
            Total:
        </l>
        <ri>
<?php echo $orden->total; ?>€;<br>
        </ri>
        <br>
        <c>
            !Gracias Por preferirnos¡<br>
        </c>
    </lista>

