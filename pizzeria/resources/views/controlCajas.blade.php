@extends('master')
@section('title', 'Control de cajas')
<html ng-app="ionicApp">
@section('head_css')
@parent
     <!--csss especificos de esta pagina-->
      {!! HTML::style('controlCajas.css') !!} 
@stop
@section('head_scripts')

@parent
@stop

@section('contenido')
@parent 
<div class="card" style=""> 
    <div class="item item-text-wrap">
        <ion-scroll zooming="true" id="scroll" direction="xy" style=" background-color:#F0F3F8; height:95%;">
            <br><br>
             <div ng-if="!cajas" style="text-align: center;">No hay cajas dsponibles por los momentos</div>
            <div id="visor"> 
                        
                <div ng-repeat="caja in cajas"  id="draggable3@{{caja.numero}}" class="draggableV"> 
                    <a href="#" style="font-size: 19px;margin: 11px;  left: -10px; top:-9px;" id ="@{{caja.numero}}" class=" button button-clear ion-record " ng-class="{rojo: caja.Estado2==0,verde: caja.Estado2==1}" ng-click="mostrarMenu($event,caja)" >
                        <b style="position: absolute; top: -20px; left: -15px; color:white;text-shadow: 2px 2px 8px black" class="icon ion-chatbox"></b>
                    </a> 
                    <a style="font-size:10px;  position: relative; top: -71px; left: -8px;">
                         <b>@{{caja.numero}}</b>
                    </a>
                </div>
            </div>
        </ion-scroll>
    </div>
</div>
@stop
@section('footer')          
    <div class="bar bar-footer" style='background-color: #DC4E1E; color: white;'>    
    </div>
@stop

@section('scripts')
@parent
<script>
    function mostrar(){
        $('#aperturar').show();
    }
    var ang   = angular.module('ionicApp', ['ionic'])
    .controller('controlador',function($scope,$interval,$ionicSlideBoxDelegate,$ionicLoading,$ionicModal,$http,$ionicPopup) {
        $scope.cajeros;
        var d = new Date();
        $scope.fecha=d.getDate()+'-'+(d.getMonth()+1)+'-'+d.getFullYear();
        $scope.datos={cajero:-1,opcion:1};
        //funcion que carga todas las cajas disponibles
        $scope.cargarCajas = function(X){
            if(X!=null){
                $ionicLoading.show({
                template: 'Cargando cajas...',
                });
            }else{
            }
           $http({
            url:'cajas/all',
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }

            }).success(function(response){
                $ionicLoading.hide();
                if(response.error==false){
                      $scope.cajas=response.cajas;
                      $scope.cajeros=response.cajeros;
                }else{
                    $scope.cajas=[];
                    $scope.cajeros=[];
                    var mensaje='Ocurrio un error al tratar de realizar la busqueda';
                    if(response.mensaje!=null){
                        mensaje=response.mensaje;
                    }
                    if(X!=null){
                        $ionicLoading.show({
                            template: mensaje,
                            duration:2500
                         });
                    }
                }

            }).error(function(error){
                $ionicLoading.hide();
                $scope.cajas=[];
                $scope.cajeros=[];
                if(X!=null){
                    $ionicLoading.show({
                        template: 'Ocurrio un error al tratar de realizar la busqueda',
                        duration:2500
                     });
                }
            });
       }
       
       //funcion que abre el popover para presentar las opciones
        $scope.mostrarMenu = function($event,caja) {
            $scope.caja=caja;
            if($scope.caja.Estado2==2){
                $ionicLoading.show({
                    template: 'Esta caja ya fue cerrada por el dia de hoy (ya fue emitido su reporte Z)',
                    duration:3000
                 });
                return;
            }
            $scope.openModal();
        };
        
        //crea el modal para las opciones de una caja
        $ionicModal.fromTemplateUrl('modificadores.html', {//N
        scope: $scope,
        animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });
        //abre el modal para los modificadores de un producto 
        $scope.openModal = function () {
            $scope.datos.cajero=-1;
            $scope.datos.opcion=1;
            $scope.modal.show();
             $ionicSlideBoxDelegate.slide(0);
            if($scope.caja.mostrar=='Cerrado'){
                 $scope.turno=$scope.caja.cierres_c[($scope.caja.numeroTurno-1)];
            }
            if($scope.caja.mostrar=='Abierto'){
                 $scope.turno=$scope.caja.cierres_a[0];
            }
            if($scope.caja.Estado2==1&&$scope.turno!=null&&$scope.turno.final=='0000-00-00 00:00:00'){
                //muestro el reporte X sin cerrar el turno
                $scope.reporteX(0);
            }
        };
        
        //cierra el modal de modificadores
        $scope.cerrar = function () {
            $scope.modal.hide();
        };
        
        //apertura una caja
        $scope.aperturarCaja= function () {
            $ionicLoading.show({
            template: 'Procesando...',
            });
           $http({
            url:'cajas/activarCaja',
            method: 'GET',
            params: {numero:  $scope.caja.numero,cajero:$scope.datos.cajero},
            headers: {
                "Content-Type": "application/json"
            }

            }).success(function(response){
                var ocultar=true;
                $ionicLoading.hide();
                if(response.error==false){
                     $scope.cajas=response.cajas;
                      $scope.cajeros=response.cajeros;
                      $ionicLoading.show({
                        template: response.mensaje,
                        duration:2500
                     });
                }else{
                    var mensaje='Ocurrio un error al tratar de realizar la operacion';
                    if(response.mensaje!=null){
                        mensaje='Error: '+response.mensaje;
                        if(response.mensaje=='Este cajero ya tiene una caja aperturada'){
                            ocultar=false;
                        }
                    }
                    $ionicLoading.show({
                        template: mensaje,
                        duration:3000
                     });
                }
                if(ocultar){
                     $scope.cerrar();
                }

            }).error(function(error){
                $ionicLoading.hide();
                $ionicLoading.show({
                    template: 'Ocurrio un error al tratar de realizar la operacion',
                    duration:2500
                 });
                 $scope.cerrar();
            });
        };
        //X=0 reporte X sin cerrar turno
        //X=1 reporte Z sin cerrar caja
        //X=2 reporte X cerrando el turno
        $scope.reporteX= function (X) {
            var ruta='';
            if(X==0){
                 $('#reporteX').html('');
                ruta='logoutcaja0/'+$scope.caja.numero+'/true';
            }else{
                if(X==1){
                     $('#reporteZ').html('');
                    ruta='logoutcaja1/'+$scope.caja.numero+'/true';
                }else{
                  if(X==2){
                       ruta='logoutcaja0/'+$scope.caja.numero+'/false';
                        $ionicLoading.show({
                        template: 'Procesando...',
                        });
                   }else{
                        ruta='logoutcaja1/'+$scope.caja.numero+'/false';
                        $ionicLoading.show({
                        template: 'Procesando...',
                        });
                   }
                }
            }
           $scope.mostrar=true;
           $http({
            url:ruta,
            method: 'GET',
            headers: {
                "Content-Type": "application/html"
            }

            }).success(function(response){
                $scope.mostrar=false;
               if(X==0){
                    $('#reporteX').html(response);
                }else{
                    if(X==1){
                        $('#reporteZ').html(response);
                    }else{
                       $ionicLoading.hide();
                       if(response.error==false){
                            $scope.cajas=response.cajas;
                            $scope.cajeros=response.cajeros;
                             $ionicLoading.show({
                               template: response.mensaje,
                               duration:2500
                            });
                       }else{
                           $ionicLoading.show({
                                template: response.mensaje,
                                duration:3000
                            });
                       }
                       $scope.cerrar();
                    }
                }
            }).error(function(error){
                $scope.mostrar=false;
                 $ionicLoading.hide();
                $ionicLoading.show({
                    template: 'Ocurrio un error al tratar de realizar la operacion',
                    duration:2500
                 });
            });
        };
        
       $scope.cambiar = function(indice) {
        if(indice==0){
            $scope.datos.opcion=1;
        }else{
             $scope.reporteX(1);
            $scope.datos.opcion=0;
        }
      };
      $scope.confirmaropcion = function(opcion) {
          var mensaje;
          if(opcion==2){
              mensaje="Esta seguro que desea cerrar el turno?";
          }else{
              mensaje="Al cerrar la caja ya no podra seguir factura, esta seguro que desea hacerlo?";
          }
            var confirmPopup = $ionicPopup.confirm({
              title: 'Confirmar Opcion',
              template: '<div>'+mensaje+'</div>'
            });

            confirmPopup.then(function(res) {
              if(res) {
                  if(opcion==2){
                      $scope.reporteX(2);
                  }else{
                       $scope.reporteX(3);
                  }
              } 
            });
        
      };
        angular.element(document).ready(function() {
            $scope.cargarCajas(1);
            $interval(function () {
                 $scope.cargarCajas(null);
            }, 10000);
        });
    });
 </script>
 <script id="modificadores.html" type="text/ng-template">
    <div id="compilar">
    <ion-modal-view>
    //header//
    <div class="bar bar-header" style="background-color: #DC4E1E;">
        <h1 class="title" style="color: white;text-align: left;">caja @{{caja.numero}}&rarr;Opciones</h1>
        <div class="buttons">
            <button class="button-clear ion-close-circled" ng-click="cerrar()" style="color:#FFE6DE;font-size: 28px;"></button>
        </div>
    </div>
    //sub header//
    <div class="bar bar-subheader" style="background-color: #F5F3F3;color: gray;padding: 0px 10px 0px 10px;">
         <h1 class="title"  style="text-align: left;margin-right:0px;color:gray"> Estado: @{{caja.Estado2==0?'Cerrada':'Aperturada'}}
            <span class="ion-record" ng-class="{rojo: caja.Estado2==0, verde: caja.Estado2!=0}"></span>
         </h1>
    </div>
    <ion-content class="has-subheader has-footer">
           <div ng-if="caja.Estado2==0"><!--caja cerrada, solo puedo abrirla-->
                <div class="list">
                    <label class="item item-input" style="height: 49px;">
                        <p>Numero de la caja: @{{caja.numero}} <span style="padding-left:20px">Fecha: @{{fecha}}<span></p>
                    </label>
                    <label class="item item-input" style="height: 49px;">
                        <p ng-if="datos.cajero==-1" style="color:red">Seleccione un cajero para aperturar la caja.</p>
                        <p ng-if="datos.cajero!=-1" style="color:green">Cajero seleccionado</p>
                    </label>
                    <label class="item item-input item-select">
                      <div class="input-label">
                        Cajero
                      </div>
                      <select ng-model="datos.cajero">
                        <option ng-repeat="cajero in cajeros" value="@{{cajero.dni}}">@{{cajero.name+'('+cajero.tipo+')'}}</option>
                      </select>
                    </label>

                </div>
           </div>
           <div ng-if="caja.Estado2==1"><!--caja abierta, puedo abrir un turno,cerrar un turno o cerrar la caja-->
                <div ng-if="turno.final=='0000-00-00 00:00:00'"><!--Turno abierto asi que muestro los datos y doy la opcion de cerralo-->
                  <ion-slide-box on-slide-changed="slideHasChanged($index)">
                    <ion-slide>
                      <div>
                          <div class="item" style="padding:10px">
                               Turno @{{caja.numeroTurno}}: @{{turno.final=='0000-00-00 00:00:00'?'Abierto':'Cerrado'}}
                           </div>
                           <div class="item" style="padding:10px">
                               Aperturado el: @{{turno.inicio}}
                           </div>
                           <div class="item" style="padding:10px">
                               Cajero: @{{turno.get_cajero.name+' ('+turno.get_cajero.dni+')'}}
                           </div>
                      </div>
                    </ion-slide>
                    <ion-slide>
                       <ion-content class="desbordamiento" >
                      <div >
                        <div  ng-if="mostrar" style="    height: 100%;">
                          <div  style="text-align: center;margin-top: 25%;">
                                <img src="imagenes/cargando.gif">Cargando Datos del reporte X...
                          </div>
                       </div>
                        <div  id="reporteX" style="padding: 10px;"> 
                        </div>
                    </div>
                    </ion-content>
                   </ion-slide>
                  </ion-slide-box>
              </div><!--Fin Turno abierto-->
        
                <div ng-if="turno.final!='0000-00-00 00:00:00'"><!--Turno cerrado asi que doy la opcion de abrir un nuevo turno y de cerrar la caja-->
                   <ion-slide-box on-slide-changed="cambiar($index)">
                        <ion-slide>
                                 <div ng-if="datos.opcion==1">
                                    <label class="item " style="height: 49px;text-align: center;padding: 12px;">
                                        Abrir Turno
                                    </label>
                                    <label class="item item-input" style="height: 49px;">
                                        <p>Numero de la caja: @{{caja.numero}} <span style="padding-left:20px">Fecha: @{{fecha}}<span></p>
                                    </label>
                                    <label class="item item-input" style="height: 49px;">
                                        <p ng-if="datos.cajero==-1" style="color:red">Seleccione un cajero para aperturar la caja.</p>
                                        <p ng-if="datos.cajero!=-1" style="color:green">Cajero seleccionado</p>
                                    </label>
                                    <label class="item item-input item-select">
                                      <div class="input-label">
                                        Cajero
                                      </div>
                                      <select ng-model="datos.cajero">
                                        <option ng-repeat="cajero in cajeros" value="@{{cajero.dni}}">@{{cajero.name+'('+cajero.tipo+')'}}</option>

                                      </select>
                                    </label>
                                </div> 
                        </ion-slide>
                        <ion-slide>
                            <ion-content class="desbordamiento">
                                <div ng-if="datos.opcion==0">
                                    <div  ng-if="mostrar" style="    height: 100%;">
                                        <div  style="text-align: center;margin-top: 25%;">
                                              <img src="imagenes/cargando.gif">Cargando Datos del reporte Z...
                                        </div>
                                     </div>
                                      <div  id="reporteZ" style="padding: 10px;"> 
                                      </div>
                               </div>
                             </ion-content>
                        </ion-slide>
                </ion-slide-box>
                </div><!--Fin Turno cerrado-->
               
           </div>
    </ion-content>
   
    <ion-footer-bar style="background-color: #DC4E1E;" ng-if="caja.Estado2==0||datos.opcion==1&&turno.final!='0000-00-00 00:00:00'">
             <button ng-if="caja.Estado2==0||datos.opcion==1&&turno.final!='0000-00-00 00:00:00'" id="aperturar" class="button-clear"  style="font-size:19px;color: white;width:50%"  ng-click="aperturarCaja()" ng-disabled="datos.cajero==-1">Aperturar</button>
                <button  class="button-clear" style="font-size:20px;color:white;width:50%" ng-click="cerrar()">Cancelar</button>
    </ion-footer-bar>
    <ion-footer-bar style="background-color: #DC4E1E;" ng-if="caja.Estado2==1&&turno.final=='0000-00-00 00:00:00'">
            <button  class="button-clear"  style="font-size:19px;color: white;width:50%"  ng-click="confirmaropcion(2)" >Cerrar Turno</button>
            <button  class="button-clear" style="font-size:20px;color:white;width:50%" ng-click="cerrar()">Cancelar</button>
    </ion-footer-bar>
    <ion-footer-bar style="background-color: #DC4E1E;" ng-if="caja.Estado2==1&&datos.opcion==0">
            <button class="button-clear"  style="font-size:19px;color: white;width:50%"  ng-click="confirmaropcion(1)" >Cerrar Caja</button>
            <button  class="button-clear" style="font-size:20px;color:white;width:50%" ng-click="cerrar()">Cancelar</button>
    </ion-footer-bar>
    </ion-modal-view>
    </div>
</script>
@stop     





