<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Todo</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
    
    {!! HTML::style('nuevo.css') !!} 
    {!! HTML::style('lib/ionic/css/ionic.css') !!} 
    {!! HTML::script('lib/ionic/js/ionic.bundle.js') !!} 
    {!! HTML::script('lib/cordova-js-master/src/cordova.js') !!} 
	    <script>
   var ang= angular.module('ionicApp', ['ionic'])
.controller('controlador',function($scope, $ionicPopup,$timeout) {
//variable global de productos seleccionados
 $scope.cont=0;

//funcion que agrega un producto a la orden.
   $scope.pedido = function(i) {
       console.log('se seleccionan la presentacion '+i);
        $scope.cont+=1;
        console.log($scope.cont);
   }

 //funcion que cierra el popup.
   $scope.closepopup = function() {
         $scope.cont=0;
        $scope.ventana.close();
        $scope.ventana=null;
   }  
 
 //funcion para armar el popup
   $scope.armarpopup = function(preciosP,nombreP, idP) {
         $scope.plantilla ='<div style=" width: 100%; border:solid; border-color:#81BEF7;background-color:#A9E2F3">'
                            +'<div class="row"><div class="col-90"><strong>Presentaciones->'+nombreP+'</strong></div>'
                            +'<button class="col-10 ion-close-circled" ng-click="closepopup()"></button></div>'
                            +'<div id="fila" class="row" style="width:100%;margin-top: 6px; overflow: auto;">';
        for(var i=0;i<preciosP.length;i++)
        {
           if(i==0){
            $scope.plantilla +='<div id="producto" class="row" style="height:80px; width: 122px;">'
                                +'<button id="presentacion'+i+'" class="productop" ng-click="AddorRemDetalle('+idP+','+preciosP[i]['id']+",1,'+'"+')">'
                                +'<div class="" style="min-width: 80px; width: 94%; height:48px; margin-left: 2px; margin-right: 2px; margin-top: 2px; position: relative;">'
                                +'<!--div que contiene la imagen--!>'
                                +'<img src="'+preciosP[i]["imagen"]+'" style="width:100%; height:100%;">'
                                
                                +'</div>'
                                +'<!--div que contiene el nombre--!>'
                                +'<div style="height: 20px;">'
                                +'<center><strong>'+preciosP[i]["presentacion"]+' &euro;'+preciosP[i]['costo']+'</center>'
                                +'</div></button></div>';
        }else{
            $scope.plantilla +='<div id="producto" class="row" style="height:80px; width: 122px;  margin-top: 2px;">'
                                +'<button id="presentacion'+i+'" class="productop"  ng-click="AddorRemDetalle('+idP+','+preciosP[i]['id']+",1,'+'"+')">'
                                +'<div class="" style="min-width: 80px; width: 94%; height:48px; margin-left: 2px; margin-right: 2px; margin-top: 2px; position: relative;">'
                                +'<!--div que contiene la imagen--!>'
                                +'<img src="'+preciosP[i]["imagen"]+'" style="width:100%; height:100%;">'
                                
                                +'</div>'
                                +'<!--div que contiene el nombre--!>'
                                +'<div style="height: 20px;">'
                                +'<center><strong>'+preciosP[i]["presentacion"]+' &euro;'+preciosP[i]['costo']+'</center>'
                                +'</div></button></div>';
        }
        }
         $scope.plantilla+='</div><div class="row" style="  margin-top: 2px;"><strong>Productos seleccionados->@{{cont}}</strong></div></div>';
         $scope.showPopup();
   }
 
$scope.showPopup = function() {

   // An elaborate, custom popup
   var modal = $ionicPopup.show({
     template: $scope.plantilla,
     scope: $scope,
   }
   );
   $scope.ventana=modal;
   
   
  };
  
   $scope.AddorRemDetalle = function(idProduccto,idPrecio, cantidad,A) {
                  AddorRemDetalle(idProduccto,idPrecio, cantidad,A);      
  };
});


function AddorRemDetalle(idProducto,idPrecio, cantidad,A){

                        toSend= {
                                    cantidad: cantidad,
                                    idProducto: idProducto,
                                    idPrecio: idPrecio,
                                    A:A,
                                 };
                         $.ajax({
                                url: "ndetalle_temp/{{$orden['num_mesa']}}",
                                 data:toSend,
                                type: "get",
                                success: function (data){
                                    console.log(data);
                                    if(data.tipo=="R"){
                                        $("#"+idProducto+"_"+idPrecio).remove();
                                    }else if(data.tipo=="C"){
                                        console.log("#"+idProducto+"_"+idPrecio+" #detalle_cant");
                                        $("#"+idProducto+"_"+idPrecio).find("#detalle_cant").text(data.detalle.cantidad);
                                        $("#"+idProducto+"_"+idPrecio).find("#detalle_cant").text(data.detalle.cantidad*data.detalle.precio);
                                    }else{
                                        if ( e== "n"){
                                           $("#detalles .list").empty(); 
                                        }
                                        var n= '<ion-item id="'+idProducto+'_'+idPrecio+'" style=" padding: 0;" class="item">'+                                        
                                               '<a class="item item-thumbnail-left" href="#" ng-click="showConfirm('+idProducto+','+idPrecio+')" style=" padding-left: 20%; border-bottom: 2px double #444444;">'+                                  
                                               '<div class="item item-image" style="width: 15%; height: 20%;">'+
                                               '<!--div que contiene la imagen-->'+
                                               '<img src="'+data.detalle.imagen+'">'+
                                               '</div>' +
                                               '<div class="row" style="font-size:8px">'+
                                               '<div class="col" style="font-size:10px">'+
                                               data.producto.nombre+
                                               '</div>'+
                                               '<div class="row" style="font-size:8px">'+
                                               '<div class="col" id="detalle_cant">'+
                                               data.detalle.cantidad+
                                               '</div>'+
                                               '<div class="col">'+
                                               data.detalle.precio+
                                               '</div>'+
                                               '<div class="col" id="detalle_tol">'+
                                                    data.detalle.cantidad*data.detalle.precio+
                                               '</div>'+
                                               '</div>'+
                                               '</div>'+
                                               '</a>'+
                                               '</ion-item>';
                                       $("#detalles .list").append(n);
                                    }
                                    $("#lbmonto").text(data.total);
                                    
                                },
                                error: function (){
                                    alert("#ERROR");
                                }
                            }
                        );
}
    </script>
  </head>
  <body animation="slide-left-right-ios7" ng-app="ionicApp" ng-controller="controlador">
  <div id="header" class="bar bar-header bar-calm">
        <button class="button"> imagen</button>
        <div class="h1 title">Header Buttons</div>
        <button class="button button-icon icon ion-ios7-search"></button>                
    </div>    
        <ion-content  class="has-header" >
         <!-- contenedor-->
        <div  id="contenedor" class="row"  style="width: 100%; height: 100%;">    
           <!-- aca es donde estara la parte de las categorias y los productos del menu-->
           <div class="" style="width: 70%; height: 100%;">
                <div class="row" style="width: 100%; height: 35%;">
                    <!-- categorias-->
                    <div class="" id="categorias" style=" width: 91%; height: 100%; border:solid; border-radius: 5px; border-color:#81BEF7;background-color:#A9E2F3">
                         <!-- scroll de categorias-->
                        <ion-scroll zooming="true" direction="xy" style="height: 100%;width: 100%">
                            <?php $cont=0 ; $conta=0; $pri=true?>
                                 @if(count($categorias) > 0)
                                 @foreach ($categorias as $categoria)
                                 <?php $cont++ ;?>
                                     @if($cont==1)
                                         <?php $conta+=5;?>
                                         <!--cada una de las filas de las categorias-->
                                        <div class="row" style="width: 100%;  margin-top: 2px;">
                                             <!--cada una de las columnas de las categorias-->
                                             <div class="row" style="width: 20%; min-width: 124px; height:90px;">
                                             <!--cada una de los cuadros de las categorias-->
                                               <button id="categoria{{ $categoria->idCategoria }}" class="@if($pri) categoriaS  @else categoria @endif" onclick="presiono({{ $categoria->idCategoria }})" >
                                                   <div class="" style="min-width: 110px; width: 94%; height:65px; margin-left: 4px; margin-right: 2px;  margin-top: 2px;">
                                                        <img src="{{ $categoria->imagen }}" style="width:100%; height:100%;">
                                                   </div>
                                                   <div style="height: 25px;">
                                                       <center><font color="white">{{ $categoria->nombre }}</font></center>
                                                   </div>
                                                </button>
                                             </div>
                                             
                                     @else  
                                            <!--cada una de las columnas de las categorias-->
                                             <div class="row" style="width: 20%; min-width: 124px; height:90px; margin-top: 2px;">
                                                <!--cada una de los cuadros de las categorias-->
                                                <button id="categoria{{ $categoria->idCategoria }}" class="categoria" onclick="presiono({{ $categoria->idCategoria }})">
                                                  <div class="" style="min-width: 110px; width: 94%; height:65px; margin-left: 4px; margin-right: 2px;  margin-top: 2px;">
                                                        <img src="{{ $categoria->imagen}}" style="width:100%; height:100%;">
                                                   </div>
                                                    <div style="height: 25px;">
                                                       <center><font color="white">{{ $categoria->nombre }}</font></center>
                                                   </div>
                                                </button>
                                             </div>
                                            
                                     @endif
                                     @if($cont==5)
                                        </div>
                                        
                                        <?php $cont=0 ; $pri=false;?>
                                      @endif
                                 @endforeach
                                 @if(count($categorias)<$conta)
                                </div><!-- ifn contenedo-->
                                        
                                        <?php $conta=0 ;?>
                                 @endif
                              @else
                                no hay categorias n el sistema
                              @endif
                        </ion-scroll>
                    </div>
                    <div class="" id="botonera1" style=" width:9%; height: 100%;">
                            <button class="col button icon ion-arrow-up-a" id="arriba" style=" width: 95%; height: 32%;"></button>
                            <button class="col button icon ion-arrow-down-a " id="abajo" style=" width: 95%;height: 32%;"></button>
                            <button class="col button icon ion-arrow-expand" id="expandir"style=" width: 95%; height: 32%;"></button>
                    </div>
                </div>
           <!-- contenedor productos-->
            <div class="" id="productos"style="width: 100%; height:62%; margin-top: 6px;  border:solid; border-radius: 5px;  border-color:#81BEF7;background-color:#A9E2F3">
                <ion-scroll zooming="true" direction="xy" style="height: 100%;width: 100%;">
                            <?php $cont=0 ; $conta=0; ?>
                                 @if(count($datos['productos']) > 0)
                                 @foreach ($datos['productos'] as $producto)
                                 <?php $cont++ ;?>
                                     @if($cont==1)
                                         <?php $conta+=8;?>
                                         <!--cada una de las filas de los productos-->
                                        <div class="row" style="width: 100%;  margin-top: 2px;">
                                             <!--cada una de las columnas de los productos-->
                                             <div class="row" style="width: 12.5%; min-width: 88px; height:60px;">
                                             <!--cada una de los cuadros de los productos (boton)-->
                                             <?php //dd($producto)?>
                                               <button id="producto{{$producto['idProducto']}}" class="producto" ng-click="@if(count($producto['precios'])==1) AddorRemDetalle({{$producto['idProducto']}},{{$producto['precios'][0]['id']}},1,'+') @else armarpopup({{json_encode($producto['precios'])}},'{{$producto['nombre']}}', {{$producto['idProducto']}})@endif">
                                                   <!--div que contiene la imagen y la presentacion-->
                                                   <?php $j=0 ;?>
                                                   @foreach ($producto['precios'] as $precio)
                                                   <?php $j++ ;?>
                                                   <div class="" style="min-width: 80px; width: 94%; height:45px; margin-left: 2px; margin-right: 2px; margin-top: 2px; position: relative;">
                                                        <!--div que contiene la imagen-->
                                                        @if($j==1)
                                                            <img src="{{$precio['imagen']}}" style="width:100%; height:100%;">
                                                            <!--div que contiene la presentacion o un boton de mas presentaciones si tienen mas de una-->
                                                            <div style="position: absolute; height: 35%;text-align:center; line-height:1;right:2%; bottom:2%;font-size:8px; border-radius: 2px; background:#A9E2F3;">
                                                            @if(count($producto['precios'])==1)
                                                                <strong>{{$precio['presentacion']}} <br>{{$precio['costo']}} &euro;</strong>
                                                             @else
                                                                <img src="imagenes/1.png" style="width:13px; height:13px;">                                                               
                                                             @endif
                                                            </div>
                                                        @endif
                                                   </div>
                                                   <!--div que contiene el nombre-->
                                                   <div style="height: 20px;">
                                                        <center>{{ $producto['nombre'] }}</center>
                                                   </div>
                                                   @endforeach
                                                </button>
                                             </div>
                                             
                                     @else
                                            <!--cada una de las columnas de las categorias-->
                                             <div class="row" style="width: 12.5%; min-width: 88px; height:65px; margin-top: 2px;">
                                                <!--cada una de los cuadros de las categorias-->
                                                <button id="producto{{$producto['idProducto'] }}" class="producto" ng-click="@if(count($producto['precios'])==1) AddorRemDetalle({{$producto['idProducto']}},{{$producto['precios'][0]['id']}},1,'+') @else armarpopup({{json_encode($producto['precios'])}},'{{$producto['nombre']}}', {{$producto['idProducto']}})@endif">
                                                   <!--div que contiene la imagen y la presentacion-->
                                                   <?php $j=0 ;?>
                                                   @foreach ($producto['precios'] as $precio)
                                                   <?php $j++ ;?>
                                                   <div class="" style="min-width: 80px; width: 94%; height:45px; margin-left: 2px; margin-right: 2px; margin-top: 2px; position: relative;">
                                                        <!--div que contiene la imagen-->
                                                        @if($j==1)
                                                            <img src="{{$precio['imagen']}}" style="width:100%; height:100%;">
                                                            <!--div que contiene la presentacion o un boton de mas presentaciones si tienen mas de una-->
                                                            <div style="position: absolute; height: 35%;text-align:center; line-height:1;right:2%; bottom:2%;font-size:8px; border-radius: 2px; background:#A9E2F3;">
                                                            @if(count($producto['precios'])==1)
                                                                <strong>{{$precio['presentacion']}} <br>{{$precio['costo']}} &euro;</strong>
                                                             @else
                                                                <img src="imagenes/1.png" style="width:13px; height:13px;">                                                          
                                                             @endif
                                                            </div>
                                                        @endif
                                                   </div>
                                                   <!--div que contiene el nombre-->
                                                   <div style="height: 20px;">
                                                        <center>{{ $producto['nombre'] }}</center>
                                                   </div>
                                                   @endforeach
                                                </button>
                                             </div>
                                            
                                     @endif
                                     @if($cont==8)
                                        </div>
                                        
                                        <?php $cont=0 ;?>
                                      @endif
                                 @endforeach
                                 @if(count($producto)<$conta)
                                </div><!-- ifn contenedo-->
                                        
                                        <?php $conta=0 ;?>
                                 @endif
                              @else
                                no hay productos en el sistema
                              @endif  
                </ion-scroll>
            </div><!--fin contenedor productos-->
           <!-- ifn contenedor-->
           
           <!-- aca es donde estara la parte de la facturacion de alejandro-->
           <div class="" style="width: 30%; height: 100%;overflow: hidden;">
                @include('orden',$orden)      
           </div>
       </div>
</body>
   {!! HTML::script('js/jquery.min.js') !!}
   {!! HTML::script("lib/ionic/js/jquery.plugins.js") !!}
   <script >
    var mostrar=true;
    var anterior="";
    var actual="";
    var primero=true;
    $( document ).ready(function() {
    
  
   $("#expandir").on( "click", function() {
            if(mostrar){
                $('#productos').hide();
                $('#categorias').css("height","290%");
                alert('se cambio el css');
                 mostrar=false;
            }else{
                 $('#categorias').css("height","100%");
                $('#productos').show();
			     mostrar=true; 
            }
		});

     });
     function presiono(codigo){
        var id="#categoria"+codigo;
         if(id!="#categoria1"){
                $("#categoria1").removeClass("categoriaS");
                $("#categoria1").addClass("categoria"); 
            }
        if(primero){
            console.log(id!="#categoria1");
             anterior=id;
             actual=id;
            $(actual).addClass("categoriaS");
        }else{
            anterior=actual;
            actual=id;
            $(anterior).removeClass("categoriaS"); 
            $(actual).addClass("categoriaS");  
        }
        primero=false;
        alert(id);
     }

     </script>
</html>

	



