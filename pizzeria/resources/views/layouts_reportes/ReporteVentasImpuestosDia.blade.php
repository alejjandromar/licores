@extends('layouts.master')

@section('title', 'Ventas Diarias Por Impuesto')
@section('bread', 'Reportes/Ventas diarias por impuestos')

@section('head_css')
@parent
    
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
     {!! HTML::style('Recursos/css/bootstrap-datepicker.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent
<!--panel de resultados-->
<div class="row" id="Ptabla">
    <div class="col-md-12">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Ventas Diarias Por Impuesto</h3>
                <div class="vd_panel-menu">
                   
                </div>
            </div>
            <div class="panel-body table-responsive" id="Cont" style="padding: 15px;">
                <div class="row">
                    <div class="col-lg-2" style="width:auto;">
                        <button id="filtro" class="btn vd_btn vd_bg-red btn-xs" onclick="cambiarFiltro()" type="button">31</button>
                         <spam id="titulo">Mes:</spam>
                    </div>
                    <div id="Dmes" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;">
                        <select id="mes" class="form-control input-sm" onchange="filtrarPorMesAno()">
                            <option value="01">Enero</option><option value="02">Febrero</option>
                            <option value="03">Marzo</option><option value="04">Abril</option>
                            <option value="05">Mayo</option><option value="06">Junio</option>
                            <option value="07">Julio</option><option value="08">Agosto</option>
                            <option value="09">Septiembre</option><option value="10">Octubre</option>
                            <option value="11">Noviembre</option><option value="12">Diciembre</option>
                        </select>
                    </div>
                    <div id="Daño" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;">
                        <select id="año" class="form-control input-sm" onchange="filtrar()">
                               @for ($i = 2015; $i <2031; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                        </select>
                    </div>
                    <div id="DCalendario1" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                            <div class="">
                                <div class='input-group date' id='calendario1'>
                                    <input type='text' class="form-control" readonly="" id="fecha1" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                    <div id="DH" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        Hasta:
                    </div>
                    <div id="DCalendario2" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                        <div class="">
                                <div class='input-group date' id='calendario2'>
                                    <input type='text' class="form-control" readonly="" id="fecha2" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
                <table class="table table-striped" id="data-tables" style="display:none">
                    <thead>
                        <tr>
                            <th class="celda center" >Fecha</th>         
                            <th class="celda center" >Impuesto</th>
                            <th class="celda center" >N. Facturas</th>
                            <th class="celda center" >Base</th>
                            <th class="celda center" >Cuota</th>  
                            <th class="celda center" >Total</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <thead id="resumen" style="display:none">
                        <tr>
                            <th class="celda center">Resumen:</th>         
                            <th class="celda center"><span id="Tcanti"></span></th>
                            <th class="celda center"><span id="Tcant"></span></th>
                            <th class="celda center"><span id="Tbase"></span></th>
                            <th class="celda center"><span id="Tcuota"></span></th>
                            <th class="celda center"><span id="Ttotal"></span></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- contenido -->


            
    
@endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
{!! HTML::script('Recursos/js/bootstrap-datepicker.js') !!} 
{!! HTML::script('Recursos/locales/bootstrap-datepicker.es.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        /**
         * 1=Todas las fechas
         * 2=Año
         * 3=Mes y año
         * 4=Dia, mes y año
         * 5=Rango entre dos fechas.
         */
        var filtro =3;
        var rango =false;
        var meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        function formatear(tabla,cadena){
             $('#'+tabla).DataTable( {
                        language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": cadena,
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

               });
        }
        function filtrarPorAno(){
            $('#filtro').attr('disabled',true);
            var url = 'Ventas-Diarias-Por-Impuestos/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:2,año:$("#año").val()},
                    type: "get",
            success: function (data) {
                console.log(data);
                $('#filtro').attr('disabled',false);
                $("#data-tables").show();
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
               if(data["fechas"]!=null){
                   $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                //formateo la tabla
                formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorMesAno(){
            $('#filtro').attr('disabled',true);
            var url = 'Ventas-Diarias-Por-Impuestos/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:3,año:$("#año").val(),mes:$("#mes").val()},
                    type: "get",
            success: function (data) {
                console.log(data);
                $('#filtro').attr('disabled',false);
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
                $("#data-tables").show();
               if(data["fechas"]!=null){
                   $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
              $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorDiaMesAno(){
             $('#filtro').attr('disabled',true);
            var url = 'Ventas-Diarias-Por-Impuestos/all';
            var F=$("#fecha1").val();
            var F1=F.substring(6, 10)+"-"+F.substring(3, 5)+"-"+F.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:4,fecha:F1},
                    type: "get",
            success: function (data) {
                $('#filtro').attr('disabled',false);
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
                $("#data-tables").show();
               if(data["fechas"]!=null){
                   $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
              $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorRango(){
            $('#filtro').attr('disabled',true);
            var url = 'Ventas-Diarias-Por-Impuestos/all';
            var F=$("#fecha1").val();
            var F1=F.substring(6, 10)+"-"+F.substring(3, 5)+"-"+F.substring(0, 2);
            var F2=$("#fecha2").val();
            var F3=F2.substring(6, 10)+"-"+F2.substring(3, 5)+"-"+F2.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:5,fecha1:F1,fecha2:F3},
                    type: "get",
            success: function (data) {
                $('#filtro').attr('disabled',false);
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
                $("#data-tables").show();
               if(data["fechas"]!=null){
                   $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
              $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrar(){
            if(filtro==2){
                filtrarPorAno();
            }
            if(filtro==3){
                filtrarPorMesAno();
            }
            if(filtro==4){
                filtrarPorDiaMesAno();
            }
            if(rango){
                filtrarPorRango();
            }
        }
        function actuallizarTabla(data){
            console.log(data);
            $("#data-tables").show();
            var row="";
            for (var j = 0; j < data["fechas"].length; j++) {
                for (var i = 0; i < (data["fechas"][j].impuestos.length); i++) {
                   row+='<tr class="odd gradeX" id="flotante1">';
                   var F=data["fechas"][j].fecha;
                   var salida=F.substring(8, 10)+"-"+F.substring(5, 7)+"-"+F.substring(0, 4);
                   row+='<td class="celda center">'+salida+'</td>';
                   row+='<td class="celda center">'+data["fechas"][j].impuestos[i].impuesto+'</td>';
                   row+='<td class="celda center">'+parseInt(data["fechas"][j].impuestos[i].cantidad)+'</td>';
                   row+='<td class="celda center">'+parseFloat(data["fechas"][j].base).toFixed(2)+' &euro;</td>';
                   row+='<td class="celda center">'+parseFloat(data["fechas"][j].impuestos[i].cuota).toFixed(2)+' &euro;</td>';
                   row+='<td class="celda center">'+parseFloat(data["fechas"][j].impuestos[i].total).toFixed(2)+' &euro;</td>';
                   row+='</tr>';
                 }
                   $("#data-tables tbody").append(row);
                   row="";
            }
            $("#Tcant").html(data.totalF);
            $("#Tcanti").html('# imp: '+parseInt(data.totalIm));
            $("#Tbase").html(parseFloat(data.totalB).toFixed(2)+" &euro;");
            $("#Tcuota").html(parseFloat(data.totalC).toFixed(2)+" &euro;");
            $("#Ttotal").html(parseFloat(data.totalM).toFixed(2)+" &euro;");
        }
        function load_table(){
             $('#filtro').attr('disabled',true);
            var url = 'Ventas-Diarias-Por-Impuestos/all';    
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:1},
                    type: "get",
            success: function (data) {
                 $('#filtro').attr('disabled',false);
               $('#data-tables').show();
               if(data["fechas"]!=null){
                    $('#data-tables').DataTable().destroy();
                    $("#data-tables tbody").empty();
                    $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                //formateo la tabla
                formatear('data-tables','Ningún dato disponible en esta tabla');
          },error: function () {
                $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de las ventas');
           }
           });
        }
        
        //funcion que cambia los filtros de fechas de la consulta.
        function cambiarFiltro(){
            filtro++;
            if(filtro==2){
                $("#titulo").html('Año:');
                $("#filtro").html('365');
                $(".col-lg-1").hide();
                $("#Daño").show();
                //Se llama a la funcion ajaz que actualiza las ventas por el año.
                filtrarPorAno();
                rango=false;
            }else{
                if(filtro==3){
                    var fecha = new Date();
                    $('#año').val(fecha.getFullYear());
                    $("#titulo").html('Mes:');
                    $("#filtro").html('31');
                    $(".col-lg-1").hide();
                    $("#Dmes").show();
                    $("#Daño").show();
                    filtrarPorMesAno();
                    rango=false;
                }else{
                    var fecha = new Date();
                    var F="";
                    if(fecha.getDate()<10){
                        F+="0";
                    }
                    F+=fecha.getDate();
                    if((fecha.getMonth()+1)<10){
                        F+="-0";
                    }
                    F+=(fecha.getMonth()+1)+ "-" + fecha.getFullYear();
                    $("#fecha1").val(F);
                    if(filtro==4){
                        $("#titulo").html('Dia:');
                        $("#filtro").html('1');
                        $(".col-lg-1").hide();
                        $("#DCalendario1").show();
                        filtrarPorDiaMesAno();
                        rango=false;
                    }else{
                        if(filtro==5){
                            var fecha = new Date();
                            var F="";
                            if(fecha.getDate()<10){
                                F+="0";
                            }
                            F+=fecha.getDate();
                            if((fecha.getMonth()+1)<10){
                                F+="-0";
                            }
                            F+=(fecha.getMonth()+1)+ "-" + fecha.getFullYear();
                            $("#fecha1").val(F);
                            $("#fecha2").val(F);
                            $("#titulo").html('Desde:');
                            $("#filtro").html('< >');
                            $(".col-lg-1").hide();
                            $("#DCalendario1").show();
                            $("#DCalendario2").show();
                            $("#DH").show();
                            filtrarPorRango();
                            filtro=0;
                            rango=true;
                        }else{
                            if(filtro==1){
                                $("#titulo").html('Todas las fechas.');
                                $("#filtro").html('A');
                                $(".col-lg-1").hide();
                                load_table();
                                rango=false;
                            }
                        }
                    }
                }
            }
        }
        $(document).ready(function () {
            var fecha = new Date();var valor;
            if((fecha.getMonth()+1)<10){
                valor='0'+(fecha.getMonth()+1);
            }else{
                 valor=(fecha.getMonth()+1);
            }
            $('#año').val(fecha.getFullYear());
            $('#mes').val(valor);
            filtrarPorMesAno();
            $('#calendario1').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
            $('#calendario2').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
        });
        
       
    </script>
@stop
