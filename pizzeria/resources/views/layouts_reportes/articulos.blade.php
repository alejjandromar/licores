@extends('layouts.master')

@section('title', 'Venta por Producto')

@section('bread','Reportes  /  Producto')


@section('head_css')
@parent
     {!! HTML::style('Recursos/fileinput/fileinput.css') !!}
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/css/select2.css') !!} 
     {!! HTML::style('Recursos/css/bootstrap-datepicker.css') !!} 
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
    
                                    <!--panel de Agregar-->
          
      <div class="vd_content-section clearfix">                                                                                 
                                    <div class="row" id="Ptabla">
                                        <div class="col-md-12">
                                            <div class="panel widget">
                                                <div class="panel-heading vd_bg-grey">
                                                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-usd"></i> </span>Ventas por Producto.</h3>
                                                     <div class="vd_panel-menu">
                                                         <button id="BtnPdfG" onclick="window.location.href='reportes_producto/pdf'" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Generar PDF" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-file"></i> </button>
                                                         <button  style="display: none" id="BtnPdfF" onclick="Pdf_filtro()" class="btn vd_btn vd_bg-red btn-xs" type="button" data-original-title="Generar PDF" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-file"></i> </button>
                                                    </div>
                                                </div>

                                             <div class="panel-body table-responsive">
                                                    <div class="row">
                                                        <div class="col-lg-2 form-group">
                                                            <div class="form-label">Fecha desde:</div>
                                                            <input class="datepicker" type="text" id="desde" class="mgbt-xs-20 mgbt-sm-0 form-control">
                                                         </div>
                                            
                                                           <div class="col-lg-2 form-group">
                                                            <div class="form-label">Fecha hasta:</div>
                                                            <input class="datepicker" type="text" id="hasta" class="mgbt-xs-20 mgbt-sm-0 form-control"> 
                                                           </div><br>
                                                           <button onclick="cargarDatosTablaFiltro()" title="Filtrar Fecha"  id="filtrar" type="submit" value="Enviar" class="btn  col-lg-0 vd_btn vd_bg-green">
                                                         <!--top-right-success-->
                                                         Filtrar <span class="menu-icon"><i class="fa  fa-filter"></i></span>
                                                        </button>
                                                           <button onclick="document.location.reload()" title="Actualizar"  id="filtrar" type="submit" value="Enviar" class="btn  col-lg-0 vd_btn vd_bg-green">
                                                         <!--top-right-success-->
                                                          <span class="menu-icon"><i class="fa  fa-refresh"></i></span>
                                                        </button>
                                                   
                                                    </div>       
                                              
                                             </div>
      
                                                <div class="panel-body table-responsive">
                                                     <table class="table table-striped" id="data-tables">
                                                        <thead>
                                                            <tr>
                                                                <th>Cod Pres</th>
                                                                <th>Nombre Producto</th>
                                                                 <th>Costo</th>
                                                                <th>Cantidad</th>
                                                                <th>Total</th>
                                                                

                                                            </tr>
                                                        </thead>
                                                        <tbody>                                                    
                                                          
                                                           
                                                        </tbody>
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                 <th class="celda left">Cant: <span id="cant"></span></th>
                                                                <th class="celda left">Total: <span id="total"></span> &euro;</th>
                                                            </tr>
                                                        </thead>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            
      </div>   
            
        
    @endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/fileinput/fileinput.js') !!}
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/js/select2.min.js') !!} 
{!! HTML::script('Recursos/js/jquery.prettyPhoto.js') !!}
{!! HTML::script('Recursos/js/bootstrapValidator.min.js') !!} 
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!}
{!! HTML::script('Recursos/js/bootstrap-datepicker.js') !!} 
{!! HTML::script('Recursos/locales/bootstrap-datepicker.es.min.js') !!} 

           

<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>


    <script type="text/javascript">
        
   function cargarDatosTabla(){
            var url = 'reportes_producto/all';
            
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    
                    success: function (data) {                   
                       var row="";
                       $("#data-tables").DataTable().destroy();
                       $("#data-tables tbody").empty();
                       
                        for (var i = 0; i < data["producto"].length; i++) {
                            row+='<tr role="row" class="odd gradeX">';
                            row+='  <td class="celda center">'+data["producto"][i].id+'</td>';
                            row+='  <td class="celda left">'+data["producto"][i].nombre+' <b>'+data["producto"][i].presentacion+'</b></td>';
                            row+='  <td class="celda center">'+data["producto"][i].costo+'&euro;</td>';
                            row+='  <td class="celda center">'+data["producto"][i].cantidad+'</td>'; 
                            row+='  <td class="celda right"><b>'+data["producto"][i].total+'&euro;</b></td>'; 
                            
                            row+='</td>';                           
                            row+='   </tr>';
                            $("#data-tables tbody").append(row);
                            row="";
                          
                            }
                             $("#total").text(data["cant_total"][0].montototal);
                             $("#cant").text(data["cant_total"][0].cantidadtotal);
                              $("#data-tables").DataTable(
                                      
                   {
                   
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",                  
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

                   }); 
                      validate();       
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los productos');
                    }
                }
            );
            
            
        }
        
        
        
        
        function cargarDatosTablaFiltro(){
        var desde,hasta;

          desde=document.getElementById("desde").value;
          hasta=document.getElementById("hasta").value;
    
        
            var url = 'reportes_producto/filtro';
            
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    data:{desde:desde,hasta:hasta},
                    
                    success: function (data) {                   
                       var row="";
                       $("#data-tables").DataTable().destroy();
                       $("#data-tables tbody").empty();
                       
                        for (var i = 0; i < data["producto"].length; i++) {
                            row+='<tr role="row" class="odd gradeX">';
                            row+='  <td class="celda center">'+data["producto"][i].id+'</td>';
                            row+='  <td class="celda left">'+data["producto"][i].nombre+' <b>'+data["producto"][i].presentacion+'</b></td>';
                            row+='  <td class="celda center">'+data["producto"][i].costo+'&euro;</td>';
                            row+='  <td class="celda center">'+data["producto"][i].cantidad+'</td>'; 
                            row+='  <td class="celda right"><b>'+data["producto"][i].total+'&euro;</b></td>'; 
                            
                            row+='</td>';                           
                            row+='   </tr>';
                            $("#data-tables tbody").append(row);
                            row="";
                          
                            }
                          
                             $("#total").text(data["cant_total"][0].montototal);
                             $("#cant").text(data["cant_total"][0].cantidadtotal);
                             $("#BtnPdfG").hide();
                             $("#BtnPdfF").show();
                             $("#data-tables").DataTable(
                                      
                   {
                   
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",                  
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

                   }); 
                      validate();       
                    },

                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los productos');
                    }
                }
            );
            
            
        }
        
        
        
        
   function Pdf_filtro(){
        var desde,hasta;

          desde=document.getElementById("desde").value;
          hasta=document.getElementById("hasta").value;
           desde= desde.replace('/','-');
           desde= desde.replace('/','-');
           hasta= hasta.replace('/','-');
           hasta= hasta.replace('/','-');
           var fecha =desde+ hasta;
           
        
          var url = 'reportes_producto/pdf_filtro/'+fecha;            
            window.location.href=url;
            
            
        }
        
        
        
        
        
        
        
        
        
        
        
            
   function validate(){
         
      
            
            $("tbody form #imagenC").fileinput({
                previewFileType: ["image", "text"],
                allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
                previewClass: "bg-warning"
            });
            
            $('tbody form').submit(function (e){
                if(e.isDefaultPrevented()){
                    //wrong
                }else{
                     var id = $(this).parents("tr").attr("value");
                    var formData = new FormData(this);
                  save_method(id,formData);     
                   cargarDatosTabla();                   
                    e.preventDefault();
                }
            });
        }         
            
   function mostrardiv(pCodigo,nombre,descripcion) {
         $('tbody #nombre').val(nombre);
         $('tbody #descripcion').val(descripcion);

        var nombreDiv = 'flotante2' + pCodigo;
        var nombrediv1 = '#flotante1' + pCodigo;


        $(nombrediv1).children('.celda').addClass('noprint');
        div = document.getElementById(nombreDiv);

        div.style.display = "";
    }

   function ocultardiv(codigo) {
        //$('#formulario'+codigo).bootstrapValidator('resetForm',true);
        var pnombre = 'flotante2' + codigo;
        var nombrediv1 = '#flotante1' + codigo;
        div = document.getElementById(pnombre);
        div.style.display = "none";
        $(nombrediv1).children('.celda').removeClass('noprint');

    }  

   function eliminar(btn,id){
        var url = 'categoria/delete';
        $.ajax({
                url: url,
                data:{id:id},
                dataType: "JSON",
                type: "get",    
                success: function (data) {
                     if(data.error==false){
                       BootstrapDialog.alert({
                           title: 'Notificación',
                           message: ''+data.Message,
                           type: BootstrapDialog.TYPE_SUCCESS,
                           });
                           cargarDatosTabla();
                    }else{
                        BootstrapDialog.alert({
                           title: 'Notificación',
                           message: 'No se pudo eliminar el descuento, error: '+data.mensaje,
                           type: BootstrapDialog.TYPE_DANGER,

                       });
                    }
                },
                    error: function (data) {
                    BootstrapDialog.danger('Ocurrio un error al eliminar la categoria, '+data);
                }
            }

        );
    }

   function confirmacion(btn,pCodigo) {
        BootstrapDialog.confirm({
            title: 'Confirmacion',
            type: BootstrapDialog.TYPE_DANGER,
            message:  '¿Seguro desea Eliminar esta categoria?',
            callback: function (result) {
            if (result) {
                eliminar(btn,pCodigo);
            }
        }});
    }
    
 $(document).ready(function () {
       $('.datepicker').datepicker({
            format: 'yyyy/mm/dd',
            language: 'es'
            
        });
      cargarDatosTabla();
      });
          $('#registrationForm').bootstrapValidator({
                fields: {
                    nombre: {
			 validators: {
				 notEmpty: {
					 message: 'Debe colocar un nombre'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'El nombre comienza con letras y no contiene caractes especiales'
				 }
			 }
                    },
                       descripcion: {
			 validators: {
				 notEmpty: {
					 message: 'Debe colocar una descripcion'
				 },
				 regexp: {
					 regexp: /^[a-zA-Z]([a-zA-Z]| |[0-9])+$/,
					 message: 'La descripcion comienza con letras y no contiene caractes especiales'
				 }
			 }
                    }
                }
            });
      
      
      
 </script>
     @stop