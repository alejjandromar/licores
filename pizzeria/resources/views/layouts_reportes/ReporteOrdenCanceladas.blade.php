@extends('layouts.master')

@section('title', 'Ordenes Canceladas')
@section('bread', 'Reportes/Ordenes Canceladas')

@section('head_css')
@parent
    
     {!! HTML::style('Recursos/css/jquery.dataTables.min.css') !!}
     {!! HTML::style('Recursos/css/dataTables.bootstrap.css') !!}
     {!! HTML::style('Recursos/bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
     {!! HTML::style('Recursos/css/bootstrap-datepicker.css') !!} 
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent
<!--panel de resultados-->
<div class="row" id="Ptabla">
    <div class="col-md-12">
        <div class="panel widget">
            <div class="panel-heading vd_bg-grey">
                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> </span>Tikets cancelados</h3>
                <div class="vd_panel-menu">
                    
                </div>
            </div>
            <div class="panel-body table-responsive" id="Cont" style="padding: 15px;">
                <div class="row">
                    <div class="col-lg-2" style="width:auto;">
                        <button id="filtro" class="btn vd_btn vd_bg-red btn-xs" onclick="cambiarFiltro()" type="button">A</button>
                         <spam id="titulo">Todas las fechas.</spam>
                    </div>
                    <div id="Dmes" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        <select id="mes" class="form-control input-sm" onchange="filtrarPorMesAno()">
                            <option value="01">Enero</option><option value="02">Febrero</option>
                            <option value="03">Marzo</option><option value="04">Abril</option>
                            <option value="05">Mayo</option><option value="06">Junio</option>
                            <option value="07">Julio</option><option value="08">Agosto</option>
                            <option value="09">Septiembre</option><option value="10">Octubre</option>
                            <option value="11">Noviembre</option><option value="12">Diciembre</option>
                        </select>
                    </div>
                    <div id="Daño" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        <select id="año" class="form-control input-sm" onchange="filtrar()">
                               @for ($i = 2015; $i <2031; $i++)
                                <option value="{{$i}}">{{$i}}</option>
                                @endfor
                        </select>
                    </div>
                    <div id="DCalendario1" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                            <div class="">
                                <div class='input-group date' id='calendario1'>
                                    <input type='text' class="form-control" readonly="" id="fecha1" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar" ></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                    <div id="DH" class="col-lg-1" style="padding:0px 0px 0px 0px;width:auto;margin-right: 10px;display: none">
                        Hasta:
                    </div>
                    <div id="DCalendario2" class="col-lg-1" style="padding:0px 0px 0px 0px;width:14%;margin-right: 10px;display: none">
                        <div class="">
                                <div class='input-group date' id='calendario2'>
                                    <input type='text' class="form-control" readonly="" id="fecha2" placeholder="dd-mm-yy" onchange="filtrar()" style="height: 26px;"/>
                                    <span class="input-group-addon" style="height: 26px;padding: 0px 12px 0px 12px;">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                    </div>
                </div>
                <table class="table table-striped" id="data-tables" style="display:none">
                    <thead>
                        <tr>
                            <th class="celda center" >Fecha</th>         
                            <th class="celda center">Propina</th>                    
                            <th class="celda center">Impuestos</th>
                            <th class="celda center">Extras</th>  
                            <th class="celda center">Subtotal</th>  
                            <th class="celda center" >Total</th>
                            <th class=""></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <thead id="resumen" style="display:none">
                        <tr>
                            <th class="celda center">Resumen:</th>         
                            <th class="celda center">Total de tikest:</th>
                            <th class="celda center"><span id="totaltikes"></span></th>
                            <th class="celda center"><span ></span></th>
                            <th class="celda center">Cantidad total:</th>
                            <th class="celda center"><span id="total"></span></th>
                            <th class="celda center"><span ></span></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- contenido -->
<div class="modal fade" id="myModalPre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style=" position: relative;margin-top: 10%;">
                <div class="modal-body" style="padding:0px!important;">
                    <div class="panel widget panel-bd-top vd_todo-widget light-widget" style="margin: 0px;">
                       <div class="panel-body" style="padding: 15px 15px 15px;">
                           <h4 class=""><span class="append-icon"> <i class="fa  vd_green"></i> </span> <span id="Ptitulo" style="left: -10px;position: relative;"></span></h3>
                            <div class="vd_panel-menu">
                                <button class="close" data-dismiss="modal" aria-hidden="true" type="button" data-original-title="Cerrar" data-placement="bottom" data-toggle="tooltip"><i class="fa fa-times"></i> </button>
                            </div>
                            <div class="row" style="margin-bottom: 0px">
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label" style="padding-right: 0px;">Fecha:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pf">fecha</div>
                                        <!-- col-sm-10 -->
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Mesa:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pm">No disponible</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Sub-total:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Ps">56.00 &euro;</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Propina:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Ppro">56.00 &euro;</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Impuestos:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pimp">56.00 &euro;</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Extras:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pext">56.00 &euro;</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Total:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Ptot">56.00 &euro;</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Cliente:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pcli">yoscar hernan</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Mesero:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pmes">carlos martinez</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row mgbt-xs-0">
                                        <label class="col-xs-4 control-label">Cancelada Por:</label>
                                        <div  class="col-xs-8 controls" style="padding: 0px;" id="Pcaj">yormao perez</div>
                                        <!-- col-sm-10 -->
                                    </div>
                                </div>
                            </div>
                           <h5 class="mgbt-xs-15 mgtp-10 font-semibold" style="font-weight: 700;    font-size: 13px;">Comentario:</h5>
                           <textarea id="comentario" rows="4" readonly="true" resize="false" style="height: 100%;" required>
                           </textarea>
                    </div>
                </div>  
            </div>
        </div>
   
    </div>
 </div>

            
    
@endsection
@section('scripts')
@parent
{!! HTML::script('Recursos/js/jquery.dataTables.min.js') !!}
{!! HTML::script('Recursos/js/dataTables.bootstrap.js') !!}
{!! HTML::script('Recursos/bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
{!! HTML::script('Recursos/js/bootstrap-datepicker.js') !!} 
{!! HTML::script('Recursos/locales/bootstrap-datepicker.es.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script type="text/javascript">
        /**
         * 1=Todas las fechas
         * 2=Año
         * 3=Mes y año
         * 4=Dia, mes y año
         * 5=Rango entre dos fechas.
         */
        var filtro =0;
        var rango =false;
        var meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        function formatear(tabla,cadena){
             $('#'+tabla).DataTable( {
                        language: {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": cadena,
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "ñltimo",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }

               });
        }
        function filtrarPorAno(){
             $('#filtro').attr('disabled',true);
            var url = 'Ordenes-canceladas/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:2,año:$("#año").val()},
                    type: "get",
            success: function (data) {
               $('#filtro').attr('disabled',false);
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
               if(data["resultados"].cantF!=0){
                    actuallizarTabla(data);
                      $("#resumen").show();
                }else{
                    $("#resumen").hide();
                }
                //formateo la tabla
                formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorMesAno(){
            $('#filtro').attr('disabled',true);
            var url = 'Ordenes-canceladas/all'; 
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:3,año:$("#año").val(),mes:$("#mes").val()},
                    type: "get",
            success: function (data) {
                $('#filtro').attr('disabled',false);
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
               if(data["resultados"].cantF!=0){
                   $("#resumen").show();
                    actuallizarTabla(data);
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
              $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrarPorDiaMesAno(){
            $('#filtro').attr('disabled',true);
            var url = 'Ordenes-canceladas/all';
            var Fecha=$("#fecha1").val();
            var F1=Fecha.substring(6, 10)+"-"+Fecha.substring(3,5)+"-"+Fecha.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:4,fecha:F1},
                    type: "get",
            success: function (data) {
                $('#filtro').attr('disabled',false);
               $('#data-tables').show();
               $('#data-tables').DataTable().destroy();
               $("#data-tables tbody").empty();
               if(data["resultados"].cantF!=0){
                    actuallizarTabla(data);
                    $("#resumen").show();
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
                $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al realizar la busqueda.');
           }
           });
        }
        function filtrarPorRango(){
            $('#filtro').attr('disabled',true);
            var url = 'Ordenes-canceladas/all'; 
            var F=$("#fecha1").val();
            var F1=F.substring(6, 10)+"-"+F.substring(3, 5)+"-"+F.substring(0, 2);
            var F2=$("#fecha2").val();
            var F3=F2.substring(6, 10)+"-"+F2.substring(3, 5)+"-"+F2.substring(0, 2);
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:5,fecha1:F1,fecha2:F3},
                    type: "get",
            success: function (data) {
                $('#filtro').attr('disabled',false);
                $('#data-tables').DataTable().destroy();
                $("#data-tables tbody").empty();
               if(data["resultados"].cantF!=0){
                    actuallizarTabla(data);
                    $("#resumen").show();
                }else{
                    $("#resumen").hide();
                }
                    //formateo la tabla
                    formatear('data-tables','No se encontraron resultados para la busqueda');
          },error: function () {
              $('#filtro').attr('disabled',false);
                 BootstrapDialog.danger('Ocurrio un error al tratar de filtrar la busqueda.');
           }
           });
        }
        function filtrar(){
            if(filtro==2){
                filtrarPorAno();
            }
            if(filtro==3){
                filtrarPorMesAno();
            }
            if(filtro==4){
                filtrarPorDiaMesAno();
            }
            if(rango){
                filtrarPorRango();
            }
        }
        function actuallizarTabla(data){
            console.log(data);
            $("#data-tables").show();
            var row="";
            for (var i = 0; i < (data["resultados"].ordenes.length); i++) {
                row+='<tr class="odd gradeX" id="flotante1'+i+'">';
                row+='<td class="celda center">'+data["resultados"].ordenes[i].created_at+'</td>';
                row+='<td class="celda center">'+parseFloat(data["resultados"].ordenes[i].propina).toFixed(2)+' &euro;</td>';
                row+='<td class="celda center">'+parseFloat(data["resultados"].ordenes[i].impuesto).toFixed(2)+' &euro;</td>';
                var E;if(data["resultados"].ordenes[i].extras==null){E=0.00}else{E=parseFloat(data["resultados"].ordenes[i].extras).toFixed(2)}
                row+='<td class="celda center">'+E+' &euro;</td>';
                row+='<td class="celda center">'+parseFloat(data["resultados"].ordenes[i].subTotal).toFixed(2)+' &euro;</td>';
                row+='<td class="celda center">'+parseFloat(data["resultados"].ordenes[i].total).toFixed(2)+' &euro;</td>';
                row+='<td class="menu-action celda center">';
                row+='      <a data-original-title="Ver Detalles"';
                var objeto=JSON.stringify(data["resultados"].ordenes[i]);
                row+='onclick='+"'"+'mostrardiv('+objeto+')'+"'"+' data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-eye"></i> </a>';
                row+='  </td>';
                row+='</tr>';
               $("#data-tables tbody").append(row);
               $('[data-toggle^="tooltip"]').tooltip();
               row="";
            }
            $("#total").html(parseFloat(data.resultados.Total).toFixed(2)+" &euro;");
            $("#totaltikes").html(data.resultados.cantF);
        }
        function load_table(){
            var url = 'Ordenes-canceladas/all';    
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    data:{tipo:1},
                    type: "get",
            success: function (data) {
                $('#data-tables').DataTable().destroy();
                $("#data-tables tbody").empty();
               if(data["resultados"].cantF!=0){
                    $("#data-tables tbody").empty();
                    actuallizarTabla(data);
                    $("#resumen").show();
                }else{
                    actuallizarTabla(data);
                    $("#resumen").hide();
                }
                //formateo la tabla
                formatear('data-tables','Ningún dato disponible en esta tabla');
          },error: function () {
                 BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de las ventas');
           }
           });
        }
        
        //funcion que cambia los filtros de fechas de la consulta.
        function cambiarFiltro(){
            filtro++;
            if(filtro==2){
                $("#titulo").html('Año:');
                $("#filtro").html('365');
                $(".col-lg-1").hide();
                $("#Daño").show();
                //Se llama a la funcion ajaz que actualiza las ventas por el año.
                filtrarPorAno();
                rango=false;
            }else{
                if(filtro==3){
                    var fecha = new Date();
                    $('#año').val(fecha.getFullYear());
                    $("#titulo").html('Mes:');
                    $("#filtro").html('31');
                    $(".col-lg-1").hide();
                    $("#Dmes").show();
                    $("#Daño").show();
                    filtrarPorMesAno();
                    rango=false;
                }else{
                    if(filtro==4){
                        $("#titulo").html('Dia:');
                        $("#filtro").html('1');
                        $(".col-lg-1").hide();
                        $("#DCalendario1").show();
                        filtrarPorDiaMesAno();
                        rango=false;
                    }else{
                        if(filtro==5){
                            $("#titulo").html('Desde:');
                            $("#filtro").html('< >');
                            $(".col-lg-1").hide();
                            $("#DCalendario1").show();
                            $("#DCalendario2").show();
                            $("#DH").show();
                            filtrarPorRango();
                            filtro=0;
                            rango=true;
                        }else{
                            if(filtro==1){
                                $("#titulo").html('Todas las fechas.');
                                $("#filtro").html('A');
                                $(".col-lg-1").hide();
                                load_table();
                                rango=false;
                            }
                        }
                    }
                }
            }
        }
        function mostrardiv(arreglo) {
           //codigo para abrir y llenar el modal
           console.log(arreglo);
           $('#myModalPre').modal('show'); 
           $('#Ptitulo').html('Detalles de la Orden:');
           $('#Pf').html(arreglo.created_at);
           if(arreglo.mesa!=null){$('#Pm').html(arreglo.mesa);}else{$('#Pm').html('No Disponible');}
           $('#Ps').html(parseFloat(arreglo.subTotal).toFixed(2)+" &euro;");
           $('#Ppro').html(parseFloat(arreglo.propina).toFixed(2)+" &euro;");
           $('#Pimp').html(parseFloat(arreglo.impuesto).toFixed(2)+" &euro;");
            if(arreglo.extras!=null){$('#Pext').html(parseFloat(arreglo.extras).toFixed(2)+" &euro;");}else{$('#Pext').html(0.00+" &euro;");}
           $('#Ptot').html(parseFloat(arreglo.total).toFixed(2)+" &euro;");
           if(arreglo.cliente!='00000000A'){$('#Pcli').html(arreglo.getcliente.nombreCompleto+'('+arreglo.getcliente.dni+')');}else{$('#Pcli').html("Tiket Fiscal");}
           $('#Pmes').html(arreglo.getmesero.name+'('+arreglo.getmesero.dni+')');
           $('#Pcaj').html(arreglo.getcajero.name+'('+arreglo.getcajero.dni+')');
           $('#comentario').val(arreglo.comentario);
        }
        
        $(document).ready(function () {
            var fecha = new Date();
            $('#año').val(fecha.getFullYear());
            var dia=fecha.getDate();
            var mes=(fecha.getMonth()+1);
            if(dia<10){
                dia='0'+fecha.getDate();
            }
            if(mes<10){
                mes='0'+(fecha.getMonth()+1);
            }
            $('#fecha1').val(dia+'-'+mes+'-'+fecha.getFullYear());
            $('#fecha2').val(dia+'-'+mes+'-'+fecha.getFullYear());
            cambiarFiltro();
            $('#calendario1').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
            $('#calendario2').datepicker({
                    format: 'dd-mm-yyyy',
                    todayHighlight:true,
                    autoclose: true,
                    language: 'es'});
        });
        
       
    </script>
@stop
