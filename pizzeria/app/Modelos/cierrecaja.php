<?php


namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class cierrecaja extends Model
{
    protected $table ="cierrecaja";
    protected $primaryKey = 'id';
    public $timestamps=false;
    
    public function getcaja()
    { 
        return $this->belongsTo('App\Modelos\caja','numero');
    }
    public function getCajero()
    {      
         return $this->belongsTo('App\User','cajero'); //-_- era aqui !!!! por cierto si el error dice que hay fallo en el modelo lo quehay que revisar es el modelo no el controlador lo malo es no dice que modelo
    }

}
