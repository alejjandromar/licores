<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Descuento extends Model
{
    public $primaryKey='idDescuento';

     protected $table = 'descuento';
     use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    public function Ordenes()
    {      
         return $this->belongsToMany('App\Modelos\Orden', 'FacturaTieneDescuento','idDescuento', 'factura');
    }
}
