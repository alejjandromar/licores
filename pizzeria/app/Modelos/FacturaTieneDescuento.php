<?php

namespace  App\Modelos;

use Illuminate\Database\Eloquent\Model;

class FacturaTieneDescuento extends Model
{
    public $primaryKey= array ('factura','idDescuento');
     public $timestamps = false;
     protected $table = 'facturatienedescuento';
     
    public function getOrden()
    {      
        return $this->belongsTo('App\Modelos\Orden', 'factura');
    }
}
