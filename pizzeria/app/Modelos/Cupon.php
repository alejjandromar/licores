<?php

namespace App;
namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;

class Cupon extends Model
{
     protected $table ="cupon";
     public $timestamps=false;
     protected $primaryKey = 'id';
     
      public function getImpresora()
    {
        return $this->belongsTo('App\Modelos\impresora','impresora');
    }
}
