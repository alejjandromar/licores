<?php

namespace App;
namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;

class area extends Model
{
     protected $table ="area";
     public $timestamps=false;
     protected $primaryKey = 'idArea';
     
      public function getImpresora()
    {
        return $this->belongsTo('App\Modelos\impresora','impresora');
    }
}
