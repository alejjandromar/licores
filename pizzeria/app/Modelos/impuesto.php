<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cache;
use Auth;

class impuesto extends Model
{
     protected $table = 'impuesto';
     public $primaryKey='idImpuesto';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
     public function Ordenes()
    {     
         $parametros = Cache::get('request'.Auth::user()->dni); 
         if($parametros['tipo']=='1'){
              return $this->belongsToMany('App\Modelos\Orden','facturatieneimpuestos','idImpuesto', 'factura')->withPivot('valorImpuesto');
         }else{
             if($parametros['tipo']=='2'){
              return $this->belongsToMany('App\Modelos\Orden','facturatieneimpuestos','idImpuesto', 'factura')->whereRaw('YEAR(created_at) ='.$parametros['año'])->withPivot('valorImpuesto');
            }else{
                if($parametros['tipo']=='3'){
                    return $this->belongsToMany('App\Modelos\Orden','facturatieneimpuestos','idImpuesto', 'factura')->whereRaw("Month(created_at) = ".$parametros['mes']." and YEAR(created_at) = ".$parametros['año'])->withPivot('valorImpuesto');
                }else{
                    if($parametros['tipo']=='4'){
                        return $this->belongsToMany('App\Modelos\Orden','facturatieneimpuestos','idImpuesto', 'factura')->whereRaw("DATE(created_at) = '".$parametros['fecha']."'")->withPivot('valorImpuesto');
                    }else{
                        return $this->belongsToMany('App\Modelos\Orden','facturatieneimpuestos','idImpuesto', 'factura')->whereRaw("DATE(created_at) between '".$parametros['fecha2']."'")->withPivot('valorImpuesto');
                    }
                }
            }
         }
        
    }
}
