<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;


class Detalle extends Model 
{
    protected $table = 'detallesorden';
    public    $primaryKey='idDetalle';
    public $timestamps = false;
    public $guarded = [];
    

    public function setSubcojuntoAttribute($subconjuntos)
    {
        $this->attributes["subconjuntos"] = $subconjuntos;
    }
    
     public function changeSubcojuntoAttribute($key, $sub)
    {
        if($sub->cantidad > 0)
            $this->attributes["subconjuntos"][$key]= $sub;
        else
            unset ($this->attributes["subconjuntos"][$key]);
    }
    
    public function sub_to_array(){
         $this->attributes["subconjuntos"]= array_values($this->attributes["subconjuntos"]);
    }
    
     public function sub_add($sub){
         $this->attributes["subconjuntos"][]= $sub;
    }
    
    


    public function deleteSubcojuntoAttribute($key)
    {      
       unset( $this->attributes["subconjuntos"][$key]);
    }
    
    

    public function getorden() {
        return $this->belongsTo('App\Modelos\Orden', 'factura');
    }

    public function getproducto() {
        return $this->belongsTo('App\Modelos\Producto', 'producto');
    }
    public function getpresentacion() {
        return $this->belongsTo('App\Modelos\Precio', 'idPrecio');
    }

    public function modificadores() {
        return $this->belongsToMany('App\Modelos\modificador', 'detalletienemodificador', 'idDetalle', 'idModificador')->withPivot(array('cantidadDetalles', 'total', 'precioExtra', 'cantidadModificador', 'idOpcion', 'subconjunto'))->orderBy('subconjunto', 'asc');
    }

    //Convierte un array de detalles en un arreglo que solo posea
    // atributos para insertar en la tabla detalle
    static public function create_array($pDetalles) {
        $detalles = [];
        foreach ($pDetalles as $key => $pDetalle) {
            $detalles[$key] = Detalle::create_detalle($pDetalle);
        }
        return $detalles;
    }

    //Convierte un detalle en un objeto que solo posea
    //atributos para insertar en la tabla detalle
    static public function create_detalle($pDetalle) {
        $detalle = new Detalle;
        $detalle->cantidad = $pDetalle->cantidad;
        $detalle->producto = $pDetalle->producto;
        $detalle->idPrecio = $pDetalle->idPrecio;
        $detalle->precio = $pDetalle->precio;
        if (isset($pDetalle->comentario)) {
            $detalle->comentario = $pDetalle->comentario;
        }
        return $detalle;
    }
    
}
