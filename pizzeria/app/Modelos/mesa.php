<?php

namespace App;
namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;

class mesa extends Model
{
    protected $table ="mesa";
    protected $primaryKey = 'idMesa';
    public $timestamps=false;
    
    public function ordenes()
    {      
        return $this->hasMany('App\Modelos\Orden', 'mesa');
    }
}
