<?php

namespace App;
namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;

class impresora extends Model
{
     protected $table ="impresora";
     public $timestamps=false;
     protected $primaryKey = 'id';
     protected $guarded = [];


     public function getArea()
    {
        return $this->hasMany('App\Modelos\area','impresora');
    }
    
    public function info_str(){
        return $this->nombre." - ".$this->abcho." - ".$this->alto;
    }
}
