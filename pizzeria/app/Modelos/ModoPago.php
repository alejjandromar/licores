<?php
namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModoPago extends Model 
{
    protected $table = 'modopago';
    public $primaryKey='idPago';
    
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    
    
    
}
