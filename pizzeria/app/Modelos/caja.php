<?php


namespace App\Modelos;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class caja extends Model
{
    protected $table ="caja";
    protected $primaryKey = 'numero';
    public $timestamps=false;
    protected $guarded=[];
    
     public function getImpresora()
    {
        return $this->belongsTo('App\Modelos\impresora','impresora');
        
    }
    
    public function cierresA()
    {      
        $fecha=date('Y-m-d');
        return $this->hasMany('App\Modelos\cierrecaja', 'numero')->whereRaw("DATE(inicio) = '".$fecha."' AND final='0000-00-00 00:00:00'");
    }
    
    public function cierresC()
    {      
        $fecha=date('Y-m-d');
        return $this->hasMany('App\Modelos\cierrecaja', 'numero')->whereRaw("DATE(inicio) = '".$fecha."' AND final<>'0000-00-00 00:00:00'")->orderBy('inicio');
    }
}
