<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    
    public    $primaryKey='idProducto';
    protected $table = 'productos';
    
    public function ObtenerPrecios()
    {
        return $this->hasMany('App\Modelos\Precio','producto')->where('estado',1)->orderBy('costo');
    }
	
	public function Precios()
    {
        return $this->hasMany('App\Modelos\Precio','producto')->orderBy('costo');
    }
    
     public function Descuento()
    {
        return $this->belongsToMany('App\Modelos\Descuento','descuentoproducto', 'idProducto', 'idDescuento' )->where('estado',1);
    } 
     public function modificadores()
    {      
        return $this->belongsToMany('App\Modelos\modificador', 'productotienemodificador','producto', 'modificador')->where('estado',1);
    }
}
