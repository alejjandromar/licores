<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class FacturaTienePropina extends Model
{
    public $primaryKey= array ('factura','idPropina');

     protected $table = 'facturatienepropina';
}