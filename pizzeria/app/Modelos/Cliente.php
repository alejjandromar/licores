<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

/*
 * 
 */
class Cliente extends Model {

    protected $table = 'cliente';
    public $primaryKey = 'id';
    protected $guarded = [];
    
    public function facturas(){
        return $this->hasMany('App\Modelos\Orden', 'cliente');
    }
	
	//obtener direcciones frecuentes del usuario
	public function direccionesFavoritas(){
        return $this->hasMany('App\Modelos\direccion', 'idCliente')->where('favorita',1)->orderBy('updated_at', 'DESC');
    }
	
	//ordenes
	public function ordenes(){
        return $this->hasMany('App\Modelos\orden', 'cliente');
    }
}