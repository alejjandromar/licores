<?php

namespace  App\Modelos;

use Illuminate\Database\Eloquent\Model;

class detalletienemodificador extends Model
{
    public $primaryKey= array ('idDetalle','idModificador','NumItemsDet');
     public $timestamps = false;
     protected $table = 'detalletienemodificador';
     
    
}
