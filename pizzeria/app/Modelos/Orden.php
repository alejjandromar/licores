<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model 
{
    /*
     * Estado: { 0: Desactiva 
     *           1: Guardada 
     *           2: En Cobro 
     *           3: Registrando
     *           4: Atendida
     *           5: Por cobrar
				 6:cocinada}   
     *  
     */
    protected $table = 'facturas';
    public $primaryKey='idFactura';
    public $attributes=[];
   


    public function detalles()
    {      
        return $this->hasMany('App\Modelos\Detalle', 'factura');
    }
    
    public function cupones()
    {      
        return $this->belongsToMany('App\Modelos\cupon', 'facturatienecupon','idFactura', 'idCupon')->withPivot('montoPagado');
    }
    public function getcliente()
    {      
        return $this->belongsTo('App\Modelos\Cliente','cliente');
    }
    public function getoperador()
    {      
        return $this->belongsTo('App\User', 'despachado_por');
    }
     public function getmotorizado()
    {      
         return $this->belongsToMany('App\User', 'empleado_envia_pedido','id_factura', 'id_empleado');
    }
	public function direccion()
    {      
        return $this->belongsTo('App\Modelos\direccion','idDireccion');
    }
	public function getalmacen()
    {      
        return $this->belongsTo('App\Modelos\almacen','almacen');
    }
    static public function ordenesCanceladas($request)
    {   
        if($request->tipo==1){
            //todas las ordenes
            $ordenes=Orden::with('getcliente','getmesero','getcajero')->where('estado','=',0)->orderBy('created_at','asc')->get(array('total','subTotal','propina','impuesto','descuento','extras','estado','created_at','comentario','idFactura','mesa','cajero','empleado','cliente'));
            $total=Orden::where('estado','=',0)->sum('total');
        }else{
            if($request->tipo==2){
                //todas las facturas en un año
                $ordenes=Orden::with('getcliente','getmesero','getcajero')->where('estado',0)->whereRaw('YEAR(created_at)= '.$request->año)->orderBy('created_at','asc')->get(array('total','subTotal','propina','impuesto','descuento','extras','estado','created_at','comentario','idFactura','mesa','cajero','empleado','cliente'));
                $total=Orden::where('estado',0)->whereRaw('YEAR(created_at)= '.$request->año)->sum('total');
           }else{
                if($request->tipo==3){
                    //todas las facturas de un mesa de un año
                    $ordenes=Orden::with('getcliente','getmesero','getcajero')->where('estado',0)->whereRaw("Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->orderBy('created_at','asc')->get(array('total','subTotal','propina','impuesto','descuento','extras','estado','created_at','comentario','idFactura','mesa','cajero','empleado','cliente'));
                    $total=Orden::where('estado',0)->whereRaw("Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->sum('total');
                }else{
                     if($request->tipo==4){
                        //todas las facturas de una fecha especifica
                        $ordenes=Orden::with('getcliente','getmesero','getcajero')->where('estado',0)->whereRaw("DATE(created_at) = '".$request->fecha."'")->orderBy('created_at','asc')->get(array('total','subTotal','propina','impuesto','descuento','extras','estado','created_at','comentario','idFactura','mesa','cajero','empleado','cliente'));
                        $total=Orden::where('estado',0)->whereRaw("DATE(created_at) = '".$request->fecha."'")->sum('total');
                        
                     }else{
                         //todas las facturas entre un rango de fechas
                        $ordenes=Orden::with('getcliente','getmesero','getcajero')->where('estado',0)->whereRaw("DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->orderBy('created_at','asc')->get(array('total','subTotal','propina','impuesto','descuento','extras','estado','created_at','comentario','idFactura','mesa','cajero','empleado','cliente'));
                        $total=Orden::where('estado',0)->whereRaw("DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->sum('total');
                       }
                }
            }
        }
        $cantF=count($ordenes);
         return (['cantF'=>$cantF,'Total'=>$total,'ordenes'=>$ordenes]);
    }
	
	
	//Convierte un detalle en un objeto que solo posea
    //atributos para insertar en la tabla detalle
    static public function create_orden($pOrden) {
        $orden = new Orden;
        $orden->total = $pOrden->total;
		$orden->tipo = $pOrden->tipo;
		$orden->cliente = $pOrden->cliente;
		$orden->empleado = $pOrden->empleado;
		$orden->estado = $pOrden->estado;
		$orden->subTotal = $pOrden->subTotal;
		$orden->propina = $pOrden->propina;
		$orden->descuento = $pOrden->descuento;
		$orden->impuesto = $pOrden->impuesto;
		$orden->cajero = $pOrden->cajero;
		$orden->extras = $pOrden->extras;
		if (isset($pOrden->comentario))
			$orden->comentario =  $pOrden->comentario;
		$orden->mesa = $pOrden->mesa;
		$orden->caja = $pOrden->caja;
		$orden->cambio = $pOrden->cambio;
		
        return $orden;
    }
}

