<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
   protected $guarded = [];
   protected $table = 'personas';
   public $primaryKey='dni';
   public $timestamps = false;
   
    public function ordenes()
    {      
        return $this->hasMany('App\Modelos\Orden', 'cliente');
    }
}
