<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class opcionesmodificador extends Model
{
    public    $primaryKey='id';
    protected $table = 'opcionesmodificador';
     public $timestamps = false;
    
     public function modificador()
    {      
        return $this->belongsTo('App\Modelos\modificador', 'id');
    }
}
