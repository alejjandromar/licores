<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class PagoFactura extends Model 
{
    protected $table = 'facturatienepago';
    public $timestamps = false;
    public function getOrden()
    {      
        return $this->belongsTo('App\Modelos\Orden', 'factura');
    }
    
    public function getModoPago()
    {      
        return $this->belongsTo('App\Modelos\ModoPago', 'tipoPago');
    }
    
    
}

