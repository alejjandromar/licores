<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class direccion extends Model 
{
    protected $table = 'direccion';
    public $primaryKey='id';
    public $attributes=[];
   

    public function getcliente()
    {      
        return $this->belongsTo('App\Modelos\direccion','idCliente');
    }
}

