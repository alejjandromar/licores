<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class propina extends Model
{
     public $primaryKey='idPropina';
     protected $table = 'propina';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    public function Ordenes()
    {      
         return $this->belongsToMany('App\Modelos\Orden','FacturaTienePropina','idPropina', 'factura');
    }
}
