<?php

namespace App\Modelos;

use Illuminate\Database\Eloquent\Model;

class modificador extends Model
{
    public    $primaryKey='id';
    protected $table = 'modificador';
    
    public function productos()
    {      
         return $this->belongsToMany('App\Modelos\Producto', 'productotienemodificador','modificador', 'producto');
    }
    
    public function opciones()
    {      
        return $this->hasMany('App\Modelos\opcionesmodificador', 'modificador','id');
    }
    public function detalles()
    {      
         return $this->belongsToMany('App\Modelos\Detalle', 'detalletienemodificador','idModificador', 'idDetalle');
    }
}
