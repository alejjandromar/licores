<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use View;
class AdminAuth
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $nexts
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       /* if ($this->auth->guest() || Auth::user()->tipo !='Admin') {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                if ($this->auth->guest())
                    return redirect()->guest('');
                else
                    return redirect()->guest('mesas')->with("Error", "No autorizado");;
            }
        }
        View::share('user', Auth::user());
        $resturante = \App\Modelos\Restaurante::all()[0];
        View::share('restaurante',$resturante);*/
        return $next($request);
    }
}
