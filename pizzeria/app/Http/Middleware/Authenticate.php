<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use View;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Collection;
use Session;
use Cache;
use App\Modelos\cierrecaja;


class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    
    public static function usuario()
    {
        return Auth::user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $tipo="")
    {
       $users= collect ([$tipo, "Admin","Supervisor"]);
        //($tipo != Auth::user()->tipo && Auth::user()->tipo !="Admin")
      //dd(Authenticate::usuario(),Auth::user(),$this->auth->guest(),$users);
      View::share('user', Auth::user());
      
        if ($this->auth->guest() || ($tipo!="" && !$users->contains(Auth::user()->tipo))) {
            if ($request->ajax() ) {
                return response('Unauthorized.', 401);
            } else {
                if ($this->auth->guest())
                    return redirect()->guest('');
                else
                    return redirect()->guest('mesas')->with("Error", "No autorizado");;
            }
        }
        if (Auth::user()->tipo == "Admin" || Auth::user()->tipo=="Supervisor" || Auth::user()->tipo== "Cajero"){
            $fecha=date('Y-m-d');
            $caja=cierrecaja::whereRaw("DATE(inicio) = '".$fecha."' AND cajero='".Auth::user()->dni."' AND final='0000-00-00 00:00:00'")->get();
            if(count($caja)!=0){
                 $caja = $caja[0]->numero;
             }else{
                 $caja =-1;
             }
             Cache::put('caja'.Auth::user()->dni, $caja,1500);
            //Session::forget('caja');
            //$caja=-1;
            View::share('caja',$caja );
        }else{
             View::share('caja',null );
        }
        return $next($request);
    }
}
