<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\OrdenController;
use App\Http\Controllers\ProductoController;
use App\Modelos\impuesto;
use App\Modelos\propina;
use Illuminate\Http\Request;
use Cache;

class MenuController extends Controller {

    public function Vender(Request $request) {
        if ($request->actualizar == "true") {
            $menu = ProductoController::VerMenu($request); //Producto::all(); 
            return response()->json(['error' => false, 'categorias' => $menu["categorias"]]);
        }
        $num_mesa = $request->get('mesa', "No");
        if ($num_mesa == 'No') {
            $mensaje = "Se debe seleccionar una mesa";
            return response()->json(['error' => true, 'mensaje' => $mensaje, 'direccion' => 'app.mesas']);
        }
        $orden = Cache::get('orden' . $num_mesa, []);
        if (isset($orden->estado)) {
            if ($orden->estado < 3) {//La orden puede tener mas productos
                if ($orden->estado == 2 && true) {//La orden se esta cobrando y es mismo usuario?
                    $mensaje = "Debe cancelar el pago para poder hacer más pedidos";
                    return response()->json(['error' => true, 'mensaje' => $mensaje, 'direccion' => 'app.pagar']);
                    //$redirect=redirect()->route('pagar',['mesa' => $num_mesa]);
                    //return($redirect->with('Error', 'Debe cancelar el pago para poder hacer más pedidos')); 
                } else {
                    $mensaje = "No puede acceder a esa mesa actualmente esta siendo usada por otro usuario";
                    return response()->json(['error' => true, 'mensaje' => $mensaje, 'direccion' => 'app.mesas']);
                    //$redirect=redirect()->route('ruta_mesas')->with('Error', 'No puede acceder a esa mesa actualmente esta siendo usada por otro usuario');
                    //return($redirect);
                }
            }
        }
        $orden = OrdenController::VerOrden($request);
        $impuestos = impuesto::where('estado', '=', 1)->orderBy('tipo', 'desc')->get();
        $propinas = propina::where('estado', '=', 1)->orderBy('tipo', 'desc')->get();
        Cache::forget('impuestos');
        Cache::forget('propinas');
        Cache::put('impuestos', $impuestos, 1500);
        Cache::put('propinas', $propinas, 1500);
        return response()->json(['error' => false, 'orden' => $orden, 'fecha_actual' => \Carbon\Carbon::now()->toTimeString()]);
        //return view('menunuevo', ['categorias' => $menu["categorias"], 'datos' =>$menu["datos"], 'orden' => $orden]);
    }

    public function Vend(Request $request, $id) {
        $menu = ProductoController::VerM($id); //Producto::all();      
        //$orden   = OrdenController::VerOrden($request);
        return view('product', ['datos' => $menu["datos"]]);
    }

}
