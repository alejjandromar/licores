<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Modelos\Orden;
use App\Modelos\empleadoenviapedido;
use DB;
use Auth;
use View;
use Cache;
class trackingController extends Controller {
    
	//funcion que busca los empleados que estan disponibles para enviar un pedido
	public function empleadosDisponibles(Request $request) {
        try {
            $motorizados=User::where('tipo',1)->where('estado',2)->get();
			$orden = Orden::find($request->idOden);
			$motorizado=$orden->getmotorizado->first();
			if($motorizado!=null){
				$motorizados->push($motorizado);
			}
            return response()->json(['error'=>false,'motorizados'=>$motorizados,'mensaje'=>'mensaje de pruebaas']);
        } catch (\Illuminate\Database\QueryException $ex) {
           $mensaje= $ex->errorInfo[2];
           return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }
	
	//funcion que retorna la posicion actual del motorizado que atiende un pedido
	public function posicionActualEmpleadoAdmin(Request $request) {
        $user = User::find($request->idEmpleado);
        $asistencia = Orden::find($request->idOrden);
        return response()->json([
			'latitud' => $user->latitud,
			'longitud' => $user->longitud,
			'estado' => $asistencia->estado
		]);
    }
	
	
	//funcion que guarda los datos del envio de pedido
	public function guardarEnvio(Request $request) {
        try {
			$datos=json_decode($request->datos);
			$orden = Orden::find($request->idOrden);
			if($orden!=null){
				if($datos->empresa->id==2){
					$empleado=User::find($datos->empresa->motorizado->id);
					if($orden->getmotorizado->count()>0&&$orden->getmotorizado->first()->id==$datos->empresa->motorizado->id&&
					$orden->getalmacen->id==$datos->almacen->id){
						return response()->json(['error'=>true,'mensaje'=>'Debe modificar algun parametro del envio']);
					}
					if($empleado!=null&&($orden->getmotorizado->count()>0&&$orden->getmotorizado->first()->id==$datos->empresa->motorizado->id||$empleado->estado==2)){
						//se va a realizar el envio por medios propios
						
						//se inicia una transaccion
						DB::beginTransaction();
						if ($orden->getmotorizado->count()>0&&$orden->getmotorizado->first()->id!=$datos->empresa->motorizado->id){
							//ya tenia un motorizado asignado
							$ordenCancelada=$orden;
							$empleadoAnt = User::find($orden->getmotorizado->first()->id);
							$empleadoAnt->estado = 2;	//lo coloco libre
							$empleadoAnt->save();
							$ordenCancelada->estado=-1;
							//se debe enviar una notificacion push indicando al empleado viejo que se le cancelo esa asistencia
							enviar_notificacion("La asistencia ha sido cancelada por el operador",'Asistencia Cancelada','actualizarOrden',$ordenCancelada,[$empleadoAnt->push_token]);
							
							//se guardan en cache para cuando se ejecute la funcion del segudo plano
							$ordenesCancel = Cache::get('ordenesCancel' .$empleadoAnt->id, []);
							array_push($ordenesCancel, $ordenCancelada);
							Cache::put('ordenesCancel' .$empleadoAnt->id, $ordenesCancel, 1500);
						}
						$orden->distancia_envio=round($datos->distanciaTotal/1000);
						$orden->tiempoEnvio=round($datos->tiempoTotal/60);
						$orden->tipo_envio=2;
						$orden->despachado_por = (Auth::user()!=null)?Auth::user()->id:1;
						$orden->estado=4;
						$orden->almacen=$datos->almacen->id;
						$orden->save();
						
						//pongo en ocupado el empleado que va a tender la orden
						$empleado->estado=3;
						$empleado->save();
						
						//si la orden tiene motorizado, borro en la tabla relacion
						if($orden->getmotorizado->count()>0){
							$orden->getmotorizado()->detach();
						}
						//guardo el empleado que envia el pedido
						$envio=new empleadoenviapedido();
						$envio->id_factura=$orden->idFactura;
						$envio->id_empleado=$datos->empresa->motorizado->id;
						$envio->save();
						DB::commit();
						//End Transation
						
						//debo mandar a buscar la orden asi tal cual se envian a las diferentes apps.
						$ordenAPP = Orden::select(DB::raw('(1 *0) as `notificacion`,`idFactura`,`total`,`estado`, `subTotal`,`created_at`, `updated_at`,`tiempoEnvio`,`distancia_envio`'))->where('idFactura',$request->idOrden)->where('estado', '<>', -1)->get()[0];
						
						
						//mando a enviar la notificacion push al empleado de que se le asigno una nueva orden
						//enviar_notificacion("Se ha asignado a una nueva asistencia",'Nueva asistencia','orden',$ordenAPP,[$empleado->push_token]);
						
						//mando a enviar la notificacion push al cliente de que se le asigno un motorizado a su orden
						enviar_notificacion("Se ha asignado un motorizado",'Se ha asignado un motorizado a su orden','actualizarOrden',$ordenAPP,[$orden->getcliente->push_token]);
						
						//retorno el onjeto de la manera que lo procesa el administrador
						$orden = Orden::where('idFactura',$request->idOrden)->with('getcliente', 'detalles.getproducto', 'detalles.getpresentacion', 'cupones','direccion','getoperador','getmotorizado','getalmacen')->get()[0];
						return response()->json(['error'=>false,'orden'=>$orden]);
					}else{
						return response()->json(['error'=>true,'mensaje'=>'Datos inválidos']);
					}
					
				}else{
					//se va a realizar el envio por ivoy
				}
			}else{
				 return response()->json(['error'=>true,'mensaje'=>'Datos inválidos']);
			}
        } catch (\Illuminate\Database $ex) {
			DB::rollback();
           $mensaje= 'Ocurrio un error al tratar de procesar la peticion';
           return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }
    
    
}

//funcion para enviar una natificacion push
function enviar_notificacion($body,$titulo,$tipo,$parametro,$registrationIds){
	$msg = array(
		'body' => $body,
		'title' => $titulo,
		'tipo' => $tipo,
		'info' => json_encode($parametro),
		'vibrate' => 1,
		"priority"=> 2,
		'content-available' => 1
	);
	push($registrationIds, $msg);
}

//funcion que se comunica via curld con el servidor de gcm para enviar la notificacion
function push($registrationIds, $msg) {
    $fields = array
        ('registration_ids' => $registrationIds,
        'data' => array( 'image'=>"https://www.educima.com/imagen-grua-dm28119.jpg",'largeIcon' => 'large_icon',
                    'smallIcon' => 'small_icon'),
        'notification' => $msg );

    $headers = array
        (
        'Authorization: key= AIzaSyDmfb3hI1-B1D9zNdGbJy4pC8XQkIJONgc',
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);
}
    