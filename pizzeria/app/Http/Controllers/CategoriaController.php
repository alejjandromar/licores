<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modelos\Categoria;
use DB;
use View;

class CategoriaController extends Controller
{
	/*
		Muestra la pantalla prncipal de una categoria (DESUSO)
	*/ 
    
      public function index()
    {
         return view('layouts.categoria');
    }
    
	
	/*
		OBTIENE TODAS LAS CATEGORIAS
	*/
   static public function all() {
        $categorias = Categoria::all();
        return response()->json(['categoria' => $categorias]);
    }

	/*
		INSERTA O ACTUALIZA UNA CATEGORIA
	*/
       static public function adminCatInsertar(Request $request) { 
           
              try {
					$datos = json_decode($request->categoria);
                    $var= rand(1,1000);
					if (!isset($datos->idCategoria)){
						$categoria = new Categoria();
						 try {
                if (!$request->hasFile('file') || !$request->file('file')->isValid()) {
                    return response()->json(['mensaje' => 'Debe colocar una imagen']);
                }
            } catch (Exception $ex) {
                return response()->json(['mensaje' => 'La imagen no ha podido ser cargada']);
            }
					}else{
						$categoria = Categoria::FirstorNew(['idCategoria'=> $datos->idCategoria]);
					}
                    $categoria->nombre = $datos->nombre;
                    $categoria->descripcion = $datos->descripcion;
					$categoria->ico = $datos->ico;
                    //$categoria->imagen = "noimg.jpg";
                    $categoria->save();
					//NO ELIMINADO POR TESIS VERSION
                    if ($request->has('parent_category') && $request->parent_category !=-1) {
                       $categoria->primaria = $request->parent_category;
                       $categoria_p = Categoria::find($request->parent_category);
                       $categoria_p->has_sub = 1;
                       $categoria_p->save();
                    }
					if ($request->hasFile('file') && $request->file('file')->isValid()) {
					   $request->file('file')->move("imagenes/categorias", $categoria->nombre . '_' . $var . '_id-' . $categoria->idCategoria . '.' . $request->file('file')->getClientOriginalExtension());
						
					  $categoria->imagen = "imagenes/categorias/" . $categoria->nombre . '_' . $var . '_id-' . $categoria->idCategoria . '.' . $request->file('file')->getClientOriginalExtension();
					}                  
				  $categoria->save();
				$categorias = Categoria::all();
                return response()->json(['error' => false, 'mensaje' => 'La categoria ha sido registrada exitosamente.', 'categoria' => $categorias]);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una Categoria con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una Categoria con ese nombre en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }  

    }
    
   
	/*
		FUNCION QUE ELIMINA UNA CATEGORIA
	*/
    static public function Delete(Request $request) {
        try {
            $categoria = Categoria::find($request->id);
            $categoria->delete();
			$categorias = Categoria::all();
            return response()->json(['error'=>false,'Message' => "Categoria Eliminada", 'categoria' => $categorias]);
        } catch (Exception $ex) {
            return response()->json(['error'=>true,'Message' => "La categoria no pudo ser Eliminada"]);
        }
    }
    
	
	/*
		FUNCION QUE CAMBIA EL ESTADO DE UNA CATEGORIA
	*/
      public function estado(Request $request) {
        try {
            //busca Registro 
             $categoria = Categoria::find($request->id);
             
            if($categoria!=null){
                $categoria->estado =  $categoria->estado==0?1:0;
                $categoria->save();
				$categorias = Categoria::all();
                if($request->estado==0){
                    $mensaje="El area ha sido desactivada exitosamente.";
                }else{
                     $mensaje="El area ha sido activada exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'no existe una categoria con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje,'categoria' => $categorias ]);
    }
    
    
}