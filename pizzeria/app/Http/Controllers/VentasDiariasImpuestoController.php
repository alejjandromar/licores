<?php
namespace App\Http\Controllers;
use View;

use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\impuesto;
use Illuminate\Http\Request;
use App\Modelos\FacturaTieneImpuestos;
use DB;
use Cache;
use Auth;

class salida{
        public    $fechas;
        public    $cantidad=0;
        public    $cuota=0;
}
class impuestoS{
        public    $impuesto='';
        public    $cuota=0;
        public    $total=0;
        public    $cantidad=0;
        public    $base=0;
        
}
class fecha{
        public    $fecha;
        public    $impuestos=array();
        
}
function calcularImpuestos($fechas,$impuestos){
    $cantidadTotal=0;$cuotaTotal=0;
   if(count($fechas)!=0){
        $resultados=array();
        foreach ($fechas as $row) {
            $base=0;$cantidad=0;
            $fecha=new fecha();
            $fecha->fecha=$row->fecha;
            $ordenes=Orden::whereRaw("estado=1 and DATE(created_at) = '".$row->fecha."'")->get();
            $cantidad=count($ordenes);
            $cantidadTotal+=$cantidad;
            if($cantidad>0){
                 $ids=array();
                foreach ($ordenes as $orden) {
                    $base+=$orden->subTotal;
                     array_push($ids,$orden->idFactura);
                }
                $fecha->base=$base;
                foreach ($impuestos as $impuesto) {
                    $impuestoS=new impuestoS();
                    $impuestoS->impuesto=$impuesto->nombre.' ('.$impuesto->valor;
                    if($impuesto->tipo=='porcentaje'){
                        $impuestoS->impuesto.='%)';
                    }else{
                        $impuestoS->impuesto.=' &euro;)';
                    }
                    $impuestoS->cuota=FacturaTieneImpuestos::where('idImpuesto',$impuesto->idImpuesto)->whereIn('factura',$ids)->sum('valorImpuesto');
                    $impuestoS->cantidad=FacturaTieneImpuestos::where('idImpuesto',$impuesto->idImpuesto)->whereIn('factura',$ids)->count('valorImpuesto');;
                    $cuotaTotal+=$impuestoS->cuota;
                    $impuestoS->total=($base+$impuestoS->cuota);
                    array_push($fecha->impuestos,$impuestoS);
                }
            }
            array_push($resultados,$fecha);
        }
    }else{
        $resultados=null;
    }
    $salida=new salida();
    $salida->fechas=$resultados;
    $salida->cuota=$cuotaTotal;
    $salida->cantidad=$cantidadTotal;
    return ($salida);
}
   
class VentasDiariasImpuestoController extends Controller {
    public function index()
    {
        return view('layouts_reportes.ReporteVentasImpuestosDia');
    }

    //Funciones Ajax
    //funcion ajax que devuelve los datos a mostrar en el reporte.
    public function VentasDiariasPorImpuestos(Request $request) {
         $impuestos = impuesto::where('estado','=',1)->get(array('idImpuesto','valor','nombre','tipo'));
        if($request->tipo==1){
            $fechasDiferentes=Orden::where('estado',1)->distinct()->select(DB::raw('DATE(created_at) as fecha'))->orderBy('created_at','asc')->get();
            $resultados=  calcularImpuestos($fechasDiferentes,$impuestos);
            $base=Orden::where("estado",1)->sum('subTotal');
        }else{
            if($request->tipo==2){
                $fechasDiferentes=Orden::whereRaw("estado=1 and YEAR(created_at) = ".$request->año)->distinct()->select(DB::raw('DATE(created_at) as fecha'))->orderBy('created_at','asc')->get();
                $resultados=  calcularImpuestos($fechasDiferentes,$impuestos);
                $base=Orden::whereRaw("estado=1 and YEAR(created_at) = ".$request->año)->sum('subTotal');
            }else{
                if($request->tipo==3){
                    $fechasDiferentes=Orden::whereRaw("estado=1 and Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->distinct()->select(DB::raw('DATE(created_at) as fecha'))->orderBy('created_at','asc')->get();
                    $resultados=  calcularImpuestos($fechasDiferentes,$impuestos);
                    $base=Orden::whereRaw("estado=1 and Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->sum('subTotal');
                }else{
                     if($request->tipo==4){
                         $fechasDiferentes=Orden::whereRaw("estado=1 and DATE(created_at) = '".$request->fecha."'")->distinct()->select(DB::raw('DATE(created_at) as fecha'))->orderBy('created_at','asc')->get();
                         $resultados=  calcularImpuestos($fechasDiferentes,$impuestos);
                         $base=Orden::whereRaw("estado=1 and DATE(created_at) = '".$request->fecha."'")->sum('subTotal');
                     }else{
                         $fechasDiferentes=Orden::whereRaw("estado=1 and DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->distinct()->select(DB::raw('DATE(created_at) as fecha'))->orderBy('created_at','asc')->get();
                         $resultados=  calcularImpuestos($fechasDiferentes,$impuestos);
                         $base=Orden::whereRaw("estado=1 and DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->sum('subTotal');
                     }
                }
            }
            
        }
        return response()->json(['fechas' => $resultados->fechas,'totalF'=>$resultados->cantidad,'totalC'=>$resultados->cuota,'totalB'=>$base,'totalM'=>($base+$resultados->cuota),'totalIm'=>count($impuestos)]);
    }

}
