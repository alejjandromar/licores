<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use DB;
class EmpleadoController extends Controller {

    static public function CRUD(Request $request) {
        return view('layouts.Empleado');
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla impuesto
    public function Select_all(Request $request) {
        $user = User::all();
        return response()->json(['empleado' => $user]);
    }

    //Select_one: Selecciona un solo registro
    public function Existe(Request $request) {
        $user = User::find($request->id);
        return response()->json(['empleado' => $user]);
    }

    //Create: Crea y Guarda un objeto de tipo impuesto
    public function Create(Request $request) {
        try {//Guadar Registro 
          $datos = json_decode($request->usuario);
		  if (!isset($datos->dni)){
			$user = new User();
		  }else{
			$user =User::find($datos->dni);; 
		  }
            $user->password  = bcrypt($datos->password);
            $user->email  = $datos->email;
            $user->telefono  = $datos->telefono;
            $user->name  = $datos->name;
            $user->tipo  = $datos->tipo;
            $user->save();
			 $usuarios = User::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'usuario' => $usuarios]);
         }catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ese dni en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ese dni en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
    }
    
            }

  
    //Update_status: Actualiza el estado de un impuesto
    public static function Update_status(Request $request) {
        try {//Guadar Registro 
            $user = User::find($request->id);
            $user->estado = $user->estado ==1?0:1;
            $user->save();
			
        } catch (Exception $ex) {
         return response()->json(['error' => true, 'mensaje' => "El usuario no ha podido cambiar el estado"]);
        }
       $usuarios = User::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'usuario' => $usuarios]); }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
         
            $user = User::find($request->id);
            $user->delete();
            $usuarios = User::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'usuario' => $usuarios]);
        } catch (Exception $ex) {
            return response()->json(['error' => true,'mensaje' => "El Usuario  no ha podido ser Eliminado"]);
        }
    }

}
