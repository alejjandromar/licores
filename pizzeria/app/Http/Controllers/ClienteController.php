<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\clienteusacupon;
use App\Modelos\Cliente;
use App\Modelos\direccion;
use App\Modelos\cupon;
use Illuminate\Http\Request;
use Cache;
use DB;
use Auth;
use App\User;

/*
 *  Contralador para llos clientes
 */

//retorna los errores que pueda generar manipular las tablas de este controlador(menos de clave primaria).
function buscarError($ex) {
    //se incializa el mensaje
    $mensaje = "";
    $codigo = $ex->errorInfo[1];
    //si es una violacion de llave  unica
    if ($codigo == 1062) {
        //restircciones uniques de tabla productos
    } else {
        //violacion de llave foranea
        if ($codigo == 1452 || $codigo == 1451) {
            $resultado = strpos($ex->errorInfo[2], "fk_facturas_idDireccion");
            if ($resultado !== false) {
                $mensaje = "No se puede borrar la direccion porque esta asociado a una orden";
            }
        } else {
            //si fue un trigguer o cuarquier otro error inesperado.
            $mensaje = $ex->errorInfo[2];
        }
    }
    return $mensaje;
}

class ClienteController extends Controller {

    //Funciones Ajax
    //Funcion que registra temporalmente a un cliente
    public function BuscarCliente(Request $request) {

        $cliente = Cliente::where('dni', $request->DNI)->take(1)->get();
        return response()->json(['cliente' => $cliente]);
    }

    //funcion para cargar todas las direcciones favoritas de un cliente
    public function cargarDireccionesFavoritasCliente(Request $request) {
        $asistencias = direccion::where('idCliente', $request->idUser)->where('favorita', 1)->orderBy('updated_at', 'DESC')->paginate(15);
        return response()->json(['direcciones' => $asistencias->toJson()]);
    }

    //funcion para agregar  una direccion favorita de un cliente
    public function agregarDireccionFavorita(Request $request) {
        $pDireccion = json_decode($request->direccion);
        $cliente = Cliente::find($request->idUser);
        if ($cliente == null) {
            return response()->json(['error' => true, 'mensaje' => 'Datos invalidos']);
        }
        $direccion = new direccion();
        $direccion->latitud = $pDireccion->lat;
        $direccion->longitud = $pDireccion->lon;
        $direccion->texto = $pDireccion->direccion;
        $direccion->favorita = 1;
        $direccion->idCliente = $cliente->id;
        $direccion->alias = $pDireccion->alias;
        $direccion->save();
        return response()->json(['error' => false, 'mensaje' => 'La direccion ha sido registrada exitosamente']);
    }

    //funcion para validar la direccion de un pedido
    public function verificarDireccion(Request $request) {
        $pData = json_decode($request->data);
        $fields = array('data' => $pData);
        $headers = array('Content-Type: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api-uat.ivoy.mx:2083/api/order/validZipCode/json/web');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //dd(curl_error($ch),$result);
        curl_close($ch);
        $respuesta = json_decode($result);

        //proceso la respuesta
        if ($respuesta == null) {
            //quiere decir que ocurrio un error en la peticion (404,505 etc);
            //hay que validar solo contra los repartidores de ivoy
            return response()->json(['code' => 0]);
        } else {
            //quiere decir que hubo una respuesta satisfactoria por parte de la apy de ivoy
            if ($respuesta->code == 0) {
                //direccion valida
                return response()->json(['code' => 0]);
            } else {
                //direccion invalida
                return response()->json(['code' => -1]);
            }
        }
    }

   
    //funcion para eliminar una direccion
    public function eliminarDireccion(Request $request) {
        try {
            $direccion = direccion::find($request->direccion);
            if ($direccion != null) {
                $direccion->delete();
                return response()->json(['error' => false]);
            } else {
                return response()->json(['error' => true, 'mensaje' => 'Datos invalidos']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje = buscarError($ex);
            return response()->json(['error' => true, 'mensaje' => $mensaje]);
        }
    }

    //funcion para cargar las ordenes de un cliente
    public function cargarPedidos(Request $request) {
       
        $pedidos = Orden::select(DB::raw('(1 *0) as `notificacion`,`idFactura`,`total`,`estado`, `subTotal`,`created_at`, `updated_at`,`tiempoEnvio`,`distancia_envio`'))->where('cliente', $request->idUser)->where('estado', '<>', -1)->orderBy('updated_at', 'DESC')->paginate(15);
        return response()->json(['pedidos' => $pedidos->toJson()]);
    }

    //funcion para cargar las ordenes de un empleado
    public function cargarPedidosAsig(Request $request) {
         $user = User::find(1);
   
        $pedidos = $user->envios()->select(DB::raw('(1 *0) as `notificacion`,`idFactura`,`total`,`estado`, `subTotal`,`created_at`, `updated_at`'))->where('estado', '<>', -1)->orderBy('updated_at', 'DESC')->paginate(15);
        return response()->json(['pedidos' => $pedidos->toJson()]);
    }

    //funcion para cargar los cupones disponibles
    public function cargarCupones(Request $request) {
        $result = clienteusacupon::where('idCliente', $request->idUser)->get();
        if ($result->count() == 0) {
            //Voy a retornar todos los cupones (no he usado ninguno)
            $result = cupon::where('estado', 1)->get();
            return response()->json(['error' => false, 'cupones' => $result]);
        } else {
            $ids = array();
            foreach ($result->pluck('idCupon') as $id) {
                array_push($ids, $id);
            }
            $cupones = cupon::whereNotIn('id', $ids)->where('estado', 1)->get();
            //Voy a retornar todos los cupones (no he usado ninguno)
            return response()->json(['error' => false, 'cupones' => $cupones]);
        }
    }

    //funcion para que un cliente registre el cupon de otro cliente.
    public function clienteIngresaCupon(Request $request) {
        $cliente = cliente::find($request->idUser);
        if ($cliente != null) {
            if ($cliente->IdClienteReferencio == null) {
                $clienteReferencio = cliente::where('IdCupon', $request->idCupon)->get();
                if ($clienteReferencio->count() == 1) {
                    if ($clienteReferencio->first()->IdClienteReferencio != $cliente->id) {
                        $cliente->IdClienteReferencio = $clienteReferencio->first()->id;
                        $cliente->creditoCupon+=50.00;
                        $cliente->save();
                        return response()->json(['error' => false, 'credito' => $cliente->creditoCupon, 'idRef' => $clienteReferencio->first()->id]);
                    } else {
                        return response()->json(['error' => true, 'mensaje' => 'No se puede ingresar el cupon de un usuario que haya sido refereido por ti']);
                    }
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'Por favor ingrese un cupón válido']);
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => 'Solo se puede ingresar un cupón']);
            }
        } else {
            return response()->json(['error' => true, 'mensaje' => 'Datos invalidos']);
        }
    }

}
