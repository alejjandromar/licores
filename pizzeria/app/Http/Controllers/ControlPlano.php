<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Modelos\mesa;
use App\Modelos\area;
use Auth;
use View;
use Cache;
use Session;

//este es el que usa
class ControlPlano extends Controller {

    public function index() {
        $areas = area::where('tipo', '1')->get();
        return view('layouts.mesas')->with('area', $areas);
    }

    public function crea() {
        
        $cuentas = $mesas = mesa::select()->where('Area', '1')->count();
        $mesas = mesa::all();
        $areas = area::where('tipo', '1')->get();
        return view('layouts.crear_mesa')->with('mesa', $mesas)->with('area', $areas)->with('cuenta', $cuentas);
    }

    public function home() {
        return view('layouts.home');
    }

    static public function all() {
        $mesas = mesa::orderby('idMesa')->get();
        $areas = area::where('tipo', '1')->get();
        return response()->json(['mesa' => $mesas, 'area' => $areas]);
    }

    static public function addMesa(Request $request) {

        try {

            $mesa = new mesa();
            $mesa->area = $request->area;
            $mesa->numeroSillas = $request->numerosilla;
             $mesa->idMesa = $request->idMesa;
            $mesa->save();
            return response()->json(['error' => false, 'mensaje' => 'La Mesa ha sido registrada exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1062) {
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado !== false) {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Mesa con ese identificador en la base de datos.']);
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Mesa igual en la base de datos.']);
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            }
        }
    }

    static public function updateMesa(Request $request) {

        try {//Guadar Registro 
            //$mesa = new mesa();
            $mesa = mesa::find($request->id);
            $mesa->area = $request->area;
            $mesa->numeroSillas = $request->numerosilla;
            $mesa->save();

            return response()->json(['error' => false, 'mensaje' => 'La Mesa ha sido actualizada exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1062) {
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado !== false) {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Mesa con ese identificador en la base de datos.']);
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Mesa igual en la base de datos.']);
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            }
        }
    }

    public function update_estado(Request $request) {
        try {
            //busca Registro 
            $mesa = mesa::find($request->id);

            if ($mesa != null) {
                $mesa->estado = $request->estado;
                $mesa->save();
                if ($request->estado == 0) {
                    $mensaje = "La Mesa ha sido desactivada exitosamente.";
                } else {
                    $mensaje = "La Mesa ha sido activada exitosamente";
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe una Mesa con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false, 'mensaje' => $mensaje]);
    }

    static public function delete(Request $request) {
        try {
            $mesa = mesa::find($request->id);
            $mesa->delete();
            return response()->json(['error' => false, 'Message' => "Mesa eliminada"]);
        } catch (Exception $ex) {
            return response()->json(['error' => true, 'Message' => "La Mesa no pudo ser eliminada"]);
        }
    }

    static public function mesa() {

        $mesas = mesa::all();

        $areas = area::select('nombre', 'idArea')->where('tipo', '1')->get();
        $id = "Todas";

        return View::make('mesas')->with('area', $areas)->with('mesa', $mesas)->with('ids', $id);
    }
	
    /*static public function mesas() {
        //Verde Disponible
        //Naranja Atendiendose
        //Rojo Ocupada
        $i = 1;
        $orden = [];
        $mesas = mesa::all();
        foreach ($mesas as $mesa) {
            $orden[$i] = Cache::get('orden' . $i, []);
            $mesa->estado = isset(Cache::get('orden' . $i, [])->estado) ? Cache::get('orden' . $i, [])->estado : 6;
            if ((Auth::user()->tipo == "Cajero" || Auth::user()->tipo == "Supervisor" || Auth::user()->tipo == "Admin") && ($mesa->estado == 5 || ($mesa->estado == 2 && Auth::user()->dni == $orden[$i]->empleado))) {
                $caja = Cache::get('caja' . Auth::user()->dni, -1);
                if ($caja == -1) {
                    $mesa->link = '#no_caja';
                } else {
                    $mesa->link = 'pagar?mesa=' . $i;
                }
            } else if ((Auth::user()->tipo == "Mesero" || Auth::user()->tipo == "Supervisor" || Auth::user()->tipo == "Admin")) {
                $mesa->link = 'menu?mesa=' . $i;
            } else {
                $mesa->link = '#';
            }
            //asignar el evento onLondPress
            $mesa->opciones = false;
            if ((Auth::user()->tipo == "Cajero" || Auth::user()->tipo == "Supervisor" || Auth::user()->tipo == "Admin") && ($mesa->estado != 6)) {
                $mesa->opciones = true;
            }
            $mesa->icono = " button button-clear ";
            if ($mesa->estado == 5 || $mesa->estado == 2) {
                if ($mesa->estado == 5) {
                    $mesa->icono.= "button-balanced ";
                } else if ($mesa->estado == 2 && Auth::user()->dni == $orden[$i]->empleado) {
                    $mesa->icono.= "button-energized ";
                } else {
                    $mesa->icono.= "button-assertive ";
                }
                $mesa->icono.='icon ion-social-euro ';
            } else {
                if ($mesa->estado == 6) {
                    $mesa->icono.= "button-balanced ";
                } else if ($mesa->estado == 3 && Auth::user()->dni == $orden[$i]->empleado) {
                    $mesa->icono.= "button-energized ";
                } else {
                    $mesa->icono.= "button-assertive ";
                }
                $mesa->icono.='icon ion-record ';
            }
            $i++;
        }
        return response()->json(['mesa' => $mesas, 'estado' => $orden]);
    }*/
	
	
	static public function mesas(Request $request) {
        //Verde Disponible
        //Naranja Atendiendose
        //Rojo Ocupada
        $orden = [];
       $mesas = mesa::where('estado',1)->get();
		$tipo= $request->tipo;
		$dni= $request->dni;
        foreach ($mesas as $mesa) {
			$i = $mesa->idMesa;
            $orden[$i] = Cache::get('orden' . $i, []);
            $mesa->estado = isset(Cache::get('orden' . $i, [])->estado) ? Cache::get('orden' . $i, [])->estado : 6;
            if (($tipo == "Cajero" || $tipo == "Supervisor" || $tipo == "Admin") && ($mesa->estado == 5 || ($mesa->estado == 2 && $dni == $orden[$i]->empleado))) {
                $caja = Cache::get('caja' . $dni, -1);
                if ($caja == -1) {
                    $mesa->link = '#no_caja';
                } else {
                    $mesa->link = 'pagar';
                }
            } else if (($tipo == "Mesero" || $tipo == "Supervisor" || $tipo == "Admin")) {
                $mesa->link = 'menu';
            } else {
                $mesa->link = '#';
            }
            //asignar el evento onLondPress
            $mesa->opciones = false;
            if (($tipo == "Cajero" || $tipo == "Supervisor" || $tipo == "Admin") && ($mesa->estado != 6)) {
                $mesa->opciones = true;
            }
            $mesa->icono = " button button-clear ";
            if ($mesa->estado == 5 || $mesa->estado == 2) {
                if ($mesa->estado == 5) {
                    $mesa->icono.= "button-balanced ";
                } else if ($mesa->estado == 2 && $request->dni == $orden[$i]->empleado) {
                    $mesa->icono.= "button-energized ";
                } else {
                    $mesa->icono.= "button-assertive ";
                }
                $mesa->icono.='icon ion-social-euro ';
            } else {
                if ($mesa->estado == 6 ) {
                    $mesa->icono.= "button-balanced ";
                } else if (($mesa->estado == 3 || $mesa->estado == 7 )  && $request->dni == $orden[$i]->empleado) {
                    $mesa->icono.= "button-energized ";
                }  else {
                    $mesa->icono.= "button-assertive ";
                }
               
				 if ($mesa->estado == 7) {
                    $mesa->icono.= "icon ion-coffee ";
                }else{
					 $mesa->icono.='icon ion-record ';
				}
            }
        }
        return response()->json(['mesa' => $mesas]);
    }

    public function ala(Request $request) {
        $i = 1;
        $orden = [];
        $mesas = mesa::all();
        foreach ($mesas as $mesa) {
            $orden[$i] = Cache::get('orden' . $i, []);
            $i++;
        }
        $areas = mesa::select()->where('Area', $request->id)->get();
        return response()->json(['area' => $areas, 'estado' => $orden]);
    }

    static public function save_coor(Request $request) {

        //Creamos el archivo datos.txt
//ponemos tipo 'a' para añadir lineas sin borrar
        $file = fopen("config_coordenadas.txt", "w") or die("Problemas");
        //vamos añadiendo el contenido
        fputs($file, $request->coor);

        fclose($file);
    }

    static public function get_coor() {
        $linea = "";
        $file = fopen("config_coordenadas.txt", "r");
		//Output a line of the file until the end is reached
        while (!feof($file)) {
            $linea = fgets($file);
        }
        fclose($file);
        return response()->json(['linea' => $linea]);
    }

    static public function del_coor(Request $request) {

        //Creamos el archivo datos.txt
//ponemos tipo 'a' para añadir lineas sin borrar
        $file = fopen("config_coordenadas.txt", "w") or die("Problemas");
        //vamos añadiendo el contenido
        fputs($file, "");

        fclose($file);
    }

    static public function select(Request $request) {

        $todas_mesas = mesa::all()->count();
        $mesas = mesa::select()->where('Area', $request->area)->get();
        $areas = area::select()->where('idArea', $request->area)->get();
        $cuenta = mesa::select()->where('Area', $request->area)->count();
        return response()->json(['mesa' => $mesas, 'area' => $areas, 'toda' => $todas_mesas, 'cuenta' => $cuenta]);
    }

}
