<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\Detalle;
use App\Modelos\Precio;
use Validator;
use App\Modelos\Producto;
use App\Modelos\modificador;
use App\Modelos\area;
use App\Modelos\impuesto;
use App\Modelos\detalletienemodificador;
use App\Modelos\cola_impresion;
use App\Modelos\caja;
use App\Modelos\mesa;
use App\Modelos\FacturaTieneImpuestos;
use App\Modelos\FacturaTienePropina;
use Illuminate\Http\Request;
use Cache;
use Auth;
use DB;

class nodo {

    public $canT;
    public $compuesto;
    public $idMod;
    public $nombre;
    public $precio;

}

//funcion que verifica si existe por lo menos un detalle con un producto determinado
//retorna true si existe un detalle con ese producto y falso de lo contrario
function verificarDetalles($arreglo, $id) {
    foreach ($arreglo as $row) {
        if ($row->producto == $id) {
            return true;
        }
    }
    return false;
}

function existeProducto($arreglo, $id) {
    if (count($arreglo) != 0) {
        foreach ($arreglo as $row) {
            if ($row[0]['pivot']['producto'] == $id) {
                return true;
            }
        }
        return false;
    } else {
        return false;
    }
}

function BuscarSub($arreglo, $id) {
    $X = -1;
    if (count($arreglo) != 0) {
        $i = -1;
        foreach ($arreglo as $row) {
            $i++;
            if ($row->idS == $id) {
                return $i;
            }
        }
        return $X;
    } else {
        return $X;
    }
}

//funcion que calcula y agrega el valor de los impuestos al total de la factura y los mete en las variables de cache.
function calcular($total, $subtotal, $num_mesa) {
    $impuestos = Cache::get('impuestos');
    $propinas = Cache::get('propinas');
    $impuestosO = array();
    $propinasO = array();
    $valorP = 0;
    $valorPr = 0;
    $valor_del_impuesto = 0;
    $valor_de_propina = 0;
    $valor_impuestos = 0;
    $valor_propina = 0;


    $i = 0;
    //se calculan los impuestos  
    $total = $subtotal;
    if ($subtotal > 0) {
        if (count($impuestos) > 0) {
            foreach ($impuestos as $impuesto) {
                $i++;
                $impuestoOrden = new FacturaTieneImpuestos;
                $tipo = strtolower($impuesto['tipo']);
                if ($tipo == "porcentaje") {
                    $valorPor = ($impuesto['valor']/100);
                    $valor_del_impuesto = $subtotal * $valorPor;
                    $total+=$valor_del_impuesto;
                    //se llena los datos del impuesto
                    $impuestoOrden->idImpuesto = $impuesto['idImpuesto'];
                    $impuestoOrden->porcentaje = $valorP;
                    $impuestoOrden->valorImpuesto = $valor_del_impuesto;
                    array_push($impuestosO, $impuestoOrden);
                } else {
                    $valor_del_impuesto = $impuesto['valor'];
                    $total+=$valor_del_impuesto;
                    //se llena los datos del impuesto
                    $impuestoOrden->idImpuesto = $impuesto['idImpuesto'];
                    $impuestoOrden->valorImpuesto = $valor_del_impuesto;
                    array_push($impuestosO, $impuestoOrden);
                }
            }
            $valor_impuestos = ($total - $subtotal);
        }
        $i = 0;
        //si calculan las propinas si las hay.
        if (count($propinas) > 0) {
            foreach ($propinas as $propina) {
                $i++;
                $propinaOrden = new FacturaTienePropina;
                $tipo = strtolower($propina['tipo']);
                if ($tipo == "porcentaje") {
                    $valorProp = ($propina['valor']/100);
                    $valor_de_propina = $total * $valorProp;
                    $total+=$valor_de_propina;
                    //se llena los datos de la propina
                    $propinaOrden->idPropina = $propina['idPropina'];
                    $propinaOrden->porcentaje = $valorPr;
                    $propinaOrden->valorPropina = $valor_de_propina;
                    array_push($propinasO, $propinaOrden);
                } else {
                    $valor_de_propina = $propina['valor'];
                    $total+=$valor_de_propina;
                    //se llena los datos de la propina
                    $propinaOrden->idPropina = $propina['idPropina'];
                    $propinaOrden->valorPropina = $valor_de_propina;
                    array_push($propinasO, $propinaOrden);
                }
            }
            $valor_propina = ($total - ($subtotal + $valor_impuestos));
        }
    }
    //su supone que ya tengo los detalles de unh impuesto y ahora los vaoy a guardar en mi arreglo de las variables de cache.
    Cache::put('valoImp' . $num_mesa, $valor_impuestos, 1500);
    Cache::put('valoProp' . $num_mesa, $valor_propina, 1500);
    Cache::put('impuestosO' . $num_mesa, $impuestosO, 1500);
    Cache::put('propinasO' . $num_mesa, $propinasO, 1500);
    //dd($subtotal,$valor_impuestos,$valor_propina,$total,$impuestosO,$propinasO);
    return $total;
}

class OrdenController extends Controller {
	
	public function cargarDatosPedido(Request $request) {
        $orden=Orden::where('idFactura',$request->idPedido)->with(['cupones','direccion','detalles.getpresentacion','detalles.getproducto'])->get();
		if($orden->count()>0){
			return response()->json(['error'=>false,'orden'=>$orden[0]]);
		}else{
			 return response()->json(['error'=>true,'mensaje'=>'Datos invalidos']);
		}
    }
	
	
	
	

    //Funciones Ajax
    //Funcion que registra temporalmente a un cliente
    public function RegistroCliTemp(Request $request, $num_mesa) {
        $cliente = new Persona;
        if($request->DNI!="" && $request->DNI!="00000000A"){
            $cliente->nombreCompleto = $request->nombre;
            $cliente->dni = $request->DNI;
            $cliente->telefono = $request->tel;
        }else{
            $cliente->nombreCompleto = "USUARIO";
            $cliente->dni = "00000000A";
        }
        
        Cache::put('cliente' . $num_mesa, $cliente, 1500);
        return response()->json(['cliente' => $cliente]);
    }

    public function OrdenPagar(Request $request, $num_mesa) {
        $orden = Cache::get('orden' . $num_mesa, []);
        $orden->estado = 5; //Para ser cobrada
        Cache::put('orden' . $num_mesa, $orden, 1500);
        return response()->json(["error" => false, 'Mensaje' => "Orden Enviada Para ser pagada"]);
    }

    //Envia la orden a la cocina
    public function enviar_orden(Request $request, $num_mesa) {	//preguntarle a alejandro porque me dice orden enviada con exito y cuando vuelve a cargar la orden cada uno de los detalles sale en cantidad enviada con 0
        $detalles =Cache::get('detalles' . $num_mesa, []);
    	$orden = Cache::get('orden' . $num_mesa,false);
      //  dd($orden!=false);
        if($orden!=false){
			if($orden->fechaE==0){
				$orden->fechaE = date("H:i:s");  
				Cache::put('orden' . $num_mesa, $orden, 1500);
			}
			foreach ($detalles as $detalle) {
				$detalle->cantidadCocina=$detalle->cantidad;
                $detalle->fechaE =date("H:i:s"); 
			}
			 Cache::put('detalles'. $num_mesa, $detalles, 1500);
        
			$cola=Cache::get('cola',-1);
            if($cola==-1){
                //crea la cola
                $cola = array();
                array_push($cola,$num_mesa);
                Cache::put('cola', $cola, 1500);
            }else{
               $s = false;
				foreach ($cola as $item) {
					if($num_mesa == $item){
						$s = true ;
					}
				}
             
              if ($s == false) {
                  array_push($cola,$num_mesa);
                  Cache::put('cola', $cola, 1500);
                
              }

            }

         }
            return response()->json(['error' => false, 'mensaje' =>'Se envio la orden a la cocina con exito', 'error' => false, 'orden' =>$orden]);
		}


    public function enviar_orden_pdf($num_mesa) {

        $detalles = Cache::get('detalles' . $num_mesa, []);

        $view = \View::make('ticketcocina', compact('detalles', 'num_mesa'))->render();
        $x = 0;
        $i = count($detalles);

        if ($i == 1) {
            $x = 240;
        }
        if ($i > 1) {
            $x = 240 + (25 * $i);
        }

        $a = [];
        $u = 0;
        foreach ($detalles as $detalle) {
            $detalle->cantidad_enviada = 0;
            if ($detalle->cantidad <= 0) {
                $a[$u] = $detalle->producto . "_" . $detalle->idPrecio;
                $u++;
            }
        }
        //eliminando productos con cantidad 0
        for ($j = 0; $j < count($a); $j++) {
            //   unset($detalles[$a[$j]]);
        }
        Cache::put('detalles' . $num_mesa, $detalles, 1500);
        $pdf = \App::make('dompdf.wrapper');
        Cache::put('detalles' . $num_mesa, $detalles, 1500);
        $pdf->setPaper(array(0, 0, 240, $x));
        $pdf->loadHTML($view);
        $pdf->output();

        return $pdf->download('pedido.pdf');
    }

    //Cancelar la orden
    public function cancelar_orden(Request $request, $num_mesa) {
        //$detalles = Cache::get('detalles' . $num_mesa,[]);
        //$orden    = Cache::get('detalles' . $num_mesa,[]);
        //$cliente  = Cache::get('cliente' . $num_mesa,[]);
        //$orden->save();
        //Imprimir orden cocina 
    }

    //Funcion que registra temporalmente un  detalle
    public function RegistroDetTemp(Request $request, $num_mesa) {
        if ($request->idProducto == "" || $request->idPrecio == "") {
             return response()->json(['error' => true,'mensaje'=>'Por favor seleccione un producto']);
        }
        $detalles = Cache::get('detalles' . $num_mesa, []);
        $modificadoresMesa = Cache::get('modificadores' . $num_mesa, []);
        $orden = Cache::get('orden' . $num_mesa, []);
        if ($orden == []) {
            $orden = new Orden;
            $orden->total = 0;
            $orden->subTotal = 0;
            $orden->mesa = $num_mesa;
            $orden->tipo = "comer";
            $orden->estado = 3; //Atendiendo
            $orden->empleado = $request->dni;//Auth::user()->dni;
            $orden->fechaE = 0;
            //coloco el cliente por defecto
            $cliente = new Persona;
            $cliente->nombreCompleto = "USUARIO";
            $cliente->dni = "00000000A";
            Cache::put('cliente' . $num_mesa, $cliente, 1500);
        }
        $modificadores = false;
        $EliminarMP = false;
        $index = $request->idProducto . "_" . $request->idPrecio;
        //si ya existe ese detalle
        if (isset($detalles[$index])) {
            //si lo voy a eliminar
            if ($request->A == '/-') {
                if ($detalles[$index]->cocina == 0) {	//preguntarle a alejandro, esta declaroa una sola vez esto no tiene sentido, siempre entra a este si
					//dd('fegewsf',$detalles[$index],$detalles[$index]->cocina);
                    $tipo = 'R'; //
                    $orden->subTotal -= $detalles[$index]->precio * $detalles[$index]->cantidad;
                    $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);

                    //if ($detalles[$index]->cantidad == $detalles[$index]->enviada)
                        unset($detalles[$index]);
                    //else {
                      //  $detalles[$index]->cantidad_enviada -= $detalles[$index]->cantidad;
                        //$detalles[$index]->cantidad = 0;
                    //}
                    if ((verificarDetalles($detalles, $request->idProducto) == false)) { //si no existen detalles para ese producto
                        $EliminarMP = true;                                                   //voy a eliminar los modificadores de ese producto
                    }
                    Cache::put('detalles' . $num_mesa, $detalles, 1500);
                    Cache::put('orden' . $num_mesa, $orden, 1500);
                    return response()->json(['total' => $orden->total,
                                'sub' => $orden->subTotal,
                                'tipo' => $tipo,
                                'detalles' => $detalles,
								'estadoO' => $orden->estado,
								'error'=>false,
                                'EliminarMP' => $EliminarMP]);
                }
            } else {
                $tipo = 'C'; //Cambiar (puedo agregar como disminuir)
                //si voy a aumentar la acntidad de un detalle existente
				 $detalles[$index]->cambio= true;
				 $orden->estado=3;
                if ($request->A == '+') {
                    if ($detalles[$index]->cantidad == 0) {
                        $tipo = 'N';
                    }
					$detalles[$index]->estado ="Pendiente";
                    $detalles[$index]->cantidad += $request->cantidad;
                   //$detalles[$index]->cantidad_enviada += $request->cantidad; //Cantidad a comparar +
                    $orden->subTotal += $detalles[$index]->precio * $request->cantidad;
                    $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
					Cache::put('detalles' . $num_mesa, $detalles, 1500);
                } else {
                    //si le voy a restar la cabtidad a un detalle existente
					$XA=$detalles[$index]->precio * $detalles[$index]->cantidad;
                    $detalles[$index]->cantidad_enviada -= $request->cantidad; //cantidad a comparar -
                    $detalles[$index]->cantidad -= $request->cantidad;
                    if ($detalles[$index]->cantidad <= 0) {
                        $tipo = 'R';
                        //$detalles[$index]->cantidad += $request->cantidad;	preguntarle a alejandro por esto
                        $orden->subTotal -= $XA;
                        $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
                        //if ($detalles[$index]->cantidad == $detalles[$index]->enviada)
                            unset($detalles[$index]);
                        //else {
                            //$detalles[$index]->cantidad = 0;
                        //}
                        if ((verificarDetalles($detalles, $request->idProducto) == false)) { 	  //si no existen detalles para ese producto
                            $EliminarMP = true;                                                   //voy a eliminar los modificadores de ese producto
                            //aca falta borrarde la variable de cache que guarda los modificadores de esa mesa
                        }
                        Cache::put('detalles' . $num_mesa, $detalles, 1500);
                        Cache::put('orden' . $num_mesa, $orden, 1500);
                        return response()->json(['total' => $orden->total,
                                    'sub' => $orden->subTotal,
                                    'tipo' => $tipo,
                                    'detalles' => $detalles,
									'error'=>false,
                                    'EliminarMP' => $EliminarMP]);
                    } else {
                        $orden->subTotal -= $detalles[$index]->precio * $request->cantidad;
                        $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
						Cache::put('detalles' . $num_mesa, $detalles, 1500);
                    }
                }
                //Cache::put('detalles' . $num_mesa, $detalles, 1500);
            }
        } else {
            //es un detalle nuevo
            if ($request->A == '+') {
                $tipo = 'N'; //Nuevo
                $detalle = new Detalle;
                $detalle->getproducto =  Producto::where('idProducto', $request->idProducto)->get()[0];
                $detalle->fechaE = 0;
                $detalle->cantidad = $request->cantidad;
                $detalle->producto = $request->idProducto;
                $detalle->idPrecio = $request->idPrecio;
                $presentacion = Precio::where('id', $request->idPrecio)->take(1)->get();
                $detalle->precio = $presentacion[0]->costo;
                $detalle->presentacion = $presentacion[0]->presentacion;
                $detalle->imagen = $presentacion[0]->imagen;
                $detalle->subconjuntos = false;
				$detalle->estado ="Pendiente";
                $detalle->C = "";
				$detalle->cambio= false;
				$detalle->listo =0;
                $detalle->area = $detalle->getproducto->area;
                $detalles[$index] = $detalle;
                $detalle->cantidadCocina =0;
                $orden->subTotal += $detalles[$index]->precio * $detalles[$index]->cantidad;
                $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
				$orden->estado=3;
                //si el detalle es nuevo busco todos los modificacores que pertenecen a ese producto
                $modificadores = Producto::find($request->idProducto)->modificadores;
                if (count($modificadores) != 0) {
                    //verifico si es un modificador compuesto
                    foreach ($modificadores as $row) {
                        if ($row['compuesto'] == 1) {
                            $row['opciones'] = json_encode(modificador::find($row->id)->opciones()->get(array('id', 'opcion')));
                        }
                    }
                    //verifico si ya existe un arreglo de modificadores para ese producto
                    if (existeProducto($modificadoresMesa, $request->idProducto) == false) {
                        $modificadoresMesa[] = $modificadores;
                    }
                    Cache::put('modificadores' . $num_mesa, $modificadoresMesa, 1500);
                } else {
                    $modificadores = false;
                }
                if (isset($detalles[$index]->comentario)) {
                    $detalle->C = $detalles[$index]->comentario;
                }
                Cache::put('detalles' . $num_mesa, $detalles, 1500);
            }
        }
        Cache::put('orden' . $num_mesa, $orden, 1500);
        if ($orden->subTotal == 0) {
            $orden->total = 0;
        }
        return response()->json(['detalles' => $detalles,
                    'producto' => $detalles[$index]->getproducto,
                    'total' => $orden->total,
                    'sub' => $orden->subTotal,
					'estadoO' => $orden->estado,
                    'tipoOrden' => $orden->tipo,
                    'tipo' => $tipo,
					'estado'=>$detalles[$index]->estado,
					'error'=>false,
                    'EliminarMP' => $EliminarMP,
                    'modificadoresP' => $modificadores]);
    }

    //Funciones Auxiliares de otros controladores  
    //Funcion auxiliar MenuController
    //Devuelve la orden de un cliente
    static public function VerOrden(Request $request) {
        $num_mesa = $request->get('mesa');
        $detalles = Cache::get('detalles' . $num_mesa, []);

        $a = [];
        $u = 0;

        foreach ($detalles as $detalle) {
            $detalle->cantidad_enviada = 0;
            if ($detalle->cantidad <= 0) {
                $a[$u] = $detalle->producto . "_" . $detalle->idPrecio;
                $u++;
            }
        }
        //eliminando productos con cantidad 0
        for ($j = 0; $j < count($a); $j++) {
            unset($detalles[$a[$j]]);
        }
        Cache::put('detalles' . $num_mesa, $detalles, 1500);


        //Verificar que la mesa existe sino redirecccionar
        $vendedor = Cache::get('vendedor', -1);
        $orden = Cache::get('orden' . $num_mesa, []);
        if (isset($orden->estado)) {
            if ($orden->estado != 3 && $orden->estado != 7) {
                $orden->estado = 3; //Atendiendo
                $orden->dni = $request->dni;//Auth::user()->dni;
                Cache::put('orden' . $num_mesa, $orden, 1500);
            }
        }
        $cliente = Cache::get('cliente' . $num_mesa, new Persona);
		if(!isset($cliente->nombreCompleto)&&!isset($cliente->dni)){
			$cliente->nombreCompleto = "USUARIO";
            $cliente->dni = "00000000A";
			 Cache::put('cliente' . $num_mesa, $cliente, 1500);
		}
        $modificadoresMesa = Cache::get('modificadores' . $num_mesa, []);
        if (isset($orden->subTotal)) {
            $sub = $orden->subTotal;
            $total = calcular($orden->total, $orden->subTotal, $num_mesa);
        } else {
            $sub = 0.00;
            $total = 0.00;
        }
        $comentario = "";
        if (isset($orden->comentario)) {
            $comentario = $orden->comentario;
        }
        $tipo = '';
        if (isset($orden->tipo)) {
            $tipo = $orden->tipo;
        }
        $detalles = array_where($detalles, function($key, $value) {
            return isset($value);
        });
        return ['detalles' => $detalles,
            'num_mesa' => $num_mesa,
            'orden' => $orden,
            'cliente' => $cliente,
            'vendedor' => $vendedor,
            'sub' => $sub,
            'comentario' => $comentario,
            'tipoOrden' => $tipo,
            'total' => $total,
            'modificadoresP' => $modificadoresMesa];
    }

    public function olvidar() {
          Cache::forget('cola');
        for ($index = 0; $index < 50; $index++) {
            Cache::forget('cliente' . $index);
            Cache::forget('orden' . $index);
            Cache::forget('detalles' . $index);
            Cache::forget('impuestosO' . $index);
            Cache::forget('impuestos');
            Cache::forget('propinas');
            Cache::forget('propinasO' . $index);
        }
    }

    //funcion que cancela una orden
    public function cancelarOrden(Request $request) {
		$dni= $request->dni;
		$tipo= $request->tipo;
        //debo guardar la orden con un estado de cancelada, sus detalles y luego borrar las variables de cache
        //y habilitar la mesa.
        $num_mesa = $request->mesa;
        $detalles = Cache::get('detalles' . $num_mesa, []);
        $orden = Cache::get('orden' . $num_mesa, []);
        $cliente = Cache::get('cliente' . $num_mesa);
        $valorPropina = Cache::get('valoProp' . $num_mesa);
        $valorImpuesto = Cache::get('valoImp' . $num_mesa);
        if ($orden != [] && $detalles != []) {
            //Begin Transaction
            DB::beginTransaction();
            //Guardando Orden
            $orden->descuento = 0.0;
            $orden->impuesto = $valorImpuesto;
            $orden->propina = $valorPropina;
            $orden->estado = 0;
            $orden->comentario = $request->comentario;
            $orden->cajero = $dni;
            unset($orden->dni);
            $clienteT = Cache::get('cliente' . $num_mesa, new Persona());
            $cliente = Persona::firstOrNew(['dni' => $clienteT->dni]);
            $cliente->nombreCompleto = $clienteT->nombreCompleto;
            $cliente->telefono = $clienteT->telefono;
            $cliente->save();
            $orden->cliente = $clienteT->dni;

            //guardo la orden
            if ($orden->tipo == 'comer') {
                //la orden tiene mesa por lo tanto debo buscarla y asignarsela a la orden
                $mesa = mesa::find($num_mesa);
                $mesa->ordenes()->save($orden);
            } else {
                $orden->save();
            }

            //Guardando Detalles de la orden
            $detallesO = Detalle::create_array($detalles);
            $orden->detalles()->saveMany($detallesO);
            //Guardando los modificadores de los detalles so los tiene;
            if (count($detalles) > 0) {
                foreach ($detalles as $key => $detalle) {
                    //si tengo subconjuntos
                    if ($detalle->subconjuntos != false) {
                        //recorro todos los subcnojuntos
                        foreach ($detalle->subconjuntos as $subconjunto) {
                            //recorro todos los modificadores de un subconjunto
                            foreach ($subconjunto->modificadores as $modificador) {
                                //creo un objeto de la tabla pivote y lo lleno
                                $pivote = new detalletienemodificador();
                                $pivote->idModificador = $modificador->idMod;
                                $pivote->NumItemsDet = $subconjunto->cantidad;
                                $pivote->subconjunto = ($subconjunto->idS + 1);
                                $pivote->cantidadModificador = $modificador->canT;
                                //pregunto si el modificador es de tipo extra
                                if ($modificador->precio != 0) {
                                    $pivote->precioExtra = $modificador->precio;
                                    $pivote->total = ($modificador->canT * $modificador->precio);
                                    if ($modificador->compuesto != 0) {
                                        $pivote->idOpcion = $modificador->idOp;
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'precioExtra' => $pivote->precioExtra, 'total', 'idOpcion' => $pivote->idOpcion]]);
                                    } else {
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'precioExtra' => $pivote->precioExtra, 'total' => $pivote->total]]);
                                    }
                                } else {
                                    $pivote->total = 0;
                                    if ($modificador->compuesto != 0) {
                                        $pivote->idOpcion = $modificador->idOp;
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'total' => $pivote->total, 'idOpcion' => $pivote->idOpcion]]);
                                    } else {
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'total' => $pivote->total]]);
                                    }
                                }
                            }
                        }
                    }
                    /* if($pDescuento1["tipo"]=="porcentaje"){
                      $orden->descuentos()->attach([$pDescuento1["idDescuento"] => ['valorDescuento' => $pDescuento1["valor"],'porcentaje' => $pDescuento1["valor_des"]]]);
                      }else{
                      $orden->descuentos()->attach([$pDescuento1["idDescuento"] => ['valorDescuento' => $pDescuento1["valor"]]]);
                      } */
                }
            }
            DB::commit();
            Cache::forget('cliente' . $num_mesa);
            Cache::forget('orden' . $num_mesa);
            Cache::forget('detalles' . $num_mesa);
            Cache::forget('valoImp' . $num_mesa);
            Cache::forget('valoProp' . $num_mesa);
            Cache::forget('modificadores' . $num_mesa);
            return ['error' => false,
                'mensaje' => 'la orden de la mesa  ' . $num_mesa . ' ha sido cancelada'];
        } else {
            return ['error' => true,
                'mensaje' => 'la mesa  ' . $num_mesa . ' no posee una orden'];
        }
    }

     //funcion que cancela una orden
    public function addModDet(Request $request) {
        $datos = json_decode($request->subconjuntos);
        $num_mesa = json_decode($request->mesa);
        if ($datos->idP == "" || $datos->idP == null || $datos->idPr == "" || $datos->idPr == null) {
            return response()->json(['error' => true, 'mensaje' => 'Por favor seleccione un detalle']);
        }
        $index = $datos->idP . "_" . $datos->idPr;
        $detalles = Cache::get('detalles' . $num_mesa, []);
        if ($detalles == []) {
            return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
        } else {
            //si ya existe ese detalle
            if (isset($detalles[$index])) {
                $i = 0;
                $extras = 0.0;
                $totalExtras = 0.0;
                $i = 0;
                foreach ($datos->subconjuntos as $row) {
                    $i++;
                    $ids = array();
                    foreach ($row->modificadores as $modificador) {
                        array_push($ids, $modificador->idMod);
                    }
                    $M = modificador::whereIn('id', $ids)->get()->keyBy('id');
                    if (count($ids) == count($M)) { //que los ids de los modificadores de ese subconjunto son validos
                        $nodos = array();
                        foreach ($row->modificadores as $row1) {
                            $modificador = $M->only($row1->idMod);
                            $nodo = new nodo();
                            $nodo->canT = $row1->c;
                            $nodo->compuesto = $modificador[0]->compuesto;
                            $nodo->idMod = $modificador[0]->id;
                            $nodo->nombre = $modificador[0]->nombre;
                            if ($modificador[0]->precio == null) {
                                $nodo->precio = 0;
                            } else {
                                $nodo->precio = $modificador[0]->precio;
                            }
                            if ($modificador[0]->compuesto == 1) {
                                $opcion = $modificador[0]->opciones()->where('id', $row1->idOp)->get(array('opcion'));
                                $nodo->nombreM = $opcion[0]->opcion;
                                $nodo->idOp = $row1->idOp;
                            }
                            $extras+=($nodo->canT * $nodo->precio);
                            array_push($nodos, $nodo);
                        }
                        $row->modificadores = $nodos;
                    } else {
                        return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
                    }
                    $extras = ($extras * $row->cantidad);
                    $row->extras = $extras;
                    $totalExtras+=$extras;
                    $extras = 0;
                }
                //si llega hasta aca es porque todo estaba bien.
                $detalles[$index]->subconjuntos = $datos->subconjuntos;
                $orden = Cache::get('orden' . $num_mesa, []);
                if ($orden->extras != 0.0) {
                    //quiere decir que ya este detalle tiene modificadors que posiblimente tiene extras
                    $orden->subTotal -= $orden->extras;
                }
                $orden->extras = $totalExtras;
                $orden->subTotal += $totalExtras;
                $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
                Cache::put('detalles' . $num_mesa, $detalles, 1500);
                Cache::put('orden' . $num_mesa, $orden, 1500);
                return response()->json([
                            'error' => false,
                            'total' => $orden->total,
                            'sub' => $orden->subTotal,
                            'subconjuntos' => $detalles[$index]->subconjuntos
                ]);
            } else {
                return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
            }
        }
    }
	

    //funcion que cancela una orden
    public function borrarModDet(Request $request) {
        $num_mesa = json_decode($request->mesa);
        if ($request->idP == "" || $request->idP == null || $request->idPr == "" || $request->idPr == null) {
            return response()->json(['error' => true, 'mensaje' => 'Por favor seleccione un detalle']);
        }
        $index = $request->idP . "_" . $request->idPr;
        $detalles = Cache::get('detalles' . $num_mesa, []);
        $orden = Cache::get('orden' . $num_mesa, []);
        if ($detalles == [] || $orden == []) {
            return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error4']);
        } else {
            //si ya existe ese detalle
            if (isset($detalles[$index])) {
                $subconjuntos = $detalles[$index]->subconjuntos;
                if ($subconjuntos != false) {
                    $pos = BuscarSub($subconjuntos, $request->posSub);
                    if ($pos != -1) {
                        $eliminado = array_splice($subconjuntos, $pos, 1);
                        if (count($subconjuntos) == 0) {
                            $subconjuntos = false;
                        }
                        $detalles[$index]->subconjuntos = $subconjuntos;
                        Cache::put('detalles' . $num_mesa, $detalles, 1500);
                        if ($eliminado[0]->extras != 0.0) {
                            //si el subconjunto que elimine tenia extras debo actualizar el total de extras de la factura
                            //y el subtotal y el totale de la misma
                            $orden->extras-= $eliminado[0]->extras;
                            $orden->subTotal -= $eliminado[0]->extras;
                            $orden->total = calcular($orden->total, $orden->subTotal, $num_mesa);
                        }
                        Cache::put('orden' . $num_mesa, $orden, 1500);
                        return response()->json([
                                    'error' => false,
                                    'total' => $orden->total,
                                    'sub' => $orden->subTotal,
                                    'subconjunto' => $detalles[$index]->subconjuntos
                        ]);
                    } else {
                        return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
                    }
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
            }
        }
    }

    public function agregarComentario(Request $request) {
        //si tipo =1 voy a agregar un comentario a la orden y tipo = 2 voy a gregar un comentario a un detalle
        $datos = json_decode($request->datos);
        $orden = Cache::get('orden' . $datos->mesa, []);
        if ($orden != []) {
            if ($datos->tipo == 1) {
                $orden->comentario = $datos->comentario;
                Cache::put('orden' . $datos->mesa, $orden, 1500);
                return response()->json(['error' => false]);
            } else {
                $index = $datos->idP . "_" . $datos->idPr;
                $detalles = Cache::get('detalles' . $datos->mesa, []);
                if ($detalles != []) {
                    //si ya existe ese detalle
                    if (isset($detalles[$index])) {
                        $detalles[$index]->comentario = $datos->comentario;
                        $detalles[$index]->C = $datos->comentario;
                        Cache::put('detalles' . $datos->mesa, $detalles, 1500);
                        return response()->json(['error' => false]);
                    } else {
                        return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
                    }
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
                }
            }
        } else {
            return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
        }
    }

    public function cambiarTipoOrden(Request $request) {
        $orden = Cache::get('orden' . $request->mesa, []);
        if ($orden != []) {
            $orden->tipo = $request->valor;
            Cache::put('orden' . $request->mesa, $orden, 1500);
            return response()->json(['error' => false]);
        } else {
            return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error']);
        }
    }

    /////Gestion Orden////////
    static public function dividir_orden(Request $request, $num_mesa, $num_mesa2) {

        return view('dividir_mesas', ['mesa' => $num_mesa, 'mesa2' => $num_mesa2]);
    }

    public function orden(Request $request, $num_mesa) {
        $detalles = Cache::get('detalles' . $num_mesa, []);
        $orden = Cache::get('orden' . $num_mesa, []);
        //dd($detalles);
        return response()->json(['orden' => $orden, 'detalles' => $detalles, 'mesa' => $num_mesa]);
    }

    public function dividir_mesas(Request $request, $num_mesa, $num_mesa2) {
        $detalles = Cache::get('detalles' . $num_mesa, []);
        $orden = Cache::get('orden' . $num_mesa, []);
        $orden_nueva = clone $orden;
        $detalles_nuevos = [];
        $detalle_d = json_decode($request->detalles);
        $orden_nueva->subTotal = 0;
        $subtotalN = 0;
        foreach ($detalle_d as $key => $item) {
            if (isset($item->cantidad_mov)) {
                $detalle_nuevo = clone $detalles[$key];
                $detalles[$key]->cantidad -= $item->cantidad_mov;
                $detalle_nuevo->cantidad = $item->cantidad_mov;
                $detalles_nuevos[$key] = $detalle_nuevo;
                $orden_nueva->subTotal+= $detalle_nuevo->cantidad * $detalle_nuevo->precio;
                $orden->subTotal-= $detalle_nuevo->cantidad * $detalle_nuevo->precio;
                if ($detalles[$key]->cantidad <= 0) {
                    unset($detalles[$key]);
                } else {
                    if ($item->cantsub_mov > 0) {
                        foreach ($item->subconjuntos as $key2 => $value) {
                            if (isset($value->cantidad_mov)) {
                                $aux = clone($detalles[$key]->subconjuntos[$key2]);
                                $aux->cantidad -= $value->cantidad_mov;
                                $detalles[$key]->changeSubcojuntoAttribute($key2, $aux);
                                $aux->cantidad = $value->cantidad_mov;
                                $detalles_nuevos[$key]->changeSubcojuntoAttribute($key2, $aux);
                            } else {
                                $aux = clone($detalles_nuevos[$key]->subconjuntos[$key2]);
                                $aux->cantidad = 0;
                                $detalles_nuevos[$key]->changeSubcojuntoAttribute($key2, $aux);
                            }
                        }
                        $detalles[$key]->sub_to_array();
                        $detalles_nuevos[$key]->sub_to_array();
                    }
                }
            }
        }

        // dd($detalles, $detalles_nuevos, Cache::get('detalles' . $num_mesa, []));
        $orden_nueva->total = calcular(0, $orden_nueva->subTotal, $num_mesa);
        $orden->total = calcular(0, $orden->subTotal, $num_mesa);
        Cache::put('detalles' . $num_mesa, $detalles, 1500);
        Cache::put('orden' . $num_mesa, $orden, 1500);
        Cache::put('detalles' . $num_mesa2, $detalles_nuevos, 1500);
        Cache::put('orden' . $num_mesa2, $orden_nueva, 1500);
        $cliente = Cache::get('cliente' . $num_mesa);
        Cache::put('cliente' . $num_mesa2, $cliente, 1500);
        return response()->json(['error' => false, 'mensaje' => ""]);
    }

    //Funcion que une las facturas de dos mesas
    public function unir_mesas(Request $request, $num_mesa1, $num_mesa2) {

        $detalles1 = Cache::get('detalles' . $num_mesa1, []);
        $orden1 = Cache::get('orden' . $num_mesa1, []);

        $detalles2 = Cache::get('detalles' . $num_mesa2, []);
        $orden2 = Cache::get('orden' . $num_mesa2, []);
        //dd($detalles1, $detalles2);
        foreach ($detalles1 as $key => $detalle1) {

            if (isset($detalles2[$key])) {

                $detalle1->cantidad += $detalles2[$key]->cantidad;
                $detalle1->cantidad_enviada += $detalles2[$key]->cantidad_enviada;
                if ($detalle1->subconjuntos != false) {
                    foreach ($detalle1->subconjuntos as $key2 => $subconjuto) {
                        if (!isset($subconjuto->unico)) {
                            $subconjuto->unico = clave_unica($subconjuto->modificadores);
                        }
                        if ($detalles2[$key]->subconjuntos != false) {
                            foreach ($detalles2[$key]->subconjuntos as $key3 => $subconjuto2) {
                                if (!isset($subconjuto2->unico)) {
                                    $subconjuto2->unico = clave_unica($subconjuto2->modificadores);
                                }
                                if ($subconjuto->unico == $subconjuto2->unico) {
                                    $subconjuto->cantidad+=$subconjuto2->cantidad;
                                    $subconjuto->extras+=$subconjuto2->extras;
                                    unset($subconjuto2);
                                }
                            }
                        }
                    }
                }
                if ($detalles2[$key]->subconjuntos != false) {
                    if ($detalle1->subconjuntos != false) {
                        $detalle1->subconjuntos = [];
                    }
                    foreach ($detalles2[$key]->subconjuntos as $key3 => $subconjuto2) {
                        if (!isset($subconjuto2->unico)) {
                            $subconjuto2->unico = clave_unica($subconjuto2->modificadores);
                        }
                        $detalle1->sub_add($subconjuto2);
                    }
                }
                $detalle1->sub_to_array();
                unset($detalles2[$key]);
            }
        }
        foreach ($detalles2 as $key => $detalle2) {
            $detalles1[$key] = $detalle2;
            unset($detalle2);
        }

        $orden1->total += $orden2->total;
        $orden1->subTotal += $orden2->subTotal;
        Cache::forget('orden' . $num_mesa1);
        Cache::forget('detalles' . $num_mesa1);
        $orden1->total = calcular(0, $orden1->subTotal, $num_mesa2);
        Cache::put('detalles' . $num_mesa2, $detalles1, 1500);
        Cache::put('orden' . $num_mesa2, $orden1, 1500);
        $cliente = Cache::get('cliente' . $num_mesa1);
        Cache::forget('cliente' . $num_mesa1);
        Cache::put('cliente' . $num_mesa2, $cliente, 1500);
        return response()->json(['error' => false, 'mensaje' => ""]);
    }

    //Funcion que une las facturas de dos mesas
    public function mover_mesa(Request $request, $num_mesa1, $num_mesa2) {
        $detalles = Cache::get('detalles' . $num_mesa1, []);
        $orden = Cache::get('orden' . $num_mesa1, []);
        $cliente = Cache::get('cliente' . $num_mesa1);
        Cache::forget('orden' . $num_mesa1);
        Cache::forget('detalles' . $num_mesa1);
        Cache::put('detalles' . $num_mesa2, $detalles, 1500);
        Cache::put('orden' . $num_mesa2, $orden, 1500);
        Cache::put('cliente' . $num_mesa2, $cliente, 1500);
        return response()->json(['error' => false, 'mensaje' => ""]);
    }

}

function clave_unica($mods) {
    $mods = new Database\Eloquent\Collection($mods);
    $mods = $mods->sortBy('idMod');
    $key = "";
    foreach ($mods as $mod) {
        $key.=$mod->idMod . "|";
    }
    return $key;
}
