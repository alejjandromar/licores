<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\propina;
use App\Http\Controllers\Controller;

class propinaController extends Controller
{
   //al cargar la pagina.
    public function index()
    {
         return view('layouts.Propina');
    }

    //selecciona todos los registros
    public function selectAll()
    {
       try {
        $propinas = propina::all();
       } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
       }
       return response()->json(['propina' => $propinas ]);
    }
    
    //Update_status: Actualiza el estado de una propina
    public function Update_status(Request $request) {
        try {
            //busca Registro 
            $propina = propina::find($request->id);
            if($propina!=null){
                $propina->estado = $request->estado;
                $propina->save();
                if($request->estado==0){
                    $mensaje="La propina ha sido desactivado exitosamente.";
                }else{
                     $mensaje="La propina ha sido activado exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'no existe una propina con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje]);
    }
   
    //Update: Actualiza los datos de una propina
    static public function Update(Request $request) {
        try {
            if($request->insertar==1){
                $D = propina::onlyTrashed()->where('nombre', $request->nombre)->get();
                $D[0]->restore();
            }
            //busca Registro 
            $propina = propina::find($request->id);
            if( $propina!=null){
                 $propina->nombre = $request->nombre;
                 $propina->tipo = $request->tipo;
                 $propina->valor = $request->valor;
                 $propina->estado = 1;
                 $propina->save();
                if($request->insertar==1){
                    $mensaje="La propina fue registrado exitosamente.";
                }else{
                    $mensaje="La propina fue actualizado exitosamente.";
                }
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe una propina con ese identificador']);
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado != false){
                    if($request->insertar==1){
                        $descuento->delete();
                     }
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una propina con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una propina con ese nombre en la base de datos.']);
                }
            }else{
                if($request->insertar==1){
                        $descuento->delete();
                }
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }
        return response()->json(['error'=>false,'mensaje'=>$mensaje]);
    }
    //Delete: Borra un descuento
    public function Delete(Request $request) {
        try {
            //busca Registro 
            $propina = propina::find($request->id);
            if($propina!=null){
                $propina->delete();
                return response()->json(['error'=>false,'mensaje' => "La propina ha sido eliminado exitosamente."]);
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe una propina con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
           return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
        }
    }
    
    //Create: Crea y Guarda un objeto de tipo descuento
    public function Create(Request $request) {
        $D = propina::onlyTrashed()->where('nombre', $request->nombre)->get();
        if(isset($D[0])){
            $request->id=$D[0]->idPropina;
            $request->insertar=1;
            return propinaController::Update($request);
        }
        try {
            //Guadar Registro 
            $propina =  new propina();
            $propina->nombre = $request->nombre;
            $propina->tipo = $request->tipo;
            $propina->valor = $request->valor;
            $propina->save();
            return response()->json(['error' => false,'mensaje'=>'La propina ha sido registrado exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una propina con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe una propina con ese nombre en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }
    }
}
