<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\impuesto;
use Illuminate\Http\Request;
use DB;
class ImpuestoController extends Controller {

    static public function CRUD(Request $request) {
        return view('layouts.Impuesto');
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla impuesto
    public function Select_all(Request $request) {
        try {
        $impuestos = impuesto::all();
       } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
       }
       return response()->json(['impuestos' => $impuestos]);
    }
    
    //Create: Crea y Guarda un objeto de tipo impuesto
    public function Create(Request $request) {
        $D = impuesto::onlyTrashed()->where('nombre', $request->nombre)->get();
        if(isset($D[0])){
            $request->id=$D[0]->idImpuesto;
            $request->insertar=1;
            return ImpuestoController::Update($request);
        }
        try {
            //Guadar Registro 
            $impuesto = new impuesto();
            $impuesto->nombre = $request->nombre;
            $impuesto->valor = $request->valor;
            $impuesto->tipo  = $request->tipo;
            $impuesto->save();
            return response()->json(['error' => false,'mensaje'=>'El impuesto ha sido registrado exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un impuesto con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un impuesto con ese nombre en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }
    }

    //Update: Actualiza los datos de un impuesto
    static public function Update(Request $request) {
        try {
            if($request->insertar==1){
                $D = ImpuestoController::onlyTrashed()->where('nombre', $request->nombre)->get();
                $D[0]->restore();
            }
            //busca Registro 
            $impuesto = impuesto::find($request->id);
            if($impuesto!=null){
                 $impuesto = impuesto::find($request->id);
                $impuesto->nombre = $request->nombre;
                $impuesto->valor = $request->valor;
                $impuesto->tipo  = $request->tipo;
                $impuesto->save();
                if($request->insertar==1){
                    $mensaje="El impuesto fue registrado exitosamente.";
                }else{
                    $mensaje="El impuesto fue actualizado exitosamente.";
                }
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe un impuesto con ese identificador']);
            }
        }catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado != false){
                     if($request->insertar==1){
                        $impuesto->delete();
                     }
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un impuesto con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un impuesto con ese nombre en la base de datos.']);
                }
            }else{
                 if($request->insertar==1){
                     $descuento->delete();
                  }
                 return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
             }
        }
        return response()->json(['error'=>false,'mensaje'=>$mensaje]);
    }
    
    //Update_status: Actualiza el estado de un impuesto
    public static function Update_status(Request $request) {
        try {
            //buscar Registro 
            $impuesto = impuesto::find($request->id);
            if($impuesto!=null){
                $impuesto->estado = $request->estado;
                $impuesto->save();
                if($request->estado==0){
                        $mensaje="El impuesto ha sido desactivado exitosamente.";
                    }else{
                         $mensaje="El impuesto ha sido activado exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'no existe un impuesto con ese identificador']);
            }
        } catch (Exception $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje]);
    }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
            $impuesto = impuesto::find($request->id);
            if($impuesto!=null){
                $impuesto->delete();
                return response()->json(['error'=>false,'mensaje' => "El Impuesto  ha sido Eliminado exitosamente"]);
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe un impuesto con ese identificador']);
            }
            
        } catch (Exception $ex) {
            return response()->json(['error'=>true,'mensaje' => "El impuesto  no ha podido ser Eliminado"]);
        }
    }

}
