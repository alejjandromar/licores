<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\impresora;
use Illuminate\Http\Request;
use App\Modelos\area;

class ImpresoraController extends Controller {

    static public function CRUD(Request $request) {
        $areas = area::all();
        return view('layouts.Impresora');
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla impresora
    public function Select_all(Request $request) {
        $impresoras = impresora::all();
       // dd($impresoras);
        if ($request->has('app')) {
            return view('auxiliar.impresoras', ['impresoras' => $impresoras]);
        }
        return response()->json(['impresoras' => $impresoras]);
    }

    //Select_one: Selecciona un solo registro
    public function Existe(Request $request) {
        $impresora = impresora::find($request->id);
        return response()->json(['impresora' => $impresora]);
    }

    //Create: Crea y Guarda un objeto de tipo impresora
    public function Create(Request $request) {
        try {//Guadar Registro 
            $impresora = impresora::firstOrNew(["nombre" => $request->name]);
            $impresora->nombre = $request->name;
            $impresora->real_name = $request->real_name;
            $impresora->save();
            if ($request->has('app')) {
                echo true;
                return;
            }
            return response()->json(['error' => false, 'impresora' => $impresora]);
        } catch (Exception $ex) {
            if ($request->has('app')) {
                echo false;
                return;
            }
            echo response()->json(['Error' => 'El impresora no ha podido ser insertado']);
        }
    }

    //Update: Actualiza los datos de un impresora
    static public function Update(Request $request) {
        try {//Guadar Registro 
            $impresora = impresora::find($request->id);

            $impresora->nombre = $request->name;

            $impresora->ip = $request->ip != "" ? $request->ip : null;
            $impresora->alto = $request->alto;
            $impresora->ancho = $request->ancho;

            $impresora->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1062) {
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado != false) {
                    $resp = response()->json(['error' => true, 'mensaje' => 'Ya existe un metodo de pago con ese identificador en la base de datos.']);
                    $resp->st = 0;
                    return $resp;
                } else {
                    $resp = response()->json(['error' => true, 'mensaje' => 'Ya existe un metodo de pago con ese nombre en la base de datos.']);
                    $resp->st = 0;
                    return $resp;
                }
            } else {
                $resp = response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
                $resp->st = 0;
                return $resp;
            }
        } catch (Exception $ex) {
            $resp = response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            $resp->st = 0;
            return $resp;
        }

        $resp = response()->json(['error' => false, 'impresora' => $impresora]);
        $resp->st = 1;
        return $resp;
    }

    //Update_status: Actualiza el estado de un impresora
    public static function Update_status(Request $request) {
        try {//Guadar Registro 
            $impresora = impresora::find($request->id);

            $impresora->estado = $request->estado;
            $impresora->save();
        } catch (Exception $ex) {
            return response()->json(['Error' => 'El impresora no ha podido ser actualizado']);
        }
        return response()->json(['impresora' => $impresora]);
    }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
            $impresora = impresora::find($request->id);
            $impresora->delete();
            return response()->json(['error' => false, 'mensaje' => "Impresora Eliminada"]);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1451) {
                $resultado = strpos($ex->errorInfo[2], "caja_impresora_fk");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado != false) {
                    $resp = response()->json(['error' => true, 'mensaje' => 'No se puede eliminar la impresora porque esta asociada a una caja del restaurante']);
                    $resp->st = 0;
                    return $resp;
                } else {
                    $resp = response()->json(['error' => true, 'mensaje' => 'No se puede eliminar la impresora porque esta asociada a un area del restaurante']);
                    $resp->st = 0;
                    return $resp;
                }
            };
            
        }
    }

}
