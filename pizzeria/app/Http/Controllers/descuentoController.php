<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modelos\Descuento;
use App\Http\Controllers\Controller;

class descuentoController extends Controller
{
   //al cargar la pagina.
    public function index()
    {
         return view('layouts.Descuentos');
    }

    //selecciona todos los registros
    public function selectAll()
    {
       $descuentos = Descuento::all();
       return response()->json(['descuento' => $descuentos ]);
    }
    
    //Update_status: Actualiza el estado de un descuento
    public function Update_status(Request $request) {
        try {
            //busca Registro 
            $descuento = Descuento::find($request->id);
            if($descuento!=null){
                $descuento->estado = $request->estado;
                $descuento->save();
                if($request->estado==0){
                    $mensaje="El descuento ha sido desactivado exitosamente.";
                }else{
                     $mensaje="El descuento ha sido activado exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'no existe un descuento con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje]);
    }
   
    //Update: Actualiza los datos de un descuento
    static public function Update(Request $request) {
        try {
            if($request->insertar==1){
                $D = Descuento::onlyTrashed()->where('nombre', $request->nombre)->get();
                $D[0]->restore();
            }
            //busca Registro 
            $descuento = Descuento::find($request->id);
            if($descuento!=null){
                $descuento->nombre = $request->nombre;
                $descuento->tipo = $request->tipo;
                $descuento->valor = $request->valor;
                $descuento->estado = 1;
                $descuento->save();
                if($request->insertar==1){
                    $mensaje="El descuento fue registrado exitosamente.";
                }else{
                    $mensaje="El descuento fue actualizado exitosamente.";
                }
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe un descuento con ese identificador']);
            }
            
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado != false){
                    if($request->insertar==1){
                        $descuento->delete();
                     }
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Descuento con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Descuento con ese nombre en la base de datos.']);
                }
            }else{
                if($request->insertar==1){
                        $descuento->delete();
                     }
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }
        return response()->json(['error'=>false,'mensaje'=>$mensaje]);
    }
    //Delete: Borra un descuento
    public function Delete(Request $request) {
        try {
            //busca Registro 
            $descuento = Descuento::find($request->id);
            if($descuento!=null){
                $descuento->delete();
                return response()->json(['error'=>false,'mensaje' => "El descuento ha sido eliminado exitosamente."]);
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe un descuento con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
           return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
        }
    }
    
    //Create: Crea y Guarda un objeto de tipo descuento
    public function Create(Request $request) {
        $D = Descuento::onlyTrashed()->where('nombre', $request->nombre)->get();
        if(isset($D[0])){
            $request->id=$D[0]->idDescuento;
            $request->insertar=1;
            return descuentoController::Update($request);
        }
        try {
            //Guadar Registro 
            $descuento =  new Descuento();
            $descuento->nombre = $request->nombre;
            $descuento->tipo = $request->tipo;
            $descuento->valor = $request->valor;
            $descuento->save();
            return response()->json(['error' => false,'mensaje'=>'El descuento ha sido registrado exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Descuento con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Descuento con ese nombre en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        }
    }
}
