<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Modelos\mesa;
use App\Modelos\area;
use Auth;
use View;
use Cache;
use Session;

//este es el que usa
class GeneralController extends Controller {

    public function index() {
        return view('layouts.datos_generales');
    }

    static public function all() {

        /* $file = fopen("config_datos_generales.txt", "r");
          //Output a line of the file until the end is reached

          $nombret=fgets($file);
          $nombre=fgets($file);
          $direccion=fgets($file);
          $nif=fgets($file);
          $telefono=fgets($file);

          fclose($file); */
        $resturante = \App\Modelos\Restaurante::all()[0];
        return response()->json(['datos' => $resturante]);
    }

    static public function save(Request $request) {
        try {
			
            $datos = json_decode($request->general);
            $resturante = \App\Modelos\Restaurante::all()[0];
            $resturante->nombre = $datos->nombre;
            $resturante->direccion = $datos->direccion;
            $resturante->rif = $datos->rif;
            $resturante->telefono = $datos->telefono;
            $resturante->simbolo = $datos->simbolo;
            $resturante->moneda = $datos->moneda;
            $resturante->ciudad = $datos->ciudad;
            $resturante->estado = $datos->estado;
            $resturante->save();
            return response()->json(['error' => false]);
        } catch (Exception $ex) {
            return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error al tratar de guardar la informacion']);
        }
    }

    static public function get_coor() {
        $linea = "";
        $file = fopen("config_coordenadas.txt", "r");
//Output a line of the file until the end is reached
        while (!feof($file)) {
            $linea = fgets($file);
        }
        fclose($file);
        return response()->json(['linea' => $linea]);
    }

}
