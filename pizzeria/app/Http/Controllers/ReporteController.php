<?php

namespace App\Http\Controllers;

use View;
use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use Illuminate\Http\Request;
use App\Modelos\caja;
use App\Modelos\Persona;
use App\Modelos\FacturaTieneDescuento;
use App\Modelos\PagoFactura;
use App\Modelos\cola_impresion;
use App\Modelos\cierrecaja;
use App\User;
use App\Modelos\impresora;
use Auth;
use DB;
use Cache;

global $query;
/*
 *  Contralador para la orden del producto!!
 */

class VentasReportController extends Controller {

    public $query;

    public function index() {
        $columnas = ['Dia', 'Importe 1', 'Importe 2', 'Diferencia'];
        $tipos = ['cad', 'moneda', 'moneda', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por DÃ­a",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "ventas"]);
    }

    public function ventas_generales() {
        $columnas = ['fecha', 'No. de ventas', 'subtotal', 'propina', 'impuesto', 'descuento', 'extras', 'total'];
        $tipos = [ 'cad', 'entero', 'moneda', 'moneda', 'moneda', 'moneda', 'moneda', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas Generales",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "ventas_g"]);
    }

    public function ventas_clientes() {
        $columnas = ['DNI', 'Nombre', 'No. compras', 'total'];
        $tipos = [ 'cad', 'cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas A Clientes",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "cliente_g"]);
    }

    public function ventas_mesero() {
        $columnas = ['DNI', 'Nombre', 'No. compras', 'total'];
        $tipos = ['cad', 'cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por Mesero",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "mesero_g"]);
    }

    public function ventas_cajero() {
        $columnas = ['DNI', 'Nombre', 'No. compras', 'total'];
        $tipos = ['cad', 'cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por Cajero",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "cajero_g"]);
    }

    public function ventas_metodo_pago() {
        $columnas = ['Nombre', 'No. compras', 'total'];
        $tipos = ['cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por M&eacute;todo de Pago",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "pagos_g"]);
    }

    public function ventas_producto() {
        $columnas = ['Nombre', 'No. compras', 'total'];
        $tipos = ['cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por Producto",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "producto_g"]);
    }

    public function ventas_productoxpresentacion() {
        $columnas = ['Nombre', 'Presentacion', 'No. compras', 'total'];
        $tipos = ['cad', 'cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por Producto por Presentacion",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "prensentacion_g"]);
    }

    public function ventas_categoria() {
        $columnas = ['Nombre', 'No. compras', 'total'];
        $tipos = ['cad', 'entero', 'moneda'];
        return view('layouts.reporte', ['titulo' => "Ventas por Categoria",
            "columnas" => $columnas,
            "tipos" => $tipos,
            'ruta' => "categoria_g"]);
    }

    //Funciones Ajax
    //Funcion que realiza la compracion entre los dias de dos meses diferentes
    public function Comp_Ventasxdia(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->mes, $request->fechai, $request->fechaf);
        $query2 = VentasReportController::querymaker($request->tipo, $request->year2, $request->mes2, $request->fechai2, $request->fechaf2);
        $ordenes1 = Orden::whereRaw($query)->select(DB::raw('SUM(total) as totalxdia'), DB::raw('DAY(created_at) as dia'))->groupBy("dia")->get();
        $ordenes2 = Orden::whereRaw($query2)->select(DB::raw('SUM(total) as totalxdia2'), DB::raw('DAY(created_at) as dia'))->groupBy("dia")->get();
        $tabla = mezclar($ordenes2, $ordenes1, "dia");
        return response()->json(['datos' => $tabla, "columnas" => ['Dia', 'totalxdia', 'totalxdia2', 'totalxdia-totalxdia2']]);
    }

    public function Ventas_generales_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }

        $tabla = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(subtotal) as subtotales,'
                                . ' SUM(propina) as propinas,'
                                . ' SUM(impuesto) as impuestos,'
                                . ' SUM(descuento) as decuentos,'
                                . ' SUM(extras) as extras,'
                                . ' SUM(total) as totales'), DB::raw("DATE_FORMAT(created_at,'" . $f . "' ) as fecha"))->groupBy("fecha")->get();

        return response()->json(['datos' => $tabla,
                    "columnas" => ['fecha', 'n_facturas', 'subtotales', 'propinas', 'impuestos', 'decuentos', 'extras', 'totales'],
                    "columnas_lb" => ['fecha', 'No. de ventas', 'subtotal', 'propina', 'impuesto', 'descuento', 'extras', 'total'],
                    "tipos" => ['fecha', 'entero', 'moneda', 'moneda', 'moneda', 'moneda', 'moneda', 'moneda']]);
    }
    
    public function Ventas_canceladas_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf, false);
        $query.= " and estado = 0";
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }

        $tabla = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(subtotal) as subtotales,'
                              
                                . ' SUM(extras) as extras,'
                                . ' SUM(total) as totales'), DB::raw("DATE_FORMAT(created_at,'" . $f . "' ) as fecha"))->groupBy("fecha")->get();

        return response()->json(['datos' => $tabla,
                    "columnas" => ['fecha', 'n_facturas', 'subtotales', 'extras', 'totales'],
                    "columnas_lb" => ['fecha', 'No. de ventas', 'subtotal',  'extras', 'total'],
                    "tipos" => ['fecha', 'entero', 'moneda', 'moneda', 'moneda']]);
    }

    public function Ventas_clientes_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $tabla = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(total) as totales, cliente'))->groupBy("cliente")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = Persona::where('dni', $value->cliente)->get()[0]->nombreCompleto;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        return response()->json(['datos' => $tabla,
                    "columnas" => ['cliente', 'nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Cedula', 'Nombre', 'Compras', 'Total'],
                    "tipos" => ['cad', 'cad', 'entero', 'moneda']]);
    }

    public function Ventas_mesero_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $tabla = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(total) as totales, empleado'))->groupBy("empleado")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\User::where('dni', $value->empleado)->get()[0]->name;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        return response()->json(['datos' => $tabla,
                    "columnas" => ['empleado', 'nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Cedula', 'Nombre', 'Compras', 'Total'],
                    "tipos" => ['cad', 'cad', 'entero', 'moneda']]);
    }

    public function Ventas_cajero_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $tabla = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(total) as totales, cajero'))->groupBy("cajero")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\User::where('dni', $value->cajero)->get()[0]->name;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        return response()->json(['datos' => $tabla, "columnas" => ['cajero', 'nombre', 'n_facturas', 'totales'], "columnas_lb" => ['Cedula', 'Nombre', 'Compras', 'Total'],
                    "tipos" => ['cad', 'cad', 'entero', 'moneda']]);
    }

    public function Ventas_pago_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        /*  $aux_tabla = Orden::whereRaw($query)->select(DB::raw(' SUM(cambio) as total_cambio'))->get()[0];
          $resta= $aux_tabla->total_cambio;
         */
        $tabla = PagoFactura::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(monto) as totales, tipoPago'))->groupBy("tipoPago")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\ModoPago::where('idPago', $value->tipoPago)->get()[0]->tipoPago;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        
        
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Metodo de Pago', 'Usos', 'Total'],
                    "tipos" => ['cad', 'entero', 'moneda']]);
    }

    
    public function Ventas_impuesto_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
       
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        /*  $aux_tabla = Orden::whereRaw($query)->select(DB::raw(' SUM(cambio) as total_cambio'))->get()[0];
          $resta= $aux_tabla->total_cambio;
         */
        $tabla = \App\Modelos\FacturaTieneImpuestos::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(valorImpuesto) as totales, idImpuesto'))->groupBy("idImpuesto")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\impuesto::where('idImpuesto', $value->idImpuesto)->get()[0]->nombre;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        
        
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Impuesto', 'Usos', 'Total'],
                    "tipos" => ['cad', 'entero', 'moneda']]);
    }
    
    public function Ventas_descuento_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        /*  $aux_tabla = Orden::whereRaw($query)->select(DB::raw(' SUM(cambio) as total_cambio'))->get()[0];
          $resta= $aux_tabla->total_cambio;
         */
        $tabla = \App\Modelos\FacturaTieneDescuento::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(valorDescuento) as totales, idDescuento'))->groupBy("idDescuento")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\Descuento::where('idDescuento', $value->idDescuento)->get()[0]->nombre;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        
        
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Descuento', 'Usos', 'Total'],
                    "tipos" => ['cad', 'entero', 'moneda']]);
    }
    
     public function Ventas_propina_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        /*  $aux_tabla = Orden::whereRaw($query)->select(DB::raw(' SUM(cambio) as total_cambio'))->get()[0];
          $resta= $aux_tabla->total_cambio;
         */
        $tabla = \App\Modelos\FacturaTienePropina::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . ' SUM(valorPropina) as totales, idPropina'))->groupBy("idPropina")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\propina::where('idPropina', $value->idPropina)->get()[0]->nombre;
            } catch (\ErrorException $e) {
                $value->nombre = "Propina";
            }
        }
        
        
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Descuento', 'Usos', 'Total'],
                    "tipos" => ['cad', 'entero', 'moneda']]);
    }

    
    public function Ventas_producto_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        $tabla = \App\Modelos\Detalle::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . 'SUM(cantidad) as cantidad_total,'
                                . ' SUM((cantidad*precio)) as totales, producto'))->groupBy("producto")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\Producto::where('idProducto', $value->producto)->get()[0]->nombre;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Nombre', 'Compras', 'Total'],
                    "tipos" => ['cad', 'entero', 'moneda']]);
    }

    public function Ventas_categoria_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        $tabla = \App\Modelos\Detalle::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . 'SUM(cantidad) as cantidad_total,'
                                . ' SUM((cantidad*precio)) as totales, producto'))->groupBy("producto")->get();
        foreach ($tabla as $value) {
            try {
                $value->idCategoria = \App\Modelos\Producto::where('idProducto', $value->producto)->get()[0]->categoria;
            } catch (\ErrorException $e) {
                $value->nombre = "";
            }
        }
        $tabla = $tabla->groupBy('idCategoria');
        $tabla2 = [];
        foreach ($tabla as $value) {
            $value_aux = [];
            foreach ($value as $key => $value2) {
                if ($value_aux == []) {
                    $value_aux = $value2;
                } else {

                    $value_aux->n_facturas+=$value2->n_facturas;
                    $value_aux->totales+=$value2->totales;
                    unset($value[$key]);
                }
            }
            $tabla2[] = $value_aux;
        }
        unset($tabla);
        $tabla = $tabla2;
        foreach ($tabla as $value) {
            $value->nombre = \App\Modelos\Categoria::where('idCategoria', $value->idCategoria)->get()[0]->nombre;
        }
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'n_facturas', 'totales'],
                    "columnas_lb" => [ 'Nombre', 'Compras', 'Total'],
                    "tipos" => [ 'cad', 'entero', 'moneda']
        ]);
    }

    public function Ventas_presentacion_report(Request $request) {
        $query = VentasReportController::querymaker($request->tipo, $request->year, $request->yearf, $request->mes, $request->mesf, $request->fechai, $request->fechaf);
        if ($request->tipo == "") {
            $f = '01-01-%Y';
        } else if ($request->tipo == "ANUAL") {
            $f = '01-%m-%Y';
        } else if ($request->tipo == "MENSUAL") {
            $f = '%d-%m-%Y';
        } else {
            $f = '%d-%m-%Y %r';
        }
        $this->query = $query;
        $tabla = \App\Modelos\Detalle::whereHas('getOrden', function($q ) {
                    $q->whereRaw($this->query);
                })->select(DB::raw('COUNT(*) as n_facturas,'
                                . 'SUM(cantidad) as cantidad_total,'
                                . ' SUM((cantidad*precio)) as totales, producto, idPrecio'))->groupBy("producto")->groupBy("idPrecio")->get();
        foreach ($tabla as $value) {
            try {
                $value->nombre = \App\Modelos\Producto::where('idProducto', $value->producto)->get()[0]->nombre;
                $value->presentacion = \App\Modelos\Precio::where('id', $value->idPrecio)->get()[0]->presentacion;
            } catch (\ErrorException $e) {
                $value->nombre = "";
                $value->presentacion = "";
            }
        }
        return response()->json(['datos' => $tabla,
                    "columnas" => ['nombre', 'presentacion', 'n_facturas', 'totales'],
                    "columnas_lb" => ['Nombre', 'Presentacion', 'Compras', 'Total'],
                    "tipos" => ['cad', 'cad', 'entero', 'moneda']]);
    }

    /*
     * Cierre: Realiza el reporte x y z 
     * i:{0: Reporte x
     *    1: Reporte z}
     * $n_caja: Nuemero de la caja
     * $preview: Si debe realizarse el cierre o no
     */

    public function Cierre($i, $n_caja, $preview = true, $System = false) {
        $caja = caja::with('cierresA')->where(['numero' => $n_caja, 'estado' => 1])->where('Estado2', '<>', 2)->get();
        if (count($caja) != 0) {
            if ($i == 0) {
                $titulo = 'Cierre de turno, Caja';
                //Buscando el cierre de la caja
                $cierre = cierrecaja::where([ 'numero' => $n_caja, 'final' => '0000-00-00 00:00:00'])->get()[0];
                //CERRANDO LA CAJA
                $cierre->final = date_format(date_create(), 'Y-m-d H:i:s');
                if ($System == false) {
                    $cierre->usuarioCerroCaja = "Sistema";
                } else {
                    $cierre->usuarioCerroCaja = $System;
                }
                $inicio = $cierre->inicio;
                $final = $cierre->final;
                $cierres = [$cierre];
            } else {
                $titulo = 'Cierre Caja';
                $fecha = date_format(date_create(), 'Y-m-d');
                $cierres = cierrecaja::whereRaw('numero =' . $n_caja . ' and DATE(inicio) = Date(\'' . $fecha . '\')')->orderBy('inicio')->get();
                $inicio = $cierres->first()->inicio;
                $final = $cierres->last()->final;
                //dd($inicio, $final);
            }
            $query = VentasReportController::querymaker('fecha_hour', null, null,null,null,$inicio, $final, true) . " and caja =" . $n_caja;
            $query_est = VentasReportController::querymaker('fecha_hour', null, null, null, null, $inicio, $final, false) . " and caja =" . $n_caja;
            //CREANDO CONSULTA A LA FACTURA
            $f = '%d-%m-%y';
            $this->query = $query;
            //CREANDO CONSULTA A PRODUCTOS
            $Productos = \App\Modelos\Detalle::whereHas('getOrden', function($q ) {
                        $q->whereRaw($this->query);
                    })->select(DB::raw('COUNT(*) as n_facturas,'
                                    . 'SUM(cantidad) as cantidad_total,'
                                    . ' SUM((cantidad*precio)) as totales, producto, idPrecio'))->groupBy("producto")->groupBy("idPrecio")->get();
            //CREANDO CONSULTA A CATEGORIA Y PRODUCTOS
            $Categoria = [];
            $cout_p = 0;
            foreach ($Productos as $value) {
                try {
                    $Producto = \App\Modelos\Producto::where('idProducto', $value->producto)->get()[0];
                    // dd($Producto);
                    $value->nombre = $Producto->nombre;
                    $value->idCategoria = $Producto->categoria;
                    $value->presentacion = \App\Modelos\Precio::where('id', $value->idPrecio)->get()[0]->presentacion;
                } catch (\ErrorException $e) {
                    $value->nombre = "";
                    $value->presentacion = "";
                }
                $cout_p+=$value->cantidad_total;
                if (!isset($Categoria[$value->idCategoria])) {
                    $Categoria[$value->idCategoria] = clone ($value);
                } else {
                    $Categoria[$value->idCategoria]->cantidad_total+=$value->cantidad_total;
                    $Categoria[$value->idCategoria]->n_facturas+=$value->n_facturas;
                    $Categoria[$value->idCategoria]->totales+=$value->totales;
                    $Categoria[$value->idCategoria]->nombre = \App\Modelos\Categoria::where('idCategoria', $value->idCategoria)->get()[0]->nombre;
                }
            }


            //CREANDO PAGOS
            $Pagos = PagoFactura::whereHas('getOrden', function($q ) {
                        $q->whereRaw($this->query);
                    })->select(DB::raw('COUNT(*) as n_facturas,'
                                    . ' SUM(monto) as totales, tipoPago'))->groupBy("tipoPago")->get();

            foreach ($Pagos as $value) {
                try {
                    $value->nombre = \App\Modelos\ModoPago::where('idPago', $value->tipoPago)->get()[0]->tipoPago;
                } catch (\ErrorException $e) {
                    $value->nombre = "";
                }
            }

            //CREANDO DESCUENTOS
            $Descuentos = FacturaTieneDescuento::whereHas('getOrden', function($q ) {
                        $q->whereRaw($this->query);
                    })->select(DB::raw('COUNT(*) as n_facturas,'
                                    . ' SUM(valorDescuento) as totales, idDescuento'))->groupBy("idDescuento")->get();
            foreach ($Descuentos as $value) {
                try {
                    $value->nombre = \App\Modelos\Descuento::where('idDescuento', $value->idDescuento)->get()[0]->nombre;
                } catch (\ErrorException $e) {
                    $value->nombre = "";
                }
            }
            $General = [];

            //Ventas por empleados
            $cajeros = Orden::whereRaw($query_est)->select(DB::raw('COUNT(*) as n_facturas,' .
                                    ' SUM(if(estado=1,1,0)) as F,' .
                                    ' SUM(if(estado=0,1,0)) as C,' .
                                    ' SUM(if(estado=0,total,0)) as TC,' .
                                    ' SUM(if(estado=1,total,0)) as TF,' .
                                    ' SUM(total) as totales, cajero'))->groupBy("cajero")->get();
            foreach ($cajeros as $value) {
                try {
                    $value->nombre = \App\User::where('dni', $value->cajero)->get()[0]->name;
                } catch (\ErrorException $e) {
                    $value->nombre = "";
                }
            }

            //Ventas por empleado

            $meseros = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                    . ' SUM(total) as totales, empleado'))->groupBy("empleado")->get();
            foreach ($meseros as $value) {
                try {
                    $value->nombre = \App\User::where('dni', $value->empleado)->get()[0]->name;
                } catch (\ErrorException $e) {
                    $value->nombre = "";
                }
            }
            //CREANDO INFORMACION GENERAL
            $General = Orden::whereRaw($query)->select(DB::raw('COUNT(*) as n_facturas,'
                                    . ' SUM(subtotal) as subtotales,'
                                    . ' SUM(propina) as propinas,'
                                    . ' SUM(impuesto) as impuestos,'
                                    . ' SUM(descuento) as decuentos,'
                                    . ' SUM(extras) as extras,'
                                    . ' SUM(total) as totales'), DB::raw("DATE_FORMAT(created_at,'" . $f . "' ) as fecha"))->groupBy("fecha")->get();

            if ($i == 0) {
                if (count($General) > 0) {
                    $cierre->ventas = $General[0]->n_facturas;
                    $cierre->total = $General[0]->totales;
                }
            }
            $J = $i;
            $General->productos = $cout_p;
            $i = count($Productos) * 10;
            if ($i == 1) {
                $x = 250;
            }
            if ($i > 1) {
                $x = 250 + (35 * $i);
            }
            if ($i == 0) {
                $x = 250;
            }
            if ($preview == "false") {
                try {

                    $imprimir = new cola_impresion();
                    $caja = caja::find($n_caja);
                    $impresora = impresora::find($caja->impresora);
                    $imprimir->impresora = $impresora != null ? $impresora->real_name : "pdf";
                    $imprimir->url = null;
                    $imprimir->estado = 1;
                    $imprimir->save();

                    $file = fopen(public_path() . "\config_datos_generales.txt", "r");
                    //Output a line of the file until the end is reached
                    $datos = [];
                    $datos[] = strtoupper(fgets($file));
                    $datos[] = strtoupper(fgets($file));
                    $datos[] = strtoupper(fgets($file));
                    $datos[] = strtoupper(fgets($file));
                    $datos[] = fgets($file);


                    $view = \View::make('cierre', compact('datos', 'imprimir', 'titulo', 'cierres', 'n_caja', 'Productos', 'General', 'Pagos', 'Descuentos', 'Categoria', 'cajeros', 'meseros', 'inicio', 'final'))->render();
                    $imprimir->tikect = $view;
                    $imprimir->estado = 1;
                    $imprimir->save();
                    //dd(1);
                    if ($J == 0) {
                        Cache::put('caja' . $cierre->cajero, -1, 1500);
                        $cierre->save();
                        //busco todas las cajas y todos los cajeros
                        $cajaActual = caja::where('numero', $n_caja)->get();
                        $cajas = caja::where('estado', 1)->with('cierresA.getCajero', 'cierresC.getCajero')->orderBy('numero')->get();
                        $fecha = date('Y-m-d');
                        $cajeros = User::where('estado', 1)->where('tipo', '<>', 'Mesero')->get(array('dni', 'name', 'tipo'));
                        foreach ($cajas as $caja) {
                            $X = count($caja->cierresC);
                            $Y = count($caja->cierresA);
                            if ($X != 0) {
                                $caja->numeroTurno = ($X);
                                $caja->mostrar = 'Cerrado';
                            }
                            if ($Y != 0) {
                                $caja->numeroTurno = ($X + $Y);
                                $caja->mostrar = 'Abierto';
                            }
                        }
                        return response()->json(['error' => false, 'cajas' => $cajas, 'cajeros' => $cajeros, 'mensaje' => 'El turno fue cerrado exitosamente']);
                    } else {
                        //busco todas las cajas y todos los cajeros
                        $cajaActual = caja::where('numero', $n_caja)->get();
                        $cajaActual[0]->Estado2 = 2;
                        $cajaActual[0]->save();
                        $cajas = caja::where('estado', 1)->with('cierresA.getCajero', 'cierresC.getCajero')->orderBy('numero')->get();
                        $fecha = date('Y-m-d');
                        $cajeros = User::where('estado', 1)->where('tipo', '<>', 'Mesero')->get(array('dni', 'name', 'tipo'));
                        foreach ($cajas as $caja) {
                            $X = count($caja->cierresC);
                            $Y = count($caja->cierresA);
                            if ($X != 0) {
                                $caja->numeroTurno = ($X);
                                $caja->mostrar = 'Cerrado';
                            }
                            if ($Y != 0) {
                                $caja->numeroTurno = ($X + $Y);
                                $caja->mostrar = 'Abierto';
                            }
                        }
                        return response()->json(['error' => false, 'cajas' => $cajas, 'cajeros' => $cajeros, 'mensaje' => 'La caja  fue cerrada exitosamente']);
                    }
                } catch (\Illuminate\Database\QueryException $ex) {
                    return response()->json(['error' => true, 'mensaje' => 'ha ocurrido un error al tratar de realizar la operacion']);
                }
            } else {
                //imprimir
                $view = \View::make('cierre', compact('titulo', 'cierres', 'n_caja', 'Productos', 'General', 'Pagos', 'Descuentos', 'Categoria', 'cajeros', 'meseros', 'inicio', 'final'))->render();
                return $view;
            }
        } else {
            return response()->json(['error' => true, 'mensaje' => 'No existe una caja activa con esos datos']);
        }
    }

    public static function querymaker($tipo, $year, $yearf, $mes, $mesf, $fecha, $fecha2, $estado = true) {
        if ($tipo == "DIARIO") {
            $fechai = \Carbon\Carbon::createFromFormat('d-m-Y', $fecha);
            $fechaf = \Carbon\Carbon::createFromFormat('d-m-Y', $fecha2);
            $q = "DATE(created_at) between DATE('" . $fechai . "') and DATE('" . $fechaf . "')";
        } else if ($tipo == "ANUAL") {
            $q = "YEAR(created_at) >= " . $year . " AND YEAR(created_at)<= " . $yearf;
        } else if ($tipo == "MENSUAL") {
            $fechai = \Carbon\Carbon::create($year, $mes, '1');
            $fechaf = \Carbon\Carbon::create($yearf, $mesf, '1')->addMonth(1);
            $q = "DATE(created_at) between DATE('" . $fechai . "') and DATE('" . $fechaf . "')";
        } else if ($tipo == "fecha_hour") {
        $q = "created_at between '" . $fecha . "' and '" . $fecha2 . "'";
		}else{
            $q = "true";
        }
        if ($estado) {
            $q.= " and estado = 1";
        }

        return $q;
    }

}

/*
 * reportePDF: Make a reporte in pdf to download
 *          $cabeceras: Cabeceras a usar en la tabla
 *          $var_nom: nombre de las variables en el arreglo
 *          $datos: Datos de la tabla
 *     */

function reportePDF($cabeceras, $var_nom, $datos, $title, $fecha) {
    $view = \View::make('layouts.reporte_pdf', compact('cabeceras', 'var_nom', 'datos', 'title', 'fecha'))->render();
    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML($view);
    //echo $view;
    return $pdf->stream('reporte');
}

function querymaker($tipo, $year, $mes, $fecha, $fecha2, $estado = true) {
    if ($tipo == "fecha_hour") {
        $q = "created_at between '" . $fecha . "' and '" . $fecha2 . "'";
    } else if ($tipo == "fecha") {
        $q = "DATE(created_at) between DATE('" . $fecha . "') and DATE('" . $fecha . "')";
    } else if ($tipo == "ANUAL") {
        $q = "YEAR(created_at) = " . $year . " and estado = 1";
    } else if ($tipo == "MENSUAL") {
        $q = "Month(created_at) = " . $mes . " and YEAR(created_at) = " . $year;
    } else if ($tipo == "DIARIO") {
        $q = "DATE(created_at) between '" . $fecha . "' and '" . $fecha2 . "'";
    } else {
        $q = "true";
    }
    if ($estado) {
        $q.= " and estado = 1";
    }
    return $q;
}

function querymakerfactura(Request $request) {
    $query = VentasReportController::querymaker($request);
    return Orden::whereRaw($query)->select(DB::raw('idFactura'));
}

function tabla_creator() {
    $query = VentasReportController::querymaker($request);
    return Orden::whereRaw($query)->select(DB::raw('idFactura'));
}

function unir($A, $B) {
    foreach ($B as $key) {
        $A->push($key);
    }
    return $A;
}

function unir_by($A, $B, $by) {
    foreach ($B as $key) {
        $A->push($key);
    }
    return $A->sortBy($by)->groupBy($by);
}

function mezclar($A, $B, $by) {
    $A = unir_by($A, $B, $by);
    foreach ($A as $C) {
        if ($C->count() == 2) {

            foreach ($C[1]->attributes as $key => $value) {

                $C[0]->attributes[$key] = $value;
            }
            unset($C[1]);
        }
    }

    return $A;
}
