<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\Persona;
use App\Modelos\Detalle;
use App\Modelos\ModoPago;
use App\Modelos\Descuento;
use App\Modelos\PagoFactura;
use App\Modelos\Orden;
use App\Modelos\Producto;
use App\Modelos\caja;
use App\Modelos\direccion;
use App\Modelos\Cliente;
use App\Modelos\mesa;
use App\Modelos\cola_impresion;
use App\Modelos\detalletienemodificador;
use App\Modelos\PrecioProducto;
use App\Modelos\facturatienecupon;
use App\Modelos\clienteusacupon;
use App\Modelos\impuesto;
use Illuminate\Http\Request;
use Cache;
use Session;
use DB;
use Auth;

class PagoController extends Controller {

    static public function CRUD(Request $request) {
        return view('layaout.MetodoPago');
    }

    /*
      FUNCION QUE HACE LA RESERVA DE LOS PODUCTOS A SE ADQUIRIDOS POR EL USUARIO
     */

    public function Precompra(Request $request) {
        $carrito = json_decode($request->carrito);
        $productosID = [];
        /* Variable para la verificacion */
        $v = [];
        foreach ($carrito->productos as $producto) {
            $productosID[] = $producto->id_precio;
            $v[$producto->id_precio] = $producto;
        }
        /* VERIFICANDO SI EL PAGO VA POR PAYPAL */
        if ($carrito->pagos->paypal > 0) {
            //--------------PRIMER BLOQUEO TEMPORAL-----------------
            /*
             * ordenes_bl:  es un arreglo con todas las ordenes vendidas via PAYPAL
             *
             */
            $ordenes_bl = Cache::get('ordenes_bl', []);
            //Si existia una oden anterior debe ser borrada

            $this->cancelar_paypal_orden($request);
            $productos = PrecioProducto::whereIn("id", $productosID)->get();
            foreach ($productos as $producto) {
                if ($v[$producto->id]->cantidad >= $producto->stock) {
                    return response()->json(
                                    ['error' => true,
                                        'tipo' => "NOSTOCK",
                                        'productos' => $productos,]);
                }
            }
            //Guardando un nuevo carrito
            $ordenes_bl[isset($carrito->userID)? $carrito->userID:$request->idUser] = $carrito;
            foreach ($productos as $producto) {
                $p_compra = $v[$producto->id];
                $productos_bloqueados[] = $p_compra;
                $producto->stock_bl += $p_compra->cantidad;
                $producto->stock -= $p_compra->cantidad;
                $producto->save();
            }
            Cache::forever('ordenes_bl', $ordenes_bl);
            return response()->json(
                            ['error' => false,
                                'tipo' => "SAVETOPAYPAL"]);
            //------------------------------------------------------
        } else {
            //------------------------------------------------------//
            return $this->bloqueo_permanente($request);
            //------------------------------------------------------//
        }
    }

    /*
      FUNCION QUE HACE LA RESERVA DE LOS PRODUCTOS A SE ADQUIRIDOS POR EL USUARIO
     */

    public function bloqueo_permanente(Request $request) {
        $carrito = json_decode($request->carrito);
        //inicio de la transaccion
        DB::beginTransaction();
        try {
            $orden = new Orden();
            $orden->total = $carrito->total;
            $orden->cliente = isset($carrito->userID)? $carrito->userID:$request->idUser;
            $orden->despachado_por = 0;
            $orden->estado = 3;
            $orden->subtotal = $carrito->subtotal;
            $orden->descuento = $carrito->promocion;
            $orden->cambio = 0;
            $orden->envio = $carrito->envio;
            $orden->paypal = $carrito->pagos->paypal;
            $orden->directo = $carrito->pagos->directo;
            $orden->directoTarjeta = $carrito->pagos->directoTarjeta;
            $orden->referencias = $carrito->direccion->referencias;

            //calculo lo que se pago con el cupon personal
            //este variable representa lo pagado con el cupon personal
            $credito = 0.00;
            foreach ($carrito->pagos->cupones as $cupon) {
                if ($cupon->id == 0) {
                    //este es el cupon personal
                    $credito = $cupon->valorI;
                }
            }
            $orden->totalCupones = ($carrito->pagos->totalCupones - $credito);
            $orden->totalCredito = $credito;

            //verifico la direccion del pedido
            if ($carrito->direccion->id != 0) {
                //la direccion seleccionado fue una direccion favorita y esta ya existe en la base de datos, por lo tanto no debo crearla
                $orden->idDireccion = $carrito->direccion->id;
            } else {
                //es una direccion nueva y tengo que registrarla
                $direccion = new direccion();
                $pDireccion = $carrito->direccion;
                $direccion->latitud = $pDireccion->lat;
                $direccion->longitud = $pDireccion->lon;
                $direccion->texto = $pDireccion->direccion;
                if ($pDireccion->favorita == true) {
					$direccion->alias = $pDireccion->alias;
                    $direccion->favorita = 1;
                } else {
                    $direccion->favorita = 0;
                }
                $direccion->idCliente = $request->idUser;
                $direccion->save();
                $orden->idDireccion = $direccion->id;
            }
            $orden->save();
            $cliente = Cliente::find($request->idUser);

            //voy a verificar a ver si es la primera vez que compra el cliente
            if ($cliente->ordenes->count() == 1) {
                //es mi primera compra, debo verifcar si fui referenciado y si lo fui voy a aumentarle 50 al credito del usuario que me referenciado
                if ($cliente->IdClienteReferencio != null) {
                    $UsuarioRef = Cliente::find($cliente->IdClienteReferencio);
                    $UsuarioRef->creditoCupon+=50.00;
                    $UsuarioRef->save();
                }
            }

            $productosID = [];
            /* Variable para la verificacion */
            $v = [];
            foreach ($carrito->productos as $producto) {
                $productosID[] = $producto->id_precio;
                $v[$producto->id_precio] = $producto;
            }
            $productos = PrecioProducto::whereIn("id", $productosID)->get();
            /* VERIFICANDO SI EL PAGO VA POR PAYPAL */
            if ($carrito->pagos->paypal > 0) {
                //--------------PRIMER DESBLOQUEO TEMPORAL-----------------
                /*
                 * ordenes_bl:  es un arreglo con todas las ordenes vendidas via PAYPAL
                 *
                 */
                //guardando los detalles de la orden
                $ordenes_bl = Cache::get('ordenes_bl', []);
                foreach ($productos as $producto) {
                    $p_compra = $v[$producto->id];
                    $productos_bloqueados[] = $p_compra;
                    $producto->stock_bl -= $p_compra->cantidad;
                    $producto->stock_b+= $p_compra->cantidad;
                    $producto->save();
                    $detalle = new Detalle();
                    $detalle->factura = $orden->idFactura;
                    $detalle->cantidad = $p_compra->cantidad;
                    $detalle->precio = $p_compra->precio;
                    if($p_compra->promo_cant!=null&&$p_compra->cantidad_promo!=null){
						if ($p_compra->cantidad > $p_compra->promo_cant) {
							$detalle->valorDescuento = $p_compra->cantidad_promo;
						}
					}
                    $detalle->producto = $p_compra->id;
                    $detalle->idPrecio = $p_compra->id_precio;
                    $detalle->save();
                }
				
                unset($ordenes_bl[isset($carrito->userID)? $carrito->userID:$request->idUser]);
                Cache::forever('ordenes_bl', $ordenes_bl);

                //guarda los cupones que se usaron en una orden y los que uso el cliente(si los tiene);
                foreach ($carrito->pagos->cupones as $cupon) {
                    if ($cupon->id != 0) {
                        //guardar los cupones promocionales que se usaron en la orden
                        $relacion = new facturatienecupon();
                        $relacion->idCupon = $cupon->id;
                        $relacion->montoPagado = $cupon->valorI;
                        $relacion->idFactura = $orden->idFactura;
                        $relacion->save();

                        //guardar los cupones promocionales que uso el cliente
                        $relacion2 = new clienteusacupon();
                        $relacion2->idCupon = $cupon->id;
                        $relacion2->IdCliente = $request->idUser;
                        $relacion2->save();
                    } else {
                        //es el cupon personal
                        $cliente->creditoCupon = 0.00;
                        $cliente->save();
                    }
                }

                //------------------------------------------------------
            } else {
                //----------------------VERIFICANDO EXISTENCIAS--------------------------------//
                foreach ($productos as $producto) {
                    if ($v[$producto->id]->cantidad >= $producto->stock) {
                        DB::rollback();
                        return response()->json(
                                        ['error' => true,
                                            'tipo' => "NOSTOCK",
                                            'productos' => $productos,]);
                    }
                }
                $detalles = [];
                //guardando los detalles de la orden
                foreach ($productos as $producto) {
                    $p_compra = $v[$producto->id];
                    $productos_bloqueados[] = $p_compra;
                    $producto->stock -= $p_compra->cantidad;
                    $producto->stock_b+= $p_compra->cantidad;
                    $producto->save();
                    $detalle = new Detalle();
                    $detalle->factura = $orden->idFactura;
                    $detalle->cantidad = $p_compra->cantidad;
                    $detalle->precio = $p_compra->precio;
					if($p_compra->promo_cant!=null&&$p_compra->cantidad_promo!=null){
						if ($p_compra->cantidad > $p_compra->promo_cant) {
							$detalle->valorDescuento = $p_compra->cantidad_promo;
						}
					}
                    $detalle->producto = $p_compra->id;
                    $detalle->idPrecio = $p_compra->id_precio;
                    $detalle->save();
                    $detalles[] = $detalle;
                }

                //guarda los cupones que se usaron en una orden y los que uso el cliente(si los tiene);
                foreach ($carrito->pagos->cupones as $cupon) {
                    if ($cupon->id != 0) {
                        //guardar los cupones promocionales que se usaron en la orden
                        $relacion = new facturatienecupon();
                        $relacion->idCupon = $cupon->id;
                        $relacion->montoPagado = $cupon->valorI;
                        $relacion->idFactura = $orden->idFactura;
                        $relacion->save();

                        //guardar los cupones promocionales que uso el cliente
                        $relacion2 = new clienteusacupon();
                        $relacion2->idCupon = $cupon->id;
                        $relacion2->IdCliente = $request->idUser;
                        $relacion2->save();
                    } else {
                        //es el cupon personal
                        $cliente->creditoCupon = 0.00;
                        $cliente->save();
                    }
                }

                //------------------------------------------------------//
            }
            //fin de la transaccion
            DB::commit();
            return response()->json(
                            ['error' => false,
                                'tipo' => "CORRECTO",
                                'credito' => $cliente->creditoCupon]
            );
        } catch (\Illuminate\Database $ex) {
            DB::rollback();
            return response()->json(
                            ['error' => true,
                                'tipo' => "INVALIDDATA"]
            );
        }
    }

    public function cancelar_paypal_orden(Request $request) {
        $carrito = json_decode($request->carrito);
        //--------------PRIMER BLOQUEO TEMPORAL-----------------
        /*
         * ordenes_bl:  es un arreglo con todas las ordenes vendidas via PAYPAL
         *
         */
        $ordenes_bl = Cache::get('ordenes_bl', []);
        if (!isset($ordenes_bl[isset($carrito->userID)? $carrito->userID:$request->idUser])) {
            return;
        }
        $carrito = $ordenes_bl[isset($carrito->userID)? $carrito->userID:$request->idUser];

        $v = [];
        foreach ($carrito->productos as $producto) {
            $productosID[] = $producto->id_precio;
            $v[$producto->id_precio] = $producto;
        }

        $productos = PrecioProducto::whereIn("id", $productosID)->get();

        foreach ($productos as $producto) {
            $p_compra = $v[$producto->id];
            $producto->stock_bl -= $p_compra->cantidad;
            $producto->stock += $p_compra->cantidad;
            $producto->save();
            //dd($producto);
        }
        unset($ordenes_bl[isset($carrito->userID)? $carrito->userID:$request->idUser]);
        Cache::forever('ordenes_bl', $ordenes_bl);
        return response()->json(
                        ['error' => false,
                            'tipo' => "SAVETOPAYPAL"]);
        //------------------------------------------------------
    }

    static public function RealizarPago(Request $request) {
        //El cliente esta en una 
        $tipo = $request->tipo;
        $dni = $request->dni;
        $num_mesa = $request->get('mesa', "No");
        if ($num_mesa == 'No') { //Posee mesa?
            return response()->json(["error" => true, 'Mensaje' => "Mesa Invalida"]);
        } else if (!Cache::has('orden' . $num_mesa)) { //El cliente ha pedido algo?
            return response()->json(["error" => true, 'Mensaje' => "La mesa no posee orden asociada"]);
        } else if (Cache::get('orden' . $num_mesa)->total <= 0) { //El cliente ha pedido algo?
            return response()->json(["error" => true, 'Mensaje' => "El monto de la orden es cero"]);
        } else {
            // $vendedor = Cache::get('vendedor', -1);
            $cliente = Cache::get('cliente' . $num_mesa, new Persona);
            $orden = Cache::get('orden' . $num_mesa, []);
            //dd($orden);
            $orden->estado = 2; //Atendiendo
            $orden->dni = $dni;
            $orden->cajero = $dni;
            Cache::put('orden' . $num_mesa, $orden, 1500);
            $detalles = Cache::get('detalles' . $num_mesa, []);
            if (isset($orden->total)) {
                $total = $orden->total;
                $subtotal = $orden->subTotal;
            }
            $detalles = array_where($detalles, function($key, $value) {
                return isset($value);
            });
            //Metodos de pago
            $metodos_pago = ModoPago::where('estado', '=', 1)->get();
            //Descuentos
            $Descuentos = Descuento::where('estado', '=', 1)->get();
            //impuestos
            $impuesto = Cache::get('valoImp' . $num_mesa);
            //propinas
            $propina = Cache::get('valoProp' . $num_mesa);
            //dd($impuesto);
            return response()->json(
                            ['detalles' => $detalles,
                                'num_mesa' => $num_mesa,
                                'orden' => $orden,
                                'cliente' => $cliente,
                                // 'vendedor' => $vendedor,
                                'metodos_pago' => $metodos_pago,
                                'descuentos' => $Descuentos,
                                'propina' => $propina,
                                'impuesto' => $impuesto,
                                'subtotal' => $subtotal,
                                'total' => $total]);
        }
    }

    //Funcion que se utiliza para realizar el pago de la orden
    public function PagarOrden(Request $request, $num_mesa) {
        $dni = $request->dni;
        $tipo = $request->tipo;

        $caja = 1; //Cache::get('caja'.$dni, -1);
        if ($caja == -1) {
            return response()->json(['error' => true,
                        'mensaje' => 'No posee caja']);
        }
        $pPagos = json_decode($request->pagos);
        $pDescuentos = json_decode($request->descuentos);
        $orden = Cache::get('orden' . $num_mesa, []);
        $detalles = Cache::get('detalles' . $num_mesa, []);
        //impuestos
        $valorImpuesto = Cache::get('valoImp' . $num_mesa);
        $impuestos = Cache::get('impuestosO' . $num_mesa);
        //propinas
        $valorPropina = Cache::get('valoProp' . $num_mesa);
        $propinas = Cache::get('propinasO' . $num_mesa);
        //cliente
        $cliente = Cache::get('cliente' . $num_mesa);

        //obteniendo el total pagado por metodo de pago
        $monto = 0;
        $valorP = 0;
        foreach ($pPagos as $pPago) {
            $monto+= $pPago->monto;
        }

        //obteniendo el total pagado por descuentos si los hay.
        if (count($pDescuentos) > 0) {
            foreach ($pDescuentos as $pDescuento) {
                $monto+= $pDescuento->valor;
                $valorP+= $pDescuento->valor;
            }
        }
        //verificando que la orden fue pagada con exito 
        if ($orden->total > $monto) {
            return response()->json(['status' => 'Error',
                        'mensaje' => 'El monto pagado(' . $monto . ') no coincide con el monto que se debe pagar ' . $orden->total]);
        }
        $vuelto = ($monto - $orden->total);
        if ($orden != [] && $detalles != []) {
            //Begin Transaction
            DB::beginTransaction();
            //Guardando Orden
            $orden->descuento = $valorP;
            $orden->impuesto = $valorImpuesto;
            $orden->propina = $valorPropina;
            $orden->estado = 1;
            $orden->cambio = $vuelto;
            $orden->caja = $caja;
            $orden->cajero = $dni;


            unset($orden->dni);
            $clienteT = Cache::get('cliente' . $num_mesa, new Persona());
            $cliente = Persona::firstOrNew(['dni' => $clienteT->dni]);
            $cliente->nombreCompleto = $clienteT->nombreCompleto;
            $cliente->telefono = $clienteT->telefono;
            $cliente->save();
            $orden->cliente = $clienteT->dni;
            //guardo la orden
            if ($orden->tipo == 'comer') {
                //la orden tiene mesa por lo tanto debo buscarla y asignarsela a la orden
                $mesa = mesa::find($num_mesa);
                $orden = Orden::create_orden($orden);
                $mesa->ordenes()->save($orden);
            } else {
                $orden = Orden::create_orden($orden);
                $orden->save();
            }

            //Guardando Detalles de la orden
            $detallesO = Detalle::create_array($detalles);
            $orden->detalles()->saveMany($detallesO);

            //Guardando los modificadores de los detalles so los tiene;
            if (count($detalles) > 0) {
                foreach ($detalles as $key => $detalle) {
                    //si tengo subconjuntos
                    if ($detalle->subconjuntos != false) {
                        //recorro todos los subcnojuntos
                        foreach ($detalle->subconjuntos as $subconjunto) {
                            //recorro todos los modificadores de un subconjunto
                            foreach ($subconjunto->modificadores as $modificador) {
                                //creo un objeto de la tabla pivote y lo lleno
                                $pivote = new detalletienemodificador();
                                $pivote->idModificador = $modificador->idMod;
                                $pivote->NumItemsDet = $subconjunto->cantidad;
                                $pivote->subconjunto = ($subconjunto->idS + 1);
                                $pivote->cantidadModificador = $modificador->canT;
                                //pregunto si el modificador es de tipo extra
                                if ($modificador->precio != null) {
                                    $pivote->precioExtra = $modificador->precio;
                                    $pivote->total = ($modificador->canT * $modificador->precio);
                                    if ($modificador->compuesto != 0) {
                                        $pivote->idOpcion = $modificador->idOp;
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'precioExtra' => $pivote->precioExtra, 'total' => $pivote->total, 'idOpcion' => $pivote->idOpcion]]);
                                    } else {
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'precioExtra' => $pivote->precioExtra, 'total' => $pivote->total]]);
                                    }
                                } else {
                                    $pivote->total = 0;
                                    if ($modificador->compuesto != 0) {
                                        $pivote->idOpcion = $modificador->idOp;
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'total' => $pivote->total, 'idOpcion' => $pivote->idOpcion]]);
                                    } else {
                                        $detallesO[$key]->modificadores()->attach([$pivote->idModificador => ['subconjunto' => $pivote->subconjunto, 'cantidadDetalles' => $pivote->NumItemsDet, 'cantidadModificador' => $pivote->cantidadModificador, 'total' => $pivote->total]]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //Guardando Pagos
            foreach ($pPagos as $pPago) {
                $Pago = new PagoFactura();
                $Pago->tipoPago = $pPago->tipoPago;
                $Pago->monto = $pPago->monto;
                if (!$orden->pagos()->save($Pago)) {
                    DB::rollback();
                    return "error";
                }
            }
            //Guardando Descuentos;
            if (count($pDescuentos) > 0) {
                foreach ($pDescuentos as $pDescuento1) {
                    if ($pDescuento1->tipo == "porcentaje") {
                        $orden->descuentos()->attach([$pDescuento1->idDescuento => ['valorDescuento' => $pDescuento1->valor, 'porcentaje' => $pDescuento1->valorD]]);
                    } else {
                        $orden->descuentos()->attach([$pDescuento1->idDescuento => ['valorDescuento' => $pDescuento1->valor]]);
                    }
                }
            }

            //Guardando impuestos;
            if (count($impuestos) > 0) {
                foreach ($impuestos as $pimpuesto) {
                    if (!is_null($pimpuesto["porcentaje"])) {
                        $orden->impuestos()->attach([$pimpuesto["idImpuesto"] => ['valorImpuesto' => $pimpuesto["valorImpuesto"], 'porcentaje' => $pimpuesto["porcentaje"]]]);
                    } else {
                        $orden->impuestos()->attach([$pimpuesto["idImpuesto"] => ['valorImpuesto' => $pimpuesto["valorImpuesto"]]]);
                    }
                }
            }

            //Guardando propinas;
            if (count($propinas) > 0) {
                foreach ($propinas as $propina) {
                    if (!is_null($propina["porcentaje"] == "porcentaje")) {
                        $orden->propinas()->attach([$propina["idPropina"] => ['valorPropina' => $propina["valorPropina"], 'porcentaje' => $propina["porcentaje"]]]);
                    } else {
                        $orden->propinas()->attach([$propina["idPropina"] => ['valorPropina' => $propina["valorPropina"]]]);
                    }
                }
            }
            //dd($orden->descuentos()->get());
            DB::commit();
            //End Transation

            $n[0] = "OK";


            $n[0] = "OK";

            $p = PdfController::factura($orden->idFactura, 'true');
            Cache::forget('cliente' . $num_mesa);
            Cache::forget('orden' . $num_mesa);
            Cache::forget('detalles' . $num_mesa);
            Cache::forget('valoImp' . $num_mesa);
            Cache::forget('valoProp' . $num_mesa);
            Cache::forget('impuestosO' . $num_mesa);
            Cache::forget('propinasO' . $num_mesa);
            if ($p != null) {
                return response()->json(['error' => false, 'imprimio' => false, 'numero' => $orden->idFactura, 'mensaje' => 'La factura fue cobrada exitosamente, se generara un pdf por no tener una impresora configurada']);
            } else {
                return response()->json(['error' => false, 'imprimio' => true, 'mensaje' => 'La factura fue cobrada exitosamente']);
            }
        } else {
            return response()->json(['status' => 'Error', 'mensaje' => 'La mesa no posee una orden para ser cobrada']);
        }
    }

}
