<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\cola_impresion;
use View;

class ColaController extends Controller {

    static public function siguiente() {
        $imprimir = cola_impresion::where('estado', 1)->first();
        if (isset($imprimir)) {
            $imprimir->estado = 2; //imprimiendo;
            $imprimir->save();
            if ($imprimir->url != null) {
                return view('auxiliar.cola', ['imprimir' => $imprimir]);
            } else {
                return $imprimir->tikect;
            }
        } else {
            return "nada#nada#0#nada";
        }
    }

    static public function status(Request $request) {
        //busca Registro 
        try {
            $area = cola_impresion::find($request->id);

            if ($area != null) {
                $area->estado = $request->estado;
                $area->save();
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no se pudo cambiar el estado']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false, 'mensaje' => $mensaje]);
    }

}
