<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use App\Modelos\caja;
use App\Modelos\impresora;
use App\Modelos\cierrecaja;
use App\User;
use Auth;
use Session;
use Cache;

function buscarError($ex){
    //se incializa el mensaje
    $mensaje="";
    $codigo=$ex->errorInfo[1];
    //si es una violacion de llave  unica
    if($codigo==1062){
        //restircciones uniques de tabla modificadores
        $resultado = strpos($ex->errorInfo[2], "cajeroUnicaCaja");
        //si es diferente de false de ya hay un numeros adicional con esos datos..
        if($resultado !== false){
            $mensaje="Este cajero ya tiene una caja aperturada";
        }else{
            $resultado = strpos($ex->errorInfo[2], "CajaAbiertaUnica");
            if($resultado !== false){
                //viole la registrcioon unique pass
                $mensaje="La caja ya tiene un turno aperturado";
            }
        }
     }else{
         //violacion de llave foranea
        if($codigo==1451){
            $resultado = strpos($ex->errorInfo[2], "cierrecaja_caja_fk");
            if($resultado !== false){
                $mensaje="No se puede borrar la caja por tener turnos asociados a ella";
            }else{
                $resultado = strpos($ex->errorInfo[2], "factura_caja_fk");
                if($resultado !== false){
                    $mensaje="No se puede borrar la caja porque tiene facturas asociadas a ella";
                }else{
                }
            }
            
        }else{
            //si fue un trigguer o cuarquier otro error inesperado.
            $mensaje= $ex->errorInfo[2];
        }
     }
     return $mensaje;
}

class CajaController extends Controller {

    static public function controlCajas(Request $request) {
        return view('controlCajas');
    }
    
   static public function selectAll($mensaje='') {
        try{
        $cajas = caja::where('estado',1)->with('cierresA.getCajero','cierresC.getCajero')->orderBy('numero')->get();
        $fecha=date('Y-m-d');
        $cajeros = User::where('estado',1)->where('tipo','<>','Mesero')->get(array('dni','name','tipo'));
        foreach ($cajas as $caja) {
            $X=count($caja->cierresC);
            $Y=count($caja->cierresA);
            if( $X!=0){
                $caja->numeroTurno=($X);
                $caja->mostrar='Cerrado';
            }
            if($Y!=0 ){
                $caja->numeroTurno=($X+$Y);
                $caja->mostrar='Abierto';
            }
        } 
        return response()->json(['error' =>false,'cajas' =>$cajas,'cajeros'=>$cajeros,'mensaje'=>$mensaje]);
        }catch(Exception $ex){
             return response()->json(['error'=>true,'mensaje'=> 'Ha ocurrido un error al tratar de realizar la busqueda']);       
        }
    }
    
    static public function activarCaja(Request $request) {
         try{
        $caja = caja::with('cierresA')->where(['numero'=> $request->numero,'estado'=> 1])->get();
        if(count($caja)!=0){
             if($caja[0]->Estado2==0||$caja[0]->Estado2==1){
                 $cajero= User::where('dni',$request->cajero)->get(array('dni'));
                if($cajero!=null){
                    DB::beginTransaction();
                    $cierrecaja = new cierrecaja();
                    $cierrecaja->numero = $request->numero;
                    $cierrecaja->inicio = date_create();
                    $cierrecaja->cajero = $request->cajero;
                    $cierrecaja->save();
                    Cache::put('caja'.$request->cajero, $request->numero,1500);
                    $caja[0]->Estado2=1;
                    $caja[0]->save();
                    DB::commit() ;
                    $cajas = caja::where('estado',1)->with('cierresA.getCajero','cierresC.getCajero')->orderBy('numero')->get();
                    $fecha=date('Y-m-d');
                    $cajeros = User::where('estado',1)->where('tipo','<>','Mesero')->get(array('dni','name','tipo'));
                    foreach ($cajas as $caja) {
                        $X=count($caja->cierresC);
                        $Y=count($caja->cierresA);
                        if( $X!=0){
                            $caja->numeroTurno=($X);
                            $caja->mostrar='Cerrado';
                        }
                        if($Y!=0 ){
                            $caja->numeroTurno=($X+$Y);
                            $caja->mostrar='Abierto';
                        }
                    } 
                         return response()->json(['error' =>false,'cajas' =>$cajas,'cajeros'=>$cajeros,'mensaje'=>'La caja fue apertura exitosamente']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=> 'No existe un cajero con esos datos']);  
                }
             }else{
                 return response()->json(['error'=>true,'mensaje'=> 'La caja ya fue cerrada(Ya fue emitido su reporte Z)']);
                 
             }
            
        }else{
            return response()->json(['error'=>true,'mensaje'=> 'No existe una caja con esos datos']);       
        }
        }catch(\Illuminate\Database\QueryException $ex){
             DB::rollBack();
            $mensaje=  buscarError($ex);
            return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }

    static public function actualizarCaja(Request $request) {
        try{
            $fecha=date('Y-m-d');
            $caja=cierrecaja::whereRaw("DATE(inicio) = '".$fecha."' AND cajero='".$request->dni."' AND final='0000-00-00 00:00:00'")->get();
             if(count($caja)!=0){
                 $caja = $caja[0]->numero;
             }else{
                 $caja =-1;
             }
             Cache::put('caja'.$request->dni, $caja,1500);
            return response()->json(['error'=>false,'caja'=> $caja]);
        }catch(Exception $ex){
            return response()->json(['error'=>true,'mensaje'=> 'Ha ocurrido un error al tratar de realizar la busqueda']);       
        }
        return view('layouts.caja', ['impresoras' => $cajas]);
    }
    
    static public function CRUD(Request $request) {
        $impresoras = impresora::all();
        return view('layouts.caja', ['impresoras' => $impresoras]);
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla impresora
    public function Select_all(Request $request) {
        $caja = caja::all();
        foreach ($caja as $c) {
                $imp=$c->getImpresora;
                if(isset($imp))
                    $c->impresora= $c->getImpresora->nombre;
            else
                    $c->impresora="";
        }
        return response()->json(['caja' => $caja]);
    }

    //Select_one: Selecciona un solo registro
    public function Existe(Request $request) {
        $caja = caja::find($request->id);
        return response()->json(['caja' => $caja]);
    }

    //Create: Crea y Guarda un objeto de tipo impresora
    public function Create(Request $request) {
        try {//Guadar Registro  
            $caja = caja::firstOrCreate(['numero' => $request->numero]);
            $caja->impresora = $request->impresora;
            $caja->save();
            return response()->json(['error' => false, 'caja' => $caja, 'mensaje' => 'La caja fue registrada exitosamente']);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo != 1062) {
                return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            }
        } catch (Exception $ex) {
            return response()->json(['Error' => 'La caja no ha podido ser registrada']);
        }
    }

    //Update: Actualiza los datos de un impresora
    static public function Update(Request $request) {
        try {//Guadar Registro 
             
            $caja = caja::find($request->id);
            $caja->numero = $request->numero;
            $caja->impresora = $request->impresora;
            $caja->save();
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1062) {
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado != false) {
                    $resp = response()->json(['error' => true, 'mensaje' => 'Ya existe una caja con ese numero']);
                    $resp->st = 0;
                    return $resp;
                } 
            } else {
                $resp = response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
                $resp->st = 0;
                return $resp;
            }
        } catch (Exception $ex) {
            $resp = response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            $resp->st = 0;
            return $resp;
        }

        $resp = response()->json(['error' => false, 'caja' => $caja, 'mensaje' => 'La caja ha sido actualizada exitosamente']);
        $resp->st = 1;
        return $resp;
    }

    //Update_status: Actualiza el estado de un impresora
    public static function Update_status(Request $request) {
        try {//Guadar Registro 
            $caja = caja::find($request->id);
            $caja->estado = $request->estado;
            $caja->save();
            if($caja->estado==0){
                    $mensaje="La caja ha sido desactivada exitosamente.";
            }else{
                 $mensaje="La caja ha sido activada exitosamente";
            }
        } catch (Exception $ex) {
            return response()->json(['Error' => 'La caja no ha podido ser actualizado']);
        }
        return response()->json(['error' => false, 'mensaje' => $mensaje]);
    }
    
    public function Delete(Request $request) {
        try {
            $caja = caja::find($request->id);
            $caja->delete();
            return response()->json(['error' => false, 'mensaje' => "La caja ha sido Eliminada exitosamente"]);
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje= buscarError($ex);
            return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }

    public static function CerrarCaja($i, $n_caja) {
        //nota escribi 2 veces el mismo codigo de consulta porque no se porque
        //el pdf no se ejecuta cuando lo hacia con una funcion.
        if ($i == 1) {//Si el usuario le da al boton salir
        $cierre = cierrecaja::where([ 'numero' => $n_caja, 'final' => '0000-00-00 00:00:00'])->get()[0];
        //dd($cierre);
            $cierre->final =  date_format(date_create(), 'Y-m-d H:i:s');
           // $cierre->save();
            //Session::put('caja', -1);
            // Auth::logout();    
            $inicio = $cierre->inicio;
            $final = $cierre->final; 
         
            
            $consulta = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                                from precioproductos,detallesorden,productos,facturas 
                                where
                                facturas.idFactura=factura and 
                                facturas.created_at  BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" AND idProducto=detallesorden.producto and id=detallesorden.idPrecio   group by id;');
          
            $pago = DB::select('SELECT tipoPago,round(sum(monto),2) as monto from facturatienepago,facturas where factura=facturas.idFactura AND facturas.created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja.'  group by tipoPago');

            $impuestos = DB::select('select round(sum(impuesto),2) as impuesto from facturas where created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja.' ');
             dd($consulta, $pago, $impuestos); 
            $i = count($consulta);
            if ($i == 1) {
                $x = 250;
            }
            if ($i > 1) {
                $x = 250 + (35 * $i);
            }
            if ($i == 0) {
                $x = 250;
            }

           
                $view = \View::make('cierre', compact('consulta', 'i', 'inicio', 'final', 'pago', 'n_caja', 'impuestos'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->setPaper(array(0, 0, 240, $x));
                $pdf->loadHTML($view);
                $pdf->output();
                return $pdf->stream('cierre.pdf');
            
        } else {//si le da al boton cerrar caja
             $cierre = cierrecaja::find($n_caja);
            $cierre->final = date_create();
            $cierre->save();
            //Session::put('caja', -1);
            // Auth::logout();    
            $inicio = $cierre->inicio;
            $final = $cierre->final;


            $consulta = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                                from precioproductos,detallesorden,productos,facturas 
                                where
                                facturas.idFactura=factura and 
                                facturas.created_at  BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" AND idProducto=detallesorden.producto and id=detallesorden.idPrecio  and caja = '.$n_caja.' group by id;');
            $pago = DB::select('SELECT tipoPago,round(sum(monto),2) as monto from facturatienepago,facturas where factura=facturas.idFactura AND facturas.created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja.'  group by tipoPago');

            $impuestos = DB::select('select round(sum(impuesto),2) as impuesto from facturas where created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja.' ');

            $i = count($consulta);


            if ($i == 1) {
                $x = 250;
            }
            if ($i > 1) {
                $x = 250 + (35 * $i);
            }
            if ($i == 0) {
                $x = 250;
            }
            if ($i >= 1) {
                $view = \View::make('cierre', compact('consulta', 'i', 'inicio', 'final', 'pago', 'n_caja', 'impuestos'))->render();

                $pdf = \App::make('dompdf.wrapper');
                $pdf->setPaper(array(0, 0, 240, $x));
                $pdf->loadHTML($view);
                $pdf->output();
                return $pdf->stream('cierre.pdf');
            } else {
                return redirect('mesas');
            }
        }
    }
    
    
    
    public static function ReporteZ($i, $n_caja) {
        //nota escribi 2 veces el mismo codigo de consulta porque no se porque
        //el pdf no se ejecuta cuando lo hacia con una funcion.
        if ($i == 1) {//Si el usuario le da al boton salir
        $cierre = cierrecaja::where(['numero' => $n_caja, 'final' => '0000-00-00 00:00:00'])->get()[0];
        //dd($cierre);
            $cierre->final =  date_format(date_create(), 'Y-m-d H:i:s');
            //$cierre->save();
            //Session::put('caja', -1);
            // Auth::logout();    
            $inicio = $cierre->inicio;
            $final = $cierre->final; 
          dd($n_caja);
            $consulta = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                from precioproductos,detallesorden,productos,facturas 
                where
                facturas.idFactura=factura and 
                facturas.created_at  BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" AND idProducto=detallesorden.producto and id=detallesorden.idPrecio and caja = '.$n_caja.'  group by id;');
            $pago = DB::select('SELECT tipoPago,round(sum(monto),2) as monto from facturatienepago,facturas where factura=facturas.idFactura AND facturas.created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja.' group by tipoPago');
            $impuestos = DB::select('select round(sum(impuesto),2)as impuesto from facturas where created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" and caja = '.$n_caja);
            $i = count($consulta);
            if ($i == 1) {
                $x = 250;
            }
            if ($i > 1) {
                $x = 250 + (35 * $i);
            }
            if ($i == 0) {
                $x = 250;
            }

            if ($i >= 1) {
                $view = \View::make('cierre', compact('consulta', 'i', 'inicio', 'final', 'pago', 'n_caja', 'impuestos'))->render();
                $pdf = \App::make('dompdf.wrapper');
                $pdf->setPaper(array(0, 0, 240, $x));
                $pdf->loadHTML($view);
                $pdf->output();
                return $pdf->stream('cierre.pdf');
            } else {
                return redirect()->route('login');
            }
        } else {//si le da al boton cerrar caja
             $cierre = cierrecaja::find($n_caja);
            $cierre->final = date_create();
            $cierre->save();
            //Session::put('caja', -1);
            // Auth::logout();    
            $inicio = $cierre->inicio;
            $final = $cierre->final;


            $consulta = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                                from precioproductos,detallesorden,productos,facturas 
                                where
                                facturas.idFactura=factura and 
                                facturas.created_at  BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" AND idProducto=detallesorden.producto and id=detallesorden.idPrecio  group by id;');
            $pago = DB::select('SELECT tipoPago,round(sum(monto),2) as monto from facturatienepago,facturas where factura=facturas.idFactura AND facturas.created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '" group by tipoPago');

            $impuestos = DB::select('select round(sum(impuesto),2) as impuesto from facturas where created_at BETWEEN "' . $cierre->inicio . '" AND "' . $cierre->final . '"');

            $i = count($consulta);


            if ($i == 1) {
                $x = 250;
            }
            if ($i > 1) {
                $x = 250 + (35 * $i);
            }
            if ($i == 0) {
                $x = 250;
            }
            if ($i >= 1) {
                $view = \View::make('cierre', compact('consulta', 'i', 'inicio', 'final', 'pago', 'n_caja', 'impuestos'))->render();

                $pdf = \App::make('dompdf.wrapper');
                $pdf->setPaper(array(0, 0, 240, $x));
                $pdf->loadHTML($view);
                $pdf->output();
                return $pdf->stream('cierre.pdf');
            } else {
                return redirect('mesas');
            }
        }
    }


}
