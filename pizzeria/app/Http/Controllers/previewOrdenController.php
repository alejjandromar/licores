<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\Detalle;
use App\Modelos\impresora;
use App\Modelos\cola_impresion;
use Illuminate\Http\Request;
use Cache;
use DB;

class nodo {
    public $canT;
    public $compuesto;
    public $idMod;
    public $nombre;
    public $precio;

}

class previewOrdenController extends Controller {

     static  public function factura($num_orden,$imprimir='false',$num_mesa=-1) {
       // dd($num_mesa,$num_orden);
         if($num_mesa==-1){
              $orden = Orden::with('getcliente','getmesero','getcajero','detalles.getpresentacion','detalles.getproducto','pagos.getModoPago','detalles.modificadores.opciones')->where('idFactura',$num_orden)->get(); //Detalles de la orden
              $orden =$orden [0];//dd($orden);
              $i=count($orden->detalles);
               $caja = \App\Modelos\caja::find($orden->caja);
         }else{
             //traigo la data de variables de cache
             $orden = Cache::get('orden' . $num_mesa, []);
             $detalles = Cache::get('detalles' . $num_mesa, []);
             $valorImpuesto = Cache::get('valoImp' . $num_mesa);
             $valorPropina = Cache::get('valoProp' . $num_mesa);
             $cliente = Cache::get('cliente' . $num_mesa);
             //formateo de datos
             $orden->cliente=$cliente->dni;
             if($orden->cliente!='00000000A'){
                 $orden->getcliente=$cliente;
             }
             //dd($orden,count($detalles[key($detalles)]));
             if($orden->tipo=='comer'){
                 $orden->mesa=$num_mesa;
             }else{
                 $orden->mesa=null;
                 $orden->N=$num_mesa;
             }
             $orden->created_at=  $fecha=date('Y-m-d h:m:s');
             $orden->impuesto=$valorImpuesto;
             $orden->propina=$valorPropina;
             $i=count(count($detalles[key($detalles)]));
         }
         //la impresora por donde saldra el preview es aquella impresora a la cual pertenece el area de la mesa.
        $mesa = \App\Modelos\mesa::find($num_mesa);
        $area = \App\Modelos\area::with('getimpresora')->where('idArea',$mesa->Area)->get();dd($area[0]);
        $impresora = impresora::find($area[0]->getimpresora->id);
        $x = 350;
        if ($i > 1) {
            $x = 350 + (30 * $i);
        }
        if ($imprimir!='false') {
            $view = \View::make('facturahtml', compact('orden','detalles'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            $pdf->setPaper(array(0, 0, 240, $x));
            return $pdf->stream('factura.pdf');
            
        }else{
            $imprimir = new cola_impresion();
            $imprimir->impresora = $impresora != null ? $impresora->real_name : "pdf";
            $imprimir->url = null;
            $imprimir->estado = 1;
            $imprimir->save();
            $view = \View::make('facturahtml', compact('imprimir','orden','detalles'))->render();
            $imprimir->tikect = $view;
            $imprimir->estado = 1;
            $imprimir->save();
            return response()->json(['error' => false]);
        }
}

        }
