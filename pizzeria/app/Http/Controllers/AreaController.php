<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Modelos\impresora;
use App\Http\Controllers\Controller;
use App\Modelos\area;
use DB;
use View;

//retorna los errores que pueda generar manipular las tablas de este controlador(menos de clave primaria).
function buscarError($ex){
    //se incializa el mensaje
    $mensaje="";
    $codigo=$ex->errorInfo[1];
    //si es una violacion de llave  unica
    if($codigo==1062){
        //restircciones uniques de tabla modificadores
        $resultado = strpos($ex->errorInfo[2], "PRIMARY");
        //si es diferente de false quiere decir que estoy violando una primary key.
        if($resultado !== false){
            $mensaje='Ya existe un Area con ese identificador en la base de datos';
        }else{
           $mensaje='Ya existe un Area con ese nombre';
        }
    }else{
        //violacion de llave foranea
        if($codigo==1451){
            $resultado = strpos($ex->errorInfo[2], "productos_area_foreign");
            if($resultado !== false){
                $mensaje="No se puede borrar el area porque tiene un producto asociado";
            }else{
                $resultado = strpos($ex->errorInfo[2], "mesa_area_fk");
                if($resultado !== false){
                    $mensaje="No se puede borrar el area porque tiene una mesa asociada";
                }else{
                }
            }
            
        }else{
            //si fue un trigguer o cuarquier otro error inesperado.
            $mensaje= $ex->errorInfo[2];
        }
    }
    return $mensaje;
}

class AreaController extends Controller
{
    public function index()
    {
        $impresoras = impresora::all();
        return view('layouts.areas', ['impresoras' => $impresoras]);
    }
    
      static public function all() {
      $areas = area::all();
    return response()->json(['area' => $areas]);
        
    
    }
    
          static public function addArea(Request $request) { 
          
              try {
                  
            $area = new area();
            $area->nombre = $request->nombre;
            $area->tipo = $request->tipo;
             $area->impresora = $request->impresora;
            $area->save();
            return response()->json(['error' => false, 'mensaje'=>'El area ha sido registrada exitosamente.']);
            
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje= buscarError($ex);
            return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }  

    }
    
        public function update_estado(Request $request) {
        try {
            //busca Registro 
             $area  = area::find($request->id);
             
            if($area!=null){
                $area->estado = $request->estado;
                $area->save();
                if($request->estado==0){
                    $mensaje="El area ha sido desactivada exitosamente.";
                }else{
                     $mensaje="El area ha sido activada exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'no existe un Area con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje]);
    }
    
    
    
       static public function updateArea(Request $request) {
  
        try {//Guadar Registro 
            
            $area  = area::find($request->id);
            $area->nombre = $request->nombre;
            $area->tipo = $request->tipo;
             $area->impresora = $request->impresora;
            $area->save();
            
             return response()->json(['error' => false, 'mensaje'=>'El area ha sido actualizada exitosamente.']);
        } catch (\Illuminate\Database\QueryException $ex) {
         $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Area con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Area igual en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        } 
    } 
    
      static public function delete(Request $request) {
        try {
            $area = area::find($request->id);
            $area->delete();
            return response()->json(['error'=>false,'Message' => "Area eliminada"]);
        } catch (\Illuminate\Database\QueryException $ex) {
           $mensaje= buscarError($ex);
           return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }
    
    
}
    