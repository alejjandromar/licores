<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\impuesto;
use App\Modelos\Descuento;
use App\Modelos\Producto;
use App\Modelos\Categoria;
use App\Modelos\area;
use App\Modelos\modificador;
use App\Modelos\PrecioProducto;
use App\Modelos\propina;
use Request;
use DB;
use Excel;
use App;
use Input;
class cargarArchivoController extends Controller {

    public $respuesta;
    static public function CRUD(Request $request) {
    	//$excel = App::make('excel');
    	//dd($excel);
        return view('layouts.cargarArchivo');
    }
      public function add(Request $request) {
    	$caramelo = [];
		$resp="";
    	if (Input::hasFile('file'))
		    {

		  Excel::load(Input::file('file'), function($reader) use($caramelo){
          $results = $reader->toObject();//Se abre el archivo y se obtiene todas las hojas con sus celdas y se toma como un objeto
          $nombreTabla = '';
          $this->respuesta="";  
               
             //  dd($results[0][0]->nombre);
              // dd($results);
                for ($i=0;  $i < count($results) ; $i++) { 
                   $nombreTabla = $results[$i]->getTitle();
                  // dd($results[$i]->getTitle()=='Categorias');
                   for ($j=0; $j < count($results[$i]) ; $j++) { 
                        // dd($results[$i][$j]);
                         if ($nombreTabla == 'Categorias') {
                            $cat = Categoria::where('nombre', $results[$i][$j]->nombre)->get();
                          if (count($cat)==0) {
                            try{
                                $Categoria = new Categoria();
                                $Categoria->nombre = $results[$i][$j]->nombre;
                                $Categoria->descripcion = $results[$i][$j]->descripcion;
                                $Categoria->estado= $results[$i][$j]->estado;
                                $Categoria->has_sub = $results[$i][$j]->has_sub;
                                $Categoria->save();
                             }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar una categoria\n";
                             }
                            }else{
                             try{   
                                $Categoria = Categoria::find($cat[0]->idCategoria);
                                $Categoria->nombre = $results[$i][$j]->nombre;
                                $Categoria->descripcion = $results[$i][$j]->descripcion;
                                $Categoria->estado= $results[$i][$j]->estado;
                                $Categoria->has_sub = $results[$i][$j]->has_sub;
                                $Categoria->save();
                              }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)."   ha ocurrido un error al tratar de actualizar una categoria\n";
                             }
                            }   
                            
                           //QUITAR
                         }elseif ($nombreTabla == 'Cliente') {
                         
                          if (count($cat)==0) {
                            try{
                                $Categoria = new App\Modelos\Cliente();
                                $Categoria->razon_social = $results[$i][$j]->razon;
                                $Categoria->descripcion = $results[$i][$j]->ruc;
                                $Categoria->direccion= $results[$i][$j]->direccion;
                                $Categoria->cont1_nombre = $results[$i][$j]->cont1_nombre;
                                $Categoria->cont1_telefono = $results[$i][$j]->cont1_cliente;
                                $Categoria->save();
                             }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar una categoria\n";
                             }
                           
                          
                          }
                              
                         }elseif ($nombreTabla == 'Productos') {
                             $pro = Producto::where('nombre', $results[$i][$j]->nombre)->get();//Si ya existe el producto
                             $categoria = Categoria::where('nombre', $results[$i][$j]->categoria)->get();//Buscar la categoria que esta asociada a ese producto
                             $area = area::where('nombre', $results[$i][$j]->area)->get();//Buscar el area que esta asociada a ese producto
                        if (count($categoria)!=0&&count($area)!=0) {
                            if (count($pro)==0) {
                            try{
                                $Producto = new Producto();
                                $Producto->nombre = $results[$i][$j]->nombre;
                                $Producto->descripcion = $categoria[0]->idCategoria;
                                $Producto->categoria= $area[0]->idArea;
                                $Producto->area = $results[$i][$j]->area;
                                $Producto->save();
                              }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un producto\n";
                             }
                            }else{
                            try{
                                $Producto = Producto::find($pro[0]->idProducto);
                                $Producto->nombre = $results[$i][$j]->nombre;
                                $Producto->descripcion = $results[$i][$j]->descripcion;
                                $Producto->categoria= $categoria[0]->idCategoria;
                                $Producto->area = $area[0]->idArea;
                                $Producto->save();
                               
                             }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar productos\n";
                             }

                            }
                        }else{
                            $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar o insertar un  productos\n";
                            if(count($categoria)!=0){
                                $this->respuesta.="(Nombre de categoria invalido)\n";
                            }
                            if(count($area)!=0){
                                 $this->respuesta.="(Nombre de area invalido)\n";
                            }
                            $this->respuesta.="\n";
                        }
                             
                         }elseif ($nombreTabla == 'Areas') {
                               $are = area::where('nombre', $results[$i][$j]->nombre)->get();
                                if((count($are)==0)) {
                                try{
                                    $area = new area();
                                    $area->nombre = $results[$i][$j]->nombre;
                                    $area->tipo = $results[$i][$j]->tipo;
                                    $area->estado= $results[$i][$j]->estado;
                                    $area->save();
                                }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un area\n";
                               }
                                }else{
                                try{
                                    $area = area::find($are[0]->idArea);
                                    $area->nombre = $results[$i][$j]->nombre;
                                    $area->tipo = $results[$i][$j]->tipo;
                                    $area->estado= $results[$i][$j]->estado;
                                    $area->save();
                                 }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar un area\n";
                                  }
                                }
                           // $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar o insertar un  productos\n";
                         }elseif ($nombreTabla == 'Descuento') {
                              $des = Descuento::where('nombre', $results[$i][$j]->nombre)->get();
                             if((($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0))&&((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')))
                            {
                             if (count($des)==0) {
                          
                                try{
                                    $descuento = new Descuento();
                                    $descuento->nombre = $results[$i][$j]->nombre;
                                    $descuento->tipo = $results[$i][$j]->tipo;
                                    $descuento->valor= $results[$i][$j]->valor;
                                    $descuento->estado = $results[$i][$j]->estado;
                                    $descuento->save();
                                }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar una descuento\n";
                                }
                                }else{
                                try{
                                    $descuento = Descuento::find($des[0]->idDescuento);
                                    $descuento->nombre = $results[$i][$j]->nombre;
                                    $descuento->tipo = $results[$i][$j]->tipo;
                                    $descuento->valor= $results[$i][$j]->valor;
                                    $descuento->estado = $results[$i][$j]->estado;
                                    $descuento->save();
                                   
                                }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un descuento \n";
                               }
                            }
                            }else{
                            if(($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0)){
                                $this->respuesta.="(El valor debe estar contenido entre 0 y 100)\n";
                            }
                           if((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')){
                                 $this->respuesta.="(Los tipos validos son importe y porcentaje)\n";
                            }
                            $this->respuesta.="\n";
                         }

                         }elseif ($nombreTabla == 'Impuesto') {
                                 $imp = impuesto::where('nombre', $results[$i][$j]->nombre)->get();
                                  if((($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0))&&((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')))
                            {
                                if (count($imp)==0) {
                                try{
                                    $impuesto = new impuesto();
                                    $impuesto->nombre = $results[$i][$j]->nombre;
                                    $impuesto->tipo = $results[$i][$j]->tipo;
                                    $impuesto->valor= $results[$i][$j]->valor;
                                    $impuesto->estado = $results[$i][$j]->estado;
                                    $impuesto->save();
                               
                                }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un impuesto\n";
                                }
                                }else{
                                  try{
                                    $impuesto = impuesto::find($imp[0]->idImpuesto);
                                    $impuesto->nombre = $results[$i][$j]->nombre;
                                    $impuesto->tipo = $results[$i][$j]->tipo;
                                    $impuesto->valor= $results[$i][$j]->valor;
                                    $impuesto->estado = $results[$i][$j]->estado;
                                    $impuesto->save();
                                  
                                }catch(\Illuminate\Database\QueryException $ex){
                                 $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar un impuesto\n";
                                }
                                }
                            }else{
                            if(($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0)){
                                $this->respuesta.="(El valor debe estar contenido entre 0 y 100)\n";
                            }
                           if((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')){
                                 $this->respuesta.="(Los tipos validos son importe y porcentaje)\n";
                            }
                            $this->respuesta.="\n";
                         }
                         }elseif ($nombreTabla == 'Modificador') {
                              $mod = modificador::where('nombre', $results[$i][$j]->nombre)->get();
                                if (count($mod)==0) {
                                 try{
                                    $modificador = new modificador();
                                    $modificador->nombre = $results[$i][$j]->nombre;
                                    $modificador->compuesto = $results[$i][$j]->compuesto;
                                    $modificador->tipo= $results[$i][$j]->tipo;
                                    $modificador->precio = $results[$i][$j]->precio;
                                    $modificador->estado = $results[$i][$j]->estado;
                                    $modificador->save();
                                    
                                  }catch(\Illuminate\Database\QueryException $ex){
                                     $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un modificador\n";
                                  }
                                }else{
                                  try{
                                    $modificador = modificador::find($mod[0]->id);
                                    $modificador->nombre = $results[$i][$j]->nombre;
                                    $modificador->compuesto = $results[$i][$j]->compuesto;
                                    $modificador->tipo= $results[$i][$j]->tipo;
                                    $modificador->precio = $results[$i][$j]->precio;
                                    $modificador->estado = $results[$i][$j]->estado;                                  
                                    $modificador->save();
                                   
                                 }catch(\Illuminate\Database\QueryException $ex){
                                    $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar un modificador\n";
                                }
                                }                      
                       }elseif ($nombreTabla == 'PrecioProductos!!!!') {
                         $producto = Producto::where('nombre', $results[$i][$j]->producto)->get();
                           if (count($producto!=0)) {
                            $precioproducto = PrecioProducto::where('producto', $producto[0]->idProducto)->where('presentacion',  $producto[0]->presentacion)->get();
                                if (count($precioproducto)) {
                                   try{
                                    $precioprod = new PrecioProducto();
                                    $precioprod->costo = $results[$i][$j]->costo;
                                    $precioprod->producto = $producto->idProducto;
                                    $precioprod->presentacion = $results[$i][$j]->presentacion;
                                    $precioprod->estado = $results[$i][$j]->estado;
                                    $precioprod->save();
                                 
                                 }catch(\Illuminate\Database\QueryException $ex){
                                       $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar un precio al producto\n";
                                   }
                                }else{
                                 try{
                                    $precioprod = PrecioProducto::find($precioproducto->id);
                                    $precioprod->costo = $results[$i][$j]->costo;
                                    $precioprod->producto = $producto->idProducto;
                                 //   $precioprod->presentacion = $producto[0]->presentacion;
                                    $precioprod->estado = $results[$i][$j]->estado;
                                    $precioprod->save();
                                    }catch(\Illuminate\Database\QueryException $ex){
                                       $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar un precio al producto\n";
                                   }
                                }
                              
                              
                          }else{
                             $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error, el nombre del producto no existe \n";
                          }
                       }elseif ($nombreTabla == 'Propina') {
                            $propina = propina::where('nombre', $results[$i][$j]->nombre)->get();
                             if((($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0))&&((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')))
                            {
                             if (count($propina)==0) {
                                 try{
                                    $propina = new propina();
                                    $propina->nombre = $results[$i][$j]->nombre;
                                    $propina->tipo = $results[$i][$j]->tipo;
                                    $propina->valor= $results[$i][$j]->valor;
                                    $propina->estado = $results[$i][$j]->estado;
                                    $propina->save();
                                    }catch(\Illuminate\Database\QueryException $ex){
                                       $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de insertar una propina\n";
                                   }
                                }else{
                                 try{
                                    $pro = propina::find($propina[0]->idPropina);
                                    $pro->nombre = $results[$i][$j]->nombre;
                                    $pro->tipo = $results[$i][$j]->tipo;
                                    $pro->valor= $results[$i][$j]->valor;
                                    $pro->estado = $results[$i][$j]->estado;  
                                    $pro->save();
                                   }catch(\Illuminate\Database\QueryException $ex){
                                       $this->respuesta.="En la hoja $nombreTabla en la linea".($j+2)." ha ocurrido un error al tratar de actualizar un propina\n";
                                   }
                                }
                            }else{
                            if(($results[$i][$j]->valor<100)||($results[$i][$j]->valor>0)){
                                $this->respuesta.="(El valor debe estar contenido entre 0 y 100)\n";
                            }
                           if((strtolower($results[$i][$j]->tipo)!='importe')||(strtolower($results[$i][$j]->tipo)!='porcentaje')){
                                 $this->respuesta.="(Los tipos validos son importe y porcentaje)\n";
                            }
                            $this->respuesta.="\n";
                         }
                
                       }
           
                     }
              
                 }
       
             }); 
            return response()->json(['error' => false,'mensaje' =>$this->respuesta]);  
    }
   
    
  
}

}
