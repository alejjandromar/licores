<?php 
namespace App\Http\Controllers;

use App\Modelos\impuesto;
use App\Modelos\modificador;
use App\Modelos\opcionesmodificador;
use Illuminate\Http\Request;
use DB;

function buscarOpciones($cadena,$ids) {
    $opciones=array();$opcion='';$id='';$idOpciones=array();
    if($ids!=null){
        for ($j=0;$j<strlen($ids);$j++) {
             if($ids[$j]!=','){
                 $id.=$ids[$j];
             }else{
                 array_push($idOpciones,$id);
                 $id='';
             }
             if(($j+1)==strlen($ids)){
                 array_push($idOpciones,$id);
            }
        }
    }
    $j=0;
     for ($i=0;$i<strlen($cadena);$i++) {
         if($cadena[$i]!=','){
             $opcion.=$cadena[$i];
         }else{
             if($ids==null){
                 $OP= new opcionesmodificador();
             }else{
                 if($idOpciones[$j]!=-1){
                      $OP = opcionesmodificador::find($idOpciones[$j]);
                 }else{
                     $OP= new opcionesmodificador();
                 }
                 $j++;
             }
             $OP->opcion=$opcion;
             array_push($opciones,$OP);
             $opcion='';
         }
         if(($i+1)==strlen($cadena)){
             if($ids==null){
                 $OP= new opcionesmodificador();
             }else{
                 if($idOpciones[$j]!=-1){
                      $OP = opcionesmodificador::find($idOpciones[$j]);
                 }else{
                     $OP= new opcionesmodificador();
                 }
             }
             $OP->opcion=$opcion;
                 array_push($opciones,$OP);
                 $opcion='';
            }
     
         }
    return $opciones;
}

//retorna los errores que pueda generar manipular las tablas de este controlador(menos de clave primaria).
function buscarError($ex){
    //se incializa el mensaje
    $mensaje="";
    $codigo=$ex->errorInfo[1];
    //si es una violacion de llave  unica
    if($codigo==1062){
        //restircciones uniques de tabla modificadores
        $resultado = strpos($ex->errorInfo[2], "modificador_unico");
        //si es diferente de false de ya hay un numeros adicional con esos datos..
        if($resultado !== false){
            $mensaje="Ya existe un modificador de ese tipo con ese nombre.";
        }else{
            $resultado = strpos($ex->errorInfo[2], "opcion_unica");
            if($resultado !== false){
                //viole la registrcioon unique pass
                $mensaje="El modificador ya posee una de las opciones introducidas.";
            }
        }
    }else{
        //violacion de llave foranea
        if($codigo==1451){
            $resultado = strpos($ex->errorInfo[2], "fk_opcionesmodificador_modificador");
            if($resultado !== false){
                $mensaje="true";
            }else{
                $resultado = strpos($ex->errorInfo[2], "fk_detalletienemodificador_idModificador");
                if($resultado !== false){
                    $mensaje="No se puede borrar el modificador porque tiene un detalle asociado";
                }else{
                }
            }
            
        }else{
            //si fue un trigguer o cuarquier otro error inesperado.
            $mensaje= $ex->errorInfo[2];
        }
    }
    return $mensaje;
}

class modificadorController extends Controller {

    static public function CRUD(Request $request) {
        return view('layouts.modificadores');
    }

    //Select_all: Devuelve todos los objetos de la tabla modificadores
    public function Select_all(Request $request) {
        try {
        $modificadores = modificador::with('opciones')->get();
        return response()->json(['modificadores' => $modificadores]);
       } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true,'mensaje'=>$ex->errorInfo[2]]);
       }
    }
    
    //Create: Crea y Guarda un objeto de tipo modificador
    public static function Create(Request $request) {
        try {
            //Guadar Registro
            DB::beginTransaction();
            $modificador = new modificador();
            $modificador->nombre = $request->NombreR;
            $modificador->tipo  = $request->TipoR;
            if($modificador->tipo!='modificador'){
                 $modificador->precio  = $request->PrecioR;
            }
            if ($request->NumeroOpciones==0) {
                 $modificador->compuesto  = $request->NumeroOpciones;
                 $modificador->save();
            }else{
                $modificador->compuesto  = 1;
                $modificador->save();
                $opciones=buscarOpciones($request->Opciones,null);
                $modificador->opciones()->saveMany($opciones);
            }
            if ($request->hasFile('imagenR')) {
                if ($request->file('imagenR')->isValid()) {
                    $request->file('imagenR')->move("imagenes/modificadores",'imagen'.$modificador->id.'.'. $request->file('imagenR')->getClientOriginalExtension());
                    $modificador->imagen = "imagenes/modificadores/imagen".$modificador->id.'.'. $request->file('imagenR')->getClientOriginalExtension();
                }
            }else{
                if($modificador->tipo!='modificador'){
                    $modificador->imagen = "imagenes/modificadores/Edefault.png";
                }else{
                    $modificador->imagen = "imagenes/modificadores/Mdefault.jpg";
                }
            }
            $modificador->save();
            DB::commit() ;
            return response()->json(['error' => false,'mensaje'=>'El modificador ha sido registrado exitosamente.','modificador'=>$modificador]);
        } catch (\Illuminate\Database\QueryException $ex) {
             DB::rollBack();
            $mensaje=  buscarError($ex);
            return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
    }

    //Update: Actualiza los datos de un impuesto
    static public function Update(Request $request) {
            //Buscar Registro
            DB::beginTransaction();
            $modificador = modificador::find($request->id);
            if($modificador!=null){
                try {
                    $modificador->nombre = $request->NombreR;
                    $modificador->tipo  = $request->TipoR;
                    if($modificador->tipo!='modificador'){
                         $modificador->precio  = $request->PrecioR;
                    }
                    if ($request->NumeroOpciones==0) {
                         $modificador->compuesto  = $request->NumeroOpciones;
                         $modificador->save();
                         $modificador->opciones()->delete();
                    }else{
                        $modificador->compuesto  = 1;
                        $modificador->save();
                        $opciones=buscarOpciones($request->Opciones,$request->Ids);
                        $modificador->opciones()->saveMany($opciones);
                    }
                    if ($request->hasFile('imagenR')) {
                        if ($request->file('imagenR')->isValid()) {
                            $request->file('imagenR')->move("imagenes/modificadores",'imagen'.$modificador->id.'.'. $request->file('imagenR')->getClientOriginalExtension());
                            $modificador->imagen = "imagenes/modificadores/imagen".$modificador->id.'.'. $request->file('imagenR')->getClientOriginalExtension();
                        }
                    }
                    $modificador->save();
                    DB::commit() ;
                    return response()->json(['error' => false,'mensaje'=>'El modificador ha sido modificado exitosamente.','modificador'=>$modificador]);
                } catch (\Illuminate\Database\QueryException $ex) {
                     DB::rollBack();
                    $mensaje=  buscarError($ex);
                    return response()->json(['error'=>true,'mensaje'=>$mensaje]);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>'No se pudo encontrar el modificador']);
            }
    }
    
    //Update_status: Actualiza el estado de un impuesto
    public static function Update_status(Request $request) {
        try {
            //buscar Registro 
            $modificador = modificador::find($request->id);
            if($modificador!=null){
                $modificador->estado = $request->estado;
                $modificador->save();
                if($request->estado==0){
                        $mensaje="El modificador ha sido desactivado exitosamente.";
                    }else{
                         $mensaje="El modificador ha sido activado exitosamente";
                }
            }else{
                return response()->json(['error' =>true,'mensaje' => 'No se pudo encontrar el modificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje=  buscarError($ex);
            return response()->json(['error'=>true,'mensaje'=>$mensaje]);
        }
        return response()->json(['error' => false,'mensaje'=>$mensaje]);
    }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
             $modificador = modificador::with('productos')->where('id',$request->id)->get();
            if($modificador!=null){
                if(count($modificador[0]->productos)==0){
                    $modificador = modificador::find($request->id);
                    DB::beginTransaction();
                   $modificador->delete();
                    DB::commit() ;
                   return response()->json(['error'=>false,'mensaje' => "El modificador  ha sido Eliminado exitosamente"]);
                }else{
                    return response()->json(['error'=>true,'mensaje' => "No se puede borrar el modificador porque esta asociado a un producto"]);
                }
            }else{
                 return response()->json(['error'=>true,'mensaje'=>'no existe un modificador con ese identificador']);
            }
            
        }catch (\Illuminate\Database\QueryException $ex) {
            $mensaje=  buscarError($ex);
            DB::rollBack();
            if($mensaje=='true'){
                try{
                    DB::beginTransaction();
                    $modificador->opciones()->delete();
                    $modificador->delete();
                    DB::commit() ;
                    return response()->json(['error'=>false,'mensaje'=>'El modificador fue eliminado exitosamente']);
                }catch (\Illuminate\Database $ex) {
                    DB::rollBack();
                    $mensaje=  buscarError($ex);
                     return response()->json(['error'=>true,'mensaje'=>$mensaje]);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$mensaje]);
            }
        }
    }

}
