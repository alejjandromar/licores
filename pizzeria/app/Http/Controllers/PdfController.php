<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\mesa;
use App\Modelos\Producto;
use App\Modelos\Detalle;
use App\Modelos\Precio;
use App\Modelos\ModoPago;
use App\Modelos\Descuento;
use App\Modelos\PagoFactura;
use App\Modelos\impresora;
use App\Modelos\cola_impresion;
use App\Modelos\PrecioProducto;
use App\Modelos\FacturaTieneDescuento;
use Illuminate\Http\Request;
use Cache;
use DB;

class PdfController extends Controller {

    static public function factura($num_orden, $imprimir = 'false', $num_mesa = -1) {
        //dd($imprimir);
        if ($num_mesa == -1) {
            $orden = Orden::with('getcliente', 'getmesero', 'getcajero', 'detalles.getpresentacion', 'detalles.getproducto', 'pagos.getModoPago', 'detalles.modificadores.opciones')->where('idFactura', $num_orden)->get(); //Detalles de la orden
            $orden = $orden [0]; //dd($orden);
            $i = count($orden->detalles);
            $caja = \App\Modelos\caja::find($orden->caja);
            $impresora = impresora::find($caja->impresora);
        } else {
            //traigo la data de variables de cache
            $orden = Cache::get('orden' . $num_mesa, []);
            $detalles = Cache::get('detalles' . $num_mesa, []);
            $valorImpuesto = Cache::get('valoImp' . $num_mesa);
            $valorPropina = Cache::get('valoProp' . $num_mesa);
            $cliente = Cache::get('cliente' . $num_mesa);
            //formateo de datos
            $orden->cliente = $cliente->dni;
            if ($orden->cliente != '00000000A') {
                $orden->getcliente = $cliente;
            }
            //dd($orden,count($detalles[key($detalles)]));
            if ($orden->tipo == 'comer') {
                $orden->mesa = $num_mesa;
            } else {
                $orden->mesa = null;
            }
            $orden->created_at = $fecha = date('Y-m-d h:m:s');
            $orden->impuesto = $valorImpuesto;
            $orden->propina = $valorPropina;
            $i = count(count($detalles[key($detalles)]));
        }

        $x = 350;
        if ($i > 1) {
            $x = 350 + (30 * $i);
        }
        if($imprimir === 'true'){
                $imprimir=true;
        }
        if($imprimir === 'false'){
                $imprimir=false;
        }
       //dd($imprimir);
        if ($imprimir == false) {
            
            $view = \View::make('facturahtml', compact('orden', 'detalles'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            $pdf->setPaper(array(0, 0, 240, $x));
            return $pdf->stream('factura.pdf');
        } else {
            //dd($impresora);
            $imprimir = new cola_impresion();
            $imprimir->impresora = $impresora != null ? $impresora->real_name : "pdf";
            $imprimir->url = null;
            $imprimir->estado = 1;
            $imprimir->save();
            $view = \View::make('facturahtml', compact('imprimir', 'orden', 'detalles'))->render();
            $imprimir->tikect = $view;
            $imprimir->estado = 1;
            $imprimir->save();
            return null;
        }
    }

    static public function reportes_general_pdf() {

        $productos = DB::select('SELECT 
                            id,productos.nombre as nombre,
                            presentacion,
                            costo,count(id)as cantidad,costo*count(id)as total
                            from precioproductos,detallesorden,productos
                            WHERE
                            idProducto=detallesorden.producto and id=detallesorden.idPrecio group by id');
        $hasta = "-";
        $desde = "-";
        $cant = DB::select('SELECT sum(precio)as montototal,sum(cantidad)as cantidadtotal from detallesorden');
        $view = \View::make('reportes_productos_pdf', compact('productos', 'cant', 'desde', 'hasta'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('a4');
        return $pdf->download('reporte.pdf');
    }
	
	static public function comenzar($x = false) {
         $cola = Cache::get('cola', -1);
         if ($cola != -1) {
            $mesas = mesa::all();
            $ordenes = array();
             foreach ($mesas as $key => $mesa) {
                 $orden = Cache::get('orden' . $mesa->idMesa, []);
                 if($orden!=[]){
                    $orden->detalles=Cache::get('detalles' . $mesa->idMesa, []);
                    array_push($ordenes,$orden);
                 }
             }
             $ordenAux = array();
              foreach ($cola as $key => $col) {
                 foreach ($ordenes as $key => $orden) {
                    if ($col == $orden->mesa) {
                         array_push($ordenAux,$orden);
                    }
                 }
             }
           //dd($ordenAux, $cola);
            if ($x) {
               return $ordenAux;
            }
			foreach ($mesas as $key => $mesa) {
				$orden = Cache::get('orden' . $mesa->idMesa, []);
				if($orden!=[]){
					$orden->detalles=Cache::get('detalles' . $mesa->idMesa, []);
					foreach ($orden->detalles as $detalle) {
						if($detalle->cantidad==$detalle->cantidadCocina){
							$detalle->cambio=false;
						}
					}
					Cache::put('detalles' . $mesa->idMesa,$orden->detalles,1500);
				}
			}
            return response()->json(['ordenes' => $ordenAux, 'fecha_actual' => \Carbon\Carbon::now()->toTimeString()]);
         }else{
            if ($x) {
               return [];
            }
            return response()->json(['ordenes' => []]);
         }
	
	}
    static public function cambiar_estado(Request $request){
        $detalles = Cache::get('detalles' . $request->mesa, []);
        //dd($detalles);
        foreach ($detalles as $key => $detalle) {
            if($detalle->producto == $request->producto && $detalle->idPrecio == $request->precio){
                $detalle->estado = 'Terminado';
                $detalle->listo = $detalle->cantidad; 
                
            }
        }
        Cache::put('detalles' . $request->mesa, $detalles, 1500);
        return response()->json(['detalles' => $detalles]);
    }

    static public function terminarOrden(Request $request){
        $cola = Cache::get('cola', []);
        foreach ($cola as $key => $col) {
           if ($col == $request->mesa) {
               unset($cola[$key]);
           }
        }
        Cache::put('cola', $cola, 1500);
		$orden = Cache::get('orden' . $request->mesa, []);
        if ($orden != []) {
            $orden->estado = 7;
            Cache::put('orden' . $request->mesa, $orden, 1500);
        }
		$detalles = Cache::get('detalles' . $request->mesa, []);
        foreach ($detalles as $key => $detalle) {
            $detalle->estado = 'Terminado';
            $detalle->listo = $detalle->cantidad;
        }
        Cache::put('detalles' . $request->mesa, $detalles, 1500);
        $ordenes = PdfController::comenzar(true);
        //dd($ordenes);
        return response()->json(['ordenes' => $ordenes]);
    }

    static public function reportes_filtro_pdf($fecha) {

        $hasta = substr($fecha, 10);
        $desde = substr($fecha, 0, 10);

        $productos = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                from precioproductos,detallesorden,productos,facturas 
                where 
                facturas.idFactura=factura and 
                facturas.created_at BETWEEN "' . $desde . ' 00:00:00" AND "' . $hasta . ' 00:00:00"' . 'and idProducto=detallesorden.producto and id=detallesorden.idPrecio  group by id');

        $cant = DB::select('SELECT sum(precio)as montototal,sum(cantidad)as cantidadtotal from detallesorden,facturas where facturas.idFactura=factura and facturas.created_at BETWEEN "' . $desde . ' 00:00:00" AND "' . $hasta . ' 00:00:00"');

        $view = \View::make('reportes_productos_pdf', compact('productos', 'cant', 'desde', 'hasta'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('a4');
        return $pdf->download('reporte.pdf');
    }
        static public function verificarEstado(Request $request) {
             $detalles = Cache::get('detalles' . $request->mesa, []);
            $index = $request->idProducto . "_" . $request->idPrecio;
             if($detalles[$index]->estado =='Pendiente'){
                        $resultado = false;
                     }else{
                        $resultado = true;
                     }
              
          
             return response()->json(['resultado' => $resultado]);
       
    }


    static public function preview($num_mesa, $orden = null) {
        if ($orden == null) {
            $orden = Cache::get('orden' . $num_mesa, []);
            $detalles = Cache::get('detalles' . $num_mesa, []);
            $orden->impuesto = Cache::get('valoImp' . $num_mesa);
            $orden->propina = Cache::get('valoProp' . $num_mesa);
            $orden->descuento = 0;
        } else {
            $detalles = $orden->detalles();
        }

        $i = count($orden);
        if ($i == 1) {
            $x = 240;
        }
        if ($i > 1) {
            $x = 240 + (30 * $i);
        }

        if ($i > 0) {
            if (isset($request->imp)) {
                
            } else {
                $impresora = impresora::all()[0];
                if ($impresora != null) {
                    $imprimir = new cola_impresion();
                    $imprimir->impresora = $impresora != null ? $impresora->real_name : "pdf";
                    $imprimir->url = null;
                    $imprimir->estado = 1;
                    $imprimir->save();
                    $view = \View::make('facturahtml', compact('orden', 'detalles', 'imprimir'))->render();
                    $imprimir->tikect = $view;
                    $imprimir->estado = 1;
                    $imprimir->save();
                } else {
                    $view = \View::make('facturahtml', compact('orden', 'detalles', 'imprimir'))->render();
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->setPaper(array(0, 0, 240, $x));
                    $pdf->loadHTML($view);
                    $pdf->output();
                    return $pdf->download('fact_print.pdf');
                }
            }
        } else {
            return redirect('menu?mesa=' . $num_mesa);
        }
    }

}
