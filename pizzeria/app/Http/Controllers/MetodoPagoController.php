<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\ModoPago;
use Illuminate\Http\Request;
use DB;

class MetodoPagoController extends Controller {

    static public function CRUD(Request $request) {
        return view('layouts.MetodoPago');
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla modoPago
    public function Select_all(Request $request) {
        $modospago = ModoPago::all();
        return response()->json(['modopago' => $modospago]);
    }

    //Select_one: Selecciona un solo registro
    public function Existe(Request $request) {
        $modospago = ModoPago::find($request->id);
        return response()->json(['cliente' => $modospago]);
    }

    //Create: Crea y Guarda un objeto de tipo modoPago
    public function Create(Request $request) {
       // return response()->json(['Error' => 'La imagen no ha podido ser cargada']);
        //Comprobando que el metodo de pago ya existe
         $m = ModoPago::where('tipoPago', $request->nombre)->get();
        if (isset($m[0])) {
            return response()->json(['error'=>true,'mensaje'=> 'Ya existe un metodo de pago con ese nombre']);
        }
        //Comprobando que el metodo de pago no fue borrado
        $m = ModoPago::onlyTrashed()->where('tipoPago', $request->nombre)->get();
        if (isset($m[0])) {
            DB::beginTransaction();
            $m[0]->restore();
            $request->id = $m[0]->idPago;
            $request->estado=1;
             $resp= ImpuestoController::Update($request);
            if($resp->st){
                DB::commit();
            }else{
                DB::rollBack();
            }
            return $resp;
        }
        //Verificando que el el metod de pago no existe
        //
        //return response()->json(['Error' => 'La imagen no ha podido ser cargada']);
       
        try {
            if (!$request->hasFile('file') || !$request->file('file')->isValid()) {
                return response()->json(['error'=>true,'mensaje'=>'Debe colocar una imagen']);
            }
        } catch (Exception $ex) {
            return response()->json(['error'=>true,'mensaje'=>'La imagen no ha podido ser cargada']);
        }
        try {//Guadar Registro
            
            $modoPago = new ModoPago();
            $modoPago->tipoPago = $request->nombre;
            $modoPago->detalles = $request->detalle;
            $modoPago->imagen = "default.jpg";
            
            $modoPago->save();
            $request->file('file')->move("imagenes/metodos_pago", $modoPago->tipoPago . '.' . $request->file('file')->getClientOriginalExtension());
            $modoPago->imagen = 'imagenes/metodos_pago/'.$modoPago->tipoPago . '.' . $request->file('file')->getClientOriginalExtension();
            $modoPago->save();
            return response()->json(['error'=>false,'modopago' => $modoPago]);
        }catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Metodo de pago con ese identificador en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un Metodo de pago con ese nombre en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
        } catch (Exception $ex) {
            return response()->json(['error'=>true,'mensaje'=> 'El modo de pago no ha podido ser insertado']);
        }
    }

    //Update: Actualiza los datos de un modo de pago
    static public function Update(Request $request) {
        //return response()->json(['Error' => 'La imagen no ha podido ser cargada']);
        try {//Guadar Registro 
            $modoPago = ModoPago::find($request->id);
            $modoPago->tipoPago = $request->nombre;
            $modoPago->detalles = $request->detalle;
            $modoPago->save();
            
            if ($request->hasFile('file') && $request->file('file')->isValid()) {
                $request->file('file')->move("imagenes/metodos_pago", $modoPago->tipoPago . '.' . $request->file('file')->getClientOriginalExtension());
                $modoPago->imagen = "imagenes/metodos_pago/".$modoPago->tipoPago . '.' . $request->file('file')->getClientOriginalExtension();
                $modoPago->save();
            }
        }catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado != false){
                     $resp = response()->json(['error'=>true,'mensaje'=>'Ya existe un metodo de pago con ese identificador en la base de datos.']);
                     $resp->st=0;
                    return $resp;
                    
                }else{
                    $resp = response()->json(['error'=>true,'mensaje'=>'Ya existe un metodo de pago con ese nombre en la base de datos.']);
                    $resp->st=0;
                    return $resp;
                    
                }
            }else{
                $resp= response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
                $resp->st=0;
                    return $resp;
            }
        } catch (Exception $ex) {
             $resp= response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
             $resp->st=0;
             return $resp;
            
        }
        $resp= response()->json(['error'=>false,'modopago' => $modoPago]);
        $resp->st=1;
        return $resp;
    }

    //Update_status: Actualiza el estado de un metodo de pago
    public function Update_status(Request $request) {
        try {//Guadar Registro 
            $modoPago = ModoPago::find($request->id);
            //dd($request->id);
            $modoPago->estado = $request->estado;
            $modoPago->save();
        } catch (Exception $ex) {
            return response()->json(['Error' => 'El modo de pago no ha podido ser actualizado']);
        }
        return response()->json(['modopago' => $modoPago]);
    }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
            $modospago = ModoPago::find($request->id);
            $modospago->delete();
            return response()->json(['error'=>false,'mensaje'=> "Modo de Pago Eliminado"]);
        } catch (\Illuminate\Database\QueryException $ex) {
           return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
        } catch (Exception $ex) {
            return response()->json(['error'=>true,'mensaje'=>"Modo de Pago no se ha podido ser Eliminado"]);
        }
    }
    

}
