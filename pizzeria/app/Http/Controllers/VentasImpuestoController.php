<?php
namespace App\Http\Controllers;
use View;

use App\Http\Controllers\Controller;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\FacturaTieneImpuestos;
use App\Modelos\impuesto;
use Illuminate\Http\Request;
use DB;
use Cache;
use Auth;

class impuestosS{
    public    $impuesto="";             //nombre del impuesto
    public    $cuota=0;                 //Cuota de un impuesto en particular
    public    $total=0;                 //total es la suma de la base con la cuato
    public    $estado;
    public    $numFact=0;
    public    $base=0;
}
function calcularImpuestos($impuestos,$tipo,$año,$mes){
   // dd(count($impuestos[1]->ordenes));
    $j=0;
    $resultados=array();
    foreach ($impuestos as $row) {
        $impuestoS=new impuestosS();
        $impuestoS->impuesto=$row['nombre']."(".$row['valor'];
        if(strtolower($row['tipo'])=="porcentaje"){
            $impuestoS->impuesto.="%)";
        }else{
            $impuestoS->impuesto.=" S/.)";
        }
        $impuestoS->estado=$row['estado'];
        $impuestoS->numFact=count($row->ordenes);
        //valido si ese impuesto tiene ordenes
        if(count($row->ordenes)>0){
            $cuota=0.0;$base=0.0;
             foreach ($row->ordenes as $orden) {
                 //dd($orden->pivot->valorImpuesto);
                   $cuota+=$orden->pivot->valorImpuesto;
                   $base+=$orden->subTotal;
             }
             $impuestoS->cuota=$cuota;
             $impuestoS->total=($base+$impuestoS->cuota);
             $impuestoS->base=$base;
        }else{
            $impuestoS->cuota=0.0;
            $impuestoS->total=0.0;
            $impuestoS->base=0;
        }
        array_push($resultados,$impuestoS);
    }
    return $resultados;
}
        

   
class VentasImpuestoController extends Controller {
    public function index()
    {
        return view('layouts_reportes.ReporteVentasImpuestos');
    }

    //Funciones Ajax
    //funcion ajax que devuelve los datos a mostrar en el reporte.
    public function VentasPorImpuestos(Request $request) {
        $resultados=array();
        if($request->tipo==1){
            Cache::put('request'.Auth::user()->dni,array('tipo'=>$request->tipo), 10);
            $impuestos = impuesto::with('ordenes')->orderBy('tipo','asc')->get(array('idImpuesto','valor','nombre','tipo','estado'));
            $base=Orden::where('estado',1)->sum('subtotal');
            $cantF=Orden::where('estado',1)->count('subtotal');
            $totalCuota=FacturaTieneImpuestos::sum('valorImpuesto');
            $total=($base+$totalCuota);
            $resultados= calcularImpuestos($impuestos,1,null,null);
         }else{
            if($request->tipo==2){
                Cache::put('request'.Auth::user()->dni,array('tipo'=>$request->tipo,'año'=>$request->año), 10);
                $impuestos = impuesto::with('ordenes')->orderBy('tipo','asc')->get(array('idImpuesto','valor','nombre','tipo','estado'));
                $base=Orden::whereRaw('estado=1 and YEAR(created_at) ='.$request->año)->sum('subtotal');
                $cantF=Orden::whereRaw("estado=1 and YEAR(created_at) = ".$request->año)->count('subtotal');
                $año=$request->año;
                $totalCuota=FacturaTieneImpuestos::whereIn('factura', function($query) use ($año){ 
                                $query->select(DB::raw('idFactura'))->from('facturas')->whereRaw("YEAR(created_at) =".$año);})->sum('valorImpuesto');
                $total=($base+$totalCuota);
                $resultados= calcularImpuestos($impuestos,2,$año,null);
            }else{
                if($request->tipo==3){
                    Cache::put('request'.Auth::user()->dni,array('tipo'=>$request->tipo,'año'=>$request->año,'mes'=>$request->mes), 10);
                    $impuestos = impuesto::with('ordenes')->orderBy('tipo','asc')->get(array('idImpuesto','valor','nombre','tipo','estado'));
                    $cantF=Orden::whereRaw("estado=1 and Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->count('subtotal'); 
                    $base=Orden::whereRaw("estado=1 and Month(created_at) = ".$request->mes." and YEAR(created_at) = ".$request->año)->sum('subtotal');
                    $año=$request->año;$mes=$request->mes;
                    $totalCuota=FacturaTieneImpuestos::whereIn('factura', function($query) use ($año,$mes){ 
                                    $query->select(DB::raw('idFactura'))->from('facturas')->whereRaw("Month(created_at) = ".$mes." and YEAR(created_at) = ".$año);})->sum('valorImpuesto');
                    $total=($base+$totalCuota);
                    $resultados= calcularImpuestos($impuestos, $base,3,$año,$mes);
                }else{
                     if($request->tipo==4){
                         Cache::put('request'.Auth::user()->dni,array('tipo'=>$request->tipo,'fecha'=>$request->fecha), 10);
                        $impuestos = impuesto::with('ordenes')->orderBy('tipo','asc')->get(array('idImpuesto','valor','nombre','tipo','estado'));
                        $base=Orden::whereRaw("estado=1 and DATE(created_at) = '".$request->fecha."'")->sum('subtotal');
                        $cantF=Orden::whereRaw("estado=1 and DATE(created_at) = '".$request->fecha."'")->count('subtotal');
                        $año=$request->fecha;
                        $totalCuota=FacturaTieneImpuestos::whereIn('factura', function($query) use ($año){ 
                                    $query->select(DB::raw('idFactura'))->from('facturas')->whereRaw("DATE(created_at) = '".$año."'");})->sum('valorImpuesto');
                        $total=($base+$totalCuota);
                        $resultados= calcularImpuestos($impuestos, $base,4,$año,null);
                     }else{
                         //tipo==5
                         Cache::put('request'.Auth::user()->dni,array('tipo'=>$request->tipo,'fecha1'=>$request->fecha1,'fecha2'=>$request->fecha2), 10);
                         $impuestos = impuesto::with('ordenes')->orderBy('tipo','asc')->get(array('idImpuesto','valor','nombre','tipo','estado'));
                         $base=Orden::whereRaw("estado=1 and DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->sum('subtotal'); 
                         $cantF=Orden::whereRaw("estado=1 and DATE(created_at) between '".$request->fecha1."' and '".$request->fecha2."'")->count('subtotal'); 
                         $año=$request->fecha1;$mes=$request->fecha2;
                         $totalCuota=FacturaTieneImpuestos::whereIn('factura', function($query) use ($año,$mes){ 
                                    $query->select(DB::raw('idFactura'))->from('facturas')->whereRaw("DATE(created_at) between '".$año."' and '".$mes."'");})->sum('valorImpuesto');
                         $total=($base+$totalCuota);
                         $resultados= calcularImpuestos($impuestos, $base,5,$año,$mes);
                     }
                }
            }
         }
         Cache::forget('request'.Auth::user()->dni);
        return response()->json(['base' => $base,'impuestos'=>$resultados,'cantF'=>$cantF,'TotalC'=>$totalCuota,'total'=>$total]);
    }

}
