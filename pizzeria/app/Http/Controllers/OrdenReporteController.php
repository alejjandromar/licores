<?php
namespace App\Http\Controllers;
use View;
use App\Http\Controllers;
use Illuminate\Database;
use App\Modelos\Orden;
use App\Modelos\Persona;
use App\Modelos\impuesto;
use App\Modelos\almacen;
use Illuminate\Http\Request;
use DB;
use Cache;
use Auth;
class fecha{
        public    $base;
        public    $fecha;
        public    $cantF;
        
}

   
class OrdenReporteController extends Controller {
    public function inicio()
    {
        return view('layouts_reportes.ReporteOrdenCanceladas');
    }

    //Funciones Ajax
    //funcion ajax que devuelve los datos a mostrar en el reporte.
    public function ordenesCanceladas(Request $request) {
        $resultados=Orden::ordenesCanceladas($request);
        return response()->json(['resultados' => $resultados]);
    }
    
    //Funciones Ajax
    //funcion ajax que devuelve los datos a mostrar en el reporte.
    public function ordenes_all(Request $request) {
        $resultados=Orden::with('getcliente')->simplePaginate();
       // dd($resultados);
        return response()->json(['orden' => $resultados->toJson()]);
    }
    
    //Funciones Ajax
    //funcion ajax que devuelve los datos a mostrar en el reporte.
    public function get_one(Request $request) {
        $resultados=Orden::where('idFactura',$request->id )->with('getcliente', 'detalles.getproducto', 'detalles.getpresentacion', 'cupones','direccion','getoperador','getmotorizado','getalmacen')->first();
		$almacenes=almacen::where('estado',1)->get();
        return response()->json(['orden' => $resultados,'almacenes'=>$almacenes]);
    }

}
