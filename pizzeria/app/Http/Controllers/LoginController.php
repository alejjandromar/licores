<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Modelos\cierrecaja;
use App\Modelos\Cliente;
use Auth;
use View;
use Session;
use Cache;
use DB;
use File;
use Schema;
use App\User;
use Validator;

class LoginController extends Controller {

    static public function index() {
        if (Auth::user()) {
            $user = Auth::user();
            if ($user->tipo != 2) {
				Auth::logout();
                return redirect()->route('login')->with('error', 'No esta autorizado para ingresar en esta area');;;
            } else {
				return view('layouts.menu');
            }
        } else {
            return view('layouts.login');
        }
    }

    public function verificar(Request $request) {
        //dd(['email' =>$request->email, 'password' => $request->pass, 'estado' => 1]);
        if (Auth::attempt(['email' =>$request->email, 'password' => $request->pass, 'estado' => 1], true)) {
				$user = Auth::user();
                if ($user->tipo != 2) {
					Auth::logout();
                    return redirect()->route('login')->with('error', 'No esta autorizado para ingresar en esta area');
                } else {
                   return view('layouts.menu');
                }
           
        } else {
            return redirect()->route('login')->with('error', 'Usuario o Contraseña Invalida');
        }
    }
    
     public function login_empleado(Request $request) {
         $datos= json_decode($request->user);
         
        $tolongins= ['email' =>$datos->email, 'password' => $datos->pass, 'estado' => 1];
        if (Auth::attempt($tolongins, true)) {
		$user = Auth::user();
                return response()->json(['error' => false, 'user' => $user]);
        } else {
           return response()->json(['error' => true, 'mensaje' => 'Usuario o Contraseña Invalida']);
        }
    }
	
	public function verificar_app(Request $request) {
        if (Auth::attempt(['ctr' => openssl_digest($request->pass, "sha512"), 'password' => $request->pass, 'estado' => 1], true)) {
			 $caja =-1;
			if (Auth::user()->tipo == "Admin" || Auth::user()->tipo=="Supervisor" || Auth::user()->tipo== "Cajero"){
				$fecha=date('Y-m-d');
				$caja=cierrecaja::whereRaw("DATE(inicio) = '".$fecha."' AND cajero='".Auth::user()->dni."' AND final='0000-00-00 00:00:00'")->get();
				if(count($caja)!=0){
					 $caja = $caja[0]->numero;
				 }else{
					 $caja =-1;
				 }
				Cache::put('caja'.Auth::user()->dni, $caja,1500);
			}
			$user = Auth::user();
			return response()->json(['login' => true, 'usuario' => $user,'caja'=>$caja]);
        } else {
			return response()->json(['login' => false, 'mensaje' => 'Usuario o Contraseña Invalida']);
        }
    }

    public function logout(Request $request) {
       // Cache::forget('caja' . Auth::user()->dni);
        Auth::logout();
        return redirect()->route('login');
    }
	
	//login app de licores
	public function applogin(Request $request) {
        if ($request->tipo == 1) {
            //FACEBOOK WAY
            $user_data = json_decode($request->user);
            $user = Cliente::where('faceid', $user_data->id)->get();
            if ($user->count() == 1) {
                //EXISTIA ESE USUARIO
                $user = $user[0];
                $user->faceimg = $user_data->picture->data->url;
            } else {
                if (!isset($user_data->email)){
					return response()->json(['error' => true, 'mensaje' => "Este usuario no posee correo electronico\n Por favor use cuenta con correo"]);
				}
                $user = Cliente::where('correo', $user_data->email)->get();
                if ($user->count() == 0) {
                    
                    //CASE A;
                    $user = new Cliente();
                    $user->faceid = $user_data->id;
                    $user->name = $user_data->name;
                    $user->correo = $user_data->email;
                    $user->social = $request->tipo;
                    $user->faceimg = $user_data->picture->data->url;
                    $user->img = $user_data->picture->data->url;
					$var= rand(1,10000);
					$user->IdCupon = $var.substr($user->correo, 0,strripos($user->correo,'@'));
                } else {
                    //CASE D
                    $user = $user[0];
                    $user->faceid = $user_data->id;
                    $user->faceimg = $user_data->picture->data->url;
                }
            }
			/*if ($user->logueado == 1) {
                return response()->json(['error' => true, 'mensaje' => 'Ud ya tiene una sesion iniciada en otro dispositivo, por favor cierrela y vuelva a intentar']);
            }*/
            $user->active = 1;
            $user->save();
           
            return response()->json(['error' => false, 'user' => $user]);
        } else {
            //GOOGLE WAY
            $user_data = json_decode($request->user);
            $user = Cliente::where('googleid', $user_data->userId)->get();
            // dd($user_data);
            if ($user->count() == 1) {
                //EXISTIA ESE USUARIO
                $user = $user[0];
                $user->googleimg = $user_data->imageUrl;
            } else {
                $user = Cliente::where('correo', $user_data->email)->get();
                if ($user->count() == 0) {
                    //CASE B;
                    $user = new Cliente();
                    $user->googleid = $user_data->userId;
                    $user->name = $user_data->displayName;
                    $user->correo = $user_data->email;
                    $user->social = $request->tipo;
                    $user->googleimg = $user_data->imageUrl;
                    $user->img = $user->googleimg;
					$var= rand(1,10000);
					$user->IdCupon = $var.substr($user->correo, 0,strripos($user->correo,'@'));
                } else {
                    //CASE D
                    $user = $user[0];
                    $user->googleid = $user_data->userId;
                    $user->googleimg = $user_data->imageUrl;
                }
            }
			/*if ($user->logueado == 1) {
                return response()->json(['error' => true, 'mensaje' => 'Ud ya tiene una sesion iniciada en otro dispositivo, por favor cierrela y vuelva a intentar']);
            }*/
            $user->active = 1;
            $user->save();
            return response()->json(['error' => false, 'user' => $user]);
        }
    }
	
	//funcion para registrar el push token del usuario
	public function push(Request $request) {
        if (isset($request->push))
            Cliente::where('id', $request->iduser)->update(['push_token' => $request->push]);
        return response()->json(['push' => $request->push]);
    }
	
	//funcion para desconectarse de la app
	public function applogout(Request $request) {
        $user = Cliente::find($request->idUser);
        $user->logueado = 0;
        $user->push_token = null;
        $user->save();
        return response()->json(['error' => false]);
    }

}
