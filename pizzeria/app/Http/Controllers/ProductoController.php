<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modelos\Producto;
use App\Modelos\Categoria;
use App\Modelos\area;
use App\Modelos\modificador;
use App\Modelos\PrecioProducto;
use DB;
use File;
use View;

//retorna los errores que pueda generar manipular las tablas de este controlador(menos de clave primaria).
function buscarError($ex) {
    //se incializa el mensaje
    $mensaje = "";
    $codigo = $ex->errorInfo[1];
    //si es una violacion de llave  unica
    if ($codigo == 1062) {
        //restircciones uniques de tabla productos
        $resultado = strpos($ex->errorInfo[2], "productos_nombre_unique");
        //si es diferente de false ya hay un producto con ese nombre
        if ($resultado !== false) {
            $mensaje = "Ya existe un producto con ese nombre.";
        } else {
            $resultado = strpos($ex->errorInfo[2], "presentacionUnica");
            //si es diferente de false ya hay un producto con ese nombre
            if ($resultado !== false) {
                $mensaje = "El producto ya dispone de esa presentacion";
            } else {
                
            }
        }
    } else {
        //violacion de llave foranea
        if ($codigo == 1452 || $codigo == 1451) {
            $resultado = strpos($ex->errorInfo[2], "productos_categoria_foreign");
            if ($resultado !== false) {
                $mensaje = "La categoria seleccionada no existe";
            } else {
                $resultado = strpos($ex->errorInfo[2], "productos_area_foreign");
                if ($resultado !== false) {
                    $mensaje = "El area seleccionada no existe";
                } else {
                    $resultado = strpos($ex->errorInfo[2], "detallesorden_producto_fk");
                    if ($resultado !== false) {
                        $mensaje = "No se puede borrar el producto porque esta asociado a una factura";
                    } else {
                        $resultado = strpos($ex->errorInfo[2], "precioproductos_producto_fk");
                        if ($resultado !== false) {
                            $mensaje = "No se puede borrar el producto porque esta asociado a una presentacion";
                        } else {
                            $resultado = strpos($ex->errorInfo[2], "productotienemodificador_producto_fk");
                            if ($resultado !== false) {
                                $mensaje = "No se puede borrar el producto porque esta asociado a un modificador";
                            } else {
                                $resultado = strpos($ex->errorInfo[2], "precioproductos_producto_fk");
                                if ($resultado !== false) {
                                    $mensaje = "No existe un producto con esos datos";
                                } else {
                                    $resultado = strpos($ex->errorInfo[2], "detallesorden_precion_fk");
                                    if ($resultado !== false) {
                                        $mensaje = "Nose puede borrar borrar la presentacion porque esta asociada a un detalle de una factura";
                                    } else {
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            //si fue un trigguer o cuarquier otro error inesperado.
            $mensaje = $ex->errorInfo[2];
        }
    }
    return $mensaje;
}

class ProductoController extends Controller {

    //Funciones Auxiliares de otros controladores
    //Funcion que obtiene el menu del restaurante
    static public function VerMenu(Request $request) {

        $categorias = Categoria::has('obtenerProductos')->where('estado', 1)->orWhereRaw('estado = 1 ')->with('obtenerProductos.ObtenerPrecios', 'obtenerProductos.Descuento')->orderBy('nombre')->get();

        return ['categorias' => $categorias];
    }

    //Funciones Ajax
    //Cambio de categoria Ejemplo!!
    static public function VerM($id) {

        $categoria1 = Categoria::find($id);
        $productos = $categoria1->obtenerProductos()->where('estado', 1)->orderBy('nombre')->get();
        $i = 0;
        $precios = [];
        $productos2 = [];
        foreach ($productos as $row) {
            $i++;
            $preciosp = $row->ObtenerPrecios()->where('estado', 1)->orderBy('costo')->get();
            if (count($preciosp) > 0) {
                $productos2[$i]['idProducto'] = $row['idProducto'];
                $productos2[$i]['nombre'] = $row['nombre'];
                $productos2[$i]['descripcion'] = $row['descripcion'];
                $productos2[$i]['estado'] = $row['estado'];
                $productos2[$i]['categoria'] = $row['categoria'];
                $productos2[$i]['precios'] = $preciosp;
            }
        }

        $datos['productos'] = $productos2;

        return ['datos' => $datos];
    }

    //----------------CRUDS-------------------------------------------
    static public function index() {
        $categorias = Categoria::all();
        $productos = Producto::with('modificadores')->orderBy('categoria')->get();
        $areas = area::where('estado', 1)->where('tipo', 0)->get();
        $modificadores = modificador::where('estado', 1)->get();
        return View::make('layouts.productos')->with('modificadores', $modificadores)->with('categoria', $categorias)->with('area', $areas)->with('productos', $productos);
    }

    static public function all() {
        $productos = Producto::orderBy('categoria')->with(['ObtenerPrecios' => function($q) {
                        $q->orWhere('estado', 0);
                    }])->get();
        $categorias = Categoria::all()->keyBy('idCategoria');


        return response()->json(['producto' => $productos, 'categoria' => $categorias]);
    }

    static public function allpre(Request $request) {

        $presentaciones = DB::table('precioproductos')->where('producto', $request->id)->get();
        $nombres = DB::table('productos')->where('idProducto', $request->id)->get();
        return response()->json(['presentacion' => $presentaciones, 'nombre' => $nombres]);
    }

    static public function InsertarPro(Request $request) {
        $datos = json_decode($request->producto);
        try {
            if (!isset($datos->idProducto)) {
                $Producto = new Producto();
            } else {
                $Producto = Producto::find($datos->idProducto);
                ;
            }
            $Producto->nombre = $datos->nombre;
            $Producto->descripcion = $datos->descripcion;
            $Producto->categoria = $datos->categoria;
            //$Producto->area = $request->area;
            $Producto->save();
            $productos = Producto::orderBy('categoria')->get();

            return response()->json(['error' => false, 'mensaje' => 'El producto fue registrado exitosamente.', 'producto' => $productos]);
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje = buscarError($ex);
            return response()->json(['error' => true, 'mensaje' => $mensaje]);
        }
    }

    /*
      FUNCION QUE CAMBIA EL ESTADO DE UNA PRODUCTO
     */

    public function estado(Request $request) {
        try {
            //busca Registro 
            $producto = Producto::find($request->id);

            if ($producto != null) {
                $producto->estado = $producto->estado == 0 ? 1 : 0;
                $producto->save();
                $productos = Producto::all();
                if ($request->estado == 0) {
                    $mensaje = "El area ha sido desactivada exitosamente.";
                } else {
                    $mensaje = "El area ha sido activada exitosamente";
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false, 'mensaje' => $mensaje, 'producto' => $productos]);
    }

    /*
      FUNCION QUE CAMBIA EL ESTADO DE UNA PRESENTACIONS
     */

    public function estado_pre(Request $request) {
        try {
            //busca Registro 
            $presentacion = PrecioProducto::find($request->id);

            if ($presentacion != null) {
                $presentacion->estado = $presentacion->estado == 0 ? 1 : 0;
                $presentacion->save();
                $presentaciones = PrecioProducto::where('producto', $presentacion->producto)->get();
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false, 'presentaciones' => $presentaciones]);
    }

    /*
      FUNCION QUE AGREGA DECUENTOS A LA PRESENTCION
     */

    public function descuento_pre(Request $request) {
        try {
            //busca Registro 
            $presentacion = PrecioProducto::find($request->id);

            if ($presentacion != null) {
                $presentacion->cantidad_desc = $request->cantidad;
                $presentacion->descuento = $request->valor;
                $presentacion->save();
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false]);
    }

    /*
      FUNCION QUE AUMENTA O DISMINUYE EL STOCK
     */

    public function add_dis(Request $request) {
        try {
            //busca Registro 
            $presentacion = PrecioProducto::find($request->id);

            if ($presentacion != null) {
                if ($request->tipo == "+") {
                    $presentacion->stock += $request->cantidad;
                    $presentacion->stock_r += $request->cantidad;
                } else {
                    if ($presentacion->stock > $request->cantidad) {
                        $presentacion->stock -= $request->cantidad;
                        $presentacion->stock_r -= $request->cantidad;
                    } else {
                        return response()->json(['error' => true, 'mensaje' => 'Cantidad muy alta no se puede descontar']);
                    }
                }
                $presentacion->save();
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con ese identificador']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
        }
        return response()->json(['error' => false]);
    }

    /*
      INSERTA O ACTUALIZA UNA PRESENTACION
     */

    static public function adminPreInsertar(Request $request) {

        try {
            $datos = json_decode($request->presentacion);
            $var = rand(1, 1000);
            if (!isset($datos->id)) {
                $presentacion = new PrecioProducto();
                try {
                    if (!$request->hasFile('file') || !$request->file('file')->isValid()) {
                        return response()->json(['mensaje' => 'Debe colocar una imagen']);
                    }
                } catch (Exception $ex) {
                    return response()->json(['mensaje' => 'La imagen no ha podido ser cargada']);
                }

                $presentacion->stock_r = $datos->stock;
                $presentacion->stock = $datos->stock;
            } else {
                $presentacion = PrecioProducto::FirstorNew(['id' => $datos->id]);
            }
            $presentacion->presentacion = $datos->presentacion;
            $presentacion->costo = $datos->costo;
            $presentacion->producto = $datos->producto;
            $presentacion->save();

            if ($request->hasFile('file') && $request->file('file')->isValid()) {
                $request->file('file')->move("imagenes/categorias", $presentacion->nombre . '_' . $var . '_id-' . $presentacion->id . '.' . $request->file('file')->getClientOriginalExtension());

                $presentacion->imagen = "imagenes/presentaciones/" . $presentacion->nombre . '_' . $var . '_id-' . $presentacion->id . '.' . $request->file('file')->getClientOriginalExtension();
            }
            $presentacion->save();
            $presentaciones = PrecioProducto::where('producto', $presentacion->producto)->get();
            return response()->json(['error' => false, 'mensaje' => 'La categoria ha sido registrada exitosamente.', 'presentaciones' => $presentaciones]);
        } catch (\Illuminate\Database\QueryException $ex) {
            $codigo = $ex->errorInfo[1];
            if ($codigo == 1062) {
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if ($resultado !== false) {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Categoria con ese identificador en la base de datos.']);
                } else {
                    return response()->json(['error' => true, 'mensaje' => 'Ya existe una Categoria con ese nombre en la base de datos.']);
                }
            } else {
                return response()->json(['error' => true, 'mensaje' => $ex->errorInfo[2]]);
            }
        }
    }

    static public function presentacion(Request $request) {


        $presents = DB::table('PrecioProductos')
                        ->where('producto', $request->id)->get();

        $nombres = DB::table('Productos')
                        ->where('idProducto', $request->id)->get();




        return response()->json(['present' => $presents, 'nombre' => $nombres]);
    }

    static public function Delete(Request $request) {
        try {
            $Producto = Producto::find($request->id);
            if ($Producto != null) {
                $Producto->delete();
                $productos = Producto::orderBy('categoria')->get();
                return response()->json(['error' => false, 'mensaje' => "El producto  ha sido Eliminado exitosamente", 'producto' => $productos]);
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con esos datos']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje = buscarError($ex);
            return response()->json(['error' => true, 'mensaje' => $mensaje]);
        }
    }

    static public function DeletePre(Request $request) {
        try {
            $precio = PrecioProducto::find($request->id);
            if ($precio != null) {
                $producto= $precio->producto;
                $precio->delete();
               
                $presentaciones = PrecioProducto::where('producto', $producto)->get();
                return response()->json(['error' => false, 'mensaje' => "El producto  ha sido Eliminado exitosamente", 'presentaciones' => $presentaciones]);
            } else {
                return response()->json(['error' => true, 'mensaje' => 'no existe un producto con esos datos']);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            $mensaje = buscarError($ex);
            return response()->json(['error' => true, 'mensaje' => $mensaje]);
        }
    }

    static public function reporte_index() {

        return View::make('layouts_reportes.articulos');
    }

    static public function reportes_all() {
        ///Consulta sql
        $productos = DB::select('SELECT 
                                  id,productos.nombre as nombre,
                                  presentacion,
                                  costo,count(id)as cantidad,costo*count(id)as total
                                  from precioproductos,detallesorden,productos
                                  WHERE
                                  idProducto=detallesorden.producto and id=detallesorden.idPrecio group by id');

        $cant = DB::select('SELECT sum(precio)as montototal,sum(cantidad)as cantidadtotal from detallesorden');

        return response()->json(['producto' => $productos, 'cant_total' => $cant]);
    }

    static public function reportes_filtro(Request $request) {
        ///Consulta sql        
        $productos = DB::select('SELECT id,nombre,presentacion,costo,count(id)as cantidad,costo*count(id)as total 
                from precioproductos,detallesorden,productos,facturas 
                where 
                facturas.idFactura=factura and 
                facturas.created_at BETWEEN "' . $request->desde . ' 00:00:00" AND "' . $request->hasta . ' 00:00:00"' . 'and idProducto=detallesorden.producto and id=detallesorden.idPrecio  group by id');

        $cant = DB::select('SELECT sum(precio)as montototal,sum(cantidad)as cantidadtotal from detallesorden,facturas where facturas.idFactura=factura and facturas.created_at BETWEEN "' . $request->desde . ' 00:00:00" AND "' . $request->hasta . ' 00:00:00"');


        return response()->json(['producto' => $productos, 'cant_total' => $cant]);
    }

}
