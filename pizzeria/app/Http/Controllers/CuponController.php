<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modelos\Cupon;
use Illuminate\Http\Request;
use DB;
class CuponController extends Controller {

    static public function CRUD(Request $request) {
        return view('layouts.Empleado');
    }

    //Funciones Ajax
    //Select_all: Devuelve todos los objetos de la tabla impuesto
    public function Select_all(Request $request) {
        $cupones = cupon::all();
        return response()->json(['cupon' => $cupones]);
    }

    //Create: Crea y Guarda un objeto de tipo impuesto
    public function Create(Request $request) {
        try {//Guadar Registro 
          $datos = json_decode($request->cupon);
		  if (!isset($datos->id)){
			$cupon = new Cupon();
		  }else{
			$cupon =Cupon::find($datos->id);; 
		  }
           
            $cupon->descripcion  = $datos->descripcion;
            $cupon->valor  = $datos->valor;
            $cupon->save();
			 $cupones = Cupon::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'cupon' => $cupones]);
         }catch (\Illuminate\Database\QueryException $ex) {
            $codigo=$ex->errorInfo[1];
            if($codigo==1062){
                $resultado = strpos($ex->errorInfo[2], "PRIMARY");
                //si es diferente de false quiere decir que estoy violando una primary key.
                if($resultado !== false){
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ese dni en la base de datos.']);
                }else{
                    return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ese dni en la base de datos.']);
                }
            }else{
                return response()->json(['error'=>true,'mensaje'=>$ex->errorInfo[2]]);
            }
    }
    
            }

  
    //Update_status: Actualiza el estado de un impuesto
    public static function Update_status(Request $request) {
        try {//Guadar Registro 
            $cupon = Cupon::find($request->id);
            $cupon->estado =  $cupon->estado==0?1:0;
            $cupon->save();
			
        } catch (Exception $ex) {
         return response()->json(['error' => true, 'mensaje' => "El usuario no ha podido cambiar el estado"]);
        }
       $cupones = Cupon::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'cupon' => $cupones]); }

    //Delete: Borra un modo de pago
    public function Delete(Request $request) {
        try {
         
            $cupon = Cupon::find($request->id);
            $cupon->delete();
            $cupones = Cupon::all();
            // return response()->json(['error'=>true,'mensaje'=>'Ya existe un usuario con ññ{ese dni en la base de datos.']);
            return response()->json(['error'=>false,'cupon' => $cupones]);
        } catch (Exception $ex) {
            return response()->json(['error' => true,'mensaje' => "El Usuario  no ha podido ser Eliminado"]);
        }
    }

}
