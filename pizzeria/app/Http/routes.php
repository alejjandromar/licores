<?php

/* ----------------------Generales----------------------------------------- */
Route::get('olvidar', 'OrdenController@olvidar');                                                               //Elimina todas las variables de cache

Route::get('datos_general', ['middleware' => 'auth:AdminAuth', 'uses' => 'GeneralController@index']);
Route::get('general/all', ['uses' => 'GeneralController@all']);
Route::post('general/save', [ 'uses' => 'GeneralController@save']);

/* ----------------------Sesiones--------------------------------------- */
Route::get('', ['uses' => 'LoginController@index', 'as' => 'login']);
Route::get('inicio', ['uses' => 'LoginController@index', 'as' => 'login']);
Route::post('auth', ['uses' => 'LoginController@verificar', 'as' => 'auth']);                                //Iniciar Sesion
Route::get('auth_app', ['uses' => 'LoginController@verificar_app', 'as' => 'auth_app']);                                //Iniciar Sesion
Route::get('logout_admin', [ 'uses' => 'LoginController@logout', 'as' => 'logout']);        //Cerrar Sesion
//sesiones app
Route::get('login_cliente', [ 'uses' => 'LoginController@applogin']);
Route::get('login_empleado', [ 'uses' => 'LoginController@login_empleado']);
Route::get('push_token', [ 'uses' => 'LoginController@push']);
Route::get('logout_cliente', [ 'uses' => 'LoginController@appLogout']);


/* ----------------------Cliente--------------------------------------- */
Route::get('buscar_cliente', 'ClienteController@BuscarCliente');        //Busca un cliente ya registrado
Route::get('cargarDireccionesFavoritas', [ 'uses' => 'ClienteController@cargarDireccionesFavoritasCliente']);
Route::get('agregarDireccionFavorita', [ 'uses' => 'ClienteController@agregarDireccionFavorita']);
Route::get('verificarDireccion', [ 'uses' => 'ClienteController@verificarDireccion']);
Route::get('eliminarDireccion', [ 'uses' => 'ClienteController@eliminarDireccion']);
Route::get('cargarPedidos', [ 'uses' => 'ClienteController@cargarPedidos']);
Route::get('cargarDatosPedido', 'OrdenController@cargarDatosPedido');
Route::get('cargarCupones', 'ClienteController@cargarCupones');
Route::get('clienteIngresaCupon', 'ClienteController@clienteIngresaCupon');

/* -------------------------EMPLEADO----------------------------*/
Route::get('cargarPedidosAsig', [ 'uses' => 'ClienteController@cargarPedidosAsig']);
Route::get('cambiar_estado', 'OrdenController@cambiar_estado');

/* ----------------------Rutas del  Menu------------------------------------ */
Route::get('CargarDatos', 'MenuController@Vender'); //Vista del Menu de la Orden
Route::get('menu', ['middleware' => 'auth:Mesero', 'uses' => 'MenuController@Vender', 'as' => 'ruta_menu']); //Vista del Menu de la Orden
Route::get('pedido{num_mesa}', 'OrdenController@enviar_orden'); //enviar orden
Route::get('pedido/{num_mesa}', ['middleware' => 'auth:Mesero', 'uses' => 'OrdenController@enviar_orden_pdf']); //enviar orden
Route::get('menu{id}', 'MenuController@Vend');                             //Cambia de categoria
Route::get('cocina/', 'PdfController@comenzar');                //Cambia la categoria
Route::get('estadoCocina/', 'PdfController@cambiar_estado'); //cambia el estado de una orden a terminado
Route::get('terminarOrden/', 'PdfController@terminarOrden'); //saca de la cola la orden que se termmino
Route::get('ncliente_temp/{num_mesa}', 'OrdenController@RegistroCliTemp');  //Guarda un cliente temporalmente
Route::get('ndetalle_temp/{num_mesa}', 'OrdenController@RegistroDetTemp');  //Agrega un nuevo detalle a la orden
Route::get('env_orden_pagar/{num_mesa}', 'OrdenController@OrdenPagar');  //Agrega un nuevo detalle a la orden
Route::get('cat_change/', ['middleware' => 'auth:Mesero', 'uses' => 'ProductoController@Cambio_cat']);                //Cambia la categoria
Route::get('cancelarOrden', ['middleware' => 'auth', 'uses' => 'OrdenController@cancelarOrden']);            //cacela una orden existente
Route::get('agregarModDet', ['middleware' => 'auth:Mesero', 'uses' => 'OrdenController@addModDet']);            //agrega n modificadores a un detalle
Route::get('borrarModDet', ['middleware' => 'auth:Mesero', 'uses' => 'OrdenController@borrarModDet']);            //borra un modificador a un detalle existente
Route::get('orden/agregarComentario', 'OrdenController@agregarComentario');            //agrega un comentario a un detalle o a una orden 
Route::get('OrdenLlevar', 'OrdenController@cambiarTipoOrden');
Route::get('preview_orden{num_mesa}', [ 'uses' => 'PdfController@preview']);
Route::get('logoutcaja{i}/{n_caja}/{preview}/{System}', 'VentasReportController@Cierre');
Route::get('logoutcaja{i}', 'LoginController@logout');

/*-----------------------Rutas del tracking--------------------------------*/
Route::get('buscarEmpleadosDisponibles', 'trackingController@empleadosDisponibles');
Route::get('envio/save', 'trackingController@guardarEnvio');
Route::get('posicionActualEmpleadoAdmin', 'trackingController@posicionActualEmpleadoAdmin');

/* ----------------------Rutas del Pago------------------------------------ */
Route::post('precompra', [ 'uses' => 'PagoController@Precompra', 'as' => 'pagar']);
Route::post('compra_done_paypal', [ 'uses' => 'PagoController@bloqueo_permanente', 'as' => 'pagar']);
Route::post('compra_cancelar_paypal', [ 'uses' => 'PagoController@cancelar_paypal_orden', 'as' => 'pagar']);

Route::get('pagar', [ 'uses' => 'PagoController@RealizarPago', 'as' => 'pagar']);              //Vista la ventana de cobrar
Route::get('cambiar_caja', ['middleware' => 'auth:Cajero', 'uses' => 'CajaController@Cambiar', 'as' => 'caja_cambiar']);
Route::get('Control-de-cajas', ['middleware' => 'auth:Supervisor', 'uses' => 'CajaController@controlCajas', 'as' => 'Control-de-cajas']);
Route::get('cajas/all', 'CajaController@selectAll');
Route::get('cajas/activarCaja', 'CajaController@activarCaja');
Route::get('actualizar_caja', 'CajaController@actualizarCaja');
Route::get('orden_pagar/{num_mesa}', [ 'uses' => 'PagoController@PagarOrden']);          //Envia la orden para que se ejecute el pago
Route::get('pdf{num_orden}/{imprimir}/{num_mesa}', [ 'uses' => 'PdfController@factura']);
Route::get('verificarEstado', [ 'uses' => 'PdfController@verificarEstado']);
Route::get('preview{num_orden}/{imprimir}/{num_mesa}', [ 'uses' => 'previewOrdenController@factura']);
Route::get('bus_imp', [ 'uses' => 'ColaController@siguiente']);
Route::get('status', [ 'uses' => 'ColaController@status']);

/* ----------------------Plano Mesas-------------------------------------- */
Route::get('mesa/select', ['middleware' => 'auth:AdminAuth', 'uses' => 'ControlPlano@select']);
Route::get('mesas', ['middleware' => 'auth', 'uses' => 'ControlPlano@mesa', 'as' => 'ruta_mesas']);
Route::get('mesas/ala', ['uses' => 'ControlPlano@ala']);                                      //Cambiar el Ala
Route::get('mesas/all', ['uses' => 'ControlPlano@mesas']);
Route::get('get_orden/{num_mesa}', ['middleware' => 'auth:Cajero', 'uses' => 'OrdenController@orden']);  //Agrega un nuevo detalle a la orden
Route::get('dividir_m/{num_mesa}/{num_mesa2}', ['middleware' => 'auth:Cajero', 'uses' => 'OrdenController@dividir_orden']);  //Agrega un nuevo detalle a la orden
Route::get('unir_mesa/{num_mesa1}/{num_mesa2}', ['middleware' => 'auth:Cajero', 'uses' => 'OrdenController@unir_mesas']);  //Agrega un nuevo detalle a la orden
Route::get('dividir_mesa/{num_mesa}/{num_mesa2}', ['middleware' => 'auth:Cajero', 'uses' => 'OrdenController@dividir_mesas']);  //Agrega un nuevo detalle a la orden
Route::get('mover_mesa/{num_mesa1}/{num_mesa2}', ['middleware' => 'auth:Cajero', 'uses' => 'OrdenController@mover_mesa']);  //Agrega un nuevo detalle a la orden


/* --PAGINA INICIO ADMINISTRACION */
Route::get('home_admin', ['middleware' => 'auth', 'uses' => 'ControlPlano@home']);


/* ----------------------Rutas de CRUDS------------------------------------ */
/* ----------------------Crud Metodos de Pago----------------------------------- */
Route::get('Metodos-De-Pago', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@CRUD', 'as' => 'metodos_pago']);
Route::post('metodos_pago/add', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Create', 'as' => 'metodos_pago_add']);                       //A�ade un nuevo metodo de pago
Route::get('metodos_pago/all', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Select_all', 'as' => 'metodos_pago_all']);                   //Seleciona todos los metodos de pago
Route::get('metodos_pago/one', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Select_one', 'as' => 'metodos_pago_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::post('metodos_pago/update', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Update', 'as' => 'metodos_pago_updt']);                   //Actualiza un metodo de pago
Route::get('metodos_pago/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Update_status', 'as' => 'metodos_pago_updt_stus']);  //Cambia el estado de un metodo de pago
Route::get('metodos_pago/delete', ['middleware' => 'AdminAuth', 'uses' => 'MetodoPagoController@Delete', 'as' => 'metodos_pago_del']);                    //Elimina el estado de un metodo de pago

/* ----------------------crud Impuesto----------------------------------- */
Route::get('Impuestos', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@CRUD', 'as' => 'impuesto']);
Route::get('impuesto/add', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Create', 'as' => 'impuesto_add']);                       //A�ade un nuevo metodo de pago
Route::get('impuesto/all', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Select_all', 'as' => 'impuesto_all']);                   //Seleciona todos los metodos de pago
Route::get('impuesto/one', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Select_one', 'as' => 'impuesto_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::get('impuesto/update', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Update', 'as' => 'impuesto_updt']);                   //Actualiza un metodo de pago
Route::get('impuesto/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Update_status', 'as' => 'impuesto_updt_stus']);  //Cambia el estado de un metodo de pago
Route::get('impuesto/delete', ['middleware' => 'AdminAuth', 'uses' => 'ImpuestoController@Delete', 'as' => 'impuesto_del']);                    //Elimina el estado de un metodo de pago

/* ----------------------crud Cliente----------------------------------- */
Route::get('cliente', ['middleware' => 'AdminAuth', 'uses' => 'ClienteController@CRUD', 'as' => 'cliente']);
Route::get('cliente/all', ['middleware' => 'AdminAuth', 'uses' => 'ClienteController@Select_all', 'as' => 'cliente_all']);                   //Seleciona todos clientes

/* ---------------------- crud Empleado----------------------------------- */
Route::get('empleado', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@CRUD', 'as' => 'empleado']);
Route::post('usuario/save', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Create', 'as' => 'empleado_add']);                       //A�ade un nuevo metodo de pago
Route::get('usuario/all', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Select_all', 'as' => 'empleado_all']);                   //Seleciona todos los metodos de pago
Route::get('empleado/one', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Select_one', 'as' => 'empleado_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::post('empleado/update', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Update', 'as' => 'empleado_updt']);                   //Actualiza un metodo de pago
Route::post('usuario/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Update_status', 'as' => 'empleado_updt_stus']);  //Cambia el estado de un metodo de pago
Route::post('usuario/delete', ['middleware' => 'AdminAuth', 'uses' => 'EmpleadoController@Delete', 'as' => 'empleado_del']);                    //Elimina el estado de un metodo de pago


/* ---------------------- crud Cupo----------------------------------- */
Route::get('cupon', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@CRUD', 'as' => 'empleado']);
Route::post('cupon/save', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Create', 'as' => 'empleado_add']);                       //A�ade un nuevo metodo de pago
Route::get('cupon/all', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Select_all', 'as' => 'empleado_all']);                   //Seleciona todos los metodos de pago
Route::get('cupon/one', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Select_one', 'as' => 'empleado_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::post('cupon/update', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Update', 'as' => 'empleado_updt']);                   //Actualiza un metodo de pago
Route::post('cupon/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Update_status', 'as' => 'empleado_updt_stus']);  //Cambia el estado de un metodo de pago
Route::post('cupon/delete', ['middleware' => 'AdminAuth', 'uses' => 'CuponController@Delete', 'as' => 'empleado_del']);                    //Elimina el estado de un metodo de pago



/* ---------------------- crud Caja----------------------------------- */
Route::get('caja', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@CRUD', 'as' => 'caja']);
Route::post('caja/add', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Create', 'as' => 'caja_add']);                       //A�ade un nuevo metodo de pago
Route::get('caja/all', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Select_all', 'as' => 'caja_all']);                   //Seleciona todos los metodos de pago
Route::get('caja/one', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Select_one', 'as' => 'caja_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::post('caja/update', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Update', 'as' => 'caja_updt']);                   //Actualiza un metodo de pago
Route::get('caja/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Update_status', 'as' => 'caja_updt_stus']);  //Cambia el estado de un metodo de pago
Route::get('caja/delete', ['middleware' => 'AdminAuth', 'uses' => 'CajaController@Delete', 'as' => 'caja_del']);                    //Elimina el estado de un metodo de pago

/* ----------------------crud Impresora----------------------------------- */
Route::get('impresora', ['middleware' => 'AdminAuth', 'uses' => 'ImpresoraController@CRUD', 'as' => 'impresora']);
Route::get('impresora/add', ['uses' => 'ImpresoraController@Create', 'as' => 'impresora_add']);                       //A�ade un nuevo metodo de pago
Route::get('impresora/all', ['uses' => 'ImpresoraController@Select_all', 'as' => 'impresora_all']);                   //Seleciona todos los metodos de pago
Route::get('impresora/one', ['middleware' => 'AdminAuth', 'uses' => 'ImpresoraController@Select_one', 'as' => 'impresora_one']);                   //Selecciona un registro en especifico de las metodos de pago
Route::post('impresora/update', ['middleware' => 'AdminAuth', 'uses' => 'ImpresoraController@Update', 'as' => 'impresora_updt']);                   //Actualiza un metodo de pago
Route::get('impresora/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'ImpresoraController@Update_status', 'as' => 'impresora_updt_stus']);  //Cambia el estado de un metodo de pago
Route::get('impresora/delete', ['middleware' => 'AdminAuth', 'uses' => 'ImpresoraController@Delete', 'as' => 'impresora_del']);                    //Elimina el estado de un metodo de pago

/* ------------------------crud CATEGORIA------------------------------------------------ */
Route::get('Categorias', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@index']);
Route::get('categoria/all', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@all']);
Route::post('categoria/save', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@adminCatInsertar', 'as' => 'categoria_add']);
Route::any('categoria/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@estado', 'as' => 'categoria_update_stus']);
Route::post('categoria/update', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@adminCatUpdate', 'as' => 'categoria_udt']);
Route::post('categoria/delete', ['middleware' => 'AdminAuth', 'uses' => 'CategoriaController@Delete']);

/* ------------------------crud PRODUCTOS-------------------------------------- */
Route::get('Productos', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@index', 'as' => 'producto']); //inicio 
Route::post('producto/save', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@InsertarPro', 'as' => 'producto_add']); //inserta producto
Route::post('presentacion/save', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@adminPreInsertar', 'as' => 'present_add']); //inserta presentacion
Route::get('producto/all', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@all']); //ajax para obterner productos
Route::get('producto/allpre', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@allpre']); //ajax para obtener presentaciones
Route::post('producto/updatepro', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@UpdatePro', 'as' => 'producto_udt_pro']); // actualizar productos
Route::get('producto/present', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@presentacion']);
Route::get('producto/activarMod', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@activarModificadores']);
Route::post('producto/delete', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@Delete']);
Route::post('presentacion/delete', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@DeletePre']);
Route::any('producto/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@estado']);
Route::post('presentacion/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@estado_pre']);
Route::post('presentacion/update_descuento', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@descuento_pre']);
Route::post('presentacion/add_dis', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@add_dis']);
Route::post('producto/updatePre', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@Update_Pre']);

/* ---------------------- crud Propinas----------------------------------------- */
Route::get('Propinas', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@index']);
Route::get('Propina/add', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@Create']);
Route::get('Propina/all', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@selectAll']);
Route::get('Propina/update', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@Update']);
Route::get('Propina/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@Update_status']);
Route::get('Propina/delete', ['middleware' => 'AdminAuth', 'uses' => 'propinaController@Delete']);

/* ----------------------crud Descuentos----------------------------------- */
Route::get('Descuentos', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@index']);
Route::get('Descuentos/add', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@Create']);
Route::get('Descuentos/all', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@selectAll']);
Route::get('Descuentos/update', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@Update']);
Route::get('Descuentos/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@Update_status']);
Route::get('Descuentos/delete', ['middleware' => 'AdminAuth', 'uses' => 'descuentoController@Delete']);

/* ------------------------crud Mesas ------------------------------ */
Route::get('adm_mesas', ['middleware' => 'auth', 'uses' => 'ControlPlano@index']);
Route::get('crear_plano', ['middleware' => 'auth', 'uses' => 'ControlPlano@crea']); //Crea el plano de las mesas
Route::get('adm_mesas/all', ['middleware' => 'auth', 'uses' => 'ControlPlano@all']);
Route::get('adm_mesas/delete', ['middleware' => 'auth', 'uses' => 'ControlPlano@delete']);
Route::post('adm_mesas/add', ['middleware' => 'auth', 'uses' => 'ControlPlano@addMesa', 'as' => 'mesas_add']);
Route::post('adm_mesas/udt', ['middleware' => 'auth', 'uses' => 'ControlPlano@updateMesa', 'as' => 'mesa_udt']);
Route::get('adm_mesas/estado', ['middleware' => 'auth', 'uses' => 'ControlPlano@update_estado']);
Route::get('mesa/coor', ['middleware' => 'auth', 'uses' => 'ControlPlano@save_coor']);
Route::get('mesa/coor_get', ['middleware' => 'auth', 'uses' => 'ControlPlano@get_coor']);
Route::get('mesa/delete', ['middleware' => 'auth', 'uses' => 'ControlPlano@del_coor']);

/* -------------------------crud Areas--------------------------------------------- */
Route::get('areas', ['middleware' => 'AdminAuth', 'uses' => 'AreaController@index']);
Route::post('areas/add', ['middleware' => 'AdminAuth', 'uses' => 'AreaController@addArea', 'as' => 'areas_add']);
Route::get('areas/all', ['uses' => 'AreaController@all']);
Route::get('areas/estado', ['middleware' => 'AdminAuth', 'uses' => 'AreaController@update_estado']);
Route::post('areas/udt', ['middleware' => 'AdminAuth', 'uses' => 'AreaController@updateArea', 'as' => 'areas_udt']);
Route::get('areas/delete', ['middleware' => 'AdminAuth', 'uses' => 'AreaController@delete']);

/* ----------------------crud Modificadores----------------------------------- */
Route::get('Modificadores', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@CRUD']);
Route::post('modificador/add', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@Create']);
Route::get('modificador/all', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@Select_all']);
Route::post('modificador/update', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@Update']);
Route::get('modificador/update_stus', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@Update_status']);
Route::get('modificador/delete', ['middleware' => 'AdminAuth', 'uses' => 'modificadorController@Delete']);
/* -------------------------Reportes--------------------------------------------- */

/* -------------------------ORDENES----------------------------------- */
Route::get('orden/all ', ['middleware' => 'AdminAuth', 'uses' => 'OrdenReporteController@ordenes_all']);
Route::get('orden/one ', ['middleware' => 'AdminAuth', 'uses' => 'OrdenReporteController@get_one']);

/* -------------------------Reportes de ventas----------------------------------- */
Route::get('ventasxdia ', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@index']);
Route::get('ventas_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_generales']);
Route::get('ventas_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_generales_report']);
Route::get('cliente_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_clientes']);
Route::get('cliente_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_clientes_report']);
Route::get('mesero_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_mesero']);
Route::get('mesero_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_mesero_report']);
Route::get('cajero_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_cajero']);
Route::get('cajero_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_cajero_report']);

Route::get('pagos_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_metodo_pago']);
Route::get('pagos_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_pago_report']);

Route::get('producto_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_producto']);
Route::get('producto_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_producto_report']);

Route::get('prensentacion_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_productoxpresentacion']);
Route::get('prensentacion_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_presentacion_report']);

Route::get('categoria_g', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@ventas_categoria']);
Route::get('categoria_g/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventas_categoria_report']);

Route::get('ventasxpago ', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@index']);
Route::get('ventasxdescuento/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventasxdescuento']);
Route::get('ventasxcliente/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventasxcliente']);
Route::get('ventasxdia/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Comp_Ventasxdia']);
Route::get('ventasxpago/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasReportController@Ventasxpago']);
Route::get('Ventas-Diarias-Por-Impuestos', ['middleware' => 'AdminAuth', 'uses' => 'VentasDiariasImpuestoController@index']);
Route::get('Ventas-Por-Impuestos', ['middleware' => 'AdminAuth', 'uses' => 'VentasImpuestoController@index']);
Route::get('Ventas-Por-Impuestos/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasImpuestoController@VentasPorImpuestos']);
Route::get('Ventas-Diarias-Por-Impuestos/all', ['middleware' => 'AdminAuth', 'uses' => 'VentasDiariasImpuestoController@VentasDiariasPorImpuestos']);

/* -------------------------Reportes de productos-------------------------------- */
Route::get('reportes_producto', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@reporte_index']);
Route::get('reportes_producto/filtro', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@reportes_filtro']);
Route::get('reportes_producto/all', ['middleware' => 'AdminAuth', 'uses' => 'ProductoController@reportes_all']);

/* -------------------------Reportes de ordenes cancelados--------------------------------------------- */
Route::get('Ordenes-canceladas', ['middleware' => 'AdminAuth', 'uses' => 'OrdenReporteController@inicio']);
Route::get('Ordenes-canceladas/all', ['middleware' => 'AdminAuth', 'uses' => 'OrdenReporteController@ordenesCanceladas']);

//Generando PDF
Route::get('reportes_producto/pdf', ['middleware' => 'AdminAuth', 'uses' => 'PdfController@reportes_general_pdf']);
Route::get('reportes_producto/pdf_filtro/{fecha}', ['middleware' => 'AdminAuth', 'uses' => 'PdfController@reportes_filtro_pdf']);

/* -------------------CARGA DE ARCHIVO EXCEL-------------------------- */
Route::get('cargarArchivo', ['middleware' => 'AdminAuth', 'uses' => 'cargarArchivoController@CRUD']);
Route::post('cargaArchivo/add', ['uses' => 'cargarArchivoController@add']);
