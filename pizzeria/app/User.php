<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'empleados';
    public $primaryKey='id';
    public $timestamps = false;
     use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['dni','usuario','cargo','name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function ordenesMesero()
    {      
        return $this->hasMany('App\Modelos\Orden', 'empleado');
    }
    public function ordenescajero()
    {      
        return $this->hasMany('App\Modelos\Orden', 'cajero');
    }
    public function turnosCaja()
    {      
        return $this->hasMany('App\Modelos\cierrecaja', 'cajero');
    }
    
       public function envios()
    {      
        return $this->belongsToMany('App\Modelos\Orden', 'empleado_envia_pedido','id_empleado','id_factura');
    }
}
