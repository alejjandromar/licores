<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Modelos\caja;
use App\Modelos\cierrecaja;
use App\Http\Controllers\VentasReportController;
use DB;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule) {
        $schedule->call(function () {
            $cierres = cierrecaja::where([ 'final' => '0000-00-00 00:00:00'])->get();
            $controller = new VentasReportController();
            foreach ($cierres as $key => $cierre) {
                $controller->Cierre(0, $cierre->numero, "false", false);
            }
            $cajas = caja::where([ 'Estado2' => '1'])->get();
            foreach ($cajas as $key => $caja) {
                $controller->Cierre(1, $caja->numero, "false", false);
            }
            DB::table('caja')->update(['Estado2' => 0]);
            cierrecaja::where([ 'final' => '0000-00-00 00:00:00'])->update(['final' => date_format(date_create(), 'Y-m-d H:i:s'), 'usuarioCerroCaja' => "Sistema"]);
        })->everyMinute();//->dailyAt('23:55');
		
		$schedule->call(function () {
            DB::table('caja')->update(['Estado2' => 0]);
            cierrecaja::where([ 'final' => '0000-00-00 00:00:00'])->update(['final' => date_format(date_create(), 'Y-m-d H:i:s'), 'usuarioCerroCaja' => "Sistema"]);
        })->dailyAt('00:05');
    }

}
