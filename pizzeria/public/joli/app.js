'use strict';


// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.version',
  
  'myApp.categoria',
  'myApp.producto',
  'myApp.general',
  //'myApp.modificadores',
  'myApp.empleado',
  'myApp.cupon',
  'myApp.ordenes',
  'myApp.factura',
  'myApp.reporte',
  'myApp.dashboard'

]).directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }                                       
            });
        }
    };
}).
config(['$routeProvider', function($routeProvider) {

  $routeProvider.otherwise({redirectTo: '/dashboard'});

}]);
