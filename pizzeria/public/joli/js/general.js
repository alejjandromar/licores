'use strict';
var path = "";



		
angular.module('myApp.general', ['ngRoute'])

// Declared route 
.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/general', {
		templateUrl: 'joli/pages/general.html',
		controller: 'generarCtrl'
	});
}])

 .controller('generarCtrl', ['$http', '$scope','$rootScope', function ($http, $scope,$rootScope) {
	//controlador para tener variables y funciones globales
		
	/*
	funciones globales
	*/
	$rootScope.error={};$rootScope.confirm={};
	
	//funcion para cerra el alerta
	$rootScope.cerraAlert=function(id){
		$('#'+id).hide();
	}
	
	//funcion para abrir el alerta
	$rootScope.abrirAlert=function(id,sonido){
		$('#'+id).show();
		 playAudio(sonido);
	}
	
	//funcion para abrir el confirm
	$rootScope.abrirConfirm=function(id,sonido){
		$('#'+id).show();
		 playAudio(sonido);
	}
	
	//funcion para cerra el alerta
	$rootScope.cerraConfirm=function(id){
		$('#'+id).hide();
	}
	
	//funcion para ser reutilizada en cada llamada de un alerta
	$rootScope.llamarAbrirAlert=function(titulo,clase,icono,mensaje,sonido){
		$rootScope.error.titulo=titulo;$rootScope.error.class=clase;
		$rootScope.error.icono=icono;$rootScope.error.mensaje=mensaje;
		$rootScope.abrirAlert('alerta',sonido);
	}
	
	
	$rootScope.llamarComfir=function(titulo,clase,icono,mensaje,sonido){
		$rootScope.confirm.titulo=titulo;$rootScope.confirm.class=clase;
		$rootScope.confirm.icono=icono;$rootScope.confirm.mensaje=mensaje;
		$rootScope.abrirConfirm('confirm',sonido);
	}
	
	//funcio0n para loading
	function panel_refresh(panel,top){        
		if(!panel.hasClass("panel-refreshing")){
			panel.append('<div class="panel-refresh-layer col-md-12 col-sm-12 col-lg-12"><img src="img/loaders/default.gif"/></div>');
			panel.find(".panel-refresh-layer").height(panel.height());
			if(top!=0){
				panel.find(".panel-refresh-layer").height(panel.height()+50);
				panel.find(".panel-refresh-layer").css('top',top+'px')
			}
			panel.addClass("panel-refreshing");
			
		}
	}
	
	//funcion que inicailiza un panel de espera
	$rootScope.iniciarPanelEspera=function(id,top){
		var panel = $("#"+id);
		panel_refresh(panel,top);
		return false;
	}
	
	//funcion que finaliza un panel de espera
	$rootScope.TerminarPanelEspera=function(id){
		var panel = $("#"+id);
		panel.find(".panel-refresh-layer").remove();
		panel.removeClass("panel-refreshing");  
		return false;
	}
	
	$rootScope.iniciarPlgImg=function(id){
		$(id).fileinput({
			showUpload:false, 
			showPreview:false, 
			previewFileType:false,
			allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
			removeClass: "btn btn-danger",
			removeLabel: "",
			removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
		});
		$('.form-control.file-caption.kv-fileinput-caption').css('height','36px');
	}
	
	$rootScope.confirmarEliminar=function(elemento,x){
		$rootScope.Y=elemento;
		if(x==1){
			//voy a eliminar una categoria
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la categoria?','fail');
			$rootScope.X=x;
		}
		if(x==2){
			//voy a eliminar un producto
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el producto?','fail');
			$rootScope.X=x;
		}
		if(x==3){
			//voy a eliminar una presentacion
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la presentación?','fail');
			$rootScope.X=x;
		}
		if(x==4){
			//voy a eliminar una propina
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la propina?','fail');
			$rootScope.X=x;
		}
		if(x==5){
			//voy a eliminar un descuento
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el descuento?','fail');
			$rootScope.X=x;
		}
		if(x==6){
			//voy a eliminar un impuesto
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el impuesto?','fail');
			$rootScope.X=x;
		}
		if(x==7){
			//voy a eliminar un metodo de pago
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el método de pago?','fail');
			$rootScope.X=x;
		}
		if (x == 8) {
			//voy a eliminar un area
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar el área?', 'fail');
			$rootScope.X = x;
		}
		if (x == 9) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar la caja?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 10) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar la mesa?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 11) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar el usuario?', 'fail');
			$rootScope.X = x;
		}
                
                 if (x == 12) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta accion aumentará el precio de todos los productos?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 13) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta accion bajará el precio de todos los productos?', 'fail');
			$rootScope.X = x;
		}
	}
	
	$rootScope.intermediaria=function(){
		console.log('intermediaria');
		if($rootScope.X==1){
			$rootScope.llamarEliminarCategoria($rootScope.Y);
		}
		if($rootScope.X==2){
			$rootScope.llamarEliminarProducto($rootScope.Y);
		}
		if($rootScope.X==3){
			$rootScope.llamarEliminarPresentacion($rootScope.Y);
		}
		if($rootScope.X==4){
			$rootScope.llamarEliminarPropina($rootScope.Y);
		}
		if($rootScope.X==5){
			$rootScope.llamarEliminarDescuento($rootScope.Y);
		}
		if($rootScope.X==6){
			$rootScope.llamarEliminarImpuesto($rootScope.Y);
		}
		if($rootScope.X==7){
			$rootScope.llamarEliminarPago($rootScope.Y);
		}
		if ($rootScope.X == 8) {
			$rootScope.llamarEliminarArea($rootScope.Y);
		}
		if ($rootScope.X == 9) {
			$rootScope.llamarEliminarCaja($rootScope.Y);
		}
                if ($rootScope.X == 10) {
			$rootScope.llamarEliminarMesa($rootScope.Y);
		}
                 if ($rootScope.X == 11) {
			$rootScope.llamarEliminarUsuario($rootScope.Y);
		}
                
                if ($rootScope.X == 12) {
			$rootScope.llamarSubirPrecio($rootScope.Y);
		}
                
                 if ($rootScope.X == 13) {
			$rootScope.llamarBajarPrecio($rootScope.Y);
		}
	}
	/*
	funciones globales
	*/

	
}])
