'use strict';
var path = "";




angular.module('myApp.reporte', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/reporte/:v', {
                    templateUrl: 'joli/pages/reporte.html',
                    controller: 'reporteCtrl'
                });
            }])

// Home controller
        .controller('reporteCtrl', ['$http', '$scope', '$routeParams', function ($http, $scope, $routeParams) {
                $scope.opc = $routeParams.v;
                $scope.fecha = new Date();
                $scope.p = {tipo: 'DIARIO',
                    fechai: $scope.fecha.getDate() + "-" + $scope.fecha.getMonth() + "-" + $scope.fecha.getFullYear(),
                    fechaf: $scope.fecha.getDate() + "-" + $scope.fecha.getMonth() + "-" + $scope.fecha.getFullYear(),
                    year: $scope.fecha.getFullYear(),
                    yearf: $scope.fecha.getFullYear(),
                    mes: $scope.fecha.getMonth(),
                    mesf: $scope.fecha.getMonth()
                };
                $scope.meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                $scope.years = ['2014', '2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024', '2025', '2026', '2028']
                $scope.rutas = ['ventas_g/all',//0
                    'cliente_g/all',//1
                    'mesero_g/all',//2
                    'cajero_g/all',//3
                    'pagos_g/all',//4
                    'producto_g/all',//5
                    'prensentacion_g/all',//6
                    'categoria_g/all',//7
                    'impuesto_g/all',//8
                    'descuento_g/all',//9
                    'propinas_g/all',//10
                    'canceladas_g/all'//10
                    ];

                /*
                 Funcion que carga los datos de una pago
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + $scope.rutas[$scope.opc],
                        method: 'get',
                        params: $scope.p,
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.datos = response.datos;
                        $scope.columnas = response.columnas;
                        $scope.columnas_lb = response.columnas_lb;
                        $scope.tipos = response.tipos;
                        $(".fecha").datepicker();
                    });
                };

                $scope.cargar_datos();

                $scope.print = function () {
                    var source = $("#print").html();
                    $("#print-page").html(source);
                    mostrar_print();


                }

                $scope.make_fecha = function (fecha) {
                    return fecha;


                }

            }])