'use strict';
var path = "";var markerCache=[];

angular.module('myApp.ordenes', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/ordenes', {
                    templateUrl: 'joli/pages/ordenes.html',
                    controller: 'OrdenesCtrl'
                });
            }])

// Home controller
        .controller('OrdenesCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.norden = {ico: "ion-fork"};


                $scope.ver_factura= function(orden){
                    localStorage.factura=JSON.stringify(orden);
                }
                
                /*
                 Funcion que la animacion del agregar nueva producto
                 */
                $scope.order_by = function (word) {
                    if ($scope.col_ord == word) {
                        $scope.rev = !$scope.rev;
                    } else {
                        $scope.col_ord = word;
                        $scope.rev = false;
                    }
                }
                /*
                 Funcion que �rmite la animacion del agregar nueva orden
                 */
                $scope.BtnOrden = function (v) {
                    if (v) {
                        $('#PDatosOrden').show("slow");
                        $('#BtnOrden').hide("slow");
                    } else {
                        $('#PDatosOrden').hide("slow");
                        $('#BtnOrden').show("slow");
                    }

                }

                /*
                 Funcion que �rmite la animacion del agregar editar orden
                 */
                $scope.BtnOrden = function (v) {
                    if (v) {
                        $('#PDatosOrden').show("slow");
                        $('#BtnOrden').hide("slow");
                    } else {
                        $('#PDatosOrden').hide("slow");
                        $('#BtnOrden').show("slow");
                    }

                }

                /*
                 Funcion que carga los datos de una orden
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'orden/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        var data= JSON.parse(response.orden);
                        $scope.ordens = data.data;
                       
                    });
                };

                /*
                 Funcion que edita los datos de una orden
                 */
                $scope.edit_or_update = function (orden) {
                    if (orden.idOrden == undefined || orden.idOrden == null) {
                        var file = $scope.extra.imagen;
                    } else {
                        var file = orden.imagen;
                        orden.imagen = null;
                    }
                    var fd = new FormData();
                    fd.append('file', file);
                    fd.append('orden', JSON.stringify(orden));
                    $http.post("orden/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error != false) {
                            $scope.ordens = response.orden;
                            $('#filename').fileinput();
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                            $scope.norden = {ico: "ion-fork"};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una orden
                 */
                $scope.eliminar = function (orden) {
                    $http({
                        url: path + 'orden/delete',
                        method: 'post',
                        data: {id: orden.idOrden},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ordens = response.orden;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que cambia los estados de una orden
                 */
                $scope.estado = function (orden) {
                    $http({
                        url: path + 'orden/update_stus',
                        method: 'post',
                        data: {id: orden.idOrden},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ordens = response.orden;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                $scope.estados = [];
                $scope.estados ["-1"] = "Cancelada";
                $scope.estados [3] = "Por enviar";
                $scope.estados [4] = "Enviado";
                $scope.estados [5] = "Enviado";
				$scope.estados [6] = "Entregado";



                $scope.cargar_datos();




            }])

    
    
    
    angular.module('myApp.factura', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/factura', {
                    templateUrl: 'joli/pages/facturaCliente.html',
                    controller: 'FacturaCtrl'
                });
            }])

// Home controller
        .controller('FacturaCtrl', ['$http', '$scope','$timeout','$location', function ($http, $scope,$timeout,$location) {

                $scope.norden = {ico: "ion-fork"};
				$scope.mostrarDetalles=true;$scope.mostrar=false;
				$scope.mostrarDetalles=true;
				

                /*
                 Funcion que la animacion del agregar nueva producto
                 */
                $scope.order_by = function (word) {
                    if ($scope.col_ord == word) {
                        $scope.rev = !$scope.rev;
                    } else {
                        $scope.col_ord = word;
                        $scope.rev = false;
                    }
                }
                /*
                 Funcion que �rmite la animacion del agregar nueva orden
                 */
                $scope.BtnOrden = function (v) {
                    if (v) {
                        $('#PDatosOrden').show("slow");
                        $('#BtnOrden').hide("slow");
                    } else {
                        $('#PDatosOrden').hide("slow");
                        $('#BtnOrden').show("slow");
                    }

                }
				/*
                 Funcion que oculta los detalles de la orden y el resumen para hacer el tracking
				 X=true oculta panel de detalles y muestra el panel de las opciones del tracking.
				 X=false muestra el panel de detalles y oculta el panel de las opciones del tracking.
				 Y=1 despachar orden.
                 */
                $scope.mostrarOpciones = function (X,Y) {
					console.log($scope.editando);
					if(($scope.factura.estado==4||$scope.factura.estado==5)&&$scope.editando==true){
						//voy a cancelar la modificacion de un envio
						$scope.opcionTracking=Y;
						$scope.editando=false;
						return;
					}
					if(X==true){
						$scope.mostrarDetalles=false;
						$scope.mostrar=true;
						$scope.opcionTracking=Y;
						if(Y==1){
							$scope.inicarMapas();
						}
						if(Y==2){
							$scope.inicarMapaRastreo();
						}
					}else{
						$scope.TerminarPanelEspera('panelMapaPedido');
						$scope.TerminarPanelEspera('panelMapaAlmacen');
						$scope.mostrarDetalles=true;
						$scope.mostrar=false;
					}
                }
				/*
                 Funcion que retorna un objeto de tipo fecha
                 */
                $scope.getFecha = function (cadena) {
                    var fecha =new Date(cadena);
					return fecha;
                }

                /*
                 Funcion que �rmite la animacion del agregar editar orden
                 */
                $scope.BtnOrden = function (v) {
                    if (v) {
                        $('#PDatosOrden').show("slow");
                        $('#BtnOrden').hide("slow");
                    } else {
                        $('#PDatosOrden').hide("slow");
                        $('#BtnOrden').show("slow");
                    }

                }

                /*
                 Funcion que carga los datos de una orden
                 */
                $scope.cargar_datos = function () {
					//manda a inicar los paneles de espera de los mapas
                    $http({
                        url: path + 'orden/one',
                        params: {id: $scope.factura.idFactura},
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.factura= response.orden;
						$scope.almacenes= response.almacenes;
						if($scope.factura.estado==4||$scope.factura.estado==5){
								$scope.obtener_posicion();
						}
                    });
                };

                /*
                 Funcion que edita los datos de una orden
                 */
                $scope.edit_or_update = function (orden) {
                    if (orden.idOrden == undefined || orden.idOrden == null) {
                        var file = $scope.extra.imagen;
                    } else {
                        var file = orden.imagen;
                        orden.imagen = null;
                    }
                    var fd = new FormData();
                    fd.append('file', file);
                    fd.append('orden', JSON.stringify(orden));
                    $http.post("orden/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error != false) {
                            $scope.ordens = response.orden;
                            $('#filename').fileinput();
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                            $scope.norden = {ico: "ion-fork"};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una orden
                 */
                $scope.eliminar = function (orden) {
                    $http({
                        url: path + 'orden/delete',
                        method: 'post',
                        data: {id: orden.idOrden},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ordens = response.orden;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que cambia los estados de una orden
                 */
                $scope.estado = function (orden) {
                    $http({
                        url: path + 'orden/update_stus',
                        method: 'post',
                        data: {id: orden.idOrden},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ordens = response.orden;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                $scope.estados = [];
                $scope.estados ["-1"] = "Cancelada";
                $scope.estados [3] = "Por enviar";
                $scope.estados [4] = "En camino hacia el almacen";
                $scope.estados [5] = "En camino hacia el cliente";
				$scope.estados [6] = "Entregado";
				
				//funcion que me dice si la api de google esta cargada. (deberia ser una funcion global)
				$scope.checkLoaded = function () {
					if (typeof google == "undefined" || typeof google.maps == "undefined") {
						return false; //no cargo la api
					} else {
						return true;
					}
				}
				
				//funcion que carga el api de google maps. (deberia ser una funcion global)
				$scope.loadGoogleMaps = function () {
					/*$ionicLoading.show({
						template: 'Cargando google maps<br>Por favor espere...'
					});*/
					$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
						setTimeout(function () {
							//$ionicLoading.hide();
							$scope.pintarMapas();
						}, 1000);
					});
				}
				//funcion que carga el api de google maps. (deberia ser una funcion global)
				$scope.loadGoogleMapsRas = function () {
					/*$ionicLoading.show({
						template: 'Cargando google maps<br>Por favor espere...'
					});*/
					$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
						setTimeout(function () {
							//$ionicLoading.hide();
							$scope.pintarMapaRastreo();
						}, 1000);
					});
				}
				
				//funcion que me dice si hay internet o no. (deberia ser una funcion global)
				$scope.onLine = function () {
					if(navigator.onLine){
                        var respuesta=true;
                         $.ajax({
                          type: "get",
                          async:false,  
                          url: "http://www.pichresearch.com/danzapp/markers.php",
                          success: function (response,xhr, ajaxOptions, thrownError) {
                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                             if(xhr.status==0){
                                  respuesta= false;
                              }
                          }
                        });
                        return respuesta;
                    }else{
                         return false;
                    }
				}
				
				/*
                 Funcion que inica el proceso de pintar los mapas.
                 */
                $scope.inicarMapas = function () {
					if($scope.factura==null){return;$scope.noMapa=true;}
						if (mapaPedido == null&&mapaAlmacen==null) {
							$scope.iniciarPanelEspera('panelMapaPedido',0);$scope.iniciarPanelEspera('panelMapaAlmacen',0);
						}
					if ($scope.checkLoaded()) {
						$scope.pintarMapas();
					} else {
						if($scope.onLine){
							$scope.loadGoogleMaps();
						}else{
							//mostrar mensaje de quye no posee internet y por ende nose puede cargar el mapa
							$scope.TerminarPanelEspera('panelMapaPedido');$scope.TerminarPanelEspera('panelMapaAlmacen');
							$scope.noMapa=true;
						}
					}	
                };
				
				/*
                 Funcion que inica el proceso de pintar el mapa para el rastreo
                 */
                $scope.inicarMapaRastreo = function () {
					if($scope.factura==null){return;$scope.noMapa=true;}
						if (mapaRastreo == null) {
							$scope.iniciarPanelEspera('panelMapaRastreo',0);
						}
					if ($scope.checkLoaded()) {
						$scope.pintarMapaRastreo();
					} else {
						if($scope.onLine){
							$scope.loadGoogleMapsRas();
						}else{
							//mostrar mensaje de quye no posee internet y por ende nose puede cargar el mapa
							$scope.TerminarPanelEspera('panelMapaRastreo');
							$scope.noMapa=true;
						}
					}	
                };
				
				/*
					funcion que pinta el mapa del rastreo del pedido
				*/
				$scope.pintarMapaRastreo=function(){
					var posicion = new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					if (mapaRastreo == null) {
						console.log('no existe el mapa');
						var mapOptions = {
							center: posicion,
							zoom: 14,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						mapaRastreo = new google.maps.Map(document.getElementById("MapaRastreo"), mapOptions);
						$scope.TerminarPanelEspera('panelMapaRastreo');
						$scope.pintarPingPedido(2);
					} else {
						mapaRastreo.setCenter(posicion);
						$timeout(function () {
							$scope.TerminarPanelEspera('panelMapaRastreo');
						}, 800);
						$scope.pintarPingPedido(2);
					}
				}
				
				/*
					funcion que busca la posicion actual del empleado en el proceso del tracking
				*/
				$scope.obtener_posicion = function () {
                    $http({
                        url: "posicionActualEmpleadoAdmin",
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        },
                        params: {
                            idEmpleado: $scope.factura.getmotorizado[0].id,
                            idOrden: $scope.factura.idFactura

                        }
                    }).success(function (response) {
                        $scope.factura.getmotorizado[0].latitud = response.latitud;
                        $scope.factura.estado = response.estado;
                        $scope.factura.getmotorizado[0].longitud = response.longitud;
						if(mapaRastreo != null){
							$scope.pintarPingPedido(2);
						}
                        if (($scope.factura.estado==4||$scope.factura.estado==5)&&$location.url() == '/factura'&&$scope.factura.getmotorizado.length!=0)
                            var v = window.setTimeout($scope.obtener_posicion, 2500);
                        else
                            clearTimeout(v);

                    }).error(function(error){
						if (($scope.factura.estado==4||$scope.factura.estado==5)&&$location.url() == '/factura'&&$scope.factura.getmotorizado.length!=0)
                            var v = window.setTimeout($scope.obtener_posicion, 2500);
                        else
                            clearTimeout(v);
					});
                }
                
				//funcion que determina si puedo cancelar el envio de una orden o no.
				$scope.factible=function(){
					if(($scope.factura.estado==3||$scope.factura.estado==4||$scope.factura.estado==5)
						&&($scope.factura.paypal==0&&$scope.factura.totalCredito==0&&$scope.factura.totalCupones==0)){
						return true;
					}else{
						return false;
					}
				}
				
				
				/*
					funcion que pinta el ´ping del pedido
				*/
				$scope.pintarPingPedido=function(x){
					var posicion = new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					if(x==0){
						//va a pintar el ping en el mapa del pedido
						if(pingPedido==null){
							pingPedido  = new google.maps.Marker({map: mapaPedido, animation: google.maps.Animation.DROP, label:'P'});
						}
						pingPedido.setPosition(posicion);
						var infoWindow = new google.maps.InfoWindow({
							content: 'Posición del pedido'
						});
						google.maps.event.addListener(pingPedido, 'click', function () {
							infoWindow.open(mapaPedido, pingPedido);
						});
					}else{
						if(x==1){
							//va a pintar el ping en el mapa del almacen
							if(pingAlmacen==null){
								pingAlmacen  = new google.maps.Marker({map: mapaAlmacen, animation: google.maps.Animation.DROP, label:'P'});
								var infoWindow = new google.maps.InfoWindow({
									content: 'Posición del pedido'
								});
								google.maps.event.addListener(pingAlmacen, 'click', function () {
									infoWindow.open(mapaPedido, pingAlmacen);
								});
							}
							pingAlmacen.setPosition(posicion);
						}else{
							//va a pintar los pines en el mapa del rastreo
							var posicionA = new google.maps.LatLng($scope.factura.getalmacen.latitud, $scope.factura.getalmacen.longitud);
							var posicionEmp = new google.maps.LatLng($scope.factura.getmotorizado[0].latitud, $scope.factura.getmotorizado[0].longitud);
							
							if(pingPedidoR==null){
								pingPedidoR  = new google.maps.Marker({map: mapaRastreo, animation: google.maps.Animation.DROP, label:'P'});
								var ventana = new google.maps.InfoWindow({content: 'Posición del pedido'});
								google.maps.event.addListener(pingPedidoR, 'click', function () {
									ventana.open(mapaRastreo, pingPedidoR);
								});
							}
							pingPedidoR.setPosition(posicion);
							if($scope.factura.estado==4||$scope.factura.estado==5){
								if(pingAlmacenR==null){
									pingAlmacenR  = new google.maps.Marker({map: mapaRastreo, animation: google.maps.Animation.DROP, icon:'joli/img/pines/pin-almacen.png'});
									var ventanaA = new google.maps.InfoWindow({content:'<span id="tituloPA"></span>'});
									google.maps.event.addListener(pingAlmacenR, 'click', function () {
										ventanaA.open(mapaRastreo, pingAlmacenR);
										$('#tituloPA').text($scope.factura.getalmacen.nombre);
									});
								}
								pingAlmacenR.setPosition(posicionA);
								
								if(pingEmpR==null){
									pingEmpR  = new google.maps.Marker({map: mapaRastreo, animation: google.maps.Animation.DROP, icon:'joli/img/pines/pin-moto-green.png'});
									var ventanaE = new google.maps.InfoWindow({
										content:'<span id="tituloNom"></span><br><span id="tituloPM"></span><br>Distancia Total: <span id="disT"></span> Km<br>Tiempo estimado:  <span id="tiemT"></span> Min.'
									});
									google.maps.event.addListener(pingEmpR, 'click', function () {
										ventanaE.open(mapaRastreo, pingEmpR);
										$('#tituloNom').text($scope.factura.getmotorizado[0].name);
										$('#tituloPM').text($scope.estados[$scope.factura.estado]);
										$('#disT').text($scope.factura.distancia_envio);
										$('#tiemT').text($scope.factura.tiempoEnvio);
									});
								}
								pingEmpR.setPosition(posicionEmp);
							}
						}
					}
					
				}
				
				/*
					funcion que pinta el mapa del pedido
				*/
				$scope.pintarMapaPedido=function(){
					var posicion = new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					if (mapaPedido == null) {
						console.log('no existe el mapa');
						var mapOptions = {
							center: posicion,
							zoom: 14,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						mapaPedido = new google.maps.Map(document.getElementById("mapaDelPedido"), mapOptions);
						$scope.TerminarPanelEspera('panelMapaPedido');
						$scope.pintarPingPedido(0);
					} else {
						mapaPedido.setCenter(posicion);
						$timeout(function () {
							$scope.TerminarPanelEspera('panelMapaPedido');
						}, 800);
						$scope.pintarPingPedido(0);
					}
				}
				
				/*
					funcion que pinta el mapa de los almacenes
				*/
				$scope.pintarMapaAlmacenes=function(){
					var posicion = new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					if (mapaAlmacen == null) {
						console.log('no existe el mapa de almacenes');
						var mapOptions = {
							center: posicion,
							zoom: 14,
							streetViewControl: false,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						};
						mapaAlmacen = new google.maps.Map(document.getElementById("mapaDelAlmacen"), mapOptions);
						$scope.pintarPingPedido(1);
						$scope.pintarPinesAlmacenes($scope.almacenes);
					} else {
						console.log('existe el mapa de almacenes');
						mapaAlmacen.setCenter(posicion);
						$timeout(function () {
							$scope.TerminarPanelEspera('panelMapaPedido');
						}, 800);
						$scope.pintarPingPedido(1);
					}
				}
				
				/*
					funcion para los mapas de almacen y del pedido
				*/
				$scope.pintarMapas=function(){
					$scope.pintarMapaPedido();
					$scope.pintarMapaAlmacenes();
				}
				

				//funcion para inicalkizar variables
				$scope.inicalizarVariables=function(){
					$scope.noMapa=false;$scope.opcionTracking=0;
					//envio.empresa =1 ivoy; envio.empresa =2 medios propios; 
					$scope.envio={empresa:null,almacen:null}
					$scope.factura= JSON.parse(localStorage.factura);
					$scope.error={};$scope.motorizados=[];
					mapaPedido=null; mapaAlmacen=null; pingPedido=null; markerCache = [];
					$scope.editando=false;
				}
				
				//funcio0n para loading
				function panel_refresh(panel,top){        
					if(!panel.hasClass("panel-refreshing")){
						panel.append('<div class="panel-refresh-layer col-md-12 col-sm-12 col-lg-12"><img src="img/loaders/default.gif"/></div>');
						panel.find(".panel-refresh-layer").height(panel.height());
						if(top!=0){
							panel.find(".panel-refresh-layer").height(panel.height()+50);
							panel.find(".panel-refresh-layer").css('top',top+'px')
						}
						panel.addClass("panel-refreshing");
						
					}
				}
				
				//funcion que inicailiza un panel de espera
				$scope.iniciarPanelEspera=function(id,top){
					var panel = $("#"+id);
					panel_refresh(panel,top);
					return false;
				}
				
				//funcion que finaliza un panel de espera
				$scope.TerminarPanelEspera=function(id){
					var panel = $("#"+id);
					panel.find(".panel-refresh-layer").remove();
					panel.removeClass("panel-refreshing");  
					return false;
				}
				
				//funcion para abrir el dialogo de confirmacion
				$scope.abrirConfirm=function(){
					$('#confirmCancelar').show();
					playAudio('alert');
				}
				
				//funcion para cerrar el dialogo de confirmacion
				$scope.cancelarOrden=function(){
					$scope.cerrarConfirm();
					console.log('cancelar orden');
				}
				
				//funcion para cerrar el dialogo de confirmacion
				$scope.cerrarConfirm=function(){
					$('#confirmCancelar').hide();
				}
				
				//funcion que para realizar los calculos de ivoy
				$scope.envioIvoy=function(){
					var aux;
					if($scope.envio.almacen==null){
						$scope.error.titulo="Alerta";$scope.error.class="message-box-info";
						$scope.error.icono="fa-info";$scope.error.mensaje='Se debe seleccionar un almacen.';
						$('#alertError').show();
						playAudio('alert');
						$scope.envio.empresa=null;
						return;
					}
					for (var j = 0; j < markerCache.length; j++) {
						if(markerCache[j].id!='PA'){
							markerCache[j].marker.setMap(null);
						}else{
							aux=markerCache[j];
						}
					}
					markerCache=[];
					markerCache.push(aux);
				}
				
				//funcion que para buscar los motorizados por medios propios
				$scope.envioPropio=function(){
					if($scope.envio.almacen==null){
						$scope.error.titulo="Alerta";$scope.error.class="message-box-info";
						$scope.error.icono="fa-info";$scope.error.mensaje='Se debe seleccionar un almacen.';
						$('#alertError').show();
						playAudio('alert');
						$scope.envio.empresa=null;
						return;
					}
					//ahora debo mandar a buscar todos los motorizados disponibles
					$scope.iniciarPanelEspera('panelMapaPedido',0);
					$http({
						url:path+ 'buscarEmpleadosDisponibles',
						method: 'Get',
						params: {idOden:$scope.factura.idFactura},
						headers: {
							"Content-Type": "application/json"
						}

					}).success(function (response) {
						if(response.error==false){
							$scope.motorizados=response.motorizados;
							$scope.pintarPinesMotorizados($scope.motorizados,'PM');
						}else{
							$scope.error.titulo="Error";$scope.error.class="message-box-danger";
							$scope.error.icono="fa-times";$scope.error.mensaje=response.mensaje;
							$('#alertError').show();
						}
					}).error(function (error) {
						$scope.TerminarPanelEspera('panelMapaPedido');
					});
					
				}
				
				
				//funcion que verifica si un marcador existe en la cache pertinen
				$scope.markerExists=function (cache,id,distancia,retornar,nombre) {
					var exists = false;
					for (var i = 0; i < cache.length; i++) {
						if (cache[i].id == id) {
							if(distancia!=null){
								var inf =''+nombre+distancia.toFixed(2)+" Km";
								cache[i].ventana.setContent(inf);
								cache[i].marker.record.distancia=distancia;
							}
							if(retornar){
								exists=cache[i];
							}else{
								exists = true;
							}
						}
					}
					return exists;
				}
				
				
				//funcion que agrega la info a un marcador
				$scope.addInfoWindow=function(marker, message) {

					var infoWindow = new google.maps.InfoWindow({
						content: message
					});

					google.maps.event.addListener(marker, 'click', function () {
						console.log(marker);
						infoWindow.open(mapaPedido, marker);
						$timeout(function () {
							infoWindow.close();
						}, 5000);
						console.log(marker);
						if(marker.record.dni==null){
							//quiere decir que es un pin de algun almacen que presione
							if(marker.record.idP==null){
								$scope.$apply(function () {
									$scope.envio.almacen=marker.record;
									$scope.envio.almacen.distancia=marker.record.distancia;
								});
								//mando a pintar el almacen seleccionado en el mapa de los motorizados
								$scope.pintarPingAlmacen(marker.record);
							}else{
								//es el ping del alamacen selecionado en el mapa de pedido
							}
						}else{
							//es un pin de algun motorizado que se presiono
							$scope.$apply(function () {
								$scope.envio.empresa.motorizado=marker.record;
							});
							$scope.cambiarColor(marker,'PM',null);
						}
						
					});
					return infoWindow;
				}
				
				$scope.modificarEnvio=function(){
					$scope.mostrarOpciones(true,1);
					$scope.editando=!$scope.editando;
					//procedo a inicializar las variables segun el envio actual
					$scope.envio.almacen=$scope.factura.getalmacen;
					var start=new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					var end=new google.maps.LatLng($scope.factura.getalmacen.latitud,$scope.factura.getalmacen.longitud);
					$scope.envio.almacen.distancia=(google.maps.geometry.spherical.computeDistanceBetween(start, end)/1000);
					$scope.pintarPingAlmacen($scope.envio.almacen);
					if($scope.factura.tipo_envio==2){
						$scope.cambiarColor(null,'PM',$scope.factura.getmotorizado[0].id);
						var start=new google.maps.LatLng($scope.factura.getmotorizado[0].latitud, $scope.factura.getmotorizado[0].longitud);
						//el envio se hizo por medios propios
						$scope.envio.empresa={id:2,name:'Medios propios',motorizado:$scope.factura.getmotorizado[0]};
						$scope.envio.empresa.motorizado.distancia=(google.maps.geometry.spherical.computeDistanceBetween(start, end)/1000);
						$scope.envioPropio();
						window.setTimeout(function(){$('#ck2').prop('checked',true) }, 1000);
					}else{
						//el envio se hizo por 
						window.setTimeout(function(){$('#ck1').prop('checked',true) }, 1000);
						$scope.envio.empresa={id:1,name:'Ivoy'};
					}
				}
				
				$scope.cambiarColor=function(marcador,tipo,id){
					if(tipo=='PM'){
						//voy a cambiarle el color a el ping de un motorizado
						var M=null;
						for (var i = 0; i < markerCache.length; i++) {
							if(markerCache[i].id!='PA'){
								markerCache[i].marker.setIcon("joli/img/pines/pin-moto-red.png");
								if(markerCache[i].id==('PM'+id)){
									markerCache[i].marker.setIcon("joli/img/pines/pin-moto-green.png");
								}
							}
						}
						if(marcador!=null){
							marcador.setIcon("joli/img/pines/pin-moto-green.png");
						}
					}else{
						//voy a cabiarle el color a un ping de un almacen
					}
				}
				
				//funcion que pinta el ping del almacen en la mapa del pedido
				$scope.pintarPingAlmacen=function(almacen){
					$scope.iniciarPanelEspera('panelMapaPedido',0);
					var arreglo=[];
					arreglo.push(almacen);
					$scope.pintarPinesMotorizados(arreglo,'PA');
					if($scope.envio.empresa!=null&&$scope.envio.empresa.id==2){
						console.log('actualizando los motorizados');
						$scope.pintarPinesMotorizados($scope.motorizados,'PM');
					}else{
						console.log('no actualizo nada');
					}
				}
				
				
				
				//funcion para pintar pines en un mapa dado.
				//mapa=mapa en el cual se pintaran los marcadores
				//records=marcadores que se pintaran
				$scope.pintarPinesMotorizados=function(records,prefijo){
					//calcula la posicion del pedido
					if(prefijo=='PA'){
						//si es el ping del almacen seleccionado
						var start=new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					}else{
						//si es un ping de motorizado
						var start=new google.maps.LatLng($scope.envio.almacen.latitud, $scope.envio.almacen.longitud);
					}
					var idPA=null;
					for (var i = 0; i < records.length; i++) {
						var record = records[i];
						var end=new google.maps.LatLng(record.latitud,record.longitud);
						if(prefijo=='PA'){
							//si es el ping del almacen seleccionado
							var idPing='PA';var icono='joli/img/pines/pin-almacen.png';
							var name=record.nombre+'<br>Distancia lineal al pedido: ';
						}else{
							//si es un ping de motorizado
							var idPing=prefijo+record.id;var icono='joli/img/pines/pin-moto-red.png';
							var name=record.name+'<br>Distancia lineal al almacen: ';
						}
						//calculo la distancia expresada en km, entre el punto del pedido y el punto del motorizado o almacen segun sea el caso
						record.distancia=(google.maps.geometry.spherical.computeDistanceBetween(start, end)/1000);
						if (!$scope.markerExists(markerCache,idPing,null,false,null)) {
							console.log('record no existe');
							if($scope.envio.empresa!=null&&$scope.envio.empresa.id==2&&$scope.envio.empresa.motorizado!=null&&$scope.envio.empresa.motorizado.id==record.id){
								icono='joli/img/pines/pin-moto-green.png';
							}
							//se crea y se agrega el marcador si este no existe
							var markerPos = new google.maps.LatLng(record.latitud, record.longitud);
							var marker = new google.maps.Marker({map: mapaPedido, animation: google.maps.Animation.DROP,position: markerPos,icon:icono});
							marker.record=record;
							// Add the marker to the markerCache so we know not to add it again later
							var markerData = {marker: marker,ventana:null,id:idPing};
							var info=name+record.distancia.toFixed(2)+" Km";
							if(prefijo!='PA'){
								//si es un ping de motorizado
								var infoWindowContent =info;
								markerData.ventana=$scope.addInfoWindow(marker, infoWindowContent);
							}else{
								//si es el ping del lamacen seleccionado
								var infoWindow = new google.maps.InfoWindow({
									content: info
								});
								google.maps.event.addListener(marker, 'click', function () {
									infoWindow.open(mapaPedido, marker);
								});
								markerData.ventana=infoWindow;
							}
							markerCache.push(markerData);
						}else{
							console.log('record existe');
							 //actualizo la distancia entre el pedido y el motoizado
							if(prefijo=='PA'){
								var name=record.nombre+'<br>Distancia lineal al pedido: ';
							}else{
								var name=record.name+'<br>Distancia lineal al almacen: ';
							}
							 var X= $scope.markerExists(markerCache,idPing,record.distancia,true,name);
							if(record.dni==null){
								//es un almacen
								X.marker.setPosition(end);
								
							}
							
						}
					}
					$scope.TerminarPanelEspera('panelMapaPedido');
				}
				
				
				//funcion para pintar los pines de los almacenes.
				$scope.pintarPinesAlmacenes=function(records){
					//calcula la posicion del pedido
					var start=new google.maps.LatLng($scope.factura.direccion.latitud, $scope.factura.direccion.longitud);
					for (var i = 0; i < records.length; i++) {
						var record = records[i];
						var end=new google.maps.LatLng(record.latitud,record.longitud);
						//calculo la distancia expresada en km, entre el punto del pedido y el punto del motorizado o almacen segun sea el caso
						record.distancia=(google.maps.geometry.spherical.computeDistanceBetween(start, end)/1000);
						//se crea y se agrega el marcador si este no existe
						var markerPos = new google.maps.LatLng(record.latitud, record.longitud);
						var marker = new google.maps.Marker({map: mapaAlmacen, animation: google.maps.Animation.DROP,position: markerPos,icon:'joli/img/pines/pin-almacen.png'});
						//creo la ventana
						marker.record=record;
						var info=record.nombre+"<br>Distancia lineal al pedido: "+record.distancia.toFixed(2)+" Km";
						var infoWindowContent =info;
						$scope.addInfoWindow(marker, infoWindowContent);
					}
					$scope.TerminarPanelEspera('panelMapaAlmacen');
				}
				
				$scope.peticion=function(){
					$http({
						url:path+ 'envio/save',
						method: 'Get',
						params: {datos: JSON.stringify($scope.envio),idOrden:$scope.factura.idFactura},
						headers: {
							"Content-Type": "application/json"
						}

					}).success(function (response) {
						$scope.TerminarPanelEspera('tracking');
						if(response.error==false){
							$scope.error.titulo="Notificación";$scope.error.class="message-box-success";
							$scope.error.icono="fa-check";$scope.error.mensaje='Se ha iniciado el proceso de envio del pedido exitosamente';
							$('#alertError').show();
							$scope.factura= response.orden;
							$scope.obtener_posicion();
							$scope.mostrarOpciones(true,2);
							$scope.inicarMapaRastreo();
						}else{
							$scope.error.titulo="Error";$scope.error.class="message-box-danger";
							$scope.error.icono="fa-times";$scope.error.mensaje=response.mensaje;
							$('#alertError').show();
						}
					}).error(function (error) {
						$scope.TerminarPanelEspera('tracking');
						$scope.error.titulo="Error";$scope.error.class="message-box-danger";
						$scope.error.icono="fa-times";$scope.error.mensaje='Ocurrio un error al intentar procesar la solicitud';
						$('#alertError').show();
						$timeout(function () {
							$('#alertError').hide();
						}, 2500);
					});
				}
				
				//funcion para guardar el envio en la base de datos
				$scope.guardarEnvio=function(){
					$scope.iniciarPanelEspera('tracking',100);
					if($scope.envio.empresa.id==2){
						$.ajax({
						  type: "get",
						  async:false,  
						  url: "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+$scope.envio.empresa.motorizado.latitud+","+$scope.envio.empresa.motorizado.longitud+"|"+$scope.envio.almacen.latitud+","+$scope.envio.almacen.longitud+"&destinations="+$scope.envio.almacen.latitud+","+$scope.envio.almacen.longitud+"|"+$scope.factura.direccion.latitud+","+$scope.factura.direccion.longitud+"&key=AIzaSyCEl35ZWrgoLFHMmsELAVc1ijjJTxYu9KI",
						  success: function (response,xhr, ajaxOptions, thrownError) {
							  var distancia=0;  var tiempo=0;
							  for(var i=0; i<response.rows[0].elements.length;i++){
								  console.log("T",response.rows[0].elements[i].duration.value);
								  console.log("D",response.rows[0].elements[i].distance.value);
								  distancia+=response.rows[0].elements[i].distance.value;
								  tiempo+=response.rows[0].elements[i].duration.value;
							  }
							  console.log(distancia,tiempo);
							  $scope.envio.distanciaTotal=distancia;
							  $scope.envio.tiempoTotal=tiempo;
							 $scope.peticion();
						  },
						  error: function (xhr, ajaxOptions, thrownError) {
							$scope.peticion();
						  }
						});
					}
				}
				
				//funcion que cierra el alerta de error
				$scope.cerraAlert=function(){
					$('#alertError').hide();
				}
				
				var mapaPedido=null; var mapaAlmacen=null; var pingPedido=null; var pingAlmacen=null; var mapaRastreo=null;
				var pingPedidoR=null; var pingAlmacenR=null; var pingEmpR=null;
				angular.element(document).ready(function () {
					console.log('angular ready');
					$scope.inicalizarVariables();
					$scope.cargar_datos();
				});
            }])
			
			
