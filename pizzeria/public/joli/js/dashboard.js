'use strict';
var path = "";



		
		
angular.module('myApp.dashboard', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/dashboard', {
                    templateUrl: 'joli/pages/dashboard.html',
                    controller: 'HomeCtrl'
                });
            }])

// Home controller
        .controller('HomeCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'dolar/listar',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.paginas = response.paginas;

                    });
                };


                $scope.cambiar_estado = function (pagina) {
                    $http({
                        url: path + 'pagina/estado?id=' + pagina.id,
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        pagina.estado = response.pagina.estado;
                        console.log(response);
                        $scope.paginas[0] = response.pagina_real;
                    });
                };

                $scope.cargar_datos();




            }])

        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])



angular.module('myApp.general', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/general', {
                    templateUrl: 'joli/pages/general.html',
                    controller: 'GeneralCtrl'
                });
            }])

// Home controller
        .controller('GeneralCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.general = {ico: "ion-fork"};

                /*
                 Funcion que carga los datos de una categoria
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'general/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {

                        $scope.datos = response.datos;


                    });
                };

                /*
                 Funcion que edita los datos de una categoria
                 */
                $scope.edit_or_update = function (general) {
                    var fd = new FormData();
                    fd.append('general', JSON.stringify(general));
                    $http.post("general/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error != false) {
                            $scope.general = response.general;


                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una categoria
                 */
                $scope.eliminar = function (categoria) {
                    $http({
                        url: path + 'categoria/delete',
                        method: 'post',
                        data: {id: categoria.idCategoria},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que cambia los estados de una categoria
                 */
                $scope.estado = function (categoria) {
                    $http({
                        url: path + 'categoria/update_stus',
                        method: 'post',
                        data: {id: categoria.idCategoria},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                $scope.iconos = [["ion-fork", "ion-knife", "ion-spoon"], ["ion-beer", "ion-wineglass", "ion-coffee"], ["ion-icecream", "ion-pizza",
                        "ion-aperture"], ["ion-flash", "ion-bag", "ion-tshirt-outline"], ["ion-ribbon-b", "ion-egg", "ion-ios-medical"], ["ion-ios-medkit-outline",
                        "ion-ios-musical-notes", "ion-ios-wineglass-outline"], ["ion-ios-pint", "ion-ios-nutrition-outline", "ion-ios-paw-outline"]];



                $scope.cargar_datos();




            }])

        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])




angular.module('myApp.categoria', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/categoria', {
                    templateUrl: 'joli/pages/categoria.html',
                    controller: 'CategoriaCtrl'
                });
            }])

// Home controller
        .controller('CategoriaCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.ncategoria = {ico: "ion-fork"};

                /*
                 Funcion que �rmite la animacion del agregar nueva categoria
                 */
                $scope.BtnCategoria = function (v) {
                    if (v) {
                        $('#PDatosCategoria').show("slow");
                        $('#BtnCategoria').hide("slow");
                    } else {
                        $('#PDatosCategoria').hide("slow");
                        $('#BtnCategoria').show("slow");
                    }

                }

                /*
                 Funcion que �rmite la animacion del agregar editar categoria
                 */
                $scope.BtnCategoria = function (v) {
                    if (v) {
                        $('#PDatosCategoria').show("slow");
                        $('#BtnCategoria').hide("slow");
                    } else {
                        $('#PDatosCategoria').hide("slow");
                        $('#BtnCategoria').show("slow");
                    }

                }

                /*
                 Funcion que carga los datos de una categoria
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'categoria/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ncategoria = {icono: "ion-fork"};
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que edita los datos de una categoria
                 */
                $scope.edit_or_update = function (categoria) {
                    if (categoria.idCategoria == undefined || categoria.idCategoria == null) {
                        var file = $scope.extra.imagen;
                    } else {
                        var file = categoria.imagen;
                        categoria.imagen = null;
                    }
                    var fd = new FormData();
                    fd.append('file', file);
                    fd.append('categoria', JSON.stringify(categoria));
                    $http.post("categoria/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error != false) {
                            $scope.categorias = response.categoria;
                            $('#filename').fileinput();
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                            $scope.ncategoria = {ico: "ion-fork"};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una categoria
                 */
                $scope.eliminar = function (categoria) {
                    $http({
                        url: path + 'categoria/delete',
                        method: 'post',
                        data: {id: categoria.idCategoria},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que cambia los estados de una categoria
                 */
                $scope.estado = function (categoria) {
                    $http({
                        url: path + 'categoria/update_stus',
                        method: 'post',
                        data: {id: categoria.idCategoria},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                $scope.iconos = [["ion-fork", "ion-knife", "ion-spoon"], ["ion-beer", "ion-wineglass", "ion-coffee"], ["ion-icecream", "ion-pizza",
                        "ion-aperture"], ["ion-flash", "ion-bag", "ion-tshirt-outline"], ["ion-ribbon-b", "ion-egg", "ion-ios-medical"], ["ion-ios-medkit-outline",
                        "ion-ios-musical-notes", "ion-ios-wineglass-outline"], ["ion-ios-pint", "ion-ios-nutrition-outline", "ion-ios-paw-outline"],
                    ["icon-botanas", "icon-botella-2", "icon-botellal"], ["icon-otros", "icon-promociones"]];



                $scope.cargar_datos();




            }])

        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])


angular.module('myApp.producto', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/producto', {
                    templateUrl: 'joli/pages/producto.html',
                    controller: 'ProductoCtrl'
                });
            }])

// Home controller
        .controller('ProductoCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.nproducto = {ico: "ion-fork"};
                $scope.sel = {producto_sel: null};
                $scope.decimal = '\\d+(.\\d+)?';
                $scope.entero = '\\d+';

                /*
                 Funcion que la animacion del agregar nueva producto
                 */
                $scope.order_by = function (word) {
                    if ($scope.col_ord == word) {
                        $scope.rev = !$scope.rev;
                    } else {
                        $scope.col_ord = word;
                        $scope.rev = false;
                    }
                }

                $scope.order_by2 = function (word) {
                    if ($scope.col_ord2 == word) {
                        $scope.rev2 = !$scope.rev2;
                    } else {
                        $scope.col_ord2 = word;
                        $scope.rev2 = false;
                    }
                }
                /*
                 Funcion que �rmite la animacion del agregar nueva producto
                 */
                $scope.BtnProducto = function (v) {

                    if (v) {
                        $('#PDatosProducto').show("slow");
                        $('#BtnProducto').hide("slow");
                        $('#BtnPresentacion').hide("slow");
                    } else {
                        $('#PDatosProducto').hide("slow");
                        $('#BtnProducto').show("slow");
                        $('#BtnPresentacion').show("slow");

                    }

                }

                $scope.BtnPresentacion = function (v) {

                    if (v) {
                        $('#PDatosPresentacion').show("slow");
                        $('#BtnPresentacion').hide("slow");
                        $('#BtnProducto').hide("slow");
                    } else {
                        $('#PDatosPresentacion').hide("slow");
                        $('#BtnPresentacion').show("slow");
                        $('#BtnProducto').show("slow");
                    }

                }





                /*
                 Funcion que carga los datos de una producto y las categorias
                 */
                $scope.cargar_datos = function () {

                    $http({
                        url: path + 'producto/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.nproducto = {};
                        $scope.productos = response.producto;
                        $scope.categorias = response.categoria;
                        $('#filename').fileinput();
                        if ($scope.productos.length > 0)
                            $scope.sel.producto_sel = $scope.productos[0];
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que edita los datos de una producto
                 */
                $scope.edit_or_update = function (producto) {

                    if (producto.categoria == undefined || producto.categoria == null) {
                        alert('Seleccione una categoria');
                        return;
                    }
                    var fd = new FormData();
                    fd.append('producto', JSON.stringify(producto));
                    $http.post("producto/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error == false) {
                            $scope.productos = response.producto;
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);

                            $scope.nproducto = {};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una producto
                 */
                $scope.eliminar = function (producto) {
                    $http({
                        url: path + 'producto/delete',
                        method: 'post',
                        data: {id: producto.idProducto},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.productos = response.producto;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };

                /*
                 Funcion que cambia los estados de una producto
                 */
                $scope.estado = function (producto) {
                    $http({
                        url: path + 'producto/update_stus',
                        method: 'post',
                        data: {id: producto.idProducto},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.productos = response.producto;
                        $('#filename').fileinput();
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);

                    });
                };


                /* ESPACIO DE LAS PRESENTACIONES*/

                $scope.input_file = function () {
                    setTimeout(function () {
                        $('td #filename').fileinput();
                    }, 1000);
                }
                /*M�tdo que inserta una nueva presentacion*/
                $scope.edit_or_update_presentacion = function (presentacion) {
                    var file = presentacion.imagen;
                    //presentacion.imagen= null;
                    presentacion.producto = $scope.sel.producto_sel.idProducto;
                    var fd = new FormData();
                    fd.append('file', file);

                    fd.append('presentacion', JSON.stringify(presentacion));
                    $http.post("presentacion/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error == false) {

                            $scope.sel.producto_sel.obtener_precios = response.presentaciones;
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                            $scope.npresentacion = {};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una producto
                 */
                $scope.eliminar_presentacion = function (presentacion) {
                    $http({
                        url: path + 'presentacion/delete',
                        method: 'post',
                        data: {id: presentacion.id},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.sel.producto_sel.obtener_precios = response.presentaciones;
                        setTimeout(function () {
                            $('td #filename').fileinput();
                        }, 1000);
                        $scope.npresentacion = {};

                    });
                };

                /*
                 Funcion que cambia los estados de una producto
                 */
                $scope.estado_presentacion = function (presentacion) {
                    $http({
                        url: path + 'presentacion/update_stus',
                        method: 'post',
                        data: {id: presentacion.id},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        if (response.error == false) {
                            $scope.sel.producto_sel.obtener_precios = response.presentaciones;
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                            setTimeout(function () {
                                $('td #filename').fileinput();
                            }, 1000);
                        }
                    });
                };

                /*
                 Funcion que agrega descuentos
                 */
                $scope.add_descuento_presentacion = function (presentacion) {
                    $http({
                        url: path + 'presentacion/update_descuento',
                        method: 'post',
                        data: {id: presentacion.id, cantidad: presentacion.cantidad_desc, valor: presentacion.descuento},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        if (response.error == false) {

                        }
                    });
                };

                /*	
                 FUNCION QUE AUMENTA LA CANTIDAD O LA DIMINUYE DEPENDIENDO DEL TIPO
                 */
                /*
                 Funcion que agrega descuentos
                 */
                $scope.add_dis_presentacion = function (presentacion, cantidad, tipo) {

                    $http({
                        url: path + 'presentacion/add_dis',
                        method: 'post',
                        data: {id: presentacion.id, cantidad: cantidad, tipo: tipo},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        if (response.error == false) {
                            if (tipo == "+") {
                                presentacion.stock = parseInt(presentacion.stock) + parseInt(cantidad);
                                presentacion.stock_r = parseInt(presentacion.stock_r) + parseInt(cantidad);
                            } else {
                                presentacion.stock = parseInt(presentacion.stock) - parseInt(cantidad);
                                presentacion.stock_r = parseInt(presentacion.stock_r) - parseInt(cantidad);

                            }
                        }
                    });
                };

                /*---------------------------------------*/
                $scope.iconos = [["ion-fork", "ion-knife", "ion-spoon"], ["ion-beer", "ion-wineglass", "ion-coffee"], ["ion-icecream", "ion-pizza",
                        "ion-aperture"], ["ion-flash", "ion-bag", "ion-tshirt-outline"], ["ion-ribbon-b", "ion-egg", "ion-ios-medical"], ["ion-ios-medkit-outline",
                        "ion-ios-musical-notes", "ion-ios-wineglass-outline"], ["ion-ios-pint", "ion-ios-nutrition-outline", "ion-ios-paw-outline"]];



                $scope.cargar_datos();




            }])

        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])



angular.module('myApp.empleado', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/empleado', {
                    templateUrl: 'joli/pages/usuario.html',
                    controller: 'EmpleadoCtrl'
                });
            }])

// Home controller
        .controller('EmpleadoCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.ncategoria = {ico: "ion-fork"};

                /*
                 Funcion que �rmite la animacion del agregar nueva categoria
                 */
                $scope.BtnUsuario = function (v) {
                    if (v) {
                        $('#PDatosUsuario').show("slow");
                        $('#BtnUsuario').hide("slow");
                    } else {
                        $('#PDatosUsuario').hide("slow");
                        $('#BtnUsuario').show("slow");
                    }

                }

                /*
                 Funcion que �rmite la animacion del agregar editar usuario
                 */
                $scope.BtnUsuario = function (v) {
                    if (v) {
                        $('#PDatosUsuario').show("slow");
                        $('#BtnUsuario').hide("slow");
                    } else {
                        $('#PDatosUsuario').hide("slow");
                        $('#BtnUsuario').show("slow");
                    }

                }

                /*
                 Funcion que carga los datos de una usuario
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'usuario/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.nusuario = {tipo: "Empleado"};
                        $scope.usuarios = response.empleado;

                    });
                };

                /*
                 Funcion que edita los datos de una usuario
                 */
                $scope.edit_or_update = function (usuario) {
                    var fd = new FormData();
                    fd.append('usuario', JSON.stringify(usuario));
                    $http.post("usuario/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error != false) {
                            $scope.usuarios = response.usuario;
                            $scope.nusuario = {tipo: "Empleado"};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una usuario
                 */
                $scope.eliminar = function (usuario) {
                    $http({
                        url: path + 'usuario/delete',
                        method: 'post',
                        data: {id: usuario.dni},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.usuarios = response.usuario;

                    });
                };

                /*
                 Funcion que cambia los estados de una usuario
                 */
                $scope.estado = function (usuario) {
                    $http({
                        url: path + 'usuario/update_stus',
                        method: 'post',
                        data: {id: usuario.dni},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.usuarios = response.usuario;


                    });
                };




                $scope.cargar_datos();




            }])

        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])



angular.module('myApp.cupon', ['ngRoute'])

// Declared route 
        .config(['$routeProvider', function ($routeProvider) {
                $routeProvider.when('/cupon', {
                    templateUrl: 'joli/pages/cupon.html',
                    controller: 'CuponCtrl'
                });
            }])

// Home controller
        .controller('CuponCtrl', ['$http', '$scope', function ($http, $scope) {

                $scope.ncategoria = {ico: "ion-fork"};

                /*
                 Funcion que �rmite la animacion del agregar nueva categoria
                 */
                $scope.BtnCupon = function (v) {
                    if (v) {
                        $('#PDatosCupon').show("slow");
                        $('#BtnCupon').hide("slow");
                    } else {
                        $('#PDatosCupon').hide("slow");
                        $('#BtnCupon').show("slow");
                    }

                }

                /*
                 Funcion que �rmite la animacion del agregar editar cupon
                 */
                $scope.BtnCupon = function (v) {
                    if (v) {
                        $('#PDatosCupon').show("slow");
                        $('#BtnCupon').hide("slow");
                    } else {
                        $('#PDatosCupon').hide("slow");
                        $('#BtnCupon').show("slow");
                    }

                }

                /*
                 Funcion que carga los datos de una cupon
                 */
                $scope.cargar_datos = function () {
                    $http({
                        url: path + 'cupon/all',
                        method: 'get',
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.ncupon = {tipo: "Cupon"};
                        $scope.cupons = response.cupon;

                    });
                };

                /*
                 Funcion que edita los datos de una cupon
                 */
                $scope.edit_or_update = function (cupon) {
                    var fd = new FormData();
                    fd.append('cupon', JSON.stringify(cupon));
                    $http.post("cupon/save", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).success(function (response) {
                        if (response.error == false) {
                            $scope.cupons = response.cupon;
                            $scope.ncupon = {tipo: "Cupon"};

                        }
                    });
                };

                /*
                 Funcion que elimina los datos de una cupon
                 */
                $scope.eliminar = function (cupon) {
                    $http({
                        url: path + 'cupon/delete',
                        method: 'post',
                        data: {id: cupon.id},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.cupons = response.cupon;

                    });
                };

                /*
                 Funcion que cambia los estados de una cupon
                 */
                $scope.estado = function (cupon) {
                    $http({
                        url: path + 'cupon/update_stus',
                        method: 'post',
                        data: {id: cupon.id},
                        headers: {
                            "Content-Type": "application/json"
                        }

                    }).success(function (response) {
                        $scope.cupons = response.cupon;


                    });
                };




                $scope.cargar_datos();




            }])
			
			

 .controller('generarCtrl', ['$http', '$scope','$rootScope', function ($http, $scope,$rootScope) {
	//controlador para tener variables y funciones globales
		
	/*
	funciones globales
	*/
	$rootScope.error={};$rootScope.confirm={};
	
	//funcion para cerra el alerta
	$rootScope.cerraAlert=function(id){
		$('#'+id).hide();
	}
	
	//funcion para abrir el alerta
	$rootScope.abrirAlert=function(id,sonido){
		$('#'+id).show();
		 playAudio(sonido);
	}
	
	//funcion para abrir el confirm
	$rootScope.abrirConfirm=function(id,sonido){
		$('#'+id).show();
		 playAudio(sonido);
	}
	
	//funcion para cerra el alerta
	$rootScope.cerraConfirm=function(id){
		$('#'+id).hide();
	}
	
	//funcion para ser reutilizada en cada llamada de un alerta
	$rootScope.llamarAbrirAlert=function(titulo,clase,icono,mensaje,sonido){
		$rootScope.error.titulo=titulo;$rootScope.error.class=clase;
		$rootScope.error.icono=icono;$rootScope.error.mensaje=mensaje;
		$rootScope.abrirAlert('alerta',sonido);
	}
	
	
	$rootScope.llamarComfir=function(titulo,clase,icono,mensaje,sonido){
		$rootScope.confirm.titulo=titulo;$rootScope.confirm.class=clase;
		$rootScope.confirm.icono=icono;$rootScope.confirm.mensaje=mensaje;
		$rootScope.abrirConfirm('confirm',sonido);
	}
	
	//funcio0n para loading
	function panel_refresh(panel,top){        
		if(!panel.hasClass("panel-refreshing")){
			panel.append('<div class="panel-refresh-layer col-md-12 col-sm-12 col-lg-12"><img src="img/loaders/default.gif"/></div>');
			panel.find(".panel-refresh-layer").height(panel.height());
			if(top!=0){
				panel.find(".panel-refresh-layer").height(panel.height()+50);
				panel.find(".panel-refresh-layer").css('top',top+'px')
			}
			panel.addClass("panel-refreshing");
			
		}
	}
	
	//funcion que inicailiza un panel de espera
	$rootScope.iniciarPanelEspera=function(id,top){
		var panel = $("#"+id);
		panel_refresh(panel,top);
		return false;
	}
	
	//funcion que finaliza un panel de espera
	$rootScope.TerminarPanelEspera=function(id){
		var panel = $("#"+id);
		panel.find(".panel-refresh-layer").remove();
		panel.removeClass("panel-refreshing");  
		return false;
	}
	
	$rootScope.iniciarPlgImg=function(id){
		$(id).fileinput({
			showUpload:false, 
			showPreview:false, 
			previewFileType:false,
			allowedFileExtensions: ["pdf", "jpg", "gif", "png"],
			removeClass: "btn btn-danger",
			removeLabel: "",
			removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> ",
		});
		$('.form-control.file-caption.kv-fileinput-caption').css('height','36px');
	}
	
	$rootScope.confirmarEliminar=function(elemento,x){
		$rootScope.Y=elemento;
		if(x==1){
			//voy a eliminar una categoria
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la categoria?','fail');
			$rootScope.X=x;
		}
		if(x==2){
			//voy a eliminar un producto
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el producto?','fail');
			$rootScope.X=x;
		}
		if(x==3){
			//voy a eliminar una presentacion
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la presentación?','fail');
			$rootScope.X=x;
		}
		if(x==4){
			//voy a eliminar una propina
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar la propina?','fail');
			$rootScope.X=x;
		}
		if(x==5){
			//voy a eliminar un descuento
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el descuento?','fail');
			$rootScope.X=x;
		}
		if(x==6){
			//voy a eliminar un impuesto
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el impuesto?','fail');
			$rootScope.X=x;
		}
		if(x==7){
			//voy a eliminar un metodo de pago
			$rootScope.llamarComfir("Confirmar","message-box-warning","fa-warning",'Esta seguro que desea eliminar el método de pago?','fail');
			$rootScope.X=x;
		}
		if (x == 8) {
			//voy a eliminar un area
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar el área?', 'fail');
			$rootScope.X = x;
		}
		if (x == 9) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar la caja?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 10) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar la mesa?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 11) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta seguro que desea eliminar el usuario?', 'fail');
			$rootScope.X = x;
		}
                
                 if (x == 12) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta accion aumentará el precio de todos los productos?', 'fail');
			$rootScope.X = x;
		}
                
                if (x == 13) {
			//voy a eliminar una caja
			$rootScope.llamarComfir("Confirmar", "message-box-warning", "fa-warning", 'Esta accion bajará el precio de todos los productos?', 'fail');
			$rootScope.X = x;
		}
	}
	
	$rootScope.intermediaria=function(){
		console.log('intermediaria');
		if($rootScope.X==1){
			$rootScope.llamarEliminarCategoria($rootScope.Y);
		}
		if($rootScope.X==2){
			$rootScope.llamarEliminarProducto($rootScope.Y);
		}
		if($rootScope.X==3){
			$rootScope.llamarEliminarPresentacion($rootScope.Y);
		}
		if($rootScope.X==4){
			$rootScope.llamarEliminarPropina($rootScope.Y);
		}
		if($rootScope.X==5){
			$rootScope.llamarEliminarDescuento($rootScope.Y);
		}
		if($rootScope.X==6){
			$rootScope.llamarEliminarImpuesto($rootScope.Y);
		}
		if($rootScope.X==7){
			$rootScope.llamarEliminarPago($rootScope.Y);
		}
		if ($rootScope.X == 8) {
			$rootScope.llamarEliminarArea($rootScope.Y);
		}
		if ($rootScope.X == 9) {
			$rootScope.llamarEliminarCaja($rootScope.Y);
		}
                if ($rootScope.X == 10) {
			$rootScope.llamarEliminarMesa($rootScope.Y);
		}
                 if ($rootScope.X == 11) {
			$rootScope.llamarEliminarUsuario($rootScope.Y);
		}
                
                if ($rootScope.X == 12) {
			$rootScope.llamarSubirPrecio($rootScope.Y);
		}
                
                 if ($rootScope.X == 13) {
			$rootScope.llamarBajarPrecio($rootScope.Y);
		}
	}
	/*
	funciones globales
	*/

	
}])


        .directive('fileModel', ['$parse', function ($parse) {
                return {
                    restrict: 'A',
                    link: function (scope, element, attrs) {
                        var model = $parse(attrs.fileModel);
                        var modelSetter = model.assign;

                        element.bind('change', function () {
                            scope.$apply(function () {
                                modelSetter(scope, element[0].files[0]);
                            });
                        });
                    }
                };
            }])
