angular.module('ionicApp', ['ionic'])

.controller('SlideController', function($scope, $ionicSlideBoxDelegate) {
  
  $scope.myActiveSlide = 0;

  
  $scope.slidePrevious = function() {
    
    $ionicSlideBoxDelegate.previous();
  }
  
  $scope.slideNext = function() {
    
    $ionicSlideBoxDelegate.next();
  }
  $scope.lockSlide = function () {
        $ionicSlideBoxDelegate.enableSlide( false );
    }

})