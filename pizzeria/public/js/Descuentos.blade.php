@extends('layouts.master')

@section('title', 'Page Title')

@section('head_css')
@parent
     {!! HTML::style('css/jquery.dataTables.min.css') !!}
     {!! HTML::style('css/dataTables.bootstrap.css') !!}
      {!! HTML::style('bootstrap-dialog/css/bootstrap-dialog.min.css') !!}
@stop
@section('head_scripts')
@parent
     <!--script especificos de esta pagina-->
@stop
@section('contenido')
@parent
<div class="panel  Margin-min">
    <!-- contenedor para registrar descuentos-->
    <div id="PAgregarDescuento" class="panel widget light-widget panel-bd-top ">
        
        <div class="panel-body" style="padding-top: 10px; padding-bottom: 10px;">
            <div class=" col-lg-3 col-md-3 col-sm-3">
                 <button id="BtnADescuento" class="btn vd_btn vd_bg-red btn-block" type="button">Nuevo Descuento <i class="fa fa-plus fa-fw"></i></button>
            </div>
        </div>
                                <!-- contenedor para registrar un nuevo descuento-->
                                <div class="" id="PRegistrarCliente" style="display: none">
                                        <div class="panel-body">
                                            <div class="col-lg-12">
                                            <div class="panel-heading vd_bg-default">
                                                <h3 class="colorLetra"> <span class="menu-icon"> <i class="fa fa-pencil-square-o"></i> </span>Datos del nuevo descuento.</h3>
                                                <div class="vd_panel-menu" style="    padding-right: 13px">
                                                    <span class="vd_red">*</span> <span class="vd_red">Indica campos requeridos.</span>
                                                </div>
                                            </div>
                                            <div class="panel-body" style="border: 1px solid #ddd">
                                                <form id="registrarD" method="post">
                                                    <div class="row">
                                                         <div class="col-lg-4 form-group">
                                                            <div class="form-label">Nombre<span class="vd_red">*</span></div>
                                                            <input type="text" id="NombreR" class="mgbt-xs-20 mgbt-sm-0 form-control" name="NombreR" maxlength="100">
                                                        </div>
                                                        <div class="col-lg-4 form-group">
                                                            <div class="form-label">Tipo<span class="vd_red">*</span></div>
                                                            <select id="TipoR" name="TipoR" class="mgbt-xs-20 mgbt-sm-0">
                                                                <option value="porcentaje">Porcentaje</option>
                                                                <option value="importe">Importe</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4 form-group">
                                                            <div class="form-label">Valor<span class="vd_red">*</span></div>
                                                            <input type="text" id="ValorR" class="mgbt-xs-20 mgbt-sm-0 form-control" name="ValorR" maxlength="5">
                                                        </div>
                                                    </div>
                                                    <div class="mgtp-20">
                                                         <div class="row mgbt-xs-0">
                                                             <div class="col-xs-6"></div>
                                                             <div class="col-xs-6 text-right">
                                                                 <button  type="reset" onclick="cancelarReg()" class="btn vd_btn btn-default" style="color: #0E2145!important">
                                                                     Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>
                                                                 </button>

                                                                 <button id="BtnGCliente" type="submit" value="Enviar" class="btn vd_btn vd_bg-green">
                                                                     <!--top-right-success-->
                                                                     Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>
                                                                 </button>

                                                             </div>
                                                         </div>
                                                     </div>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div> <!-- contenedor para registrar un nuevo cliente-->
    </div> 
    <!-- contenedor para mostrar los datos del cliente-->
    <!-- contenedor que muestra la tabla con todos los descuientos.-->
    <div>
         <div class="row" id="Ptabla">
                                        <div class="col-lg-12">
                                             <div class="panel widget">
                                                 <div class="panel-heading vd_bg-grey" style="border-radius: 0px;">
                                                        <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-user"></i> </span>Descuentos Registrados.</h3>
                                                </div>
                                                <div class="panel-body table-responsive">
                                                    <table class="table table-striped" id="data-tables" style="display: none" >
                                                        <thead>
                                                            <tr role="row">
                                                                <th class="celda center">Nombre</th>
                                                                <th class="celda center">Tipo</th>
                                                                <th class="celda center">Valor</th>
                                                                <th class="celda center">Estado</th>
                                                                <th class=""></th>
                                                                <th style="display:none;"></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                           
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- Panel Widget -->
                                        </div>
                                        <!-- col-md-12 -->
                                    </div>
        
    </div>
     <!-- contenedor que muestra la tabla con todos los descuientos.-->
</div>
@endsection
@section('scripts')
@parent
{!! HTML::script('js/jquery.dataTables.min.js') !!}
{!! HTML::script('js/dataTables.bootstrap.js') !!}
{!! HTML::script('js/bootstrapValidator.min.js') !!} 
{!! HTML::script('bootstrap-dialog/js/bootstrap-dialog.min.js') !!} 
    <!--script especificos de esta pagina-->
    <script>
         //para formatear las tablas
        function formato(id) {
                $(id).dataTable(
                   {
                     detroy:true,  
                     language: {
                       "sProcessing": "Procesando...",
                       "sLengthMenu": "Mostrar _MENU_ registros",
                       "sZeroRecords": "No se encontraron resultados",
                       "sEmptyTable": "No hay descuentos registrados en el sistema",
                       "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                       "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                       "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                       "sInfoPostFix": "",
                       "sSearch": "Buscar:",
                       "sUrl": "",
                       "sInfoThousands": ",",
                       "sLoadingRecords": "Cargando...",
                       "oPaginate": {
                           "sFirst": "Primero",
                           "sLast": "Último",
                           "sNext": "Siguiente",
                           "sPrevious": "Anterior"
                       },
                       "oAria": {
                           "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                           "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                       }

                   }

                   }

                   );
            }
        //para validar el formulario de registro
        function validarReg(){
            $('#registrarD').bootstrapValidator({
                fields: {
                        NombreR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un nombre'
                                         },
                                         regexp: {
                                                 regexp: /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ%\s]+$/,
                                                 message: 'Solo se permiten caracteres alfanumericos y el "%".'
                                         }
                                 }
                         },
                         TipoR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un tipo.'
                                         }
                                 }
                         },
                         ValorR: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un valor.'
                                         },
                                         regexp: {
                                                 regexp: /^[0-9]+$/,
                                                 message: 'Solo se permiten caracteres numericos.'
                                         }
                                 }
                         }
                     }
            });
            $('#registrarD').submit(function (e){
                if(e.isDefaultPrevented()){
                }else{
                     e.preventDefault();
                     console.log('se envia');
                     save_method(0, $('#NombreR').val(),$('#TipoR').val(),$('#ValorR').val());
                }
            });
        }
         $(document).ready(function () {
            cargarDatosTabla();
            formato('#data-tables');
            validarReg();
         });
         //para cancelar el registro de un descuento
         function cancelarReg(){
              $('#registrarD').bootstrapValidator('resetForm',true);
              $('#PRegistrarCliente').toggle("fast");
              $('#BtnADescuento').toggle();
         }
         //para mostrar el formulario de registro de un descuento
         $('#BtnADescuento').click(function () {
              $('#PRegistrarCliente').toggle("slow");
              $('#BtnADescuento').toggle("fast");
         });
        //para cargar todos los descuentos
        function cargarDatosTabla(){
            var url = 'Descuentos/all';
            $("#data-tables tbody").empty();
            $.ajax({
                    url: url,
                    dataType: "JSON",
                    type: "get",
                    async: false,
                    
                    success: function (data) {
                       var row="";
                       if(data["descuento"]!=null){
                           $('#data-tables').show();
                            $('#data-tables').DataTable().destroy();
                            $("#data-tables tbody").empty();
                            for (var i = 0; i < data["descuento"].length; i++) {
                                row+='<tr role="row" class="odd gradeX" id="flotante1'+data["descuento"][i].idDescuento+'" value="'+data["descuento"][i].idDescuento+'">';
                                row+='  <td class="celda center">'+data["descuento"][i].nombre+'</td>';
                                row+='  <td class="celda center">'+data["descuento"][i].tipo+'</td>';
                                row+='  <td class="celda center">'+data["descuento"][i].valor+'</td>';
                                 row+='  <td id="estadoD'+data["descuento"][i].idDescuento+'" class="celda center">';
                                 if(data["descuento"][i].estado==1){
                                     row+='<span class="label vd_bg-green">Activo</span>';
                                 }else{
                                      row+='<span class="label vd_bg-red">Inactivo</span>';
                                 }
                                 row+='</td>';
                                row+='  <td class="menu-action celda center" style="width: 141px;">';
                                row+='      <a data-original-title="Editar" onclick="mostrardiv('+data["descuento"][i].idDescuento+','+"'"+data["descuento"][i].nombre+"'"+','+"'"+data["descuento"][i].valor+"'"+','+"'"+data["descuento"][i].tipo+"'"+')" id="btnEditar'+data["descuento"][i].idDescuento+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-yellow vd_yellow"> <i class="fa fa-pencil"></i> </a>';
                                if(data["descuento"][i].estado==1){
                                row+='<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+data["descuento"][i].idDescuento+')" valor='+data["descuento"][i].idDescuento+' id="btnDesact'+data["descuento"][i].idDescuento+'" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>';
                                }else{
                                row+='<span id="status">      <a data-original-title="Activar"  onclick="estado(this, 1, '+data["descuento"][i].idDescuento+')" valor='+data["descuento"][i].idDescuento+' id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>';
                                }
                                row+=' <a data-original-title="Eliminar" id="btnEliminar"    onclick="confirmacion(this, '+data["descuento"][i].idDescuento+')" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-trash"></i> </a>';
                                row+='  </td>';
                                row+='  <td colspan="12" id="flotante2'+data["descuento"][i].idDescuento+'" style="display:none; padding:0;">';
                                row+='   <form class="form-horizontal" method="post" role="form" iden="'+data["descuento"][i].idDescuento+'" id="formulario'+data["descuento"][i].idDescuento+'"> <div class="panel-body" style="border: 1px solid #ddd">   <div class="row">'+
     '                                                    <div class="col-lg-4">'+
     '                                                        <div class="form-label">Nombre<span class="vd_red">*</span></div>'+
     '                                                        <input type="text" id="nombre'+data["descuento"][i].idDescuento+'"  class="mgbt-xs-20 mgbt-sm-0" name="nombre">'+
     '                                                    </div>'+
     '                                                    <div class="col-lg-4">'+
     '                                                        <div class="form-label">Tipo<span class="vd_red">*</span></div>'+
                                                                '<select id="tipo'+data["descuento"][i].idDescuento+'" class="mgbt-xs-20 mgbt-sm-0">'+
                                                                '<option value="porcentaje">Porcentaje</option>'+
                                                                '<option value="importe" selected>Importe</option>'+
                                                            '</select>'+
     '                                                    </div>'+
     '                                                    <div class="col-lg-4">'+
     '                                                        <div class="form-label">Valor<span class="vd_red">*</span></div>'+
     '                                                        <input type="text" class="mgbt-xs-20 mgbt-sm-0" name="valor" id="valor'+data["descuento"][i].idDescuento+'" maxlength="4">'+
     '                                                    </div>'+
     '                                                </div>';
                                row+='              <div class="mgtp-20">';
                                row+='                  <div class="row mgbt-xs-0">';
                                row+='                      <div class="col-xs-6"></div>';
                                row+='                      <div class="col-xs-6 text-right">';
                                row+='                          <button type="reset" onclick="ocultardiv('+data["descuento"][i].idDescuento+')" class="btn vd_btn btn-default" style="color: #0E2145!important">';
                                row+='                              Cancelar <span class="menu-icon"><i class="fa fa-fw fa-times-circle"></i></span>';
                                row+='                          </button>';
                                row+='                          <button type="submit" value="Mostrar"  class="btn vd_btn vd_bg-green">';
                                row+='                              Guardar <span class="menu-icon"><i class="fa fa-fw fa-check"></i></span>';
                                row+='                          </button>';
                                row+='                       </div>';
                                row+='               </div>';
                                row+='          </div>';
                                row+='       </form>';
                                row+='    </td>';
                                row+='   </tr>';
                                 $("#data-tables tbody").append(row);
                                 row="";
                                 $('[data-toggle="tooltip"]').tooltip(); 

                                }
                            validate();
                       }
                    },
                    error: function () {
                       BootstrapDialog.danger('Ocurrio un error al realizar la busqueda de los descuentos');
                    }
                }
            );
        }
        //para validar los formularios de edicion.
        function validate(){
            $('tbody form').bootstrapValidator({
                fields: {
                    nombre: {
			 validators: {
				 notEmpty: {
					 message: 'a name is needed.'
				 }
			 }
                    },
                    valor: {
                                 validators: {
                                         notEmpty: {
                                                 message: 'Se necesita un valor.'
                                         },
                                         regexp: {
                                                 regexp: /^[0-9]+$/,
                                                 message: 'Solo se permiten caracteres numericos.'
                                         }
                                 }
                         }
                }
            });
            
            $('tbody form').submit(function (e){
                var id=parseInt($(this).attr('iden'));
                if(e.isDefaultPrevented()){
                }else{
                     e.preventDefault();
                     save_method(id, $('#nombre'+id).val(),$('#tipo'+id).val(),$('#valor'+id).val());
                }
            });
        }
        //para activar o desactivar un descuento(1 para activar, 0 parea desactivar).
        function estado(btn, val, id){
            var url = 'Descuentos/update_stus';
            $.ajax({
                    url: url,
                    data:{id:id,estado:val},
                    dataType: "JSON",
                    type: "get",  
                    success: function (data) {
                        if(data.error==false){
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                           if(val==1){
                               $(btn).parent("#status").html('<span id="status">      <a data-original-title="Desactivar" onclick="estado(this, 0, '+id+')" id="btnDesact" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-red vd_red"> <i class="fa fa-times"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-green">Activo</span>');
                                $('[data-toggle="tooltip"]').tooltip();
                            }else{
                               $(btn).parent("#status").html('<span id="status">  <a data-original-title="Activar"  onclick="estado(this, 1, '+id+')" id="btnAct" data-toggle="tooltip" data-placement="top" class="btn menu-icon vd_bd-green vd_green"> <i class="fa fa-check"></i> </a></span>');
                               $('#estadoD'+id).html('<span class="label vd_bg-red">Inactivo</span>');
                               $('[data-toggle="tooltip"]').tooltip(); 
                           }
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al actualizar el descuento, '+data);
                    }
                }
                        
            );
        }
        //para eliminar logicamente un registro
         function eliminar(btn,id){
            var url = 'Descuentos/delete';
            $.ajax({
                    url: url,
                    data:{id:id,estado:0},
                    dataType: "JSON",
                    type: "get",    
                    success: function (data) {
                         if(data.error==false){
                           BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,
                               });
                               cargarDatosTabla();
                               formato('#data-tables');
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'No se pudo eliminar el descuento, error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                        error: function (data) {
                        BootstrapDialog.danger('Ocurrio un error al eliminar el descuento, '+data);
                    }
                }
                        
            );
        }
        //funcion para registrar o actualizar un descuento dependiendo del valor del id (0=registrar)
        function save_method(id, nombre, tipo, valor){
            var url = 'Descuentos/add';//ruta para agregar
            var data= {
                        id:id,
                        nombre: nombre,
                        valor: valor,
                        tipo: tipo,
                        insertar: 1
             };
             //si el id es diferente de 0 voy a actualizar
            if (id !=0){
                url = 'Descuentos/update'; 
                data.insertar=0;
            }
            $.ajax({
                    url: url,
                    dataType: "json",
                    data:data,
                    type: "get",
                    success: function (data) {
                         if(data.error==false){
                            //si estoy editando oculto el panel de edicion. 
                            if(id!=0){
                             ocultardiv(id);
                             }else{
                                 //estoy insertando
                                 $('#PRegistrarCliente').hide();
                            $('#BtnADescuento').show();
                             }
                            cargarDatosTabla();
                             formato('#data-tables');
                             BootstrapDialog.alert({
                               title: 'Notificación',
                               message: ''+data.mensaje,
                               type: BootstrapDialog.TYPE_SUCCESS,

                           });
                        }else{
                            BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                        }
                    },
                    error: function (data) {
                       BootstrapDialog.alert({
                               title: 'Notificación',
                               message: 'Error: '+data.mensaje,
                               type: BootstrapDialog.TYPE_DANGER,

                           });
                    }
                }
            );
        } 
        //funcion para mostrar el formulario de edicion de un descuento
        function mostrardiv(pCodigo,nombre,valor,tipo) {
             $('#nombre'+pCodigo).val(nombre);
             $('#valor'+pCodigo).val(valor);
             $('#tipo'+pCodigo).val(tipo);
            var nombreDiv = 'flotante2' + pCodigo.toString();
            var nombrediv1 = '#flotante1' + pCodigo.toString();

           
            $(nombrediv1).children('.celda').addClass('noprint');
            div = document.getElementById(nombreDiv);

            div.style.display = "";
        }
        //para ocultar los formularios de edicion
        function ocultardiv(codigo) {
            //$('#formulario'+codigo).bootstrapValidator('resetForm',true);
            var pnombre = 'flotante2' + codigo.toString();
            var nombrediv1 = '#flotante1' + codigo.toString();
            div = document.getElementById(pnombre);
            div.style.display = "none";
            $(nombrediv1).children('.celda').removeClass('noprint');

        }
        //funcion que confirma si van a eliminar un desccuento
        function confirmacion(btn,pCodigo) {
            BootstrapDialog.confirm({
                title: 'Confirmacion',
                type: BootstrapDialog.TYPE_DANGER,
                message:  '¿Seguro desea Eliminar este descuento?',
                callback: function (result) {
                if (result) {
                    eliminar(btn,pCodigo);
                }
            }});
        }
    </script>
@stop