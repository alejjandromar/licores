<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */

    // get the HTML
    ob_start();
    include('C:\wamp\www\Restaurant0.4\resources\views\facturahtml.blade.php');
   
    $content = ob_get_clean();

    // convert to PDF
    require_once(dirname(__FILE__).'./html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('mm', array(100,200), 'en');
        $html2pdf->pdf->SetDisplayMode('fullpage');
//      $html2pdf->pdf->SetProtection(array('print'), 'spipu');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple07.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
