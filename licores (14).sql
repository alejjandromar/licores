-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-11-2017 a las 20:15:50
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `licores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `id` int(11) NOT NULL,
  `nombre` int(11) NOT NULL COMMENT 'nombre del almacen',
  `latitud` double NOT NULL COMMENT 'latitud del almacen',
  `longitud` double NOT NULL COMMENT 'longitud del alamacen',
  `direccion` text NOT NULL COMMENT 'direccion del almacen'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCategoria` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'imagenes/categorias/default.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ico` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ion-beer'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `nombre`, `descripcion`, `imagen`, `created_at`, `updated_at`, `estado`, `ico`) VALUES
(16, 'Vinos', '', 'imagenes/categorias/default.jpg', '2017-09-21 21:16:54', '0000-00-00 00:00:00', 1, 'ion-wineglass'),
(17, 'Cervezas', '', 'imagenes/categorias/default.jpg', '2017-09-21 21:17:48', '0000-00-00 00:00:00', 1, 'ion-beer'),
(18, 'Otros', '', 'imagenes/categorias/default.jpg', '2017-09-21 21:19:19', '0000-00-00 00:00:00', 1, 'ion-ios-medical');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` bigint(20) NOT NULL,
  `correo` text CHARACTER SET utf8 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL,
  `telefono` text CHARACTER SET utf8,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(50) NOT NULL,
  `social` int(11) DEFAULT NULL,
  `paypal` varchar(50) DEFAULT NULL,
  `googleid` varchar(100) DEFAULT NULL,
  `faceid` varchar(100) DEFAULT NULL,
  `push_token` text,
  `faceimg` text,
  `googleimg` text,
  `img` text,
  `sbimg` text,
  `remember_token` varchar(200) NOT NULL,
  `logueado` int(11) NOT NULL DEFAULT '0',
  `IdCupon` text NOT NULL COMMENT 'Es el id del cupon personal del usuario',
  `creditoCupon` float UNSIGNED NOT NULL DEFAULT '50' COMMENT 'Es el valor del cupon personal del usuario',
  `IdClienteReferencio` bigint(20) DEFAULT NULL COMMENT 'Es el id del cliente al que pertenecia el cupon que yo registre (el usuario que me referencio en la app)'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `correo`, `active`, `password`, `telefono`, `created_at`, `updated_at`, `name`, `social`, `paypal`, `googleid`, `faceid`, `push_token`, `faceimg`, `googleimg`, `img`, `sbimg`, `remember_token`, `logueado`, `IdCupon`, `creditoCupon`, `IdClienteReferencio`) VALUES
(1, 'gilbberto@gmail.com', 1, '', NULL, '2017-09-22 00:22:37', '2017-10-31 01:35:35', 'Gilberto Gonzalez', 1, NULL, NULL, '1926808454251477', 'undefined', 'https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/19399786_10208259720403892_4694885524927681022_n.jpg?oh=d9cdfa1c76c5bc0a394e4bd4d5d5ec72&oe=5A346484&__gda__=1508723988_8e5f02865640adc4d6c6af6893b47bd1', NULL, 'https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/19399786_10208259720403892_4694885524927681022_n.jpg?oh=d9cdfa1c76c5bc0a394e4bd4d5d5ec72&oe=5A346484&__gda__=1508723988_8e5f02865640adc4d6c6af6893b47bd1', NULL, '', 1, '8220gilbberto', 0, 2),
(2, 'alejjandromar@gmail.com', 1, '', NULL, '2017-10-05 20:08:46', '2017-10-21 01:51:19', 'Alejandro Luis Marcano Silva', 2, NULL, '109774596669669426428', NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-0WrK_WLBwlk/AAAAAAAAAAI/AAAAAAAAABU/uRkZHOv-_Ws/photo.jpg', 'https://lh6.googleusercontent.com/-0WrK_WLBwlk/AAAAAAAAAAI/AAAAAAAAABU/uRkZHOv-_Ws/photo.jpg', NULL, '', 0, '1456alejjandromar', 100, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clienteusacupon`
--

CREATE TABLE `clienteusacupon` (
  `id` bigint(20) NOT NULL,
  `idCupon` bigint(20) NOT NULL,
  `IdCliente` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clienteusacupon`
--

INSERT INTO `clienteusacupon` (`id`, `idCupon`, `IdCliente`, `created_at`, `updated_at`) VALUES
(14, 4, 1, '2017-10-28 00:32:50', '2017-10-28 00:32:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupon`
--

CREATE TABLE `cupon` (
  `id` bigint(20) NOT NULL,
  `estado` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `valor` float UNSIGNED NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cupon`
--

INSERT INTO `cupon` (`id`, `estado`, `valor`, `descripcion`, `created_at`, `updated_at`, `nombre`) VALUES
(2, 1, 50, 'Obten $50 al momento de comprar por el dia de holaween', '2017-10-06 19:07:58', '2017-10-06 19:07:58', 'Haloween'),
(3, 1, 80, 'Obten $80 al momento de comprar por ser el dia del padre', '2017-10-06 19:07:58', '2017-10-06 19:07:58', 'Dia del padre'),
(4, 1, 12.5, 'Obten $12.5 al momento de comprar por ser hoy una fiesta patria', '2017-10-06 19:07:58', '2017-10-06 19:07:58', 'Fiesta patria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento`
--

CREATE TABLE `descuento` (
  `idDescuento` int(10) UNSIGNED NOT NULL,
  `estado` smallint(1) NOT NULL DEFAULT '1',
  `tipo` varchar(10) NOT NULL DEFAULT 'porcentaje',
  `valor` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descuento`
--

INSERT INTO `descuento` (`idDescuento`, `estado`, `tipo`, `valor`, `nombre`, `deleted_at`, `updated_at`, `created_at`, `cantidad`) VALUES
(100, 1, 'porcentaje', 10, 'descuento_comun', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentoproducto`
--

CREATE TABLE `descuentoproducto` (
  `idProducto` int(11) NOT NULL,
  `idDescuento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descuentoproducto`
--

INSERT INTO `descuentoproducto` (`idProducto`, `idDescuento`) VALUES
(92, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallesorden`
--

CREATE TABLE `detallesorden` (
  `idDetalle` int(10) UNSIGNED NOT NULL,
  `factura` int(10) UNSIGNED NOT NULL,
  `cantidad` int(10) UNSIGNED NOT NULL,
  `precio` double UNSIGNED NOT NULL,
  `producto` int(10) UNSIGNED NOT NULL,
  `idPrecio` int(255) NOT NULL,
  `valorDescuento` double UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detallesorden`
--

INSERT INTO `detallesorden` (`idDetalle`, `factura`, `cantidad`, `precio`, `producto`, `idPrecio`, `valorDescuento`) VALUES
(1, 50, 1, 10, 92, 113, 0),
(3, 51, 10, 90, 91, 112, 0),
(4, 51, 10, 10, 92, 113, 0),
(5, 51, 4, 20, 92, 114, 0),
(6, 52, 3, 90, 91, 112, 0),
(7, 52, 3, 10, 92, 113, 0),
(8, 52, 3, 20, 92, 114, 0),
(9, 66, 6, 90, 91, 112, 0),
(10, 66, 10, 10, 92, 113, 0),
(11, 66, 7, 20, 92, 114, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `texto` text NOT NULL COMMENT 'texto de la direccion. ej: calle martinez, porlamar',
  `favorita` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'indica si es una direccion frecuente del usuario',
  `idCliente` bigint(20) NOT NULL COMMENT 'cliente al que pertenece esta direccion',
  `alias` text COMMENT 'Alias de la direccion',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  `env_dis_por` int(11) NOT NULL DEFAULT '1' COMMENT '1=ivoy 2=medios propios'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direccion`
--

INSERT INTO `direccion` (`id`, `latitud`, `longitud`, `texto`, `favorita`, `idCliente`, `alias`, `created_at`, `updated_at`, `env_dis_por`) VALUES
(1, 19.395143223081, -99.177376618555, 'Indianapolis 31, Nápoles, 03810 Ciudad de México, CDMX, México', 1, 1, 'pepsi center', '2017-09-29 21:01:04', '2017-09-29 21:01:04', 2),
(2, 19.4326077, -99.13320799999997, 'Ciudad de mexico', 1, 1, 'casa', '2017-10-27 20:23:21', '2017-10-27 20:23:21', 1),
(16, 19.434509807339, -99.129774772461, 'Calle del Correo Mayor 3, Centro Histórico, Centro, 06000 Ciudad de México, CDMX, México', 1, 1, 'catedral', '2017-10-31 01:43:02', '2017-10-31 01:43:02', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `id` int(11) NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `telefono` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado_envia_pedido`
--

CREATE TABLE `empleado_envia_pedido` (
  `id` int(11) NOT NULL,
  `id_factura` int(10) NOT NULL,
  `id_empleado` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `idFactura` int(10) UNSIGNED NOT NULL,
  `total` float UNSIGNED NOT NULL,
  `cliente` bigint(20) NOT NULL,
  `despachado_por` int(11) DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `subTotal` float UNSIGNED NOT NULL,
  `descuento` float UNSIGNED DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cambio` float NOT NULL,
  `envio` float UNSIGNED NOT NULL DEFAULT '0',
  `idDireccion` int(11) NOT NULL,
  `paypal` float NOT NULL,
  `directo` float NOT NULL,
  `totalCupones` float UNSIGNED NOT NULL DEFAULT '0',
  `totalCredito` float NOT NULL DEFAULT '0',
  `referencias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `directoTarjeta` double UNSIGNED NOT NULL DEFAULT '0',
  `distancia_envio` float DEFAULT NULL COMMENT 'distancia total del envio',
  `tiempo_llegada_envio` float DEFAULT NULL COMMENT 'tiempo de llegada del envio',
  `tipo_envio` tinyint(1) DEFAULT NULL COMMENT '1 ivoy 2 propio'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`idFactura`, `total`, `cliente`, `despachado_por`, `estado`, `subTotal`, `descuento`, `created_at`, `updated_at`, `cambio`, `envio`, `idDireccion`, `paypal`, `directo`, `totalCupones`, `totalCredito`, `referencias`, `directoTarjeta`, `distancia_envio`, `tiempo_llegada_envio`, `tipo_envio`) VALUES
(50, 57, 1, NULL, 3, 10, 0, '2017-10-21 01:51:19', '2017-10-21 01:51:19', 0, 47, 1, 0, 0, 57, 0, 'dsfvdsvfsa', 0, NULL, NULL, NULL),
(51, 1127, 1, NULL, 3, 1080, 0, '2017-10-25 06:50:19', '2017-10-25 06:50:19', 0, 47, 1, 500, 514.5, 12.5, 100, 'referencias de la direccion', 0, NULL, NULL, NULL),
(52, 407, 1, NULL, 3, 360, 0, '2017-10-28 00:32:50', '2017-10-28 00:32:50', 0, 47, 1, 100, 50, 12.5, 100, 'referencias del pedido', 144.5, NULL, NULL, NULL),
(66, 827, 1, NULL, 3, 780, 0, '2017-10-31 01:43:02', '2017-10-31 01:43:02', 0, 47, 16, 827, 0, 0, 0, 'fvgewsrfv', 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturatienecupon`
--

CREATE TABLE `facturatienecupon` (
  `id` bigint(20) NOT NULL,
  `idCupon` bigint(20) NOT NULL,
  `idFactura` int(10) UNSIGNED NOT NULL,
  `montoPagado` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturatienecupon`
--

INSERT INTO `facturatienecupon` (`id`, `idCupon`, `idFactura`, `montoPagado`, `created_at`, `updated_at`) VALUES
(1, 3, 50, 57, '2017-10-21 01:51:19', '2017-10-21 01:51:19'),
(2, 4, 51, 12.5, '2017-10-25 06:50:19', '2017-10-25 06:50:19'),
(3, 4, 52, 12.5, '2017-10-28 00:32:50', '2017-10-28 00:32:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precioproductos`
--

CREATE TABLE `precioproductos` (
  `costo` double UNSIGNED NOT NULL,
  `producto` int(10) UNSIGNED NOT NULL,
  `presentacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'imagenes/presentaciones/default.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `stock` int(11) NOT NULL,
  `stock_r` int(11) NOT NULL COMMENT 'Contiene valor real',
  `stock_bl` int(11) NOT NULL COMMENT 'Cantidad Bloque por paypal',
  `stock_b` int(11) NOT NULL COMMENT 'Cantidad epeando por ser depachada',
  `cantidad_desc` int(11) NOT NULL,
  `descuento` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `precioproductos`
--

INSERT INTO `precioproductos` (`costo`, `producto`, `presentacion`, `id`, `imagen`, `created_at`, `updated_at`, `estado`, `stock`, `stock_r`, `stock_bl`, `stock_b`, `cantidad_desc`, `descuento`) VALUES
(90, 91, '1 Litro', 112, 'imagenes/presentaciones/default.jpg', '2017-10-30 21:13:02', '2017-10-31 01:43:02', 1, 81, 100, 0, 19, 3, 3.4),
(10, 92, '2 L', 113, 'imagenes/presentaciones/default.jpg', '2017-10-30 21:13:02', '2017-10-31 01:43:02', 1, 51, 200, 0, 173, 2, 5),
(20, 92, '1/2 L', 114, 'imagenes/presentaciones/default.jpg', '2017-10-30 21:13:02', '2017-10-31 01:43:02', 1, 86, 200, 0, 14, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `categoria` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `stock_r` int(11) NOT NULL COMMENT 'Contiene valor real',
  `stock` int(11) NOT NULL COMMENT 'Cantidad para venta',
  `stock_bl` int(11) NOT NULL COMMENT 'Cantidad Bloque por paypal',
  `stock_b` int(11) NOT NULL COMMENT 'Cantidad epeando por ser depachada'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombre`, `descripcion`, `estado`, `categoria`, `created_at`, `updated_at`, `stock_r`, `stock`, `stock_bl`, `stock_b`) VALUES
(91, 'Vino blanco', '', 1, 16, '2017-09-29 19:47:01', '2017-09-29 04:30:00', 0, 0, 0, 0),
(92, 'Vino', '', 1, 16, '2017-09-29 19:47:13', '2017-09-29 04:30:00', 0, 0, 0, 0);

--
-- Disparadores `productos`
--
DELIMITER $$
CREATE TRIGGER `validar_datos_productos` BEFORE INSERT ON `productos` FOR EACH ROW IF LOWER(NEW.estado) NOT REGEXP '^(0|1)$' THEN
	signal sqlstate '45000'
    set message_text='El estado es invalido',mysql_errno='45000';
end if
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `validar_datos_productos_actualizar` BEFORE UPDATE ON `productos` FOR EACH ROW IF LOWER(NEW.estado) NOT REGEXP '^(0|1)$' THEN
	signal sqlstate '45000'
    set message_text='El estado es invalido',mysql_errno='45000';
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurante`
--

CREATE TABLE `restaurante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rif` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `moneda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `simbolo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCategoria`),
  ADD UNIQUE KEY `categorias_nombre_unique` (`nombre`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usoCuponClienteUnico` (`idCupon`,`IdCliente`),
  ADD KEY `fk_clienteusacupon_idCliente` (`IdCliente`);

--
-- Indices de la tabla `cupon`
--
ALTER TABLE `cupon`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descuento`
--
ALTER TABLE `descuento`
  ADD PRIMARY KEY (`idDescuento`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  ADD PRIMARY KEY (`idDetalle`),
  ADD UNIQUE KEY `detalleUnico` (`factura`,`precio`,`producto`),
  ADD KEY `detallesorden_producto_fk` (`producto`),
  ADD KEY `detallesorden_precion_fk` (`idPrecio`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_direccion_idCliente` (`idCliente`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado_envia_pedido`
--
ALTER TABLE `empleado_envia_pedido`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_empleadoEnviaOrden` (`id_factura`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `facturas_cliente_foreign` (`cliente`),
  ADD KEY `fk_facturas_cliente` (`idDireccion`),
  ADD KEY `fdsvbdfsb` (`despachado_por`);

--
-- Indices de la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usoCuponFacturaUnico` (`idCupon`,`idFactura`),
  ADD KEY `fk_facturatienecupon_idFactura` (`idFactura`);

--
-- Indices de la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `presentacionUnica` (`producto`,`presentacion`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`),
  ADD UNIQUE KEY `productos_nombre_unique` (`nombre`),
  ADD KEY `productos_categoria_foreign` (`categoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cupon`
--
ALTER TABLE `cupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `descuento`
--
ALTER TABLE `descuento`
  MODIFY `idDescuento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT de la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  MODIFY `idDetalle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empleado_envia_pedido`
--
ALTER TABLE `empleado_envia_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `idFactura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  ADD CONSTRAINT `fk_clienteusacupon_idCliente` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clienteusacupon_idCupon` FOREIGN KEY (`idCupon`) REFERENCES `cupon` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  ADD CONSTRAINT `fk_detallesorden_precio` FOREIGN KEY (`idPrecio`) REFERENCES `precioproductos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detallesorden_producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`idProducto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD CONSTRAINT `fk_direccion_idCliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturas_cliente` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_despachadoPor` FOREIGN KEY (`despachado_por`) REFERENCES `empleados` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_idDireccion` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  ADD CONSTRAINT `fk_facturatienecupon_idCupon` FOREIGN KEY (`idCupon`) REFERENCES `cupon` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturatienecupon_idFactura` FOREIGN KEY (`idFactura`) REFERENCES `facturas` (`idFactura`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  ADD CONSTRAINT `fk_precioproductos_producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`idProducto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_categoria` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`idCategoria`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
