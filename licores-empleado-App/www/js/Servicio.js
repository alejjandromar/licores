app.factory('User', function (CONFIG, $http, $rootScope, $ionicLoading, $interval) {
    var user = {
        "id": 982,
        "nombre": "Alejandro",
        "telefono": "+584261262608",
        "user": "user",
        "email": "alejjandromar@gmail.com",
        "direccion": "Mi casa",
    };

    return {
        /*
         get: obtiene el usuario actualmente logueado
         response: [user]
         */
        get: function () {

            return JSON.parse(localStorage.usuario || false);
            // return user;
        },
        /*
         login: Realiza el inicio de sesion
         url: null
         response: [user]
         */
        login: function (user, tipo) {
            try {
                //alert("Login");
                var req = {
                    url: CONFIG.URLAPI + "login_cliente",
                    method: 'get',
                    params: {user: JSON.stringify(user),
                        //password: password,
                        tipo: tipo,
                        push: localStorage.push_token,
                    }

                };
                return $http(req).success(function (response) {
                    return response[0];
                }).error(function (response) {
                    alert('Error repuesta login');
                    alert(response);
                    alert(JSON.stringify(response));
                });
            } catch (e) {
                alert(e);
            }

        },
        /*
         Register: Registra un usuario dentro de la aplicaion
         url: null
         response: [user]
         */
        register: function (user) {
            $ionicLoading.show();
            var req = {
                url: CONFIG.URLAPI + "registro_cliente",
                method: 'GET',
                params: user
            };
            return $http(req).success(function (response) {
                $ionicLoading.hide();
                return response[0];
            });
        },
        /*
         Register: Registra un usuario dentro de la aplicaion
         url: null
         response: [user]
         */
        update: function (user) {
            $ionicLoading.show();
            var req = {
                url: CONFIG.URLAPI + "update_cliente",
                method: 'GET',
                params: {cliente: JSON.stringify(user)}
            };
            return $http(req).success(function (response) {
                $ionicLoading.hide();
                return response[0];
            });
        },
        /*
         SaveUser: Guarda y actualiza al usuario dentro de la aplicacion
         se debe usar al iniciar sesion
         url: null
         response: [user]
         */
        SaveUser: function (user) {
            if (user != undefined) {
                localStorage.usuario = JSON.stringify(user);
                $rootScope.usuario = user;

                return user;
            }else{
                alert("Error al guardar usuario")
            }
            
        },
        /*
         logout: Cierra la sesion de un usurio
         url: null
         response: [user]
         */
        logout: function (user) {
            localStorage.clear();
            $rootScope.usuario = false;
            try {
                if (watchID != null) {
                    //alert('se va aeliminar el watch');
                    navigator.geolocation.clearWatch(watchID);
                    watchID = null;
                }
                if ($rootScope.intervaloActPosEmp != null) {
                    console.log('se cancelo el interval que pregunta');
                    $interval.cancel($rootScope.intervaloActPosEmp);
                    $rootScope.intervaloActPosEmp = null;
                }
                $rootScope.latitud = null;
                $rootScope.longitud = null;
                $rootScope.desplazo = false;
                $rootScope.presicion = 999999999999999999;
                $rootScope.servicios = [];
                $rootScope.intervaloActPosEmp = null;
                $rootScope.siguiente = null;
                $rootScope.emergencias = [];
                $rootScope.emergenciaSel = null;
            } catch (e) {
                alert(e);
            }
            return true;
        }


    };
})

app.factory('ConnectivityMonitor', function ($cordovaNetwork) {

        return {
            isOnline: function () {

                if (ionic.Platform.isWebView()) {
                        var respuesta=true;
                         $.ajax({
                          type: "get",
                          async:false,  
                          url: "http://www.pichresearch.com/danzapp/markers.php",
                          success: function (response,xhr, ajaxOptions, thrownError) {
                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                            respuesta= false;
                          }
                        });
                         return respuesta;
                } else {
                    if(navigator.onLine){
                        var respuesta=true;
                         $.ajax({
                          type: "get",
                          async:false,  
                          url: "http://www.pichresearch.com/danzapp/markers.php",
                          success: function (response,xhr, ajaxOptions, thrownError) {
                          },
                          error: function (xhr, ajaxOptions, thrownError) {
                             if(xhr.status==0){
                                  respuesta= false;
                              }
                          }
                        });
                        return respuesta;
                    }else{
                         return false;
                    }
                }

            },
            ifOffline: function () {

                if (ionic.Platform.isWebView()) {
                    
                    return !$cordovaNetwork.isOnline();
                } else {
                    return !navigator.onLine;
                }

            }
        }
    })

    .factory('DB', function ($http, $ionicLoading,$rootScope, $cordovaNetwork, ConnectivityMonitor,CONFIG) {
            var markers = [];
            return {
                getMarkers: function (params) {
                    return $http.get(CONFIG.URLAPI+"MarcadoresMapa", {params: params}).then(function (response) {
                        return response.data;
                    });
                }
            }
        })  
  
app.factory('ModelMensaje', function ($http, CONFIG, $rootScope) {
    return {
        EnviarMensajes: function (idasistencia, contenido) {
            var req = {
                url: CONFIG.URLAPI + "enviar/mensaje",
                method: 'GET',
                params: {
                    idAsistencia: idasistencia,
                    idUser: $rootScope.usuario.id,
                    msj: contenido}

            };
            return $http(req)
                    .success(function (response) {
                        return response;
                    });
        },
        AnterioresMensajes: function (idasistencia, fecha) {
            var req = {
                url: CONFIG.URLAPI + "mensaje/anteriores",
                method: 'GET',
                params: {
                    idAsistencia: idasistencia,
                    fecha: fecha}

            };
            return $http(req)
                    .success(function (response) {
                        return response;
                    });
        },
    };
});


app.factory('utils', function ($http, CONFIG, $rootScope, $cordovaCamera, $cordovaFileTransfer, $ionicLoading) {
    return {
        /*Tomar foto funcion que toma una foto desde la galeria o el telefono
         * U: Tipo de 0 galeria, 1: camara
         */
        tomarFoto: function (U) {
            try {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URL,
                    sourceType: U,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.PNG,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true
                };
                $ionicLoading.show({
                    template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px; ">\n\
                                <circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg> \n\
                            </div> <span style="font-size:16px"> Obteniendo Imagen</span>',
                    //template: 'Cargando Datos...',
                });
                return  $cordovaCamera.getPicture(options).then(function (imageData) {
                    $ionicLoading.hide();
                    return {error: false, motivo: "Correcto", foto: imageData};


                }, function (err) {
                    $ionicLoading.hide();
                    return {error: true, motivo: "No mporta en esta App", foto: null};
                });
            } catch (e) {
                alert(e);
            }
        },
        /*
         * Funcion que sube una imagen al servidor
         * url: direccion a donde se desee subir
         * foto: direccion de la imagen en el dipositivo (foto: si se uso el plugin anterior)
         * params: data extra para ser enviada
         */
        subirFoto: function (url, foto, params) {
            try {
                var server = encodeURI(CONFIG.URLAPI + url);
                var currentName = foto.replace(/^.*[\\\/]/, '');
                // Subir imagenf
                var options = {
                    fileKey: 'file',
                    fileName: currentName,
                    chunkedMode: false,
                    mimeType: 'image/jpeg',
                    params: params
                };

            } catch (e) {
                alert(e);

            }
            $ionicLoading.show(
                    {template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px; ">\n\
                                <circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg> \n\
                            </div> <span style="font-size:16px"> Subiendo Imagen</span>'
                    });

            return $cordovaFileTransfer.upload(server, foto, options)
                    .then(function (res) {
                        $ionicLoading.hide();
                       // alert(JSON.stringify(res));
                        return {
                            error: false,
                            resp: JSON.parse(res.response)}
                    }, function (err) {
                        $ionicLoading.hide();
                       // alert(JSON.stringify(err));
                        return	{error: true,
                            resp: err}
                    });
        }
    };
});