app.controller('LogCtrl', function (carrito,$scope, $state, $rootScope, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User) {


    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if ($rootScope.usuario) {
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
			if($rootScope.carrito.direccion!=null){
				$state.go("app.consulta");
			}else{
				$state.go("app.seleccionarUbicacion");
			}
        }
    });

    $scope.login = function (data) {
		console.log(data);
        User.login(data).then(
			
                function (response) {//succes
					console.log(response);
                    $ionicLoading.hide();
                    if (response.data.error != true) {
                        var us = response.data.user;
                        User.SaveUser(us);
						carrito.create_carrito(true);
                        $ionicHistory.clearHistory();
                        $rootScope.registrar_push();
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true
                        });
                        $rootScope.activarGPS();
                        $state.go("app.historialPedidos");
                         $rootScope.cargarDatos(false);
                    } else {
                        $ionicLoading.show({
                            template: response.data.mensaje,
                            duration: 2000,
                            content: 'Loading',
                            animation: 'fade-in'
                        })
                    }


                },
                function (response) {//error
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        template: 'No se pudo conectar al servicio',
                        duration: 2000,
                        content: 'Loading',
                        animation: 'fade-in'
                    });
                }
        );
    }

   
    $scope.$on("$ionicView.afterEnter", function (event, data) {
        $rootScope.mostrar = false;
    });

})


/*
 * CONTROLADO PARA LA GESITON DEL PERFIL DE LOS USUARIOS
 */

app.controller('PerfilCtrl', function ($scope, $ionicPopup, $cordovaCamera, $state, $rootScope, utils, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User, $http, CONFIG) {
   
    $scope.$on("$ionicView.enter", function (event, data) {
        if (!$rootScope.usuario) {
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
            $state.go("app.login");
        }
    });

    $scope.cambiar_imagen = function () {
        $scope.alertPopup = $ionicPopup.alert({
            title: 'Tomar foto desde',
            template:
                    ' <div class="row"> <div class="col col-50"><button class="button button-block button-clear" ng-click="tomarFoto(0); ;" style="padding:0"> <img src="img/carpeta.png" style="width: 70px;"> <br>Galeria </button></div>' +
                    '<div class="col col-50"><button class="button button-block button-clear" ng-click="tomarFoto(1)" style="padding:0"> <img src="img/camara.png" style="width: 70px;"><br> Camara</button></div></div>',
            buttons: [
                {text: 'Cancelar'}, ]

        });
        $scope.alertPopup.then(function (res) {
            console.log(res);
        });
    };

    //funcion para tomar una imagen con la camara
    //X=0 camara x=1 album. Y=false subir una imagen Y=true enviar en mensaje
    $rootScope.tomarFoto = function (X) {

        utils.tomarFoto(X).then(
                function (response) {
                    if (response.error == false) {
                        var params = {iduser: $rootScope.usuario.id};
                        utils.subirFoto('subirfotoperfil', response.foto, params).then(
                                function (response2) {

                                    try {

                                        if (response2.error == false) {

                                            if (response2.resp.error != true) {

                                                User.SaveUser(response2.resp.user);


                                            } else {
                                                alert("Ha ocurrido un error intente nuevamente");
                                            }


                                        } else {
                                          //  alert("bad")
                                        }
                                    } catch (e) {
                                       // alert(e);
                                       // alert(JSON.stringify(response2))
                                    }

                                },
                                function (err) {
                                    //alert("Error")
                                }
                        );
                    }

                },
                function (err) {
                    //no show
                    $ionicLoading.hide();
                });

    };




    /*
     UPGRADE ZONE
     */

    $scope.cuentas = function () {
        $http({
            url: ruta + "cuenta/all",
            method: 'get',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $scope.cuentas = response.tipocuentas;
        });
    };
    $scope.realizar_pago = function () {
        try {
            PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment(10, "Account upgrade"), $scope.upgrade, $scope.cancelado);
            //alert("fin")
            // $scope.upgrade();
        } catch (e) {
            alert(e);
            //$rootScope.alert('Error', e);
        }
    }

    $scope.cancelado = function () {
        alert(e);
        // $rootScope.alert('El pago no ha podido ser procesado', 20);
    }

    $scope.suscripcion = function () {

        $http({
            url: ruta + "suscripcion/all",
            method: 'get',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $scope.suscripciones = response.suscripcion;
        });
    };
    $scope.upgrade = function (payment) {
        try {
            alert("begin to upgrade");
            $ionicLoading.show();
            $http({
                url: CONFIG.URLAPI + "user/upgrade",
                method: 'get',
                params: {
                    suscripcion: 5,
                    id: $rootScope.usuario.id,
                    paypal: "cvdkfk"
                },
                headers: {
                    "Content-Type": "application/json"
                }

            }).success(function (response) {
                $ionicLoading.hide();
                alert("success");
                User.SaveUser(response.user);
            }).error(function (response) {

                $ionicLoading.hide();
                alert("error");
                alert(JSON.stringify(response));
                //User.SaveUser(response.user);
            });
        } catch (e) {
            alert(e);
            //$rootScope.alert('Error', e);
        }
    };
    /*-------------------------*/
    $scope.registrar = function () {
        User.update($rootScope.usuario).then(function (response) {
            if (response.data.error == true) {
                $ionicLoading.show({
                    template: response.mensaje,
                    duration: 2000,
                    content: 'Loading',
                    animation: 'fade-in'
                });
            } else {
                $ionicLoading.show({
                    template: 'Registro exitoso',
                    duration: 2000,
                    content: 'Loading',
                    animation: 'fade-in'
                });
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                User.SaveUser(response.data.user);
                $state.go("app.emergencias");
            }
        });
    }
    $scope.$on("$ionicView.afterEnter", function (event, data) {
        $rootScope.mostrar = false;
    });
})




/*
 * CONTROLADO PARA LA GESITON DEL PERFIL DE LOS USUARIOS
 */

app.controller('ContactoCtrl', function ($scope, $ionicPopup, $cordovaCamera, $state, $rootScope, utils, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User, $http, CONFIG) {
   
    
})

