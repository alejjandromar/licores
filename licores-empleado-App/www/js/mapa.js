var cambio=false;
app.controller('FmapaCtrl', function ($scope, CONFIG, $ionicPopup, $cordovaGeolocation, $http, $state, $rootScope, $timeout, $ionicLoading,carrito,$ionicHistory) {
    var geocoder;
    var mapaSelUbi;
    var marcador;
    $scope.buscar = {direccion: null};
    $scope.mensajeM = {mostrar: false, texto: ""};
    $scope.buscando = false;
	$scope.inverso;
	$scope.direccion;


	//funcion que lleva una corredenada geografica a una direccion en texto
    function coordenaAdireccion(results) {
        $scope.mensajeM = {mostrar: false, texto: ''};
		var posicionPedido;
		if(!$scope.inverso){
			posicionPedido = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
		}else{
			posicionPedido = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
		}
        if (mapaSelUbi == null) {
            var mapOptions = {
                center: posicionPedido,
                zoom: 15,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            mapaSelUbi = new google.maps.Map(document.getElementById("mapaSelUbi"), mapOptions);
			 var image = "img/pin.png";
            marcador = new google.maps.Marker({map: mapaSelUbi, animation: google.maps.Animation.DROP, draggable: true, icon:image});
			
			mapaSelUbi.addListener('bounds_changed', function() {
				if(!$state.is('app.seleccionarUbicacion')){
					cambio=true;
				}
			});
        }else{
			//centro el mapa
			if(cambio==true){
				var mapOptions = {
					center: posicionPedido,
					zoom: 15,
					streetViewControl: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				mapaSelUbi = new google.maps.Map(document.getElementById("mapaSelUbi"), mapOptions);
				var image = "img/pin.png";
				cambio==false;
			}else{
				mapaSelUbi.setCenter(posicionPedido);
			}
        }

        marcador.setMap(mapaSelUbi);
        marcador.setPosition(posicionPedido);
		if(!$scope.inverso){
			$scope.$apply(function () {
				$scope.buscando = false;
				$scope.direccion.direccion=$scope.buscar.direccion = results[0].formatted_address;
				$scope.direccion.id=0;
			});
			if(marcador.eventoAsignado==null){
				marcador.eventoAsignado=true;
				marcador.addListener('dragend', function () {
					$scope.$apply(function () {
						$scope.direccion.lat = marcador.position.lat();
						$scope.direccion.lon = marcador.position.lng();
						$scope.buscando = true;
						//llamar a geaoresul
						$scope.inverso=false;
						var posicion = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
						geocoder.geocode({'latLng': posicion}, geocodeResult);
					});
				});
			}
		}else{
			$scope.$apply(function () {
				$scope.direccion.direccion=$scope.buscar.direccion = results[0].formatted_address;
				$scope.direccion.lat = results[0].geometry.location.lat();
				$scope.direccion.lon = results[0].geometry.location.lng();
				$scope.direccion.id=0;
				$scope.buscando = false;
			});
		}
		console.log(results);
    }


	function errorGeocode (valor){
		if (valor == 'ZERO_RESULTS') {
			console.log('hola 2');
			$scope.buscando=false;
			$scope.$apply(function () {$scope.mensajeM = {mostrar: true, texto: 'No se encontraron resultados para su busqueda'};});
			$timeout(function () {
				$scope.mensajeM.mostrar = false;
				$scope.mensajeM.texto = '';
			}, 2500);
		} else {
			//cuarquiero otro error
			$scope.buscando=false;
			$scope.$apply(function () {$scope.mensajeM = {mostrar: true, texto: 'No se encontraron resultados para su busqueda'};});
			$timeout(function () {
				$scope.mensajeM.mostrar = false;
				$scope.mensajeM.texto = '';
			}, 2500);
		}
	}
	

	//funcion para obtener el resultado de una direccion segun un resultado de google
    function geocodeResult(results, status) {
		// Verificamos el estatus
        if (status == 'OK') {
			// Si hay resultados
            coordenaAdireccion(results);
        } else {
			//si no se encontraron resultados para la busqueda
			errorGeocode(status);
        }
    }
	
	
	function geocodeResult2(results, status) {
		// Verificamos el estatus
        if (status == 'OK') {
			// Si hay resultados
            coordenaAdireccion(results);
        } else {
			console.log('erorrr');
			//si no se encontraron resultados para la busqueda
            errorGeocode(status);
        }
    }

	//funcion que carga el api de google maps.
    $scope.loadGoogleMaps = function () {
        $ionicLoading.show({
            template: 'Cargando google maps<br>Por favor espere...'
        });
        $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
            setTimeout(function () {
                $rootScope.enableMap();
				if($scope.bandera==true){
					setTimeout(function () {
						$scope.editarDireccion();
					}, 500);
				}else{
					 $scope.initMap();
				}
            }, 1000);
        });
    }

	//funcion que va a buscar una direccion en google con la posicion actual del usuario ($rootScope.latitud, $rootScope.longitud)
    $scope.initMap = function () {
		if(geocoder==null){
			geocoder = new google.maps.Geocoder();
		}
        $scope.mensajeM = {mostrar: true, texto: ''};
        $scope.direccion = {lat:$rootScope.latitud,lon:$rootScope.longitud,favorita:false,id:0};
        var posicion = new google.maps.LatLng($rootScope.latitud, $rootScope.longitud);
		$scope.inverso=false;
        //llamar a geaoresul
        geocoder.geocode({'latLng': posicion}, geocodeResult);
    }
	
	$scope.buscarDireccionE=function(){
		if(geocoder==null){
			geocoder = new google.maps.Geocoder();
		}
		$scope.buscando = true;
		$scope.inverso=true;
		//llamar a geaoresul
		geocoder.geocode({'address': $scope.buscar.direccion}, geocodeResult2);
	}
	
	
	//funcion que pregunta si quiero guardar la direccion en mis direcciones frecuentes.
	$scope.confirmarGuardarDireccion = function() {
		if($scope.direccion.id!=0){
			$ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
			$rootScope.carrito.direccion=$scope.direccion;
			//debo guardar en localstorge el carrito
			carrito.save();
			$state.go('app.consulta');
			return;
		}
		$scope.alias={texto:''};
	   var Popup = $ionicPopup.show({
			title: 'Agregar dirección',
			template: '¿Deseas agregar esta ubicación a tus direcciones frecuentes?<br>'
			+'<input id="aliasTexto" type="text" ng-model="alias.texto" placeholder="Alias de la dirección">'
			+'<p id="errorAlias" style="display:none;margin: 0px;color: red;position: relative;top: 10px;">Por favor introduzca un alias para su dirección</p>',
			buttons: [
			  { text: 'Cancelar' },
			  {
				text: '<b>Aceptar</b>',
				onTap: function(e) {
					$scope.alias.texto=$('.popup-body #aliasTexto').val();
					console.log($scope.alias.texto);
					  if ($scope.alias.texto=='') {
						  console.log('A');
						//don't allow the user to close unless he enters wifi password
						e.preventDefault();
						$('.popup-body #errorAlias').show();
					  } else {
						   console.log('B');
						  $('.popup-body #errorAlias').hide();
						  return $scope.alias.texto;
					  }
				}
			  }
			]
	   });

	   Popup.then(function(res) {
		if(res) {
			$scope.direccion.favorita=true;
			$scope.direccion.alias=res;
			$ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
			$rootScope.carrito.direccion=$scope.direccion;
			carrito.save();
			//debo guardar en localstorge el carrito
			$state.go('app.consulta');
		} else {
			$ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
			$rootScope.carrito.direccion=$scope.direccion;
			//debo guardar en localstorge el carrito
			carrito.save();
			$state.go('app.consulta');
		}
		console.log($rootScope.carrito);
	   });
	};
	
	
	//funcion para validar si la direccion selecciona se encuantra entre los puntos de atencion
	$scope.validarDireccion=function(){
		$scope.mensajeM.mostrar = false;
		$scope.mensajeM.texto = '';
		$scope.buscando = true;
		var parametro={
				"bOrder":{
					"orderAddresses":[
						{
							"address":{
								"latitude":$scope.direccion.lat,
								"longitude":$scope.direccion.lon

							},
						"isPickup":1,
						"isSource":1
						}
					]
				}
		}
		$http({
            url: CONFIG.URLAPI + 'verificarDireccion',
            method: 'GET',
            params: {data: parametro},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			$scope.buscando = false;
            if(response.code==0){
				$scope.confirmarGuardarDireccion();
			}else{
				if(response.code==-1){
					$rootScope.alert('Zona no soportada','Aún no entregamos pedidos en esa zona.');
				}else{
					//no es una direccion valida
					$ionicLoading.show({template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',duration: 2500});
				}
			}

        }).error(function (error) {
            $scope.buscando = false;
			$ionicLoading.show({
				template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',
				duration: 2500
			});
        });
	}
	
	
	
	//funcion que selecciona una direcion favorita con la direccion del pedido actual
	$rootScope.selDirFav=function(direccion){
		if(mapaSelUbi!=null){
			$scope.direccion.direccion=direccion.texto;
			$scope.buscar.direccion = direccion.texto;
			$scope.direccion.lat = direccion.latitud;
			$scope.direccion.lon = direccion.longitud;
			$scope.direccion.id = direccion.id;
			$scope.direccion.alias = direccion.alias;
			var posicionPedido = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
			marcador.setPosition(posicionPedido);
            mapaSelUbi.setCenter(posicionPedido);
			$rootScope.closePopover();
		}
	}
	
	//funcion para abrir el popover
	$rootScope.openPopover = function() {
		$scope.popover = $ionicPopup.show({
			title: 'Direcciones frecuentes',
			template: '<div class="list" style="margin: -10px;"><a class="item" style="padding: 10px 5px 5px;border-top: 1px solid gray;border-bottom: 1px solid gray;" ng-repeat="direccion in direcciones_favoritas track by $index" ng-click="selDirFav(direccion)"><h2 ng-if="direccion.alias">{{direccion.alias}}</h2><p>{{direccion.texto}}</p></a><div class="item" style="padding: 10px 10px 0px 10px;" ng-if="direcciones_favoritas.length==0"><p>No posee direcciones guardadas. </p></div></div>',
			buttons: [
			 { text: 'Cancelar' }
			]
	   });
	   
	   $timeout(function () {
			$('.popup .popup-buttons').css({'padding':'2px','min-height':'10px'});
	   },100);
	};
	
	//funcion que cierra  el popover
	$rootScope.closePopover = function() {
		$scope.popover.close();
	};
	
	$scope.editarDireccion=function(){
		$scope.mensajeM = {mostrar: false, texto: ''};
		var posicionPedido = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
		var mapOptions = {
			center: posicionPedido,
			zoom: 15,
			streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		mapaSelUbi = new google.maps.Map(document.getElementById("mapaSelUbi"), mapOptions);
		 var image = "img/pin.png";
		marcador = new google.maps.Marker({map: mapaSelUbi, animation: google.maps.Animation.DROP, draggable: true, icon:image});
		$scope.$apply(function () {
			$scope.buscar.direccion=$scope.direccion.direccion;
		});
        marcador.setMap(mapaSelUbi);
        marcador.setPosition(posicionPedido);
		if(marcador.eventoAsignado==null){
			marcador.eventoAsignado=true;
			marcador.addListener('dragend', function () {
				$scope.$apply(function () {
					$scope.direccion.lat = marcador.position.lat();
					$scope.direccion.lon = marcador.position.lng();
					$scope.buscando = true;
					//llamar a geaoresul
					$scope.inverso=false;
					var posicion = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
					if(geocoder==null){
						geocoder = new google.maps.Geocoder();
					}
					geocoder.geocode({'latLng': posicion}, geocodeResult);
				});
			});
		}
	}
	
	$rootScope.inicarBusqueDi=function(){
		if($rootScope.carrito.direccion==null){
			var options = {timeout: 15000, enableHighAccuracy: true, maximumAge: 3000};
			$scope.buscando = true;
			$cordovaGeolocation.getCurrentPosition(options).then(function (position) {
				L = $rootScope.latitud = position.coords.latitude;
				Lo = $rootScope.longitud = position.coords.longitude;
				if ($rootScope.checkLoaded()) {
				$scope.initMap();
				} else {
					$scope.loadGoogleMaps();
				}	
			}, function (error) {
				$scope.buscando = false;
				//coordenadas geograficas de la ciudad de mexico
				L = $rootScope.latitud = 19.4284700;
				Lo = $rootScope.longitud = -99.1276600;
				if ($rootScope.checkLoaded()) {
					$scope.initMap();
				} else {
					$scope.loadGoogleMaps();
				}
				$timeout(function () {
					$scope.mensajeM.mostrar = true;
					$scope.mensajeM.texto = 'No se pudo obtener su ubicacion';
				}, 1000);
			});
		}else{
			if ($rootScope.checkLoaded()) {
				$scope.direccion = $rootScope.carrito.direccion;
				setTimeout(function () {
						$scope.editarDireccion();
				}, 500);
				
			} else{
				$scope.bandera=true;
				$scope.loadGoogleMaps();
				$scope.bandera=false;
			}
		}
	}
	
	$scope.$on("$ionicView.enter", function (event, data) {
		$('#mapaSelUbi').css('height',$('#contenedor').height());
	});

    $scope.$on("$ionicView.beforeEnter", function (event, data) {
		if ($rootScope.direcciones_favoritas == undefined || $rootScope.direcciones_favoritas.length == 0) {
            $rootScope.cargarDireccionesFavoritas(false,false);
        }
		$rootScope.inicarBusqueDi();
    });
}).controller('direccionesFCtrl', function ($scope, $state, $rootScope,$ionicPopup,$ionicLoading,$http,CONFIG,$timeout,User) {
	$scope.mostrarDF=false;
	
	
	//funcon que confirma si quiero eliminar una direccion
	$scope.confirmarEliminar=function(direccion){
	   var confirmPopup = $ionicPopup.confirm({
		 title: 'Confirmación',
		 template: 'Esta seguro que desea eliminar la dirección "'+direccion.alias+'"?',
		 cancelText:'Cancelar',
		 okText:'Aceptar'
	   });

	   confirmPopup.then(function(res) {
		if(res) {
			$scope.eliminarDireccion(direccion);
		}
	   });
	}
	
	//funcion que carga una nueva pagina de direcciones favoritas
    $scope.pagSigDF = function () {
        if ($rootScope.siguienteDF != null) {
            $scope.mostrarDF = true;
            $http({
                url:CONFIG.URLAPI + 'cargarDireccionesFavoritas'+$rootScope.siguienteDF.substring($rootScope.siguienteDF.indexOf('?page')),
                method: 'GET',
                params: {idUser: $rootScope.usuario.id,},
            }).success(function (response) {
				response.direcciones=JSON.parse(response.direcciones);
                $scope.mostrarDF = false;
                for (var i = 0; i < response.direcciones.data.length; i++) {
                    $rootScope.direcciones_favoritas.push(response.direcciones.data[i]);
                }
                $rootScope.siguienteDF = response.direcciones.next_page_url;
				User.SaveUser($rootScope.usuario);
            }).error(function (error) {
                $scope.mostrarDF = false;
                $scope.error = 'No se pueden actualizar los datos';
                $timeout(function () {
                    $scope.error = null;
                }, 2000);
            });
        }
    }
	
	
	$scope.eliminarDireccion=function(direccion){
		$ionicLoading.show({
			template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
		});
		$http({
            url: CONFIG.URLAPI + 'eliminarDireccion',
            method: 'GET',
            params: {idUser: $rootScope.usuario.id,direccion: direccion.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			$ionicLoading.hide();
            if(response.error==false){
				$rootScope.cargarDireccionesFavoritas(false,true);
				$ionicLoading.show({template: 'La dirección ha sido eliminada exitosamente',duration: 2500});
			}else{
				$ionicLoading.show({template:response.mensaje,duration: 2500});
			}

        }).error(function (error) {
            $ionicLoading.hide();
			$ionicLoading.show({
				template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',
				duration: 2500
			});
		});
	}
	
	//ir al estado para seleccionar la ubicacion de la direccion favorita
	$scope.irMApa=function(){
		$state.go('app.agregarUbicacionF');
	}
	
	//craga la api si no esta cargada
	$scope.loadGoogleMaps = function (direccion) {
        $ionicLoading.show({
            template: 'Cargando google maps<br>Por favor espere...'
        });
        $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
            setTimeout(function () {
                $rootScope.enableMap();
                $scope.initMap(direccion);
            }, 1000);
        });
    }
	
	$scope.initMap=function(direccion) {
        $scope.mensajeM = {mostrar: false, texto: ''};
		var posicionPedido = new google.maps.LatLng(direccion.latitud, direccion.longitud);
		var mapOptions = {
			center: posicionPedido,
			zoom: 15,
			streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var id="mapaDF"+direccion.id;
		console.log(id);
		var mapaSelUbi = new google.maps.Map(document.getElementById(id), mapOptions);
		 var image = "img/pin.png";
		marcador = new google.maps.Marker({map: mapaSelUbi, animation: google.maps.Animation.DROP, draggable: false, icon:image});
        
		marcador.setPosition(posicionPedido);
    }
	
	//funcion que se llama para pintar cada uno de los mapas de las dorecciones favoritas
	$scope.pintarMapa=function(direccion){
		 setTimeout(function () {
			if ($rootScope.checkLoaded()) {
				$scope.initMap(direccion);
			} else {
				$scope.loadGoogleMaps(direccion);
			}
		}, 500);
	}
	
	$rootScope.actualizarMapas=function(){
		for (var i = 0; i < $rootScope.direcciones_favoritas.length; i++) {
			$scope.pintarMapa($rootScope.direcciones_favoritas[i]);
		}
	}								   
	//antes de entrar siempre a la ventana
	$scope.$on("$ionicView.beforeEnter", function (event, data) {
		if ($rootScope.direcciones_favoritas == undefined || $rootScope.direcciones_favoritas.length == 0) {
            $rootScope.cargarDireccionesFavoritas(true,false);
        } else {
			console.log('ya cargo las direcciones');
            $rootScope.cargarDireccionesFavoritas(false,false);
        }
    });
	
}).controller('gregarDireccionesFCtrl', function ($scope, CONFIG, $ionicPopup, $cordovaGeolocation, $http, $state, $rootScope, $ionicPopover, $timeout, $ionicLoading,$ionicHistory) {
	
	var geocoder;
    var mapaSelUbiF;
    var marcadorFav;
    $scope.buscar = {direccion: null};
    $scope.mensajeM = {mostrar: false, texto: ""};
    $scope.buscando = false;
	$scope.inverso;


	//funcion que lleva una corredenada geografica a una direccion en texto
    function coordenaAdireccion(results) {
		console.log($scope.direccion.lat, $scope.direccion.lon);
        $scope.mensajeM = {mostrar: false, texto: ''};
		var posicionPedido;
		if(!$scope.inverso){
			posicionPedido = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
		}else{
			posicionPedido = new google.maps.LatLng(results[0].geometry.location.lat(),results[0].geometry.location.lng());
		}
        if (mapaSelUbiF == null) {
            var mapOptions = {
                center: posicionPedido,
                zoom: 15,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            mapaSelUbiF = new google.maps.Map(document.getElementById("mapaSelUbiF"), mapOptions);
			 var image = "img/pin.png";
            marcadorFav = new google.maps.Marker({map: mapaSelUbiF, animation: google.maps.Animation.DROP, draggable: true, icon:image});
        } else {
			//centro el mapa
            mapaSelUbiF.setCenter(posicionPedido);
        }

        //marcadorFav.setMap(mapaSelUbi);
        marcadorFav.setPosition(posicionPedido);
		if(!$scope.inverso){
			$scope.$apply(function () {
				$scope.buscando = false;
				$scope.direccion.direccion=$scope.buscar.direccion = results[0].formatted_address;
				$scope.direccion.id=0;
			});
			if(marcadorFav.eventoAsignado==null){
				marcadorFav.eventoAsignado=true;
				marcadorFav.addListener('dragend', function () {
					$scope.$apply(function () {
						$scope.direccion.lat = marcadorFav.position.lat();
						$scope.direccion.lon = marcadorFav.position.lng();
						$scope.buscando = true;
						//llamar a geaoresul
						$scope.inverso=false;
						var posicion = new google.maps.LatLng($scope.direccion.lat, $scope.direccion.lon);
						geocoder.geocode({'latLng': posicion}, geocodeResult);
					});
				});
			}
		}else{
			$scope.$apply(function () {
				$scope.buscando = false;
				$scope.direccion.direccion=$scope.buscar.direccion = results[0].formatted_address;
				$scope.direccion.lat = results[0].geometry.location.lat();
				$scope.direccion.lon = results[0].geometry.location.lng();
				$scope.direccion.id=0;
			});
		}
		console.log(results);
    }


	function errorGeocode (valor){
		if (valor == 'ZERO_RESULTS') {
			console.log('hola 2');
			$scope.buscando=false;
			$scope.$apply(function () {$scope.mensajeM = {mostrar: true, texto: 'No se encontraron resultados para su busqueda'};});
			$timeout(function () {
				$scope.mensajeM.mostrar = false;
				$scope.mensajeM.texto = '';
			}, 2500);
		} else {
			//cuarquiero otro error
			$scope.buscando=false;
			$scope.$apply(function () {$scope.mensajeM = {mostrar: true, texto: 'No se encontraron resultados para su busqueda'};});
			$timeout(function () {
				$scope.mensajeM.mostrar = false;
				$scope.mensajeM.texto = '';
			}, 2500);
		}
	}
	

	//funcion para obtener el resultado de una direccion segun un resultado de google
    function geocodeResult(results, status) {
		// Verificamos el estatus
        if (status == 'OK') {
			// Si hay resultados
            coordenaAdireccion(results);
        } else {
			//si no se encontraron resultados para la busqueda
			errorGeocode(status);
        }
    }
	
	
	function geocodeResult2(results, status) {
		// Verificamos el estatus
        if (status == 'OK') {
			// Si hay resultados
            coordenaAdireccion(results);
        } else {
			console.log('erorrr');
			//si no se encontraron resultados para la busqueda
            errorGeocode(status);
        }
    }

	//funcion que carga el api de google maps.
    $scope.loadGoogleMaps = function () {
        $ionicLoading.show({
            template: 'Cargando google maps<br>Por favor espere...'
        });
        $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
            setTimeout(function () {
                $rootScope.enableMap();
                $scope.initMap();
            }, 1000);
        });
    }

	//funcion que va a buscar una direccion en google con la posicion actual del usuario ($rootScope.latitud, $rootScope.longitud)
    $scope.initMap = function () {
		if(geocoder==null){
			geocoder = new google.maps.Geocoder();
		}
        $scope.mensajeM = {mostrar: true, texto: ''};
        $scope.direccion= {lat:$rootScope.latitud,lon:$rootScope.longitud,favorita:true,id:0};
        var posicion = new google.maps.LatLng($rootScope.latitud, $rootScope.longitud);
		$scope.inverso=false;
        //llamar a geaoresul
        geocoder.geocode({'latLng': posicion}, geocodeResult);
    }
	
	$scope.buscarDireccionE=function(){
		$scope.buscando = true;
		$scope.inverso=true;
		//llamar a geaoresul
		geocoder.geocode({'address': $scope.buscar.direccion}, geocodeResult2);
	}
	
	
	//funcion que pregunta si quiero guardar la direccion en mis direcciones frecuentes.
	$scope.GuardarDireccion = function() {
		$ionicLoading.show({template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>'});
		$http({
            url: CONFIG.URLAPI + 'agregarDireccionFavorita',
            method: 'GET',
            params: {direccion: $scope.direccion,idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			$ionicLoading.hide();
			if(response.error==false){
				$ionicHistory.clearHistory();
				$ionicHistory.nextViewOptions({
					disableAnimate: true,
					disableBack: true
				});
				$rootScope.cargarDireccionesFavoritas(false,true);
				$state.go('app.direccionesFavoritas');
				$ionicLoading.show({
					template: response.mensaje,
					duration: 2500
				});
			}else{
				$ionicLoading.show({
					template: response.mensaje,
					duration: 2500
				});
			}

        }).error(function (error) {
            $ionicLoading.hide();
			$ionicLoading.show({
				template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',
				duration: 2500
			});
        });
	};
	
	
	//funcion para validar si la direccion selecciona se encuantra entre los puntos de atencion
	$scope.validarDireccion=function(){
		$scope.mensajeM.mostrar = false;
		$scope.mensajeM.texto = '';
		if($scope.direccion.alias==null||$scope.direccion.alias==""){
			$scope.mensajeM.mostrar = true;
			$scope.mensajeM.texto = 'Por favor introduzca el alias de la dirección';
			$timeout(function () {
				$scope.mensajeM.mostrar = false;
				$scope.mensajeM.texto = '';
			}, 2000);
			return;
		}
		$scope.buscando = true;
		var parametro={
				"bOrder":{
					"orderAddresses":[
						{
							"address":{
								"latitude":$scope.direccion.lat,
								"longitude":$scope.direccion.lon

							},
						"isPickup":1,
						"isSource":1
						}
					]
				}
		}
		$http({
            url: CONFIG.URLAPI + 'verificarDireccion',
            method: 'GET',
            params: {data: parametro},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			$scope.buscando = false;
            if(response.code==0){
				$scope.GuardarDireccion();
			}else{
				if(response.code==-1){
					$rootScope.alert('Zona no soportada','Aún no entregamos pedidos en esa zona.');
				}else{
					//no es una direccion valida
					$ionicLoading.show({template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',duration: 2500});
				}
			}

        }).error(function (error) {
            $scope.buscando = false;
			$ionicLoading.show({
				template: 'No se pudo procesar la operacion<br>Por favor vuelva a intentarlo mas tarde',
				duration: 2500
			});
        });
	}

    $scope.$on("$ionicView.enter", function (event, data) {
		$('#mapaSelUbiF').css('height',($('#contenedor2').height()-48));
        var options = {timeout: 15000, enableHighAccuracy: true, maximumAge: 3000};
        $scope.buscando = true;
		$cordovaGeolocation.getCurrentPosition(options).then(function (position) {
			L = $rootScope.latitud = position.coords.latitude;
			Lo = $rootScope.longitud = position.coords.longitude;
			if ($rootScope.checkLoaded()) {
				$scope.initMap();
			} else {
				$scope.loadGoogleMaps();
			}
		}, function (error) {
			$scope.buscando = false;
			//coordenadas geograficas de la ciudad de mexico
			L = $rootScope.latitud = 19.4284700;
			Lo = $rootScope.longitud = -99.1276600;
			if ($rootScope.checkLoaded()) {
				$scope.initMap();
			} else {
				$scope.loadGoogleMaps();
			}
			$timeout(function () {
				$scope.mensajeM.mostrar = true;
				$scope.mensajeM.texto = 'No se pudo obtener su ubicacion';
			}, 1000);
		});
    });
	
})