app.factory('User', function (CONFIG, $http, $rootScope, $ionicLoading, $interval) {
    var user = {
        "id": 982,
        "nombre": "Alejandro",
        "telefono": "+584261262608",
        "user": "user",
        "email": "alejjandromar@gmail.com",
        "direccion": "Mi casa",
    };

    return {
        /*
         get: obtiene el usuario actualmente logueado
         response: [user]
         */
        get: function () {

            return JSON.parse(localStorage.usuario || false);
            // return user;
        },
        /*
         login: Realiza el inicio de sesion
         url: null
         response: [user]
         */
        login: function (user) {
            try {
                //alert("Login");
                var req = {
                    url: CONFIG.URLAPI + "login_empleado",
                    method: 'get',
                    params: {
						user: JSON.stringify(user),
                       
                        push: localStorage.push_token,
                    }

                };
                return $http(req).success(function (response) {
                    return response[0];
                }).error(function (response) {
                    alert(response);
                    alert(JSON.stringify(response));
                });
            } catch (e) {
                alert(e);
            }

        },
        /*
         Register: Registra un usuario dentro de la aplicaion
         url: null
         response: [user]
         */
        register: function (user) {
            $ionicLoading.show();
            var req = {
                url: CONFIG.URLAPI + "registro_cliente",
                method: 'GET',
                params: user
            };
            return $http(req).success(function (response) {
                $ionicLoading.hide();
                return response[0];
            });
        },
        /*
         Register: Registra un usuario dentro de la aplicaion
         url: null
         response: [user]
         */
        update: function (user) {
            $ionicLoading.show();
            var req = {
                url: CONFIG.URLAPI + "update_cliente",
                method: 'GET',
                params: {cliente: JSON.stringify(user)}
            };
            return $http(req).success(function (response) {
                $ionicLoading.hide();
                return response[0];
            });
        },
        /*
         SaveUser: Guarda y actualiza al usuario dentro de la aplicacion
         se debe usar al iniciar sesion
         url: null
         response: [user]
         */
        SaveUser: function (user) {
            if (user != undefined) {
                localStorage.usuario = JSON.stringify(user);
                $rootScope.usuario = user;

                return user;
            } else {
                alert("Error al guardar usuario")
            }

        },
        /*
         logout: Cierra la sesion de un usurio
         url: null
         response: [user]
         */
        logout: function (user) {
            localStorage.clear();
            $rootScope.usuario = false;
            try {
                if (watchID != null) {
                    //alert('se va aeliminar el watch');
                    navigator.geolocation.clearWatch(watchID);
                    watchID = null;
                }
                if ($rootScope.intervaloActPosEmp != null) {
                    console.log('se cancelo el interval que pregunta');
                    $interval.cancel($rootScope.intervaloActPosEmp);
                    $rootScope.intervaloActPosEmp = null;
                }
                $rootScope.latitud = null;
                $rootScope.longitud = null;
                $rootScope.desplazo = false;
                $rootScope.presicion = 999999999999999999;
                $rootScope.servicios = [];
                $rootScope.intervaloActPosEmp = null;
                $rootScope.siguiente = null;
                $rootScope.emergencias = [];
                $rootScope.emergenciaSel = null;
            } catch (e) {
                alert(e);
            }
            return true;
        }


    };
})

app.factory('ConnectivityMonitor', function ($cordovaNetwork) {

    return {
        isOnline: function () {

            if (ionic.Platform.isWebView()) {
                var respuesta = true;
                $.ajax({
                    type: "get",
                    async: false,
                    url: "http://www.pichresearch.com/danzapp/markers.php",
                    success: function (response, xhr, ajaxOptions, thrownError) {
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        respuesta = false;
                    }
                });
                return respuesta;
            } else {
                if (navigator.onLine) {
                    var respuesta = true;
                    $.ajax({
                        type: "get",
                        async: false,
                        url: "http://www.pichresearch.com/danzapp/markers.php",
                        success: function (response, xhr, ajaxOptions, thrownError) {
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (xhr.status == 0) {
                                respuesta = false;
                            }
                        }
                    });
                    return respuesta;
                } else {
                    return false;
                }
            }

        },
        ifOffline: function () {

            if (ionic.Platform.isWebView()) {

                return !$cordovaNetwork.isOnline();
            } else {
                return !navigator.onLine;
            }

        }
    }
})

        .factory('DB', function ($http, $ionicLoading, $rootScope, $cordovaNetwork, ConnectivityMonitor, CONFIG) {
            var markers = [];
            return {
                getMarkers: function (params) {
                    return $http.get(CONFIG.URLAPI + "MarcadoresMapa", {params: params}).then(function (response) {
                        return response.data;
                    });
                }
            }
        })



app.factory('utils', function ($http, CONFIG, $rootScope, $cordovaCamera, $cordovaFileTransfer, $ionicLoading) {
    return {
        /*Tomar foto funcion que toma una foto desde la galeria o el telefono
         * U: Tipo de 0 galeria, 1: camara
         */
        tomarFoto: function (U) {
            try {
                var options = {
                    quality: 100,
                    destinationType: Camera.DestinationType.FILE_URL,
                    sourceType: U,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.PNG,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: true
                };
                $ionicLoading.show({
                    template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px; ">\n\
                                <circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg> \n\
                            </div> <span style="font-size:16px"> Obteniendo Imagen</span>',
                    //template: 'Cargando Datos...',
                });
                return  $cordovaCamera.getPicture(options).then(function (imageData) {
                    $ionicLoading.hide();
                    return {error: false, motivo: "Correcto", foto: imageData};


                }, function (err) {
                    $ionicLoading.hide();
                    return {error: true, motivo: "No mporta en esta App", foto: null};
                });
            } catch (e) {
                alert(e);
            }
        },
        /*
         * Funcion que sube una imagen al servidor
         * url: direccion a donde se desee subir
         * foto: direccion de la imagen en el dipositivo (foto: si se uso el plugin anterior)
         * params: data extra para ser enviada
         */
        subirFoto: function (url, foto, params) {
            try {
                var server = encodeURI(CONFIG.URLAPI + url);
                var currentName = foto.replace(/^.*[\\\/]/, '');
                // Subir imagenf
                var options = {
                    fileKey: 'file',
                    fileName: currentName,
                    chunkedMode: false,
                    mimeType: 'image/jpeg',
                    params: params
                };

            } catch (e) {
                alert(e);

            }
            $ionicLoading.show(
                    {template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px; ">\n\
                                <circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg> \n\
                            </div> <span style="font-size:16px"> Subiendo Imagen</span>'
                    });

            return $cordovaFileTransfer.upload(server, foto, options)
                    .then(function (res) {
                        $ionicLoading.hide();
                        // alert(JSON.stringify(res));
                        return {
                            error: false,
                            resp: JSON.parse(res.response)}
                    }, function (err) {
                        $ionicLoading.hide();
                        // alert(JSON.stringify(err));
                        return	{error: true,
                            resp: err}
                    });
        }
    };
});





app.factory('carrito', function ($http, CONFIG, $rootScope, $cordovaCamera, $cordovaFileTransfer, $ionicLoading) {
    /*
     carrito = {
     subtotal:  <Subtotal de la orden>,
     envio:     <Costo del envio>,
     promocion: <Valor de promocion de factura>,
     total:     <Estimado del valor a cobrar>
     productos:[{
     
     id:               <Producto relacinado>
     id_precio:        <Precio relacionado>
     cantidad:         <Cantidad_comprada>
     nombre:           <Nombre del producto>
     stock:            <Cantidad disponible>
     precio:           <Precio del producto>
     promo_cant:       <Valor promo>
     cantidad_promo:   <Cantidad_promo>
     }]
     }
     */


    return {
        //Guarda el carrito
        save: function () {
            //alert("Carrito");
            localStorage.carrito = JSON.stringify($rootScope.carrito);
            //this.total();
        },
        /*CREA UN CARRITO EN LA APLICACION
         * e: true si se necesita eliminar el carrito anterior;
         */
        create_carrito: function (E) {

            if (E || !localStorage.carrito) {
                $rootScope.carrito = {
                    listo: false,
                    subtotal: 0,
                    envio: 47,
                    direccion: null,
                    promocion: 0,
                    total: 47,
                    productos: [],
                    userID: $rootScope.usuario.id,
                    pagos: {
                        paypal: 0,
                        directo: 0,
                        directoTarjeta: 0,
                        cupones: [],
                        totalCupones: 0,
                        totalPagado: 0
                    }
                }
                this.save();
            } else {
                try {
                    $rootScope.carrito = JSON.parse(localStorage.carrito);
                } catch (e) {
                    console.error(e);
                    $rootScope.carrito = {
                        listo: false,
                        subtotal: 0,
                        envio: 47,
                        direccion: null,
                        promocion: 0,
                        total: 47,
                        userID: $rootScope.usuario.id,
                        productos: [],
                        pagos: {
                            paypal: 0,
                            directo: 0,
                            directoTarjeta: 0,
                            cupones: [],
                            totalCupones: 0,
                            totalPagado: 0
                        }
                    }
                    this.save();

                }

            }

        },
        buscar: function (precio) {
            for (var i = 0; i < $rootScope.carrito.productos.length; i++) {
                var car = $rootScope.carrito.productos[i];
                if (car.id_precio == precio.id) {
                    return car;
                }
            }
            return false;
        },
        total: function (nprecio, antprecio) {
            $rootScope.carrito.subtotal -= antprecio;
            $rootScope.carrito.total -= antprecio;
            $rootScope.carrito.subtotal += nprecio;
            $rootScope.carrito.total += nprecio;
        },
        desc: function (ndesc, antdesc) {
            $rootScope.carrito.descuento -= antdesc;
            $rootScope.carrito.descuento += ndesc;

        }
        ,
        /*Agregar un producto al carrito
         * cantidad: cantiddad a agregar;
         * producto: producto a agregar;
         * precio:   precio a agregar;
         * car: detalle de factura
         */
        add_carrito: function (cantidad, producto, precio, car) {
            if (cantidad == 0) {
                return;
            }
            if (car == null) {
                var car = this.buscar(precio);
                if (car == false) {
                    var car = {
                        id: producto.idProducto,
                        id_precio: precio.id,
                        cantidad: 0,
                        nombre: producto.nombre,
                        presentacion: precio.presentacion,
                        stock: precio.stock,
                        precio: precio.costo,
                        promo_cant: producto.descuento.length > 0 ? producto.descuento[0].cantidad : null,
                        cantidad_promo: producto.descuento.length > 0 ? producto.descuento[0].valor : 0,
                        promoact: false,
                        total: 0
                    };
                    $rootScope.carrito.productos.push(car);

                }
            }
            var pre_cant = cantidad + car.cantidad
            //Se verifica si existe esa cantidad
            if (pre_cant <= car.stock) {
                car.cantidad = pre_cant;
                console.log(car);
                if (car.promo_cant == null || car.promo_cant > car.cantidad) {
                    var new_total = car.precio * car.cantidad;

                } else {
                    var new_total = car.precio * car.cantidad - car.cantidad_promo;
                    if (car.promoact == false) {
                        car.promoact = true;
                        this.desc(car.cantidad_promo, 0);
                    }
                }
                this.total(new_total, car.total);
                car.total = new_total;
                this.save();
            } else {
                alert("Cantidad muy alta");
            }
        },
        /*Disminuye un producto en el carrito
         * cantidad: cantiddad a disminuir;
         * producto: producto a disminuir;
         * precio:   precio a disminuir;
         * car: detalle de factura
         * i: posicion si se requiere eliminar
         */
        dis_carrito: function (cantidad, precio, car, i) {
            if (car == null) {
                var car = this.buscar(precio);
                if (car == false) {
                    return;
                }
            }
            var pre_cant = car.cantidad - cantidad;
            //Se verifica si existe esa cantidad
            if (pre_cant > 0) {

                car.cantidad = pre_cant;
                if (car.promo_cant != null || car.promo_cant > car.cantidad) {
                    var new_total = car.precio * car.cantidad;
                    if (car.promoact == true) {
                        car.promoact = false;
                        this.desc(0, car.cantidad_promo);
                    }
                } else {
                    var new_total = car.precio * car.cantidad - car.cantidad_promo;
                }
                this.total(new_total, car.total);
                car.total = new_total;
                this.save();
            } else {
                car.cantidad = 0;
                var new_total = 0;
                this.total(new_total, car.total);
                $rootScope.carrito.productos.splice(i, 1);
                this.save();
                //mostrar mensaje "debe elegir una cantidad menor a 
            }
        },
        /*
         * Funcion que sube una imagen al servidor
         * url: direccion a donde se desee subir
         * foto: direccion de la imagen en el dipositivo (foto: si se uso el plugin anterior)
         * params: data extra para ser enviada
         */
        subirFoto: function (url, foto, params) {
            try {
                var server = encodeURI(CONFIG.URLAPI + url);
                var currentName = foto.replace(/^.*[\\\/]/, '');
                // Subir imagenf
                var options = {
                    fileKey: 'file',
                    fileName: currentName,
                    chunkedMode: false,
                    mimeType: 'image/jpeg',
                    params: params
                };

            } catch (e) {
                alert(e);

            }
            $ionicLoading.show(
                    {template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px; ">\n\
                                <circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg> \n\
                            </div> <span style="font-size:16px"> Subiendo Imagen</span>'
                    });

            return $cordovaFileTransfer.upload(server, foto, options)
                    .then(function (res) {
                        $ionicLoading.hide();
                        // alert(JSON.stringify(res));
                        return {
                            error: false,
                            resp: JSON.parse(res.response)}
                    }, function (err) {
                        $ionicLoading.hide();
                        // alert(JSON.stringify(err));
                        return	{error: true,
                            resp: err}
                    });
        }
    };
});