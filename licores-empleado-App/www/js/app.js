// Ionic Starter App
// https://devdactic.com/ionic-push-notifications-guide/
//https://github.com/phonegap/phonegap-plugin-push
// angular.module is a global place for creating, registering and retrieving Angular modules

// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic', 'ngCordova'])
        .run(function ($ionicPlatform, $rootScope, $state, $interval, $timeout, $ionicLoading) {

            $ionicPlatform.ready(function () {
                try {
                    window.plugins.googleplus.isAvailable(
                            function (available) {
								if(available){
									$rootScope.ready = true;
								}else{
									 $rootScope.ready = true;
								}
                            }
                    , function (error) {
                        $rootScope.ready = false;
                    }
                    );
                } catch (e) {
                    //alert(e);
                }
                $ionicPlatform.registerBackButtonAction(function (event) {
                    event.preventDefault();
                }, 100);

                $ionicPlatform.on('resume', function () {
                });
                $ionicPlatform.on('pause', function () {
                    //la la aplicacion esta en el fondo
                });
                try {
                    $rootScope.activarGPS();
                    /*
                     * Metodos refenrentes al manejo de notificacines
                     */
                    var push = new PushNotification.init({
                        "debug": false, //false
                        "android": {
                            "senderID": "912109682551", //seneder id se uso el soldolar
                            //"topics": ['nuevo_evento'],
                            "iconColor": "#F49247",
                            "sound": true,
                            "vibrate": true,
                            "forceShow": true
                        }
                    });
                    //Cuando se obtiene un nuevo push token
                    push.on('registration', function (data) {
                        // alert('me dio un push token valido: '+data.registrationId);
                        // alert(data.registrationId);
                        localStorage.push_token = data.registrationId;
                        //$rootScope.registrar_push();
                    }, function (data) {
                        alert('error al registrar el push token');
                        alert(data);
                    });

                    //Cuando obtiene una nuevva notificaion
                    push.on('notification', function (data) {
                        try {
                            alert('llego una notificacion');
                            alert(JSON.stringify(data));
                            return;
                            /*if (data.additionalData.tipo == "cancelarEvento") {
                             try {
                             var evento = data.additionalData.info;
                             $rootScope.llamarBuscarAsistencias(false);
                             $rootScope.buscarMisEventos(false);
                             $rootScope.alert('Notificacion', 'El Evento: '+evento.name+' ha sido cancelado por el operador<br>Motivo:'+evento.motivo);  
                             } catch (e) {
                             //alert('A');
                             //alert(e);
                             }
                             }
                             
                             if (data.additionalData.tipo == "nuevo_evento") {
                             //  alert("nuevo_evento");
                             //la voy a agregar nueva en el empleado
                             try {
                             var evento = data.additionalData.info;
                             localStorage.setItem("emergenciaS", JSON.stringify(evento));
                             $state.go('app.mapa');
                             } catch (e) {
                             alert(e);
                             }
                             }*/
                        } catch (e) {
                            alert('B');
                            alert(e)
                        }
                    });
                } catch (e) {
                    //  alert(e);
                }


            });
        });
app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })


            .state('app.login', {
                url: '/log',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Login.html',
                        controller: 'LogCtrl'
                    }
                }
            })

            .state('app.pago', {
                url: '/pago',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/pago.html',
                        controller: 'PagoCtrl'
                    }
                }
            })


            .state('app.consulta', {
                url: '/consulta/:p',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Consultar.html',
                        controller: 'noCtrl'
                    }
                }
            })

            .state('app.perfil', {
                url: '/perfil',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Perfil.html',
                        controller: 'PerfilCtrl'
                    }
                }
            })


            .state('app.contacto', {
                url: '/contacto',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Contacto.html',
                        controller: 'ContactoCtrl'
                    }
                }
            })

            .state('app.seleccionarUbicacion', {
                url: '/seleccionar-Ubicacion',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/mapa.html',
                        controller: 'FmapaCtrl'
                    }
                }
            })
            .state('app.direccionesFavoritas', {
                url: '/direcciones-favoritas',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/direccionesF.html',
                        controller: 'direccionesFCtrl'
                    }
                }
            })
            .state('app.agregarUbicacionF', {
                url: '/agregar-UbicacionF',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/agregarDireccionesF.html',
                        controller: 'gregarDireccionesFCtrl'
                    }
                }
            })

            .state('app.carrito', {
                url: '/carrito',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/carrito.html',
                        controller: 'carritoCtrl'
                    }
                }
            })
            .state('app.historialPedidos', {
                url: '/historial-Pedidos',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/historialPedidos.html',
                        controller: 'historialPedidosCtrl'
                    }
                }
            })
            .state('app.detallesOrden', {
                url: '/detalles-Orden',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/detallesAsistencia.html',
                        controller: 'detallesOrdenCtrl'
                    }
                }
            })
            .state('app.cuponesDisponibles', {
                url: '/cupones-disponibles',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cuponesDisponibles.html',
                        controller: 'cuponesDisponiblesCtrl'
                    }
                }
            })

            .state('app.confirarPedido', {
                url: '/confirar-pedido',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/confirarPedido.html',
                        controller: 'confirarPedidoCtrl'
                    }
                }
            });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/log');
}).constant('CONFIG', {
    //URLAPI: "http://138.197.9.85/gruas/public/",
    //URLAPI: "http://192.168.43.37/licores/pizzeria/public/",
   // URLAPI: "http://chelaplus.mx/pizzeria/public/",
    URLAPI: "http://localhost/licores/pizzeria/public/",
    APPVERSION: '1.0.0'

});