var pingUsuario = null;
var watchID = null;
var L=null;
var Lo=null;
app.controller('AppCtrl', function (carrito, $filter, $scope, $ionicHistory, User, $state, $rootScope, $cordovaGeolocation, $ionicPopup, $ionicPopover, $timeout, $cordovaGeolocation, CONFIG, $http, $ionicLoading) {
	
	$rootScope.inicializarVariables=function(){
		$rootScope.ready=false;			//indica si es viable hacer login con google
		$rootScope.latitud = null;		//lalitud del usuario que usa la app
		$rootScope.longitud = null;		//longitud del usuario quee usa la app
		$rootScope.datos={};			//categorias y productos de la app
		$rootScope.ordenSel = null;		//orden seleccionada para ver los detalles
		$rootScope.cupones=null;		//Cupones disponibles para un usuario
		$rootScope.editarD=false;		//Cupones disponibles para un usuario
		$rootScope.direcciones_favoritas=[];
		$rootScope.siguienteDF=null;
		$rootScope.pedidos=[];
		$rootScope.siguientePed = null;
		$rootScope.fav = {v: 0};
	}
	
	$rootScope.inicializarVariables();
	
    /*
     *	FUNCION QUE EMITE UN MENSAJE CORTO
     */

    $rootScope.mostra_mensaje = function (mensaje) {
        $ionicLoading.show({
            template: mensaje,
            duration: 2500
        });
    }

    /*
     FUNCION QUE ACTUALIZA EL PUSH TOKEN
     */
    $rootScope.registrar_push = function () {

        if (localStorage.usuario || false) {
            $rootScope.usuario = User.get();
            var req = {
                url: CONFIG.URLAPI + "push_token",
                method: 'get',
                params: {iduser: $rootScope.usuario.id,
                    push: localStorage.push_token}

            };
            $http(req).success(function (response) {
                // alert(JSON.stringify(response.push));
                return response[0];
            });
        }
    }

    $rootScope.usuario = User.get();

    //funcion global para mandar un mensaje en un alert
    $rootScope.alert = function (titulo, mensaje) {
        var alertPopup = $ionicPopup.alert({
            title: titulo,
            template: mensaje
        });
    }


	//-----------------AREA PARA MANEJO DE ORDENES-----//
    //funcion global que carga las categorias 
    $rootScope.cargarDatos = function (X) {
        if (X) {
            $ionicLoading.show({
                template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
                //template: 'Cargando Datos...',
            });
        }
        $http({
            url: CONFIG.URLAPI + 'CargarDatos?actualizar=true',
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $ionicLoading.hide();
			if(response.categorias.length>0){
				$rootScope.datos.categorias = response.categorias;
				$rootScope.datos.categorias_bus=[];
				for (var i = 0, max = $rootScope.datos.categorias.length; i < max; i++) {
					$rootScope.datos.categorias_bus[$rootScope.datos.categorias[i].id] = $rootScope.datos.categorias[i];
				}
				localStorage.setItem("categorias", JSON.stringify($rootScope.datos.categorias));
			}
        }).error(function (error) {
            $ionicLoading.hide();
            $rootScope.datos.categorias = [];
			$rootScope.datos.categorias_bus=[];
            $ionicLoading.show({
                template: 'Ocurrio un error al tratar de cargar los datos',
                duration: 2500
            });

        });
    }

    $rootScope.cargarDatos(false);
	
	carrito.create_carrito(false);
	$rootScope.add_cart= function(cantidad, producto, precio){
		carrito.add_carrito(cantidad, producto, precio, null);
	}
	
	$rootScope.add_cart_fromdet= function(cantidad, car){
		carrito.add_carrito(cantidad, null, null, car);
		
	}
	
	$rootScope.dis_cart_fromdet= function(cantidad, car, i){
		carrito.dis_carrito(cantidad, null, car, i);
	}
	//------------------------------FIN DE AREA DE MANEJO DE ORDENES--------------------------
    $rootScope.activarGPS = function () {
        //alert('va a verificar el gps');
        try {
            //alert('puedo obtener la ubicacion');
            cordova.plugins.locationAccuracy.request(function (success) {
                //el gps esta prendido, puedo mandar a pedir la posicion del usuario
                //alert('ya se prendio el gps');
                if ($rootScope.usuario&&$state.is('app.seleccionarUbicacion')) {
					$rootScope.inicarBusqueDi();
                }

            }, function (error) {
				//alert(JSON.stringify(error));
                if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED&&cordova.plugins.locationAccuracy.ERROR_ALREADY_REQUESTING) {//si el usuario no cancelo y no hay otra peticion en curso
                    if (window.confirm("No se pudo habilitar automáticamente el GPS. ¿Desea abrir la Configuración de ubicación y hacerlo manualmente?")) {
                        cordova.plugins.diagnostic.switchToLocationSettings();
                        //se supone que lo activo, pero como no es seguro porque puedo abrir la ventana de configuracion y aun asi no activarlo, debo verificar que se activo.
                        cordova.plugins.diagnostic.isLocationEnabled(function (enabled) {
                            if (enabled) {
                                //se activo el Gps manualmente...
                                if ($rootScope.usuario) {
                                    if ($rootScope.emergencias == undefined || $rootScope.emergencias.length == 0) {
										//alert('no tengo emergencias');
									}else{
										//alert('tengo emergencias');
                                        }
                                    }
                                } else {
                                $ionicLoading.show({template: 'La aplicacion no podra acceder a su ubicacion', duration: 2500});
                            }
                        }, function (error) {
                        });
                    } else {
                        //fallo la activacion automatica y el usuario cancelo la activacion manual
                        if ($rootScope.usuario) {
                                $rootScope.alert('Notificacion', 'Es necesario que el gps este habilitado para un correcto funcionamiento de la aplicacion');
                                setTimeout(function () {
                                    //alert('voy a lanzar el metodo de nuevo');
                                    cordova.plugins.diagnostic.switchToLocationSettings();
							}, 3000);
                            }
                        }
                } else {
                   //el usuario cancelo
                }
            }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
        } catch (e) {
            //alert('error el verificar gps');
            //alert(e);
        }
    }
	
	
	//funcion para mostrar o no el icono de las direcciones favoritas y el carrito en el header
	//X==1 icono de favoritos, x==2 icono de carrito
	$rootScope.mostrarICon1 = function () {
		if($state.is('app.seleccionarUbicacion')){
			return true;
		}else{
			return false;
		}
	}
	$rootScope.mostrarICon2 = function () {
		if($state.is('app.consulta')){
			return true;
		}else{
			return false;
		}
	}
	
	$rootScope.irEstado=function(){
		$ionicHistory.clearHistory();
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			disableBack: true
		});
		console.log($rootScope.carrito);
		if($rootScope.carrito.direccion!=null){
			$state.go("app.consulta");
		}else{
			$state.go("app.seleccionarUbicacion");
		}
	}
	
	

    //funcion global que verifica si cargo el apy de google maps
    $rootScope.checkLoaded = function (mapa) {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
            return false;//no cargo la api
        } else {
            //quiere decir que ya se cargo la api
            return true;
        }
		console.log('cargo el api 4',!typeof google == "undefined" || typeof google.maps == "undefined");
    }

    $rootScope.enableMap = function () {
        $ionicLoading.hide();
    }



    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function () {
            this.classList.toggle('active');
        });
    }

    $scope.logout = function () {
        $ionicLoading.show({template: 'Procesando...'});
        var aux = localStorage.push_token;
        $http({
            url: CONFIG.URLAPI + 'logout_cliente',
            method: 'GET',
            params: {idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $ionicLoading.hide();
            localStorage.clear();
            localStorage.push_token = aux;
            $rootScope.usuario = false;
            try {
                $rootScope.inicializarVariables();
				try {
					window.plugins.googleplus.disconnect(
					function (msg) {
					});
				} catch (e) {
					//alert(e);
				}
            } catch (e) {
                //alert(e);
            }
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
            $state.go("app.login");

        }).error(function (error) {
            $ionicLoading.hide();
            $ionicLoading.show({
                template: 'Verifique su conexion a intentar y vuelva a intentar',
                duration: 2500
            });
        });
    }	
	
	
	//funcion para cargar las direcciones favoritas de un usuario
	$rootScope.cargarDireccionesFavoritas=function(X,Y){
		if(X==true){
			$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
				//template: 'Cargando Datos...',
			});
		}
		$http({
            url: CONFIG.URLAPI + 'cargarDireccionesFavoritas',
            method: 'Get',
            params: {idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			response.direcciones=JSON.parse(response.direcciones);
			if(X==true){$ionicLoading.hide();}
            
			$rootScope.direcciones_favoritas=response.direcciones.data;
			$rootScope.siguienteDF=response.direcciones.next_page_url;
			if(Y==true){
				//debo actualizar toso los mapas en la pantalla de direcciones
				$rootScope.actualizarMapas();
			}
        }).error(function (error) {
			if(X==true){
				$ionicLoading.hide();
				$ionicLoading.show({
					template: 'No se pudo realizar la operacion<br>Intentelo mas tarde',
					duration: 2500
				});
			}
		});
	}

    $scope.$on("$ionicView.enter", function (event, data) {
        $rootScope.mostrar = true;
    });

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function () {
        //$scope.popover.remove();
    });

    //tirando la parada con soap
    /*testService.HelloWorld({user:'Yoscar123',password:'1234'},'Yoscar','Rafael','yoscar@gmail.com').then(function(response){
     console.log(response);
     });*/



}).controller('noCtrl', function ($filter, $scope, $state, $ionicHistory, $stateParams, $cordovaGeolocation, $rootScope, $ionicLoading, $http, CONFIG, $timeout) {
    $scope.filter = {$:'', idType:undefined, categoria:2};
    $scope.filter2 = {$:'', idCategoria:2};
    $scope.order = {categoria:'idCategoria', rev:false};


    //funcion para mostrar una fecha 
    $rootScope.mostrarFecha = function (fecha) {

        var fechaS = new Date(fecha);

        return fechaS;
    }

    $scope.hacer_favorito = function (evento) {
        try {
            $rootScope.favoritos = JSON.parse(localStorage.getItem('favoritos')) || [];
        } catch (e) {
            $rootScope.favoritos = [];
        }
        console.log($rootScope.favoritos);
        if ($rootScope.favoritos[evento.id] == undefined || $rootScope.favoritos[evento.id] == null) {
            $rootScope.favoritos[evento.id] = evento;
        } else {
            $rootScope.favoritos[evento.id] = null;
        }

        localStorage.setItem("favoritos", JSON.stringify($rootScope.favoritos));
    }

    //funcion para indicar que tag o filtro esta activo
    /*$scope.filtrar = function (id) {
     $rootScope.active = id;
     if ($rootScope.active == 1) {
     $rootScope.active2 = 2;
     } else {
     $rootScope.active2 = null;
     }
     }*/

    //funcion para ver los detalles de una emergencia
    $scope.VerEmpergemcia = function (emergencia) {
        localStorage.setItem("emergenciaS", JSON.stringify(emergencia));
        $state.go('app.mapa');
    }

//funcion para ver los detalles de una emergencia
    $scope.VerEvento = function (emergencia) {
        localStorage.setItem("emergenciaS", JSON.stringify(emergencia));
        $state.go('app.mapa');
    }

    //funcion que carga una nueva pagina de asistencias
    $scope.pagSig = function () {
         if ($rootScope.fav == 1){
            return;
        }
        if ($rootScope.siguiente != null) {
            $scope.mostrarC = true;
            $http({
                url: $rootScope.siguiente,
                method: 'GET',
                params: {idUser: $rootScope.usuario.id},
            }).success(function (response) {
                $scope.mostrarC = false;
                for (var i = 0; i < response.eventos.data.length; i++) {
                    $rootScope.emergencias.push(response.eventos.data[i]);
                }
                $rootScope.siguiente = response.eventos.next_page_url;
            }).error(function (error) {
                $scope.mostrarC = false;
                $scope.error = 'No se pueden actualizar los datos';
                $timeout(function () {
                    $scope.error = null;
                }, 2000);
            });
        }
    }
	
	

    //cada vez que entro
    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        $scope.ev = $stateParams.p;
     //   $rootScope.datos.tipos = localStorage.setItem("tipos", JSON.stringify($rootScope.datos.tipos));
        // alert($scope.ev);
        if ($rootScope.emergencias == undefined || $rootScope.emergencias.length == 0) {
        } else {
            //ya cargo las asistencias
            console.log('ya cargo las asistencias');
        }
    });
   
})

