-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-10-2017 a las 21:59:47
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `licores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCategoria` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'imagenes/categorias/default.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `ico` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ion-beer',
  PRIMARY KEY (`idCategoria`),
  UNIQUE KEY `categorias_nombre_unique` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `nombre`, `descripcion`, `imagen`, `created_at`, `updated_at`, `estado`, `ico`) VALUES
(1, 'Promociones', 'Promociones', 'imagenes/categorias/Promociones_156_id-21.jpg', '2017-10-24 20:21:00', '2017-10-24 23:51:46', 1, 'icon-promociones'),
(2, 'Vinos y Licores', 'Vinos y Licores', 'imagenes/categorias/default.jpg', '2017-10-24 20:22:06', '2017-10-24 23:52:57', 1, 'icon-botella-2'),
(3, 'Cervezas', 'Cervezas', 'imagenes/categorias/Cervezas_848_id-22.jpg', '2017-10-24 20:21:31', '2017-10-24 23:55:32', 1, 'icon-botellal'),
(4, 'Botanas', 'Botanas', 'imagenes/categorias/Botanas_518_id-23.jpg', '2017-10-24 20:21:35', '2017-10-24 23:55:58', 1, 'icon-botanas'),
(5, 'Otros', 'Otros', 'imagenes/categorias/Otros_54_id-24.jpg', '2017-10-24 20:21:38', '2017-10-24 23:56:16', 1, 'icon-otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` bigint(20) NOT NULL,
  `correo` text CHARACTER SET utf8 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL,
  `telefono` text CHARACTER SET utf8,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(50) NOT NULL,
  `social` int(11) DEFAULT NULL,
  `paypal` varchar(50) DEFAULT NULL,
  `googleid` varchar(100) DEFAULT NULL,
  `faceid` varchar(100) DEFAULT NULL,
  `push_token` text,
  `faceimg` text,
  `googleimg` text,
  `img` text,
  `sbimg` text,
  `remember_token` varchar(200) NOT NULL,
  `logueado` int(11) NOT NULL DEFAULT '0',
  `IdCupon` text NOT NULL COMMENT 'Es el id del cupon personal del usuario',
  `creditoCupon` float UNSIGNED NOT NULL DEFAULT '50' COMMENT 'Es el valor del cupon personal del usuario',
  `IdClienteReferencio` bigint(20) DEFAULT NULL COMMENT 'Es el id del cliente al que pertenecia el cupon que yo registre (el usuario que me referencio en la app)',

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `correo`, `active`, `password`, `telefono`, `created_at`, `updated_at`, `name`, `social`, `paypal`, `googleid`, `faceid`, `push_token`, `faceimg`, `googleimg`, `img`, `sbimg`, `remember_token`, `logueado`, `IdCupon`, `creditoCupon`, `IdClienteReferencio`) VALUES
(1, 'gilbberto@gmail.com', 1, '', NULL, '2017-09-22 00:22:37', '2017-10-28 01:05:48', 'Gilberto Gonzalez', 1, NULL, NULL, '1926808454251477', 'undefined', 'https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/19399786_10208259720403892_4694885524927681022_n.jpg?oh=d9cdfa1c76c5bc0a394e4bd4d5d5ec72&oe=5A346484&__gda__=1508723988_8e5f02865640adc4d6c6af6893b47bd1', NULL, 'https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/19399786_10208259720403892_4694885524927681022_n.jpg?oh=d9cdfa1c76c5bc0a394e4bd4d5d5ec72&oe=5A346484&__gda__=1508723988_8e5f02865640adc4d6c6af6893b47bd1', NULL, '', 1, '8220gilbberto', 0, 2),
(2, 'alejjandromar@gmail.com', 1, '', NULL, '2017-10-05 20:08:46', '2017-10-21 01:51:19', 'Alejandro Luis Marcano Silva', 2, NULL, '109774596669669426428', NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-0WrK_WLBwlk/AAAAAAAAAAI/AAAAAAAAABU/uRkZHOv-_Ws/photo.jpg', 'https://lh6.googleusercontent.com/-0WrK_WLBwlk/AAAAAAAAAAI/AAAAAAAAABU/uRkZHOv-_Ws/photo.jpg', NULL, '', 0, '1456alejjandromar', 100, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clienteusacupon`
--

CREATE TABLE `clienteusacupon` (
  `id` bigint(20) NOT NULL,
  `idCupon` bigint(20) NOT NULL,
  `IdCliente` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clienteusacupon`
--

INSERT INTO `clienteusacupon` (`id`, `idCupon`, `IdCliente`, `created_at`, `updated_at`) VALUES
(14, 4, 1, '2017-10-28 00:32:50', '2017-10-28 00:32:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cupon`
--

CREATE TABLE IF NOT EXISTS `cupon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `valor` float unsigned NOT NULL DEFAULT '0',
  `descripcion` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `cupon`
--

INSERT INTO `cupon` (`id`, `estado`, `valor`, `descripcion`, `created_at`, `updated_at`) VALUES
(2, 1, 50, 'Obten $50 al momento de comprar por el dia de holoween', '2017-10-06 23:07:58', '2017-10-06 23:07:58'),
(3, 1, 80, 'Obten $80 al momento de comprar por ser el dia del padre', '2017-10-06 23:07:58', '2017-10-06 23:07:58'),
(4, 1, 12.5, 'Obten $12.5 al momento de comprar por ser hoy una fiesta patria', '2017-10-06 23:07:58', '2017-10-06 23:07:58'),
(5, 0, 10, '10%', '2017-10-10 21:26:31', '2017-10-10 21:26:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento`
--

CREATE TABLE `descuento` (
  `idDescuento` int(10) UNSIGNED NOT NULL,
  `estado` smallint(1) NOT NULL DEFAULT '1',
  `tipo` varchar(10) NOT NULL DEFAULT 'porcentaje',
  `valor` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`idDescuento`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=101 ;

--
-- Volcado de datos para la tabla `descuento`
--

INSERT INTO `descuento` (`idDescuento`, `estado`, `tipo`, `valor`, `nombre`, `deleted_at`, `updated_at`, `created_at`, `cantidad`) VALUES
(100, 1, 'importe', 10, 'descuento_comun', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuentoproducto`
--

CREATE TABLE `descuentoproducto` (
  `idProducto` int(11) NOT NULL,
  `idDescuento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descuentoproducto`
--

INSERT INTO `descuentoproducto` (`idProducto`, `idDescuento`) VALUES
(92, 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallesorden`
--

CREATE TABLE `detallesorden` (
  `idDetalle` int(10) UNSIGNED NOT NULL,
  `factura` int(10) UNSIGNED NOT NULL,
  `cantidad` int(10) UNSIGNED NOT NULL,
  `precio` double UNSIGNED NOT NULL,
  `producto` int(10) UNSIGNED NOT NULL,
  `idPrecio` int(255) NOT NULL,
  `valorDescuento` double unsigned NOT NULL,
  PRIMARY KEY (`idDetalle`),
  UNIQUE KEY `detalleUnico` (`factura`,`precio`,`producto`),
  KEY `detallesorden_producto_fk` (`producto`),
  KEY `detallesorden_precion_fk` (`idPrecio`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `detallesorden`
--

INSERT INTO `detallesorden` (`idDetalle`, `factura`, `cantidad`, `precio`, `producto`, `idPrecio`, `valorDescuento`) VALUES
(1, 12, 4, 10, 92, 113, 0),
(2, 13, 4, 10, 92, 113, 0),
(3, 0, 4, 10, 92, 113, 0),
(4, 14, 4, 10, 92, 113, 0),
(5, 15, 4, 10, 92, 113, 0),
(6, 16, 6, 10, 92, 113, 0),
(7, 17, 6, 10, 92, 113, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direccion`
--

CREATE TABLE `direccion` (
  `id` int(11) NOT NULL,
  `latitud` double NOT NULL,
  `longitud` double NOT NULL,
  `texto` text NOT NULL COMMENT 'texto de la direccion. ej: calle martinez, porlamar',
  `favorita` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'indica si es una direccion frecuente del usuario',
  `idCliente` bigint(20) NOT NULL COMMENT 'cliente al que pertenece esta direccion',
  `alias` text COMMENT 'Alias de la direccion',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL,
  KEY `fk_direccion_idCliente` (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direccion`
--

INSERT INTO `direccion` (`id`, `latitud`, `longitud`, `texto`, `favorita`, `idCliente`, `alias`, `created_at`, `updated_at`) VALUES
(1, 19.395143223081, -99.177376618555, 'Indianapolis 31, Nápoles, 03810 Ciudad de México, CDMX, México', 1, 1, 'pepsi center', '2017-09-29 21:01:04', '2017-09-29 21:01:04'),
(2, 19.4326077, 99.13320799999997, 'Ciudad de mexico', 1, 1, 'casa', '2017-10-27 20:23:21', '2017-10-27 20:23:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE `empleados` (
  `dni` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `telefono` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`dni`),
  UNIQUE KEY `dni` (`dni`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`dni`, `password`, `email`, `name`, `remember_token`, `tipo`, `created_at`, `updated_at`, `deleted_at`, `telefono`, `estado`) VALUES
(1, '$2y$10$oyt2BlXzKn.0yuf3MYdp/.OJP2L8lrqJ7VlZtyLlAM9c8H4yCXIgi', 'alejjandromar@gmail.com', 'Alejandro M. S.', '', 'Empleado', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-10-10 20:43:46', 'alejjandromar@gmail.com', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturas`
--

CREATE TABLE `facturas` (
  `idFactura` int(10) UNSIGNED NOT NULL,
  `total` float UNSIGNED NOT NULL,
  `cliente` bigint(20) NOT NULL,
  `despachado_por` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `subTotal` float UNSIGNED NOT NULL,
  `descuento` float UNSIGNED DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cambio` float NOT NULL,
  `envio` float UNSIGNED NOT NULL DEFAULT '0',
  `idDireccion` int(11) NOT NULL,
  `paypal` float NOT NULL,
  `directo` float NOT NULL,
  `totalCupones` float UNSIGNED NOT NULL DEFAULT '0',
  `totalCredito` float NOT NULL DEFAULT '0',
  `referencias` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `directoTarjeta` double UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`idFactura`),
  KEY `facturas_cliente_foreign` (`cliente`),
  KEY `facturas_despachado_por_foreign` (`despachado_por`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `facturas`
--

INSERT INTO `facturas` (`idFactura`, `total`, `cliente`, `despachado_por`, `estado`, `subTotal`, `descuento`, `created_at`, `updated_at`, `cambio`, `envio`, `idDireccion`, `paypal`, `directo`, `totalCupones`, `totalCredito`, `referencias`, `directoTarjeta`) VALUES
(50, 57, 1, '0', 3, 10, 0, '2017-10-21 01:51:19', '2017-10-21 01:51:19', 0, 47, 1, 0, 0, 57, 0, 'dsfvdsvfsa', 0),
(51, 1127, 1, '0', 3, 1080, 0, '2017-10-25 06:50:19', '2017-10-25 06:50:19', 0, 47, 1, 500, 514.5, 12.5, 100, 'referencias de la direccion', 0),
(52, 407, 1, '0', 3, 360, 0, '2017-10-28 00:32:50', '2017-10-28 00:32:50', 0, 47, 1, 100, 50, 12.5, 100, 'referencias del pedido', 144.5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturatienecupon`
--

CREATE TABLE `facturatienecupon` (
  `id` bigint(20) NOT NULL,
  `idCupon` bigint(20) NOT NULL,
  `idFactura` int(10) UNSIGNED NOT NULL,
  `montoPagado` float NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `facturatienecupon`
--

INSERT INTO `facturatienecupon` (`id`, `idCupon`, `idFactura`, `montoPagado`, `created_at`, `updated_at`) VALUES
(1, 3, 50, 57, '2017-10-21 01:51:19', '2017-10-21 01:51:19'),
(2, 4, 51, 12.5, '2017-10-25 06:50:19', '2017-10-25 06:50:19'),
(3, 4, 52, 12.5, '2017-10-28 00:32:50', '2017-10-28 00:32:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precioproductos`
--

CREATE TABLE `precioproductos` (
  `costo` double UNSIGNED NOT NULL,
  `producto` int(10) UNSIGNED NOT NULL,
  `presentacion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id` int(11) NOT NULL,
  `imagen` varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'imagenes/presentaciones/default.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `stock` int(11) NOT NULL,
  `stock_r` int(11) NOT NULL COMMENT 'Contiene valor real',
  `stock_bl` int(11) NOT NULL COMMENT 'Cantidad Bloque por paypal',
  `stock_b` int(11) NOT NULL COMMENT 'Cantidad epeando por ser depachada',
`cantidad_desc` int(11) NOT NULL,
  `descuento` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `presentacionUnica` (`producto`,`presentacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `precioproductos`
--

INSERT INTO `precioproductos` (`costo`, `producto`, `presentacion`, `id`, `imagen`, `created_at`, `updated_at`, `estado`, `stock`, `stock_r`, `stock_bl`, `stock_b`, `cantidad_desc`, `descuento`) VALUES
(10, 92, '2 L', 113, 'imagenes/presentaciones/default.jpg', '2017-10-17 20:51:04', '2017-10-18 00:51:04', 1, 1974, 0, 2, 48, 0, 0),
(20, 92, '1/2 L', 114, 'imagenes/presentaciones/default.jpg', '2017-10-04 19:49:33', '2017-09-29 04:30:00', 1, 10000, 0, 0, 0, 0, 0),
(20, 91, '2 L', 115, 'imagenes/presentaciones/default.jpg', '2017-10-30 01:20:29', '2017-10-18 00:50:56', 1, 30, 30, 0, 0, 3, 10),
(20, 91, '2 Litros', 117, 'imagenes/presentaciones/_233_id-117.jpg', '2017-10-30 01:20:32', '2017-10-05 19:58:00', 1, 30, 30, 0, 0, 3, 10),
(19, 91, '1/2 litro', 118, 'imagenes/presentaciones/_35_id-118.jpg', '2017-10-05 18:09:09', '2017-10-05 22:09:09', 1, 10, 10, 0, 0, 0, 0),
(19, 91, '1/2 litro 2', 120, 'imagenes/presentaciones/_535_id-120.jpg', '2017-10-17 20:51:02', '2017-10-18 00:51:02', 1, 10, 10, 0, 0, 0, 0),
(15, 91, '3 L', 121, 'imagenes/presentaciones/_993_id-121.jpg', '2017-10-05 20:52:57', '2017-10-06 00:52:57', 1, 39, 39, 0, 0, 10, 20),
(50, 91, '4 Litro', 122, 'imagenes/presentaciones/_812_id-122.jpg', '2017-10-05 16:26:39', '2017-10-05 20:26:39', 1, 5000, 5000, 0, 0, 0, 0),
(250, 93, 'PROMOCION VARIOS PRODUCTOS', 123, 'imagenes/presentaciones/default.jpg', '2017-10-26 19:12:31', NULL, 1, 1000, 0, 0, 0, 0, 0),
(10, 95, 'New', 127, 'imagenes/presentaciones/_40_id-127.png', '2017-10-27 20:52:15', '2017-10-28 00:52:15', 1, 20, 20, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProducto` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `estado` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `categoria` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idProducto`),
  UNIQUE KEY `productos_nombre_unique` (`nombre`),
  KEY `productos_categoria_foreign` (`categoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=96 ;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `nombre`, `descripcion`, `estado`, `categoria`, `created_at`, `updated_at`) VALUES
(91, 'Vino blanco', 'djdjd', 1, 2, '2017-10-26 21:52:13', '2017-10-27 01:52:13'),
(92, 'Vino', '', 1, 2, '2017-10-24 20:22:24', '2017-10-05 08:42:15'),
(93, 'PROMOCION', 'VARIOS', 1, 1, '2017-10-26 21:52:18', '2017-10-27 01:52:18'),
(95, 'Nuevo', 'Nuevo', 1, 2, '2017-10-28 00:38:09', '2017-10-28 00:38:09');

--
-- Disparadores `productos`
--
DELIMITER $$
CREATE TRIGGER `validar_datos_productos` BEFORE INSERT ON `productos` FOR EACH ROW IF LOWER(NEW.estado) NOT REGEXP '^(0|1)$' THEN
	signal sqlstate '45000'
    set message_text='El estado es invalido',mysql_errno='45000';
end if
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `validar_datos_productos_actualizar` BEFORE UPDATE ON `productos` FOR EACH ROW IF LOWER(NEW.estado) NOT REGEXP '^(0|1)$' THEN
	signal sqlstate '45000'
    set message_text='El estado es invalido',mysql_errno='45000';
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `restaurante`
--

CREATE TABLE `restaurante` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rif` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `moneda` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `simbolo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCategoria`),
  ADD UNIQUE KEY `categorias_nombre_unique` (`nombre`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usoCuponClienteUnico` (`idCupon`,`IdCliente`),
  ADD KEY `fk_clienteusacupon_idCliente` (`IdCliente`);

--
-- Indices de la tabla `cupon`
--
ALTER TABLE `cupon`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `descuento`
--
ALTER TABLE `descuento`
  ADD PRIMARY KEY (`idDescuento`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  ADD PRIMARY KEY (`idDetalle`),
  ADD UNIQUE KEY `detalleUnico` (`factura`,`precio`,`producto`),
  ADD KEY `detallesorden_producto_fk` (`producto`),
  ADD KEY `detallesorden_precion_fk` (`idPrecio`);

--
-- Indices de la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_direccion_idCliente` (`idCliente`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`dni`),
  ADD UNIQUE KEY `dni` (`dni`),
  ADD UNIQUE KEY `ctr` (`ctr`);

--
-- Indices de la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `facturas_cliente_foreign` (`cliente`),
  ADD KEY `facturas_despachado_por_foreign` (`despachado_por`),
  ADD KEY `fk_facturas_cliente` (`idDireccion`);

--
-- Indices de la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usoCuponFacturaUnico` (`idCupon`,`idFactura`),
  ADD KEY `fk_facturatienecupon_idFactura` (`idFactura`);

--
-- Indices de la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `presentacionUnica` (`producto`,`presentacion`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProducto`),
  ADD UNIQUE KEY `productos_nombre_unique` (`nombre`),
  ADD KEY `productos_categoria_foreign` (`categoria`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `cupon`
--
ALTER TABLE `cupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `descuento`
--
ALTER TABLE `descuento`
  MODIFY `idDescuento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT de la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  MODIFY `idDetalle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `direccion`
--
ALTER TABLE `direccion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `facturas`
--
ALTER TABLE `facturas`
  MODIFY `idFactura` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT de la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProducto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clienteusacupon`
--
ALTER TABLE `clienteusacupon`
  ADD CONSTRAINT `fk_clienteusacupon_idCliente` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_clienteusacupon_idCupon` FOREIGN KEY (`idCupon`) REFERENCES `cupon` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `detallesorden`
--
ALTER TABLE `detallesorden`
  ADD CONSTRAINT `fk_detallesorden_precio` FOREIGN KEY (`idPrecio`) REFERENCES `precioproductos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_detallesorden_producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`idProducto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `direccion`
--
ALTER TABLE `direccion`
  ADD CONSTRAINT `fk_direccion_idCliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturas`
--
ALTER TABLE `facturas`
  ADD CONSTRAINT `fk_facturas_cliente` FOREIGN KEY (`cliente`) REFERENCES `cliente` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturas_idDireccion` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `facturatienecupon`
--
ALTER TABLE `facturatienecupon`
  ADD CONSTRAINT `fk_facturatienecupon_idCupon` FOREIGN KEY (`idCupon`) REFERENCES `cupon` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_facturatienecupon_idFactura` FOREIGN KEY (`idFactura`) REFERENCES `facturas` (`idFactura`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `precioproductos`
--
ALTER TABLE `precioproductos`
  ADD CONSTRAINT `fk_precioproductos_producto` FOREIGN KEY (`producto`) REFERENCES `productos` (`idProducto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `fk_productos_categoria` FOREIGN KEY (`categoria`) REFERENCES `categorias` (`idCategoria`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
