app.controller('PagoCtrl', function ($scope,$state, $ionicPopup, $ionicLoading, $ionicHistory, $rootScope, $ionicModal, $ionicPopover, $timeout,  CONFIG, $http, Orden) {
	$scope.data={comentario:""};
	$scope.mql1=window.matchMedia("screen and (min-width: 200px) and (max-height: 400px) and (orientation: landscape)");
    $scope.mql=window.matchMedia("screen and (min-width: 0px) and (orientation: portrait)");
    $scope.mql2=window.matchMedia("screen and (min-width: 600px) and (orientation: portrait)");
    $scope.mql3=window.matchMedia("screen and (min-width: 1024px) and (orientation: landscape)");
   $scope.$on("$ionicView.beforeEnter", function(event, data){
	   //Recibir Orden
		Orden.getByMesa($rootScope.idmesa).then(function(response){
			console.log(response);
			$scope.subtotal = response.data.subtotal;
			$scope.total = response.data.total;
			$scope.agregados = response.data.agregados || 0;
			$scope.impuesto= response.data.impuesto;
			$scope.propina= response.data.propina;
			$scope.agregados= response.data.impuesto+ response.data.propina; 
			$scope.descuento1 = 0.00;
			$scope.pagado = 0.00;
			$scope.falta = response.data.total;
			$scope.descuentos = response.data.descuentos;
			$scope.detalles = response.data.detalles;
			$scope.metodos_pago = response.data.metodos_pago;
			$scope.p=true;
			$scope.teclado="";
			$scope.txt_falta= "falta";
		
			
		});
   });
	
	$scope.pres= function(n){
		if (n=="."){
			$scope.teclado+=n;
			return;
		}
		try{
			var comp = parseFloat($scope.teclado+""+n);
			$scope.teclado=comp;
		}catch(e){
			console.log(e);
		}
		
	}
	
	$scope.borrar= function(){
		$scope.teclado=($scope.teclado +"").substring(0,($scope.teclado +"").length-1);
	}
	
	$scope.colocar_pago= function(metodo_pago){
		metodo_pago.pago= metodo_pago.pago || 0;
		metodo_pago.pago+= parseFloat($scope.teclado);
		$scope.act_pago($scope.teclado,"");
	}
	
	$scope.colocar_descuento= function(descuento){
		descuento.valorD=  descuento.valorD || 0;
		 if(descuento.tipo=="porcentaje"){
                var descuento_c ="0."+descuento.valor;
				var desc = $scope.total*descuento_c;
                descuento.valorD+=$scope.total*descuento_c;
            }else{
				var desc = descuento.valor;
                descuento.valorD+=descuento.valor;
		}
		$scope.descuento1 += desc;
		$scope.act_pago(desc,"%");
		
	}
	
	
	
	//Actualiza un metodo de pago
    $scope.act_pago= function(pago,tipo) {
		$scope.teclado="";
		if (!pago || pago ==0){
			return;
		}
        if(tipo!="%"){
            $scope.pagado += pago || 0; 
        } 
		$scope.falta =  $scope.total-$scope.pagado;
		console.log($scope.falta);
        $scope.falta-=  $scope.descuento1;
		console.log($scope.falta);
        if ($scope.falta>0){
            $scope.StylePago={'color':'red'};
             $scope.dsbbtnpagar=true;
             $scope.bandera=false;
            $("#txtfalta").html("Falta: ");
			$scope.txt_falta= "Falta";
            // $scope.bloquearDesbloquear('formas_pago','desbloquear');
            // $scope.bloquearDesbloquear('Contenedor_descuentos','desbloquear');
        }else{
            $scope.falta*=(-1)
             $scope.StylePago={'color':'green'};
            $("#txtfalta").html("Vuelto: ");
			$scope.txt_falta= "Vuelto";
            $scope.dsbbtnpagar=false;
            $scope.bandera=true;
            // $scope.bloquearDesbloquear('formas_pago','bloquear');
            // $scope.bloquearDesbloquear('Contenedor_descuentos','bloquear');
            
        }
    }
	
	 //Cambia el monto de un pago en especifico
    $scope.changepaid= function(metodo_pago){
        pago= parseFloat($scope.teclado); //Tomado del Teclado
        var topaid=pago - metodo_pago.pago;
         $scope.act_pago(topaid,"");
		 metodo_pago.pago=  parseFloat($scope.teclado) ;
    };
	
	//Cambia el monto de un pago en especifico
    $scope.rmdes= function(descuento){
		$scope.descuento1 -= descuento.valorD;
		$scope.act_pago(descuento.valorD*-1,"%");
		descuento.valorD = 0;
    };
	
	//Cambia el monto de un pago en especifico
    $scope.volver= function(){
		$state.go("app.mesas");
    };
	
	
	$scope.pagar=  function (){
        var pagos = [];
        var descuentos = [];
		for (var i=0; i < $scope.metodos_pago.length; i++){
			if ($scope.metodos_pago[i].pago > 0){
				var pago={
					tipoPago: $scope.metodos_pago[i].idPago,
					monto:$scope.metodos_pago[i].pago
				};
				pagos.push(pago);
			}
        }
       for (var i=0; i < $scope.descuentos.length; i++){
		   if ( $scope.descuentos[i].valorD > 0){
				var Descu={
					idDescuento: $scope.descuentos[i].idDescuento,
					valor: $scope.descuentos[i].valor,
					valor_des: $scope.descuentos[i].valorD,
					tipo: $scope.descuentos[i].tipo
				};
				descuentos.push(Descu);
		   }
        }
         $ionicLoading.show({
              template: 'Cargando',
              content: 'Loading',
              animation: 'fade-in'
            });
		Orden.pagar($rootScope.idmesa, descuentos, pagos).then(function(){
			 $ionicLoading.hide();
			 $state.go("app.mesas");
		}
		);
    }
	
	
	//funcio que agrega un comentario a la orden
   $scope.openPopupC= function(X) {
        $scope.destino=X
        var popup = $ionicPopup.show({
            templateUrl: 'comentario.html',
            scope: $scope,
		});
        $scope.popupComentario=popup;
   }
   
   //funcion que cierra el popup para agregar un comentario
   $scope.closepopupComentario= function() {
       $scope.popupComentario.close();
       $scope.popupComentario=null;
   }
   
   $scope.CancelarOrden = function(comentario) {
		//alert(comentario)
		Orden.cancelar($rootScope.idmesa, comentario).then(function (response){
			 $scope.closepopupComentario();
            if(!response.data.error){
                $ionicLoading.show({
                    template: response.mensaje,
                    duration:3000
				});
                $state.go("app.mesas");
            }
		});
   }
   
   //funciones para el responsi
	$scope.cambioMediaQuery= function(mql) {
        if(mql.matches){
			if(mql.media=="screen and (min-width: 0px) and (orientation: portrait)"){
                $scope.ResponsiVerticalCelularTableta();//celulares y tabletas en vertical
				console.log('A');
			}else{
                if(mql.media=="screen and (max-height: 400px) and (min-width: 200px) and (orientation: landscape)"){
                    //celulares en horizontal.
                     $scope.responsiCelularHorizontal();
					 console.log('B');
                }else{
                    if(mql.media=="screen and (min-width: 600px) and (orientation: portrait)"){
                        //tabletas en vertical(cambiando algunas cosas)
                        $scope.responsiTabletaVertical();
						console.log('C');
                    }else{
                        if(mql.media=="screen and (min-width: 1024px) and (orientation: landscape)"){
                            //tablets en horizontal y pcs.
                            $scope.responsiTabletaHorizontal();
							console.log('D');
                        }
                     }
                }
			}
		}
	}
	
	//verifica los tama�os de la pantallas
	$scope.verificaTamanos= function() {
		console.log('verifcar tama�os');
        $scope.cambioMediaQuery($scope.mql);
        $scope.cambioMediaQuery($scope.mql1);
        $scope.cambioMediaQuery($scope.mql2);
        $scope.cambioMediaQuery($scope.mql3);
    }
	
	$scope.responsiTabletaHorizontal=function (){
		$('#tag').css('height',"18%");
		$('#pagar').show();	
		$('#factura').show();
		$('#contenido').removeClass("has-footer");  
		$('#footer').hide();
		$('#TF').css('height',"29%");
		$('#TM').css('height',"50%");
	}
	
	$scope.ResponsiVerticalCelularTableta= function(){
		$('#footer').show();
		$('#display').css('height','100%');
		$('#teclado').css('background-color',"white");
		$('#teclado').show();
		$('#pagar').show();
		$('#factura').hide();
		$('#metodos').hide();
	}
	
	$scope.responsiCelularHorizontal=function(){
		$('#pagar').hide();
		$('#factura').hide();
		$('#footer').hide();
	}
	
	$scope.responsiTabletaVertical=function (){
		$('#contenido').addClass("has-footer");  
		$('#footer').show();
		$('#TF').css('height',"150px");
		$('#tag').css('height',"35px");
		var ocupado=$('#cabecera').height()+$('#TF').height();
		var alto=$('#pagar').height();
		var porCont=((ocupado*100)/alto);
		var disponible=100-porCont;
		$('#metodos').show();
		$('#TM').css('height',disponible+"%");
	}
   
     $scope.tabBtnPago =function () {
        $('#Contenedor_descuentos').hide();
		$('#formas_pago').show();
		$('#tabBtnPago').attr("style","background-color: #D6D4D4;");
		$('#tabBtnDescuento').attr("style","background-color: #F5F3F3;");
    };

	$scope.tabBtnDescuento=function () {
		$('#formas_pago').hide();
		$('#Contenedor_descuentos').css("display","block");
		$('#tabBtnDescuento').attr("style","background-color: #D6D4D4;");
		$('#tabBtnPago').attr("style","background-color: #F5F3F3;");   
	};
	
	$scope.BtnPreviu =function () {
		$('#pagar').hide();
		$('#factura').show();
		pantalla="factura";
	};
	
	$scope.BtnFor=function () {
		$('#factura').hide();
		$('#teclado').hide();
		$('#pagar').show();
		$('#metodos').show();
	}
	
	$scope.BtnT=function () {
		$('#factura').hide();
		$('#metodos').hide();
		$('#teclado').show();	
		$('#pagar').show();
	};
	
	$scope.BtnPagos=function () {
		$('#factura').hide();
		$('#pagar').show();
    }
		
	angular.element(document).ready(function() {
		$scope.verificaTamanos();
		$scope.mql.addListener($scope.cambioMediaQuery);
		$scope.mql1.addListener($scope.cambioMediaQuery);
		$scope.mql2.addListener($scope.cambioMediaQuery);
		$scope.mql3.addListener($scope.cambioMediaQuery);
    });
})
	