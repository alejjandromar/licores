// Ionic Starter App
// https://devdactic.com/ionic-push-notifications-guide/
//https://github.com/phonegap/phonegap-plugin-push
// angular.module is a global place for creating, registering and retrieving Angular modules

// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic', 'ngCordova'])
        .run(function ($ionicPlatform, $rootScope, $state, $interval, $timeout, $ionicLoading) {

            $ionicPlatform.ready(function () {
                try {
                    window.plugins.googleplus.isAvailable(
                            function (available) {
								if(available==true){
									 $rootScope.ready = true;
								}else{
									 $rootScope.ready = false;
								}
                            }
                    , function (error) {
                        $rootScope.ready = false;
                    }
                    );
                } catch (e) {
                    //alert(e);
                }
                $ionicPlatform.registerBackButtonAction(function (event) {
                    event.preventDefault();
                }, 100);

                $ionicPlatform.on('resume', function () {



                });
                $ionicPlatform.on('pause', function () {
                    //la la aplicacion esta en el fondo
                });
                try {
                    $rootScope.activarGPS();
                    try {
                        $timeout(function () {
                            app_paypal.initPaymentUI()
                        }, 3000);
                    } catch (e) {
                        alert(e);
                    }
                    /*
                     * Metodos refenrentes al manejo de notificacines
                     */
                    var push = new PushNotification.init({
                            "debug": false, //false
                            "android": {
                                "senderID": "912109682551", //seneder id se uso el soldolar
                            },
                            "ios": {
                                "senderID": "912109682551", //seneder id se uso el soldolar
								"fcmSandbox": true,
								"alert": "true",
								"badge": "true",
								"sound": "true"
                            },
                            "pluginConfig": {
                                "android": {
                                    "title": "Hey",
                                    "message": "Hello Android!",
                                    "content_available": 1,
                                },
								"ios":{
									"fcmSandbox": true,
									"alert": "true",
									"badge": "true",
									"sound": "true"
								}
                                
                            }
                        });
                    //Cuando se obtiene un nuevo push token
                    push.on('registration', function (data) {
                         alert('me dio un push token valido: '+data.registrationId);
                        localStorage.push_token = data.registrationId;
                        $rootScope.registrar_push();
                    }, function (data) {
                    });

                    //Cuando obtiene una nuevva notificaion
                    push.on('notification', function (data) {
                        try {
                            alert('llego una notificacion');
                            alert(JSON.stringify(data));
							if(data.additionalData.foreground==false&&data.additionalData.coldstart==false){
								alert('lo debe sacar');
								return;
							}
							if (data.additionalData.tipo == "actualizarOrden") {
								alert('notificacion push actualizar');
								//actualizar una orden
								var orden = data.additionalData.info;
								$rootScope.actualizarEmergencia(orden);
							}
                            
                        } catch (e) {
                            alert('B');
                            alert(e)
                        }
                    });
                } catch (e) {
                    //  alert(e);
                }


            });
        });
app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })


            .state('app.login', {
                url: '/log',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Login.html',
                        controller: 'LogCtrl'
                    }
                }
            })

            .state('app.pago', {
                url: '/pago',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/pago.html',
                        controller: 'PagoCtrl'
                    }
                }
            })


            .state('app.consulta', {
                url: '/consulta/:p',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Consultar.html',
                        controller: 'noCtrl'
                    }
                }
            })

            .state('app.perfil', {
                url: '/perfil',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Perfil.html',
                        controller: 'PerfilCtrl'
                    }
                }
            })
            
            
            .state('app.contacto', {
                url: '/contacto',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/Contacto.html',
                        controller: 'ContactoCtrl'
                    }
                }
            })

            .state('app.seleccionarUbicacion', {
                url: '/seleccionar-Ubicacion',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/mapa.html',
                        controller: 'FmapaCtrl'
                    }
                }
            })
            .state('app.direccionesFavoritas', {
                url: '/direcciones-favoritas',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/direccionesF.html',
                        controller: 'direccionesFCtrl'
                    }
                }
            })
            .state('app.agregarUbicacionF', {
                url: '/agregar-UbicacionF',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/agregarDireccionesF.html',
                        controller: 'gregarDireccionesFCtrl'
                    }
                }
            })

            .state('app.carrito', {
                url: '/carrito',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/carrito.html',
                        controller: 'carritoCtrl'
                    }
                }
            })
            .state('app.historialPedidos', {
                url: '/historial-Pedidos',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/historialPedidos.html',
                        controller: 'historialPedidosCtrl'
                    }
                }
            })
            .state('app.detallesOrden', {
                url: '/detalles-Orden',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/detallesAsistencia.html',
                        controller: 'detallesOrdenCtrl'
                    }
                }
            })
            .state('app.cuponesDisponibles', {
                url: '/cupones-disponibles',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/cuponesDisponibles.html',
                        controller: 'cuponesDisponiblesCtrl'
                    }
                }
            })
            
            .state('app.confirarPedido', {
                url: '/confirar-pedido',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/confirarPedido.html',
                        controller: 'confirarPedidoCtrl'
                    }
                }
            });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/log');
}).constant('CONFIG', {
    //URLAPI: "http://138.197.9.85/gruas/public/",
    URLAPI: "http://192.168.43.37/licores/pizzeria/public/",
  //URLAPI: "http://chelaplus.mx/pizzeria/public/",
    //URLAPI: "http://localhost/Nuevo/licores/pizzeria/public/",
    APPVERSION: '1.0.0'

});
//PAYPAL ZONE
try {
    var app_paypal = {
        // Application Constructor
        initialize: function () {
            this.bindEvents();
        },
        // Bind Event Listeners
        //
        // Bind any events that are required on startup. Common events are:
        // 'load', 'deviceready', 'offline', and 'online'.
        bindEvents: function () {
            document.addEventListener('deviceready', this.onDeviceReady, false);
        },
        // deviceready Event Handler
        //
        // The scope of 'this' is the event. In order to call the 'receivedEvent'
        // function, we must explicity call 'app_paypal.receivedEvent(...);'
        onDeviceReady: function () {
            app_paypal.receivedEvent('deviceready');
        },
        // Update DOM on a Received Event
        receivedEvent: function (id) {
            /* var parentElement = document.getElementById(id);
             var listeningElement = parentElement.querySelector('.listening');
             var receivedElement = parentElement.querySelector('.received');
             
             listeningElement.setAttribute('style', 'display:none;');
             receivedElement.setAttribute('style', 'display:block;');
             
             console.log('Received Event: ' + id);*/


            // start to initialize PayPalMobile library

        },
        initPaymentUI: function () {
            var clientIDs = {
                "PayPalEnvironmentProduction": "",
                "PayPalEnvironmentSandbox": "Aai3Tey_KIZwK-nr3sAoGRXhJ_Odhxzh5D-qIIc6EQgCpNEnpo5LP4xThn1F0QHDPTVCINHqg5QVFxQ1"
            };
            //alert("here 368");
            PayPalMobile.init(clientIDs, app_paypal.onPayPalMobileInit);
            //alert("here 370");

        },
        onSuccesfulPayment: function (payment) {
        },
        onAuthorizationCallback: function (authorization) {
        },
        createPayment: function (valor, detalles) {
            // for simplicity use predefined amount
            // optional payment details for more information check [helper js file](https://github.com/paypal/PayPal-Cordova-Plugin/blob/master/www/paypal-mobile-js-helper.js)
            var paymentDetails = new PayPalPaymentDetails(valor, "0.00", "0.00");
            var payment = new PayPalPayment(valor, "MXN", detalles, "Sale", paymentDetails);
            return payment;
        },
        configuration: function () {
            // for more options see `paypal-mobile-js-helper.js`
            var config = new PayPalConfiguration({merchantName: "Eventos Chiguagua", merchantPrivacyPolicyURL: "https://mytestshop.com/policy", merchantUserAgreementURL: "https://mytestshop.com/agreement"});
            return config;
        },
        onPrepareRender: function () {
            // buttons defined in index.html
            //  <button id="buyNowBtn"> Buy Now !</button>
            //  <button id="buyInFutureBtn"> Pay in Future !</button>
            //  <button id="profileSharingBtn"> ProfileSharing !</button>
            /*  var buyNowBtn = document.getElementById("buyNowBtn");
             var buyInFutureBtn = document.getElementById("buyInFutureBtn");
             var profileSharingBtn = document.getElementById("profileSharingBtn");
             
             buyNowBtn.onclick = function(e) {
             // single payment
             PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment(), app_paypal.onSuccesfulPayment, app_paypal.onUserCanceled);
             };
             
             buyInFutureBtn.onclick = function(e) {
             // future payment
             PayPalMobile.renderFuturePaymentUI(app_paypal.onAuthorizationCallback, app_paypal.onUserCanceled);
             };
             
             profileSharingBtn.onclick = function(e) {
             // profile sharing
             PayPalMobile.renderProfileSharingUI(["profile", "email", "phone", "address", "futurepayments", "paypalattributes"], app_paypal.onAuthorizationCallback, app_paypal.onUserCanceled);
             };*/
        },
        onPayPalMobileInit: function () {
            // must be called
            // use PayPalEnvironmentNoNetwork mode to get look and feel of the flow
            // alert("here");
            PayPalMobile.prepareToRender("PayPalEnvironmentSandbox", app_paypal.configuration(), app_paypal.onPrepareRender);
            // alert("here 417");
        },
        onUserCanceled: function (result) {
            // alert(JSON.stringify(result));
        }
    };
} catch (e) {
    alert(e)
}
//app_paypal.initialize();