var foto = null;
var ventana;
app.controller('confirarPedidoCtrl', function (carrito,$scope, CONFIG, $cordovaGeolocation, $ionicPopup, $http, $state, $rootScope, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User) {
	// console.log($rootScope.datos.tipos);
	$rootScope.decimal = '\\d+(.\\d+)?';	
	$scope.selCupPer=false;
	$scope.mostrarCup=false;
	$scope.mostrarFP=false;
	
	$scope.mostrarCupones=function(){
		$scope.mostrarCup=!$scope.mostrarCup;
	}
	
	$scope.mostrarFormasP=function(){
		$scope.mostrarFP=!$scope.mostrarFP;
		setTimeout(function () {
			$('#FPT div').css('width',$('#FPT div').width()-50);
		}, 100);
	}


	$rootScope.pago_paypal = function (paypal) {
		if (paypal > 0) {

			PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment(paypal, "Pago por los productos"), $scope.pago_sig, $scope.cancelado);
		} else {
			$scope.pago_sig()
		}
	}

  

	$rootScope.pago = function () {
		var url = "precompra";
		//return;
		$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
			});
		$http({
			url: CONFIG.URLAPI + url,
			method: 'POST',
			params: {
				carrito: JSON.stringify($rootScope.carrito),
				idUser: $rootScope.usuario.id}

		}).success(function (response) {
			$ionicLoading.hide();
			if (response.error == false) {
				if ($rootScope.carrito.pagos.paypal > 0) {
					//PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment($rootScope.carrito.pagos.paypal, "Pago por los productos"), $scope.pago_sig, $rootScope.cancelar);
					$scope.pago_sig($rootScope.carrito.pagos.paypal);
				}else{
					$rootScope.usuario.creditoCupon=response.credito;
					User.SaveUser($rootScope.usuario);
					//hay que reiniciar el carrito
					carrito.create_carrito(true);
					$ionicHistory.clearHistory();
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});
					$state.go('app.historialPedidos');
					$ionicLoading.show({
						template: 'Compra realizada con exito',
						duration: 2500
					});
				}
			} else {
				$ionicLoading.show({
					template: 'Algunos productos no estan disponibles en este momento\n Veifique su orden e intente nuevamente',
					duration: 2500
				});

			}

		}).error(function (error) {
			$ionicLoading.hide();
			$ionicLoading.show({
				template: 'Ocurrio un error al tratar de procesar su solicitud',
				duration: 2500
			});
		});
	}


	$scope.pago_sig = function (paypal) {
		var url = "compra_done_paypal";
		$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
			});
		$http({
			url: CONFIG.URLAPI + url,
			method: 'POST',
			params: {carrito: JSON.stringify($scope.carrito),
				idUser: $rootScope.usuario.id}

		}).success(function (response) {
			console.log(response);
			$ionicLoading.hide();
			if (response.error == false) {
				$rootScope.usuario.creditoCupon=response.credito;
					User.SaveUser($rootScope.usuario);
					//hay que reiniciar el carrito
					carrito.create_carrito(true);
					$ionicHistory.clearHistory();
					$ionicHistory.nextViewOptions({
						disableAnimate: true,
						disableBack: true
					});
					$state.go('app.historialPedidos');
					if ($rootScope.pedidos != undefined && $rootScope.pedidos.length > 0) {
						$ionicLoading.show({
							template: 'Compra realizada con exito',
							duration: 2500
						});
					}
			}

		}).error(function (error) {
			$ionicLoading.hide();
			$ionicLoading.show({
				template: 'Ocurrio un error al tratar de procesar su solicitud',
				duration: 2500
			});
		});
	}


	$rootScope.cancelar = function (paypal) {
		var url = "compra_cancelar_paypal";
		$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
			});
		$http({
			url: CONFIG.URLAPI + url,
			method: 'POST',
			params: {carrito: JSON.stringify($scope.carrito),
				idUser: $rootScope.usuario.id,
				paypal: paypal}

		}).success(function (response) {
			$ionicLoading.hide();
			if (response.error == false) {
				if (paypal > 0) {
					//PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment(paypal, "Pago por los productos"), $scope.pago_sig, $scope.cancelado);

					$scope.pago_sig();
				}
			}

		}).error(function (error) {
			$ionicLoading.hide();
			$ionicLoading.show({
				template: 'Ocurrio un error al tratar de procesar su solicitud',
				duration: 2500
			});
		});
	}
	
	//funcion para cargar los cupones
	$scope.cargarCupones=function(){
		$http({
            url: CONFIG.URLAPI + 'cargarCupones',
            method: 'Get',
            params: {idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			if(response.error==false){
				$rootScope.cupones=response.cupones;
				for (var i = 0; i < $rootScope.cupones.length; i++) {
					$rootScope.cupones[i].sel=false;
				}
			}
        }).error(function (error) {
		});
	}
	
	//funcion apra saber si ya un cupon fue utilizado entre los medios pagados
	$scope.existe=function(id){
		for (var i = 0; i < $rootScope.carrito.pagos.cupones.length; i++) {
			if($rootScope.carrito.pagos.cupones[i].id==id){
				return i;
			}
		}
		return -1;
	}
	
	//funcion para deselecciona graficamente un cupon
	$scope.deseleccionr=function(X){
		for (var i = 0; i < $rootScope.cupones.length; i++) {
			if($rootScope.cupones[i].id==X){
				$rootScope.cupones[i].sel=false;
				return;
			}
		}
	};
	
	
	$scope.intemediario=function(idCupon,valorCupon){
		var pos=$scope.existe(idCupon);
		if(pos==-1){
			//no existe dentro del arreglo de cupones
			console.log('no existe dentro del arreglo, los agrego y realizo los calculos pertinentes');
			if(($rootScope.carrito.pagos.totalCupones+valorCupon)>$rootScope.carrito.total){
				var real=valorCupon;
				valorCupon=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
				if(valorCupon==0){
					//ya pago a punta de cupones
					if(idCupon==0){
						$scope.selCupPer=false;
					}else{
						$scope.deseleccionr(idCupon);
					}
					return;
				}
				$rootScope.carrito.pagos.totalCupones+=valorCupon;
				$rootScope.carrito.pagos.cupones.push({id:idCupon,valorI:valorCupon,valorR:real});
				$rootScope.carrito.pagos.paypal=0.00;
				$rootScope.carrito.pagos.directoTarjeta=0.00;
				$rootScope.carrito.pagos.directo=0.00;
				$rootScope.carrito.pagos.totalPagado=0.00;
			}else{
				$rootScope.carrito.pagos.totalCupones+=valorCupon;
				$rootScope.carrito.pagos.cupones.push({id:idCupon,valorI:valorCupon,valorR:valorCupon});
				$rootScope.carrito.pagos.paypal=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
				$rootScope.carrito.pagos.directoTarjeta=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal);
				$rootScope.carrito.pagos.directo=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal-$rootScope.carrito.pagos.directoTarjeta);
				console.log($rootScope.carrito);
			}
			
		}else{
			console.log('existe dentro del arreglo, los elimino y realizo los calculos pertinentes');
			if($rootScope.carrito.pagos.cupones[pos].valorI!=$rootScope.carrito.pagos.cupones[pos].valorR){
				//estoy sacando un cupon que al ingresar se le modifico el valor (porque si ingresa con su valor completo habia que darle cambio al usuario)
				$rootScope.carrito.pagos.totalCupones-=$rootScope.carrito.pagos.cupones[pos].valorI;
				$rootScope.carrito.pagos.paypal=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
				$rootScope.carrito.pagos.directoTarjeta=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal);
				$rootScope.carrito.pagos.directo=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal-$rootScope.carrito.pagos.directoTarjeta);
				$rootScope.carrito.pagos.cupones.splice(pos,1);
				console.log($rootScope.carrito);
				console.log('A');
			}else{
				console.log('B');
				//estoy sacando un cupon que al ingresar no se le modifico el valor, debo verificar si hay cupones que fueron modificados su valor al ingresar y si los hubo le sumo el valor con que ingreso el cupon que voy a sacr con el valor con el que ingreso el cupon que se modifico
				var j=-1;
				for (var i = 0; i < $rootScope.carrito.pagos.cupones.length; i++) {
					if($rootScope.carrito.pagos.cupones[i].valorI!=$rootScope.carrito.pagos.cupones[i].valorR){
						j=i;
						break;
					}
				}
				if(j==-1){
					console.log('C');
					$rootScope.carrito.pagos.totalCupones-=$rootScope.carrito.pagos.cupones[pos].valorI;
					$rootScope.carrito.pagos.cupones.splice(pos,1);
					if($rootScope.carrito.total>$rootScope.carrito.pagos.totalCupones){
						console.log('D');
						$rootScope.carrito.pagos.paypal=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
						$rootScope.carrito.pagos.directoTarjeta=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal);
						$rootScope.carrito.pagos.directo=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal-$rootScope.carrito.pagos.directoTarjeta);
					}
				}else{
					console.log('E');
					$rootScope.carrito.pagos.cupones[i].valorI+=$rootScope.carrito.pagos.cupones[pos].valorI;
					$rootScope.carrito.pagos.cupones.splice(pos,1);
				}
				console.log($rootScope.carrito);
			}
			//$scope.actualizarEfectivo();
		}
	}
	
	//funcion para agregar un cupon en los metodos de pagos
	$scope.agregraCupon=function(cupon){
		if(cupon==1){
			console.log('A');
			//es un cupon personal
			$scope.selCupPer=!$scope.selCupPer;
			$scope.intemediario(0,parseFloat($rootScope.usuario.creditoCupon));
		}else{
			console.log('B');
			//es un cupon promocional
			cupon.sel=!cupon.sel;
			$scope.intemediario(cupon.id,parseFloat(cupon.valor));
		}
		$scope.actualizarPagado();
	}
	
	//funcion para actualizar automatica el valor del efectivo si cambia lo que se coloca en el paypal
	$scope.actualizarEfectivo=function(){
		if($rootScope.carrito.pagos.paypal==undefined){
			return;
		}else{
			//asigno el valor de paypal
			$rootScope.carrito.pagos.paypal=parseFloat($rootScope.carrito.pagos.paypal);
			console.log($rootScope.carrito.pagos.paypal);
			if(isNaN($rootScope.carrito.pagos.paypal)){
				$rootScope.carrito.pagos.paypal=0.00;
			}
			console.log('paypal',$rootScope.carrito.pagos.paypal);
			if(($rootScope.carrito.pagos.paypal+$rootScope.carrito.pagos.totalCupones)>$rootScope.carrito.total){
				$rootScope.carrito.pagos.paypal=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
			}
			
			//asigno el valor de directoTarjeta
			$rootScope.carrito.pagos.directoTarjeta=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal);
			$scope.actualizarDirectoTarjeta();
		}
	}
	
	$scope.actualizarDirectoTarjeta=function(){
		if($rootScope.carrito.pagos.directoTarjeta==undefined){
			return;
		}else{
			$rootScope.carrito.pagos.directoTarjeta=parseFloat($rootScope.carrito.pagos.directoTarjeta);
			console.log('directoTarjeta 1',$rootScope.carrito.pagos.directoTarjeta);
			if(isNaN($rootScope.carrito.pagos.directoTarjeta)){
				$rootScope.carrito.pagos.directoTarjeta=0.00;
			}
			console.log('directoTarjeta 2',$rootScope.carrito.pagos.directoTarjeta);
			if(($rootScope.carrito.pagos.directoTarjeta+$rootScope.carrito.pagos.paypal+$rootScope.carrito.pagos.totalCupones)>$rootScope.carrito.total){
				$rootScope.carrito.pagos.directoTarjeta=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal);
			}
			
			$rootScope.carrito.pagos.directo=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones-$rootScope.carrito.pagos.paypal-$rootScope.carrito.pagos.directoTarjeta);
			$scope.actualizarPagado();
		}
	}
	
	//funcion para actualizar el valor del total pagado y calcular el cambio
	$scope.actualizarPagado=function(){
		$rootScope.carrito.pagos.totalPagado=($rootScope.carrito.pagos.paypal+$rootScope.carrito.pagos.directo+$rootScope.carrito.pagos.directoTarjeta);
		carrito.save();
	}
	
	//funcion que reinica los metodos de pagos.
	$scope.reiniciarPagos=function(){
		$rootScope.carrito.pagos.cupones=[];
		$rootScope.carrito.pagos.totalPagado=0.00;
		$rootScope.carrito.pagos.directoTarjeta=0.00;
		$rootScope.carrito.pagos.totalCupones=0.00;
		$rootScope.carrito.pagos.paypal=($rootScope.carrito.total-$rootScope.carrito.pagos.totalCupones);
		$scope.actualizarEfectivo();
	}
	
	//funcion para cancelar la compra de un pedido
	$scope.cacelarPedido=function(){
		//hay que reiniciar el carrito
		carrito.create_carrito(true);
		$ionicHistory.clearHistory();
		$ionicHistory.nextViewOptions({
			disableAnimate: true,
			disableBack: true
		});
		$state.go('app.seleccionarUbicacion');
		$ionicLoading.show({
			template: 'Su pedido fue cacelado',
			duration: 2500
		});
	}
	
	//funcion que inicia el pintado del mapa
	$scope.initMap=function() {
		var marcador;
		var posicionPedido = new google.maps.LatLng($rootScope.carrito.direccion.lat, $rootScope.carrito.direccion.lon);
		var mapOptions = {
			center: posicionPedido,
			zoom: 15,
			streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var mapaSelUbi = new google.maps.Map(document.getElementById("mapaCart"), mapOptions);
		var image = "img/pin.png";
		marcador = new google.maps.Marker({map: mapaSelUbi, animation: google.maps.Animation.DROP, draggable: false, icon:image});
        
		marcador.setPosition(posicionPedido);
    }
	
	$scope.loadGoogleMaps = function (direccion) {
        $ionicLoading.show({
            template: 'Cargando google maps<br>Por favor espere...'
        });
        $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyDGEnNpJ4t7LtXRCENVUMxhwfwJIpCePKw&libraries=geometry&sensor=true", function () {
            setTimeout(function () {
                $rootScope.enableMap();
                $scope.initMap();
            }, 1000);
        });
    }
	
	//funcion que se llama para pintar el mapa de la direccion del carrito
	$scope.pintarMapa=function(){
		 setTimeout(function () {
			if ($rootScope.checkLoaded()) {
				$scope.initMap();
			} else {
				$scope.loadGoogleMaps();
			}
		}, 500);
	}
	
	//funcion que lleva a la pantalla de sel ubicacion para editar la direccion  del pedido
	$scope.editarDireccion=function(){
		$rootScope.editarD=true;
		$state.go('app.seleccionarUbicacion');
	}
	
	//antes de entrar siempre a la ventana
	$scope.$on("$ionicView.beforeEnter", function (event, data) {
		$scope.cargarCupones();
		$scope.pintarMapa();
		$scope.reiniciarPagos();
    });
	
	$scope.$on("$ionicView.enter", function (event, data) {
		$('#FPT div').css('width',$('#FPT div').width()-50);
    });

})

.controller('carritoCtrl', function ($scope,$state,$rootScope) {
	$scope.irPagar=function(){
		if($rootScope.carrito.productos.length==0){
			return;
		}
		$state.go('app.confirarPedido');
	}
})
