app.controller('historialPedidosCtrl', function ($scope,$ionicLoading,$state,$ionicHistory, $http, $rootScope, CONFIG,User,$state) {
	
	$scope.mostrarP = false;
	
	
	//funcion que carga una nueva pagina de pedidos
    $scope.pagSigP = function () {
        if ($rootScope.siguientePed != null) {
            $scope.mostrarP = true;
            $http({
                url:CONFIG.URLAPI + 'cargarPedidos'+$rootScope.siguientePed.substring($rootScope.siguientePed.indexOf('?page')),
                method: 'GET',
                params: {idUser: $rootScope.usuario.id,},
            }).success(function (response) {
				response.pedidos=JSON.parse(response.pedidos);
                $scope.mostrarP = false;
                for (var i = 0; i < response.pedidos.data.length; i++) {
                    $rootScope.pedidos.push(response.pedidos.data[i]);
                }
                $rootScope.siguientePed = response.pedidos.next_page_url;
				User.SaveUser($rootScope.usuario);
            }).error(function (error) {
                $scope.mostraP = false;
                $scope.error = 'No se pueden actualizar los datos';
                $timeout(function () {
                    $scope.error = null;
                }, 2000);
            });
        }
    }
	
	
	//funcion para cargar las ordenes de un cliente
	$rootScope.cargarPedidos=function(X){
		if(X==true){
			$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
			});
		}
		$http({
            url: CONFIG.URLAPI + 'cargarPedidos',
            method: 'Get',
            params: {idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			response.pedidos=JSON.parse(response.pedidos);
			if(X==true){$ionicLoading.hide();}
            
			$rootScope.pedidos=response.pedidos.data;
			$rootScope.siguientePed = response.pedidos.next_page_url;
			User.SaveUser($rootScope.usuario);
        }).error(function (error) {
			if(X==true){
				$ionicLoading.hide();
				$ionicLoading.show({
					template: 'No se pudo realizar la operacion<br>Intentelo mas tarde',
					duration: 2500
				});
			}
		});
	}
	
	//funcion que envia a la pantalla para ver los detalles de una ordenes
	$scope.VerDetallesOrden=function(id){
		$rootScope.ordenSel={id:id};
		$state.go('app.detallesOrden');
	}
	
	//antes de entrar siempre a la ventana
	$scope.$on("$ionicView.beforeEnter", function (event, data) {
		if ($rootScope.pedidos == undefined || $rootScope.pedidos.length == 0) {
            $rootScope.cargarPedidos(true);
        } else {
			console.log('ya cargo los pedidos');
            $rootScope.cargarPedidos(false);
        }
    });
}).controller('detallesOrdenCtrl', function ($scope,$ionicLoading,$state,$ionicHistory, $http, $rootScope, CONFIG,User,$state,$ionicModal) {
	
	//funcion para cargar las ordenes de un cliente
	$scope.cargarDatosPedido=function(X){
		if($rootScope.ordenSel==null){
			$state.go('app.historialPedidos');
			return;
		}
		if(X==true){
			$ionicLoading.show({
				template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
			});
		}
		$http({
            url: CONFIG.URLAPI + 'cargarDatosPedido',
            method: 'Get',
            params: {idPedido:$rootScope.ordenSel.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			if(X==true){$ionicLoading.hide();}
			if(response.error==false){
				$rootScope.ordenSel=response.orden;
				console.log($rootScope.ordenSel);
			}else{
				$ionicLoading.show({
					template: response.mensaje,
					duration: 2500
				});
			}
        }).error(function (error) {
			if(X==true){
				$ionicLoading.hide();
				$ionicLoading.show({
					template: 'No se pudo realizar la operacion<br>Intentelo mas tarde',
					duration: 2500
				});
			}
		});
	}
	
	//se crea el modal en donde se vera el estado del pedido
	$ionicModal.fromTemplateUrl('estadoPedido.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function (modal) {
		$scope.modal = modal;
	});
	
	$scope.estados = [];
	$scope.estados [3] = "Por despachar";
	$scope.estados [4] = "En camino hacia el almacen";
	$scope.estados [5] = "En camino hacia el cliente";
	$scope.estados [6] = "Entregado";
	
	$scope.imagenes = [];
	$scope.imagenes [3] = "img/camino.png";
	$scope.imagenes [4] = "img/camino.png";
	$scope.imagenes [5] = "img/camino.png";
	$scope.imagenes [6] = "img/listo.jpg";
	
	$scope.iconos = [];
	$scope.iconos [3] = "ion-clock";
	$scope.iconos [4] = "ion-android-bicycle";
	$scope.iconos [5] = "ion-android-bicycle";
	$scope.iconos [6] = "ion-checkmark";
	
	$scope.abrirModal=function(){
		$scope.modal.show();
	}
	$scope.cerraModal=function(){
		$scope.modal.hide();
	}
	
	//antes de entrar siempre a la ventana
	$scope.$on("$ionicView.beforeEnter", function (event, data) {
		$scope.cargarDatosPedido(true);
    });
})






