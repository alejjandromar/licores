app.controller('LogCtrl', function (carrito,$scope, $state, $rootScope, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User) {


    $scope.$on("$ionicView.beforeEnter", function (event, data) {
        if ($rootScope.usuario) {
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
			if($rootScope.carrito.direccion!=null){
				$state.go("app.consulta");
			}else{
				$state.go("app.seleccionarUbicacion");
			}
        }
    });

    $scope.loginfc = function (data, tipo) {
        User.login(data, tipo).then(
                function (response) {//succes
                    $ionicLoading.hide();
                    if (response.data.error != true) {
                        var us = response.data.user;
                        User.SaveUser(us);
						carrito.create_carrito(true);
                        $ionicHistory.clearHistory();
                        $rootScope.registrar_push();
                        $ionicHistory.nextViewOptions({
                            disableAnimate: true,
                            disableBack: true
                        });
                        $rootScope.activarGPS();
                        $state.go("app.seleccionarUbicacion");
                         $rootScope.cargarDatos(false);
                    } else {
                        $ionicLoading.show({
                            template: response.data.mensaje,
                            duration: 2000,
                            content: 'Loading',
                            animation: 'fade-in'
                        })
                    }


                },
                function (response) {//error
                    $ionicLoading.hide();
                    $ionicLoading.show({
                        template: 'No se pudo conectar al servicio',
                        duration: 2000,
                        content: 'Loading',
                        animation: 'fade-in'
                    });
                }
        );
    }

    $scope.login_pr = function () {
        $ionicLoading.show({
            template: 'Cargando',
            content: 'Loading',
            animation: 'fade-in',
        });
        var user = {"id": "1926808454251477",
            "name": "Gilberto Gonzalez",
            "email": "gilbberto@gmail.com",
            "picture": {
                "data": {
                    "is_silhouette": false,
                    "url": "https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/19399786_10208259720403892_4694885524927681022_n.jpg?oh=d9cdfa1c76c5bc0a394e4bd4d5d5ec72&oe=5A346484&__gda__=1508723988_8e5f02865640adc4d6c6af6893b47bd1"
                }
            }
        }
        return $scope.loginfc(user, 1);
    }
    $scope.login_facebook = function () {
        $ionicLoading.show({
            template: 'Iniciando conexion con Facebook',
            content: 'Loading',
            animation: 'fade-in',
        });
        facebookConnectPlugin.login(["public_profile", "email"], function (obj) {

            $ionicLoading.hide();
            $ionicLoading.show({
                template: 'Obteniendo datos',
                content: 'Loading',
                animation: 'fade-in',
            });
            // alert(JSON.stringify(obj));
            $ionicLoading.hide();

            try {
                facebookConnectPlugin.api("me?fields=id,name,email,picture.type(large)", ["email"],
                        function (result3) {

                            $ionicLoading.hide();
                            $ionicLoading.show({
                                template: 'Enviando datos',
                                content: 'Loading',
                                animation: 'fade-in',
                            });
                            //  alert(JSON.stringify(result3));
                            $scope.loginfc(result3, 1);

                        },
                        function (error) {
                            $ionicLoading.hide();
                            //alert(JSON.stringify(error));
                        }
                );

                //$ionicLoading.hide();
                //alert("wait");
            } catch (e) {
               // alert(e)
            }

        },
                function (error) {
                    $ionicLoading.hide();
                  //  alert(JSON.stringify(error));
                    $ionicLoading.show({
                        template: 'Ocurrio un error<br>Por favor vuelve a intentarlo',
                        animation: 'fade-in',
                    });
                }
        );



    }

    //logueo con google
    $scope.login_google = function () {
        $ionicLoading.show({
            template: 'Obteniendo datos...'
        });

        window.plugins.googleplus.login({},
                function (user_data) {
                    //alert(JSON.stringify(user_data));
                    $ionicLoading.show({
                        template: 'Enviando datos...',
                        animation: 'fade-in',
                    });
                    $scope.loginfc(user_data, 2);
                    $ionicLoading.hide();
                },
                function (msg) {
                    $ionicLoading.hide();
					$ionicLoading.show({
                        template: 'Ocurrio un error<br>Por favor vuelve a intentarlo',
                        animation: 'fade-in',
                    });
                    //alert('Error al ingresar con google');
                }
        );
    };


    $scope.$on("$ionicView.afterEnter", function (event, data) {
        $rootScope.mostrar = false;
    });

})


/*
 * CONTROLADO PARA LA GESITON DEL PERFIL DE LOS USUARIOS
 */

app.controller('PerfilCtrl', function ($scope, $ionicPopup, $cordovaCamera, $state, $rootScope, utils, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User, $http, CONFIG) {
   
    $scope.$on("$ionicView.enter", function (event, data) {
        if (!$rootScope.usuario) {
            $ionicHistory.clearHistory();
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
            $state.go("app.login");
        }
    });

    $scope.cambiar_imagen = function () {
        $scope.alertPopup = $ionicPopup.alert({
            title: 'Tomar foto desde',
            template:
                    ' <div class="row"> <div class="col col-50"><button class="button button-block button-clear" ng-click="tomarFoto(0); ;" style="padding:0"> <img src="img/carpeta.png" style="width: 70px;"> <br>Galeria </button></div>' +
                    '<div class="col col-50"><button class="button button-block button-clear" ng-click="tomarFoto(1)" style="padding:0"> <img src="img/camara.png" style="width: 70px;"><br> Camara</button></div></div>',
            buttons: [
                {text: 'Cancelar'}, ]

        });
        $scope.alertPopup.then(function (res) {
            console.log(res);
        });
    };

    //funcion para tomar una imagen con la camara
    //X=0 camara x=1 album. Y=false subir una imagen Y=true enviar en mensaje
    $rootScope.tomarFoto = function (X) {

        utils.tomarFoto(X).then(
                function (response) {
                    if (response.error == false) {
                        var params = {iduser: $rootScope.usuario.id};
                        utils.subirFoto('subirfotoperfil', response.foto, params).then(
                                function (response2) {

                                    try {

                                        if (response2.error == false) {

                                            if (response2.resp.error != true) {

                                                User.SaveUser(response2.resp.user);


                                            } else {
                                                alert("Ha ocurrido un error intente nuevamente");
                                            }


                                        } else {
                                          //  alert("bad")
                                        }
                                    } catch (e) {
                                       // alert(e);
                                       // alert(JSON.stringify(response2))
                                    }

                                },
                                function (err) {
                                    //alert("Error")
                                }
                        );
                    }

                },
                function (err) {
                    //no show
                    $ionicLoading.hide();
                });

    };




    /*
     UPGRADE ZONE
     */

    $scope.cuentas = function () {
        $http({
            url: ruta + "cuenta/all",
            method: 'get',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $scope.cuentas = response.tipocuentas;
        });
    };
    $scope.realizar_pago = function () {
        try {
            PayPalMobile.renderSinglePaymentUI(app_paypal.createPayment(10, "Account upgrade"), $scope.upgrade, $scope.cancelado);
            //alert("fin")
            // $scope.upgrade();
        } catch (e) {
            alert(e);
            //$rootScope.alert('Error', e);
        }
    }

    $scope.cancelado = function () {
        alert(e);
        // $rootScope.alert('El pago no ha podido ser procesado', 20);
    }

    $scope.suscripcion = function () {

        $http({
            url: ruta + "suscripcion/all",
            method: 'get',
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
            $scope.suscripciones = response.suscripcion;
        });
    };
    $scope.upgrade = function (payment) {
        try {
            alert("begin to upgrade");
            $ionicLoading.show();
            $http({
                url: CONFIG.URLAPI + "user/upgrade",
                method: 'get',
                params: {
                    suscripcion: 5,
                    id: $rootScope.usuario.id,
                    paypal: "cvdkfk"
                },
                headers: {
                    "Content-Type": "application/json"
                }

            }).success(function (response) {
                $ionicLoading.hide();
                alert("success");
                User.SaveUser(response.user);
            }).error(function (response) {

                $ionicLoading.hide();
                alert("error");
                alert(JSON.stringify(response));
                //User.SaveUser(response.user);
            });
        } catch (e) {
            alert(e);
            //$rootScope.alert('Error', e);
        }
    };
    /*-------------------------*/
    $scope.registrar = function () {
        User.update($rootScope.usuario).then(function (response) {
            if (response.data.error == true) {
                $ionicLoading.show({
                    template: response.mensaje,
                    duration: 2000,
                    content: 'Loading',
                    animation: 'fade-in'
                });
            } else {
                $ionicLoading.show({
                    template: 'Registro exitoso',
                    duration: 2000,
                    content: 'Loading',
                    animation: 'fade-in'
                });
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                User.SaveUser(response.data.user);
                $state.go("app.emergencias");
            }
        });
    }
    $scope.$on("$ionicView.afterEnter", function (event, data) {
        $rootScope.mostrar = false;
    });
})




/*
 * CONTROLADO PARA LA GESITON DEL PERFIL DE LOS USUARIOS
 */

app.controller('ContactoCtrl', function ($scope, $ionicPopup, $cordovaCamera, $state, $rootScope, utils, $ionicModal, $ionicHistory, $ionicPopover, $timeout, $ionicLoading, User, $http, CONFIG) {
   
    
})

