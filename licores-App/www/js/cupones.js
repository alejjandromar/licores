app.controller('cuponesDisponiblesCtrl', function ($scope,$ionicLoading,$state,$ionicHistory, $http, $rootScope, CONFIG,User,$state,$timeout,$cordovaSms,$cordovaSocialSharing) {
	$scope.cupon={id:null}
	$scope.mostrarL=false;
	$scope.error=null;
	
	
	//funcion para cargar las ordenes de un cliente
	$rootScope.cargarCupones=function(X){
		if(X==true){$scope.mostrarL=true;}
		$http({
            url: CONFIG.URLAPI + 'cargarCupones',
            method: 'Get',
            params: {idUser: $rootScope.usuario.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			if(X==true){$scope.mostrarL=false;}
			if(response.error==false){
				$rootScope.cupones=response.cupones;
			}else{
				 $ionicLoading.show({
					template: response.error,
					duration: 2500
				});
			}
        }).error(function (error) {
			if(X==true){$scope.mostrarL=false;}
			$scope.error='No se pudo cargar los cupones. Intentelo mas tarde';
			$timeout(function () {
				$scope.error = null;
			}, 2500);
		});
	}
	
	//funcion para cargar las ordenes de un cliente
	$scope.ingresarCupon=function(){
		if($scope.cupon.id==$rootScope.usuario.IdCupon){
			$ionicLoading.show({
				template: 'Cupón inválido, no puedes ingresar tu propio cupón',
				duration: 2500
			});
			return;
		}
		$ionicLoading.show({
			template: '<div class="loader" style="width: 40px;height: 40px;"><svg class="circular" style="width: 40px;height: 40px;"><circle class="path" cx="20" cy="20" r="10" fill="none" stroke-width="2" stroke-miterlimit="100"/></svg></div>',
		});
		$http({
            url: CONFIG.URLAPI + 'clienteIngresaCupon',
            method: 'Get',
            params: {idUser: $rootScope.usuario.id,idCupon: $scope.cupon.id},
            headers: {
                "Content-Type": "application/json"
            }

        }).success(function (response) {
			$ionicLoading.hide();
			if(response.error==false){
				$ionicLoading.show({
					template: 'El cupon ha sido registrado exitosamente',
					duration: 2500
				});
				$rootScope.usuario.IdClienteReferencio=response.idRef;
				$rootScope.usuario.creditoCupon=response.credito;
				User.SaveUser($rootScope.usuario);
			}else{
				 $ionicLoading.show({
					template: response.mensaje,
					duration: 2500
				});
			}
        }).error(function (error) {
			$ionicLoading.hide();
			$ionicLoading.show({
				template: 'No se pudo realizar la operacion<br>Intentelo mas tarde',
				duration: 2500
			});
		});
	}
	
	$scope.CompartirViaMensaje=function(){
		window.plugins.socialsharing.shareViaSMS('¿Quieres pedir bebidas alcoholicas a domicilio? Ingresa el codigo '+$rootScope.usuario.IdCupon+' en ChelaApp y recibe $50 de credito para realizar tus compras. Mas informacion en http://chelaplus.mx/pizzeria/public/', null, function(msg) {
		}, function(msg) {
		})
	}
	
	$scope.CompartirViaSocial=function(){
		window.plugins.socialsharing.available(function(isAvailable) {
			if(isAvailable==true){
				try{
					window.plugins.socialsharing.share('¿Quieres pedir bebidas alcoholicas a domicilio? Ingresa el codigo '+$rootScope.usuario.IdCupon+' en ChelaApp y recibe $50 de credito para realizar tus compras. Mas informacion en', 'Chela+',null, 'http://chelaplus.mx/pizzeria/public/',function(result) {
					}, function(err) {
					});
				}catch(e){
					alert(e);
				}
			}else{
				$ionicLoading.show({
					template: 'No se puede compartir el cupon en este momento',
					duration: 2500
				});
			}
		});
	}
	
	//antes de entrar siempre a la ventana
	$scope.$on("$ionicView.beforeEnter", function (event, data) {
		if ($rootScope.cupones == undefined || $rootScope.cupones.length == 0) {
			$rootScope.cargarCupones(true);
        } else {
            //ya cargo las asistencias
			console.log('ya tenia cupones cargados');
			$rootScope.cargarCupones(false);
        }
    });
}).controller('detallesOrdefsnCtrl', function ($scope,$ionicLoading,$state,$ionicHistory, $http, $rootScope, CONFIG,User,$state) {
})






